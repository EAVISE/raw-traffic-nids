from parsers.protocols import Protocol


class TCP(Protocol):
    """
    Class representing the Transmission Control Protocol
    """
    def __init__(self, raw_data, byteorder, *, name="TCP"):
        
        super().__init__(raw_data, byteorder, name=name)

        # Parse binary data for Ethernet-specific information:
        self.src_port = int.from_bytes(raw_data[0:2], byteorder, signed=False)
        self.dst_port = int.from_bytes(raw_data[2:4], byteorder, signed=False)
        self.seq = int.from_bytes(raw_data[4:8], byteorder, signed=False)       # Sequence number
        self.ack = int.from_bytes(raw_data[8:12], byteorder, signed=False)      # Acknowledgement number
        self.flags = int.from_bytes(raw_data[12:14], byteorder, signed=False)   # Flags
        self.data_offset = (self.flags >> 12) & 15                              # Data offset
        self.wsize = int.from_bytes(raw_data[14:16], byteorder, signed=False)   # Window size
        self.chks = int.from_bytes(raw_data[16:18], byteorder, signed=False)    # checksum
        self.urg = int.from_bytes(raw_data[18:20], byteorder, signed=False)     # Urgent pointer
        
        if self.data_offset > 5:
            self.options = int.from_bytes(raw_data[20:self.data_offset * 8], byteorder, signed=False)
            self.raw_data = raw_data[self.data_offset * 8:]
        else:
            self.options = 0
            self.raw_data = raw_data[20:]
    
    def __str__(self):
        return "{}: {}-{}".format(self.name, str(self.src_port), str(self.dst_port))

    def __find_next_layer_protocol_header(self, byteorder):
        pass

    def get_next_layer_protocol(self, byteorder):
        self.__find_next_layer_protocol_header(byteorder)
        return self.__next_layer_protcol


class UDP(Protocol):
    """
    Class representing the User Datagram Protocol
    """
    def __init__(self, raw_data, byteorder, *, name="TCP"):
        
        super().__init__(raw_data, byteorder, name=name)

        # Parse binary data for Ethernet-specific information:
        self.src_port = int.from_bytes(raw_data[0:2], byteorder, signed=False)
        self.dst_port = int.from_bytes(raw_data[2:4], byteorder, signed=False)
        self.length = int.from_bytes(raw_data[4:6], byteorder, signed=False)    # Length
        self.chks = int.from_bytes(raw_data[6:8], byteorder, signed=False)    # checksum
        self.raw_data = raw_data[8:]

    def __str__(self):
        return "{}: {}-{}".format(self.name, str(self.src_port), str(self.dst_port))

    def __find_next_layer_protocol_header(self, byteorder):
        pass

    def get_next_layer_protocol(self, byteorder):
        self.__find_next_layer_protocol_header(byteorder)
        return self.__next_layer_protcol
