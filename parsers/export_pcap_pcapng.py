import os
from parsers.flow_parsing import determine_file_type, get_flow_id, get_5_tuple
from parsers.pcap_parser import PCAPGlobalHeader, PCAPPacket
from parsers.pcapng_parser import SectionHeaderBlock, get_next_block_type, EnhancedPacketBlock
from parsers.constants import NETWORK_BYTE_ORDER


def to_axis(capture_file, output_file, fid_file, n_packets, bus_width, hex=True, only_ipv4=True, verbose=True):
    """
    Export packets from the provided packet capture file as a AXI Stream representation in a .dat file, suitable for
    VLSI simulation testing.

    :param capture_file: Path to packet capture file containing packets to use.
    :param output_file: Path to file containing the AXIS output.
    :param fid_file: Path to file containing corresponding fids, if applicable.
    :param n_packets: Number of packets to export.
    :param bus_width: AXIS bus width in Bytes.
    :param hex: If True, the AXIS data is provided as hexadecimal numers, rahter than binary. (default True).
    :param only_ipv4: If True, only include IPv4 packets with a TCP/UDP session (default True).
    :param verbose: Provide verbose information during processing (default True).
    :return:
    """

    # Check input params
    if capture_file is None:
        raise ValueError('Provided path to capture file is None.')
    elif not os.path.isfile(capture_file) or not (capture_file[-5:] == '.pcap' or capture_file[-7:] == '.pcapng'):
        raise ValueError('Provided path {} to capture file does not lead to actual capture file.'.format(capture_file))
    if output_file is None:
        raise ValueError('Provided path to output file is None.')
    if fid_file is None:
        raise ValueError('Provided path to flow ID file is None.')

    # Ensure the output files have the right suffix:
    if not output_file[-4:] == '.dat':
        output_file += '.dat'
    if not fid_file[-4:] == '.dat':
        fid_file += '.dat'

    if determine_file_type(capture_file) == 'pcap':
        return __to_axis_pcap(capture_file=capture_file, output_file=output_file, fid_file=fid_file,
                              bus_width=bus_width, n_packets=n_packets, hex=hex, only_ipv4=only_ipv4, verbose=verbose)
    else:
        return __to_axis_pcapng(capture_file=capture_file, output_file=output_file, fid_file=fid_file,
                                bus_width=bus_width, n_packets=n_packets, hex=hex, only_ipv4=only_ipv4, verbose=verbose)


def __byte_list_to_data_string(byte_list, hex=True):
    if hex:
        # Return hexadecimal
        return ''.join(['{0:02X}'.format(b) for b in byte_list])
    else:
        # Return binary
        return ''.join(['{0:08b}'.format(b) for b in byte_list])


def __packet_to_axis(packet, bus_width, hex=True):
    """
    Extract the necessary information to export a packet in a AXIS format.

    :param packet: Packet a string of bytes (FAFBA2...)
    :param bus_width: AXIS data bus width
    :param hex: If True, data bus will be exported as hexadecimal numbers
    :return: String containing the AXIS presentation of the packet prepared for output in a .dat file.
    :rtype: ``str``
    """

    packet_data_string = packet             # Get hexadecimal byte string from packet

    axis_data_strings = ''

    n_bytes = len(packet_data_string) / 2   # Number of bytes for the packet
    data_line = []                          # List containing all bytes for one data line, thus being bus_width elements

    # Line contains 'data strb keep dv dl\n'
    current_idx = 0
    while current_idx < n_bytes:

        if len(data_line) == bus_width:
            axis_data_strings += '{} {} {} {} {}\n'.format(__byte_list_to_data_string(data_line, hex),
                                                           ''.join(['1' for _ in range(bus_width)]),
                                                           ''.join(['1' for _ in range(bus_width)]),
                                                           '1', '0')
            data_line = []

        current_byte = int(packet_data_string[2 * current_idx:2 * current_idx + 2], 16)
        data_line.append(current_byte)

        current_idx += 1

    # Add zero-padding
    rem_bytes = len(data_line)
    zero_bytes = bus_width - rem_bytes
    data_line += [0 for _ in range(zero_bytes)]
    strobe = ''.join(['1' for _ in range(rem_bytes)] + ['0' for _ in range(zero_bytes)])
    axis_data_strings += '{} {} {} {} {}\n'.format(__byte_list_to_data_string(data_line, hex),
                                                   strobe, strobe, '1', '1')

    return axis_data_strings


def __to_axis_pcap(capture_file, output_file, fid_file, bus_width, n_packets=-1, hex=True, only_ipv4=True, verbose=True):

    with open(capture_file, "rb") as b:
        with open(output_file, 'w') as out, open(fid_file, 'w') as fidf:
            if n_packets == -1:
                n_packets = float('inf')

            global_header = PCAPGlobalHeader(b)
            current_position = global_header.get_size()

            i = 0  # Packet count
            valid_packet_count = 0
            while valid_packet_count < n_packets:
                # Set the pointer correctly
                b.seek(current_position)
                # Check for End-Of-File
                if not b.read(1):
                    # EOF
                    break
                # Move reading cursor one step back
                # seek(offset, whence)
                b.seek(-1, 1)  # Whence = 1 => current position of cursor

                # Extract packet
                packet = PCAPPacket(b, global_header.byteorder)
                # Update the position in the binary file
                current_position += packet.get_size(global_header.byteorder)
                # print(packet.packetheader.get_incl_len(global_header.byteorder),"\n")
                fid = get_flow_id(packet.payload, NETWORK_BYTE_ORDER, global_header.get_network())

                # Check that valid keys were returned
                if fid is not None:
                    axis_sequence = __packet_to_axis(
                        packet.string(global_header.byteorder, global_header.byteorder_inverse), bus_width, hex)
                    out.write(axis_sequence)
                    fidf.write(__fid_to_hex(fid))
                    valid_packet_count += 1
                elif not only_ipv4:
                    axis_sequence = __packet_to_axis(
                        packet.string(global_header.byteorder, global_header.byteorder_inverse), bus_width, hex)
                    out.write(axis_sequence)
                    fidf.write(__fid_to_hex(fid))
                    valid_packet_count += 1
                i += 1
                if verbose:
                    print(i, end="\r")
    return


def __to_axis_pcapng(capture_file, output_file, fid_file, bus_width, n_packets=-1, hex=True, only_ipv4=True, verbose=True):

    with open(capture_file, "rb") as b:
        with open(output_file, 'w') as out, open(fid_file, 'w') as fidf:

            shb = SectionHeaderBlock(b)
            ids = get_next_block_type(b, shb.byteorder)(b, shb.byteorder)

            i = 0  # Packet count
            valid_packet_count = 0
            while valid_packet_count < n_packets:
                # Check for EOF
                if not b.read(1):
                    # EOF
                    break
                # Move reading cursor one step back
                # seek(offset, whence)
                b.seek(-1, 1)  # Whence = 1 => current position of cursor

                packet = get_next_block_type(b, shb.byteorder)

                if issubclass(packet, EnhancedPacketBlock):
                    packet = packet(b, shb.byteorder)
                    flow_id = get_flow_id(packet.raw_data, NETWORK_BYTE_ORDER, ids.get_linktype(shb.byteorder))
                    if flow_id is not None:
                        axis_sequence = __packet_to_axis(
                            packet.string(shb.byteorder, shb.byteorder_inverse), bus_width, hex)
                        out.write(axis_sequence)
                        fidf.write(__fid_to_hex(flow_id))
                        valid_packet_count += 1
                    elif not only_ipv4:
                        axis_sequence = __packet_to_axis(
                            packet.string(shb.byteorder, shb.byteorder_inverse), bus_width,hex)
                        out.write(axis_sequence)
                        fidf.write(__fid_to_hex(flow_id))
                        valid_packet_count += 1
                i += 1
                if verbose: print(i, end="\r")

    return


def __fid_to_hex(fid, verbose=False):
    src_addr, dst_addr, src_port, dst_port, proto = fid.split('-')

    src_addr_parts = [int(x) for x in src_addr.split('.')]
    dst_addr_parts = [int(x) for x in dst_addr.split('.')]
    src_port = int(src_port)
    dst_port = int(dst_port)
    proto = int(proto)

    src_addr_hex = '{:02X}{:02X}{:02X}{:02X}'.format(src_addr_parts[0], src_addr_parts[1],
                                                     src_addr_parts[2], src_addr_parts[3])
    dst_addr_hex = '{:02X}{:02X}{:02X}{:02X}'.format(dst_addr_parts[0], dst_addr_parts[1],
                                                     dst_addr_parts[2], dst_addr_parts[3])
    src_port_hex = '{:04X}'.format(src_port)
    dst_port_hex = '{:04X}'.format(dst_port)
    proto_hex = '{:02X}'.format(proto)

    if verbose:
        print(fid, '->', '{} {} {} {} {}'.format(src_addr_hex, dst_addr_hex, src_port_hex, dst_port_hex, proto_hex))

    return '{} {} {} {} {}\n'.format(src_addr_hex, dst_addr_hex, src_port_hex, dst_port_hex, proto_hex)
