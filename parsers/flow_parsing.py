import os
from io import BytesIO

import numpy as np

from parsers.pcapng_parser import get_next_block_type, SectionHeaderBlock, EnhancedPacketBlock
from parsers.pcap_parser import PCAPGlobalHeader, PCAPPacketHeader, PCAPPacket
from parsers.layer1_2_protocols import Ethernet, LinuxCookedCapture
from parsers.layer3_protocols import IPv4
from parsers.layer4_protocols import TCP, UDP
from parsers.constants import LINKTYPE_ETHERNET, LINKTYPE_LINUX_SLL, NETWORK_BYTE_ORDER
from flow_processing.flow_sorter import add2dict, sort_flows_from_dict, sort_flows_from_dict_to_hfd5
from flow_processing.stream_based_processing import FlowStreamSorter
from flow_processing.hdf5_interface import HDF5SortedDataset, HDF5FlowStorage


def determine_file_type(ifile):
    """
    By looking at the magic number of the provided input file, determine if it is a PCAP of PCAPNG file.

    :param ifile: Path to input file
    :type ifile: ``str``

    :return: Either "pcapng" or "pcap", depending on the file's type. For other files, the output will always be "pcap".
    :rtype: ``str``
    """
    with open(ifile,"rb") as f:
        b = BytesIO(f.read(4))

        if b.read(4) == (b'\x0A\x0D\x0D\x0A'):
            return "pcapng"
        else:
            return "pcap"


def get_flow_id(packet, byteorder, linktype):
    """
    Get the flow identifier of the provided packet.

    :param packet: Packet to be examined.
    :type packet: ``bytes``
    :param byteorder: Byteorder of the bytes in the packet, 'little' for little-endian, and 'big' for big-endian
    :type byteorder: ``str``
    :param linktype: Link-layer header type value
    :type linktype: ``int``
    :return: flow identifier
    :rtype: ``str``
    """
    # Layer 2 protocol as verified with linktype
    if linktype == LINKTYPE_ETHERNET:
        l2 = Ethernet(packet, byteorder)
    elif linktype == LINKTYPE_LINUX_SLL:
        l2 = LinuxCookedCapture(packet, byteorder)
    else:
        return None

    # Check if IPv4 is next
    if l2.ethertype != 2048:
        return None

    l3 = l2.get_next_layer_protocol(byteorder)

    # Check if TCP or UDP are next
    if not((l3.proto == 6) or (l3.proto == 17)):
        return None

    l4 = l3.get_next_layer_protocol(byteorder)

    flow_id = "{}-{}-{}-{}-{}".format(IPv4.format_address(l3.src_addr),
                                      IPv4.format_address(l3.dst_addr), l4.src_port, l4.dst_port, l3.proto)

    return flow_id


def get_5_tuple(packet, byteorder, linktype):
    """
    Get the 5-tuple (source address, destination address, source port, destination port, protocol) of the provided
    packet.

    :param packet: Packet to be examined.
    :type packet: ``bytes``
    :param byteorder: Byteorder of the bytes in the packet, 'little' for little-endian, and 'big' for big-endian
    :type byteorder: ``str``
    :param linktype: Link-layer header type value
    :type linktype: ``int``
    :return: 5-tuple
    :rtype: ``str``, ``str``, ``str``, ``str``, ``str``
    """
    # Layer 2 protocol as verified with linktype
    if linktype == LINKTYPE_ETHERNET:
        l2 = Ethernet(packet, byteorder)
    elif linktype == LINKTYPE_LINUX_SLL:
        l2 = LinuxCookedCapture(packet, byteorder)
    else:
        return None

    # Check if IPv4 is next
    if l2.ethertype != 2048:
        return None

    l3 = l2.get_next_layer_protocol(byteorder)

    # Check if TCP or UDP are next
    if not((l3.proto == 6) or (l3.proto == 17)):
        return None

    l4 = l3.get_next_layer_protocol(byteorder)

    return IPv4.format_address(l3.src_addr), IPv4.format_address(l3.dst_addr), \
           str(l4.src_port), str(l4.dst_port), str(l3.proto)


def store_raw_pcap_packet(output_file, include_ts, packet, byteorder, network_byte_order, time_resolution, linktype):
    flow_id = get_flow_id(packet.payload, network_byte_order, linktype)

    if flow_id is None:
        return
    else:
        with open(output_file, "a") as f:
            if include_ts:
                sec, nsec = packet.packetheader.get_ts(byteorder, time_resolution)
                f.write(flow_id + "," + str(sec) + "," + str(nsec) + "," +
                        packet.string(byteorder, network_byte_order) + "\n")
            else:
                f.write(flow_id + "," + packet.string(byteorder, network_byte_order) + "\n")


def store_raw_pcapng_packet(output_file, include_ts, packet, byteorder, network_byte_order, ids):
    linktype = ids.get_linktype(byteorder)
    flow_id = get_flow_id(packet.raw_data, network_byte_order, linktype)

    if flow_id is None:
        return
    else:
        with open(output_file, "a") as f:
            if include_ts:
                high = packet.get_timestamp_high(byteorder)
                low = packet.get_timestamp_low(byteorder)
                sec, ns = ids.get_ts_parts(high, low)
                f.write(flow_id + "," + str(sec) + "," + str(ns) + "," +
                        packet.string(byteorder, network_byte_order) + "\n")
            else:
                f.write(flow_id + "," + packet.string(byteorder, network_byte_order) + "\n")


def __get_flow_streamer(config, day=None, storage_path=None):

    if config.enable_hdf5:
        return FlowStreamSorter(n_flows=config.n_stream_flows, n_packets=config.n_stream_packets,
                                hdf5=HDF5FlowStorage(config.get_hdf5_path(day), mode='a',
                                                     packet_size=config.hdf5_packet_size))
    else:
        return FlowStreamSorter(n_flows=config.n_stream_flows, n_packets=config.n_stream_packets,
                                root=storage_path, dynamic_queue=True)


def __get_sort_flows_from_dict_function(**kwargs):
    use_hdf5 = kwargs.get('use_hdf5', False)
    verbose = kwargs.get('verbose', True)

    if use_hdf5:
        hdf5_packet_size = kwargs.get('hdf5_packet_size', 100)
        hdf5_filename = kwargs.get('hdf5_filename')
        labels = kwargs.get('labels')

        sort_flows_from_dict_func = sort_flows_from_dict_to_hfd5
        params = {'hdf5_sorted': HDF5FlowStorage(filename=hdf5_filename, mode='a', packet_size=hdf5_packet_size,
                                                 labels=labels),
                  'verbose': verbose}
    else:
        storage_path = kwargs.get('storage_path')
        sort_flows_from_dict_func = sort_flows_from_dict
        params = {'storage_root': storage_path, 'verbose': verbose}

    return sort_flows_from_dict_func, params


def __packet_to_data_pcapng(**kwargs):
    use_hdf5 = kwargs.get('use_hdf5', False)

    if use_hdf5:

        packet_size = kwargs.get('hdf_packet_size', 100)

        def extraction(shb, ids, packet, include_ts):
            if include_ts:
                high = packet.get_timestamp_high(shb.byteorder)
                low = packet.get_timestamp_low(shb.byteorder)
                sec, ns = ids.get_ts_parts(high, low)
            else:
                sec, ns = 0, 0

            packet_data = packet.integer_array(size=packet_size, byteorder=shb.byteorder)

            return sec, ns, packet_data
    else:

        def extraction(shb, ids, packet, include_ts):
            data = ''
            if include_ts:
                high = packet.get_timestamp_high(shb.byteorder)
                low = packet.get_timestamp_low(shb.byteorder)
                sec, ns = ids.get_ts_parts(high, low)
                data = str(sec) + "," + str(ns) + ","
            data += packet.string(shb.byteorder, NETWORK_BYTE_ORDER) + "\n"
            return data
        # data += hex(int.from_bytes(packet.raw_data, shb.byteorder_inverse, signed=False))[2:] + "\n"
    return extraction


def __packet_to_data_pcap(**kwargs):
    use_hdf5 = kwargs.get('use_hdf5', False)

    if use_hdf5:
        packet_size = kwargs.get('hdf_packet_size', 100)

        def extraction(global_header, packet, include_ts):
            if include_ts:
                sec, nsec = packet.packetheader.get_ts(global_header.byteorder, global_header.time_resolution)
            else:
                sec, nsec = 0, 0
            packet_data = packet.integer_array(size=packet_size, byteorder=global_header.byteorder)

            return sec, nsec, packet_data
    else:
        def extraction(global_header, packet, include_ts):
            data = ''
            if include_ts:
                sec, nsec = packet.packetheader.get_ts(global_header.byteorder, global_header.time_resolution)
                data = str(sec) + "," + str(nsec) + ","
            data += packet.string(global_header.byteorder, NETWORK_BYTE_ORDER) + "\n"

            return data

    return extraction


def pcap_file_to_sorted_flow(ifile, batch_size, storage_path, include_ts, verbose=True, **kwargs):
    """
    ifile (str): Path to input pcapng file
    batch_size(int): Number of packets to process in one iteration
    """
    sort_flows_from_dict_func, params = __get_sort_flows_from_dict_function(verbose=verbose,
                                                                            storage_path=storage_path,
                                                                            **kwargs)
    get_data_function = __packet_to_data_pcap(**kwargs)

    with open(ifile,"rb") as b:

        global_header = PCAPGlobalHeader(b)
        isEOF = False
        current_position = global_header.get_size()
        i = 0
        while not isEOF:
            i = 0           # Packet count
            flows_dict = {}
            # We process the data in batches:
            while i < batch_size:
                # Set the pointer correctly
                b.seek(current_position)
                # Check for End-Of-File
                if not b.read(1):
                    # EOF
                    isEOF = True
                    break
                # Move reading cursor one step back
                # seek(offset, whence)
                b.seek(-1, 1) # Whence = 1 => current position of cursor
                
                # Extract packet
                packet = PCAPPacket(b, global_header.byteorder)
                # Update the position in the binary file
                current_position += packet.get_size(global_header.byteorder)

                keys = get_5_tuple(packet.payload, NETWORK_BYTE_ORDER, global_header.get_network())

                # Check that valid keys were returned
                if not (keys is None):

                    data = get_data_function(global_header, packet, include_ts)
                    add2dict(flows_dict, keys, data)
                i += 1
                if verbose:
                    print(i, end="\r")

            sort_flows_from_dict_func(flows_dict=flows_dict, **params)


def pcapng_file_to_sorted_flow(ifile, batch_size, storage_path, include_ts, verbose=True, **kwargs):
    """
    ifile (str): Path to input pcapng file
    batch_size(int): Number of packets to process in one iteration
    """
    sort_flows_from_dict_func, params = __get_sort_flows_from_dict_function(verbose=verbose,
                                                                            storage_path=storage_path,
                                                                            **kwargs)
    get_data_function = __packet_to_data_pcapng(**kwargs)

    with open(ifile, "rb") as b:

        shb = SectionHeaderBlock(b)
        ids = get_next_block_type(b, shb.byteorder)(b, shb.byteorder)
        # Indicates if the end of the file has been reached
        isEOF = False

        current_position = shb.get_block_total_length(shb.byteorder) + ids.get_block_total_length(shb.byteorder)

        while not isEOF:
            i = 0           # Packet count
            flows_dict = {}
            # We process the data in batches:
            while i < batch_size:
                # Set the pointer correctly
                b.seek(current_position)

                # Check for EOF
                if not b.read(1):
                    # EOF
                    isEOF = True
                    break
                # Move reading cursor one step back
                # seek(offset, whence)
                b.seek(-1, 1) # Whence = 1 => current position of cursor

                # Extract packet
                packet = get_next_block_type(b, shb.byteorder)(b, shb.byteorder)
                # Update the position in the binary file
                current_position += packet.get_block_total_length(byteorder=shb.byteorder)
                
                if isinstance(packet, EnhancedPacketBlock):

                    keys = get_5_tuple(packet.raw_data, NETWORK_BYTE_ORDER, ids.get_linktype(shb.byteorder))
                    # Check that valid keys were returned
                    if not (keys is None):

                        data = get_data_function(shb, ids, packet, include_ts)
                        add2dict(flows_dict, keys, data)
                i += 1
                if verbose: print(i, end="\r")

            sort_flows_from_dict_func(flows_dict=flows_dict, **params)


def pcap_file_to_raw_packets(ifile, output_dir, bran, eran, batch_size, include_ts, verbose=True):
    ofile_base = os.path.split(ifile)[1].split(".")[-2]
    with open(ifile,"rb") as b:

        globalheader = PCAPGlobalHeader(b)
        i = 0
        while True:
            # Check for EOF
            if not b.read(1):
                # EOF
                break
            # Move reading cursor one step back
            # seek(offset, whence)
            b.seek(-1, 1) # Whence = 1 => current position of cursor

            batch_no = i // batch_size
            output_file = os.path.join(output_dir, ofile_base + str(batch_no) + ".txt")
            packet = PCAPPacket(b, globalheader.byteorder)

            if ((eran > -1) and (bran <= i <= eran)) or ((eran <= -1) and (bran <= i)):
                store_raw_pcap_packet(output_file, include_ts, packet, globalheader.byteorder, NETWORK_BYTE_ORDER,
                                      globalheader.time_resolution, globalheader.get_network())
            i += 1
            if verbose: print(i, end="\r")

            if (eran > bran) and (i > eran):
                break


def pcapng_file_to_raw_packets(ifile, output_dir, bran, eran, batch_size, include_ts, verbose=True):
    ofile_base = os.path.split(ifile)[1].split(".")[-2]
    with open(ifile,"rb") as b:

        shb = SectionHeaderBlock(b)
        ids = get_next_block_type(b, shb.byteorder)(b, shb.byteorder)
        i = 0
        while True:
            # Check for EOF
            if not b.read(1):
                # EOF
                break
            # Move reading cursor one step back
            # seek(offset, whence)
            b.seek(-1, 1) # Whence = 1 => current position of cursor

            batch_no = i // batch_size
            output_file = os.path.join(output_dir, ofile_base + str(batch_no) + ".txt")

            packet = get_next_block_type(b, shb.byteorder)

            if ((eran > -1) and (bran <= i <= eran)) or ((eran <= -1) and (bran <= i)):
                if issubclass(packet, EnhancedPacketBlock):
                    packet = packet(b, shb.byteorder)
                    store_raw_pcapng_packet(output_file, include_ts, packet, shb.byteorder, NETWORK_BYTE_ORDER, ids)
            i += 1
            if verbose: print(i, end="\r")

            if (eran > bran) and (i > eran):
                break


def pcap_file_to_sorted_stream_flows(ifile, config, storage_path, include_ts, day, verbose):
    """
    Parse a PCAP file to extract flows in a stream-based fashion.

    :param ifile: Path to input PCAP file.
    :param config: Dataset configuration.
    :param storage_path: Path to root storage directory for the flows
    :param include_ts: Also extract packet timestamps
    :param verbose: Output progress information
    :type ifile: ``str``
    :type config: ``DatasetConfig``
    :type storage_path: ``str``
    :type include_ts: ``bool``
    :type verbose: ``bool``
    :return: None
    """

    flow_streamer = __get_flow_streamer(config, day, storage_path=storage_path)

    get_data_function = __packet_to_data_pcap(**{
        'use_hdf5': config.enable_hdf5,
        'hdf5_packet_size': config.hdf5_packet_size})

    with open(ifile, "rb") as b:

        global_header = PCAPGlobalHeader(b)
        i = 0
        while True:
            # Check for EOF
            if not b.read(1):
                # EOF
                break
            # Move reading cursor one step back
            # seek(offset, whence)
            b.seek(-1, 1) # Whence = 1 => current position of cursor

            # Extract packet
            packet = PCAPPacket(b, global_header.byteorder)

            keys = get_5_tuple(packet.payload, NETWORK_BYTE_ORDER, global_header.get_network())

            # Check that valid keys were returned
            if not (keys is None):

                data = get_data_function(global_header, packet, include_ts)

                flow_streamer.add_packet(packet_data=data, flow_id='-'.join(keys))
            i += 1
            if verbose:
                print(i, end="\r")

    flow_streamer.store_remaining_flows()


def pcapng_file_to_sorted_stream_flows(ifile, config, storage_path, include_ts, day, verbose):
    """
    Parse a PCAPNG file to extract flows in a stream-based fashion.

    :param ifile: Path to input PCAPNG file.
    :param config: Dataset configuration.
    :param storage_path: Path to root storage directory for the flows
    :param include_ts: Also extract packet timestamps
    :param verbose: Output progress information
    :type ifile: ``str``
    :type config: ``DatasetConfig``
    :type storage_path: ``str``
    :type include_ts: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    flow_streamer = __get_flow_streamer(config, day, storage_path=storage_path)

    get_data_function = __packet_to_data_pcapng(**{
        'use_hdf5': config.enable_hdf5,
        'hdf5_packet_size': config.hdf5_packet_size})

    with open(ifile, "rb") as b:

        shb = SectionHeaderBlock(b)
        ids = get_next_block_type(b, shb.byteorder)(b, shb.byteorder)

        current_position = shb.get_block_total_length(shb.byteorder) + ids.get_block_total_length(shb.byteorder)
        i = 0
        while True:
            # Check for EOF
            if not b.read(1):
                # EOF
                break
            # Move reading cursor one step back
            # seek(offset, whence)
            b.seek(-1, 1)  # Whence = 1 => current position of cursor

            # Extract packet
            packet = get_next_block_type(b, shb.byteorder)(b, shb.byteorder)
            # Update the position in the binary file
            current_position += packet.get_block_total_length(byteorder=shb.byteorder)

            if isinstance(packet, EnhancedPacketBlock):
                # packet = packet(b, shb.byteorder)
                keys = get_5_tuple(packet.raw_data, NETWORK_BYTE_ORDER, ids.get_linktype(shb.byteorder))
                # Check that valid keys were returned
                if not (keys is None):

                    data = get_data_function(shb, ids, packet, include_ts)
                    flow_streamer.add_packet(packet_data=data, flow_id='-'.join(keys))
            i += 1
            if verbose:
                print(i, end="\r")
    flow_streamer.store_remaining_flows()


def packet_file_to_sorted_stream_flows(ifile, config, storage_path, day, verbose=True):
    """
    Extract and sort flows from the provided packet capture file (.pcap or .pcapng) in a stream-based fashion.

    :param ifile: Path to input PCAP(NG) file
    :param config: Dataset configuration
    :param storage_path: Path to storage root for the extracted flows.
    :param verbose: If True, output progress reporting.
    :type ifile: ``str``
    :type: ``DatasetConfig``
    :type storage_path: ``str``
    :type verbose: ``bool``
    :return: None
    """
    if determine_file_type(ifile) == "pcap":
        return pcap_file_to_sorted_stream_flows(ifile, config, storage_path, config.include_ts, day, verbose)
    else:
        return pcapng_file_to_sorted_stream_flows(ifile, config, storage_path, config.include_ts, day, verbose)


def packet_file_to_sorted_flows(ifile, batch_size, include_ts, storage_path, verbose=True, **kwargs):
    if determine_file_type(ifile) == "pcap":
        return pcap_file_to_sorted_flow(ifile, batch_size, storage_path, include_ts, verbose, **kwargs)
    else:
        return pcapng_file_to_sorted_flow(ifile, batch_size, storage_path, include_ts, verbose, **kwargs)


def packet_file_to_raw_packets(ifile, output_dir, bran, eran, batch_size, include_ts, verbose=True):
    if determine_file_type(ifile) == "pcap":
        pcap_file_to_raw_packets(ifile, output_dir, bran, eran, batch_size, include_ts, verbose=verbose)
    else:
        pcapng_file_to_raw_packets(ifile, output_dir, bran, eran, batch_size, include_ts, verbose=verbose)


if __name__ == "__main__":
    pass
