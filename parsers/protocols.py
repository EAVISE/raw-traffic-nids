from abc import ABC, ABCMeta, abstractmethod
"""
class Layer1ProtocolName(Enum):
    ETHERNET = 0

class Layer2ProtocolName(Enum):
    ETHERNET = 0

class Layer3ProtocolName(Enum):
    IPV4 = 0
    IPV6 = 1

class Layer4ProtocolName(Enum):
    TCP = 0
    UDP = 1
"""


class Protocol(object):
    __metaclass__ = ABCMeta
    @abstractmethod
    def __init__(self, raw_data, byteorder, *, name):
        self.raw_data = None
        self.name = name
        self.__next_layer_protcol = None
    
    @abstractmethod
    def __str__(self):
        pass

    @abstractmethod
    def get_next_layer_protocol(self, byteorder):
        return self.__next_layer_protcol

        
