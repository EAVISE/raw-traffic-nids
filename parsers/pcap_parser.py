from io import BytesIO
import numpy as np
import os


def byteorder_to_numpy(byteorder):

    #order = ''
    #if byteorder == 'little':
    #    order = '<'
    #else:
    #    order = '>'

    # Byte-order is irrelevant for 8-bit numbers
    # https://numpy.org/doc/stable/reference/generated/numpy.dtype.byteorder.html
    return np.dtype('u1')


class PCAPGlobalHeader(object):
    def __init__(self, bytestring):
        
        # Header decoding: https://wiki.wireshark.org/Development/LibpcapFileFormat, https://github.com/hokiespurs/velodyne-copter/wiki/PCAP-format
        self.magic_number = bytestring.read(4)
        
        if self.magic_number == b'\xd4\xc3\xb2\xa1':
            self.byteorder = 'little'
            self.byteorder_inverse = 'big'
            self.time_resolution = 'us'
        elif self.magic_number == b'\x4d\x3c\xb2\xa1':
            self.byteorder = 'little'
            self.byteorder_inverse = 'big'
            self.time_resolution = 'ns'
        elif self.magic_number == b'\xa1\xb2\x3c\x4d':
            self.byteorder = 'big'
            self.byteorder_inverse = 'little'
            self.time_resolution = 'ns'
        elif self.magic_number == b'\xa1\xb2\xc3\xd4':
            self.byteorder = 'big'
            self.byteorder_inverse = 'little'
            self.time_resolution = 'us'

        # Magic number should be 0xa1b2c3d4 or 2712847316
        #print(self.magic_number, hex(int.from_bytes(self.magic_number, byteorder=self.byteorder)))

        self.version_major = bytestring.read(2)     # major number of the file format
        self.version_minor = bytestring.read(2)     # minor number of the file format

        self.thiszone = bytestring.read(4)      # Time zone information, normally zero (= GMT), SIGNED
        self.sigfigs = bytestring.read(4)       # sigfigs: in theory, the accuracy of time stamps in the capture, rarely used
        self.snaplen = bytestring.read(4)       # the "snapshot length" for the capture (typically 65535 or even more, but might be limited by the user)
        self.network = bytestring.read(4)       # link-layer header type, specifying the type of headers at the beginning of the packet
    
    def get_version_major(self):
        return int.from_bytes(self.version_major, byteorder=self.byteorder, signed=False)
    
    def get_version_minor(self):
        return int.from_bytes(self.version_minor, byteorder=self.byteorder, signed=False)

    def get_thiszone(self):
        return int.from_bytes(self.thiszone, byteorder=self.byteorder, signed=True)
    
    def get_sigfigs(self):
        return int.from_bytes(self.sigfigs, byteorder=self.byteorder, signed=False)
    
    def get_snaplen(self):
        return int.from_bytes(self.snaplen, byteorder=self.byteorder, signed=False)

    def get_network(self):
        return int.from_bytes(self.network, byteorder=self.byteorder, signed=False)

    def get_size(self):
        """
        Get size of the global header in bytes
        """
        return 24 # 24 bytes


class PCAPPacketHeader(object):
    def __init__(self, bytestring):
    
        self.ts_sec = bytestring.read(4)    # Timestamp seconds, unsigned
        self.ts_usec = bytestring.read(4)   # Timestamp useconds, unsigned
        self.incl_len = bytestring.read(4)  # Number of bytes included in packet, unsigned
        self.orig_len = bytestring.read(4)  # Original length of packet, unsigned
        #print(self.ts_sec, self.ts_usec, self.incl_len, self.orig_len)
        #print(int.from_bytes(self.ts_sec, byteorder='little', signed=False))

    def get_ts_1(self, byteorder):
        return int.from_bytes(self.ts_sec, byteorder=byteorder, signed=False)

    def get_ts_2(self, byteorder):
        return int.from_bytes(self.ts_usec, byteorder=byteorder, signed=False)

    def get_ts(self, byteorder, time_resolution):
        """
        Get the timestamp of this packet, divided into seconds and a nanosecond offset.
        The seconds are counted since January 1, 1970 00:00:00 GMT, aka UNIX time_t.
        The nanoseconds are an offset to this value, and are always smaller than 1,000,000,000.

        :param byteorder:
        :param time_resolution: Either 'us' or 'ns'
        :return: Timestamp of the packet, split into seconds (1st) and nanoseconds (2nd)
        :return: ``tuple`` of (``int``, ``int``)
        """
        sec = self.get_ts_1(byteorder)

        if time_resolution == 'us':
            return sec, self.get_ts_2(byteorder) * 1000
        else:
            return sec, self.get_ts_2(byteorder)

    def get_incl_len(self, byteorder):
        #print(self.incl_len)
        return int.from_bytes(self.incl_len, byteorder=byteorder, signed=False)

    def get_orig_len(self, byteorder):
        return int.from_bytes(self.orig_len, byteorder=byteorder, signed=False)


class PCAPPacket(object):

    def __init__(self, bytestring, byteorder):
        self.packetheader = PCAPPacketHeader(bytestring)
        #print(self.packetheader.get_ts_sec(byteorder))
        #print(self.packetheader.get_ts_usec(byteorder))
        #print(self.packetheader.get_incl_len(byteorder))
        #print(self.packetheader.get_orig_len(byteorder))
        #print(bytestring)
        #test = self.packetheader.get_incl_len(byteorder)
        #result = bytestring.read(test)
        #print(result)
        self.payload = bytestring.read(self.packetheader.get_incl_len(byteorder))
        #print(self.payload)

    def string(self, byteorder, byteorder_inverse):
        """

        byteorder(str): Byteorder as required to decode packet header data
        byteorder_inverse(str): Inverse byteorder to decode packet payloads
        """
        # Using hex() will remove leading zeros, which is incorrect
        # Using format(), size is required to get the total length of the string
        # format will then add leading zero padding to get to this length
        # Size * 2 since one byte corresponds to TWO characters
        return "{:0{size}X}".format(int.from_bytes(self.payload, byteorder=byteorder_inverse),
                                    size=self.packetheader.get_incl_len(byteorder) * 2)
        #return hex(int.from_bytes(self.payload, byteorder=byteorder))

    def integer_array(self, byteorder, size=100):

        packet_size = self.packetheader.get_incl_len(byteorder)
        if packet_size > size:
            return np.frombuffer(self.payload, dtype=byteorder_to_numpy(None), count=size)
        else:
            data = np.zeros(100, dtype='u1')
            data[:packet_size] = np.frombuffer(self.payload, dtype=byteorder_to_numpy(None), count=-1)
            return data

    def get_size(self, byteorder):
        # Return payload size + packet header size (=16)
        return self.packetheader.get_incl_len(byteorder) + 16

    def __str__(self):
        #print(self.payload)
        return hex(int.from_bytes(self.payload, byteorder='big'))


if __name__ == '__main__':
    pcap_path = os.path.join("..","preprocess","cicids2017tester","CICIDS2017","pcap_data","friday","Friday-WorkingHours.pcap")
    
    with open(pcap_path,"rb") as f:
        #buffer = BytesIO(f.read()).getbuffer()
        #global_header_line = f.read(100)
        #print(global_header_line)
        #b = BytesIO(global_header_line)
        #print(b)
        b = BytesIO(f.read())
        
        #print(b.read(100))
        #b.seek(0)
        global_header = PCAPGlobalHeader(b)
        print("Magic:", global_header.magic_number)
        print("Version major:", global_header.get_version_major())
        print("Version minor:", global_header.get_version_minor())
        print("Thiszone:", global_header.get_thiszone())
        print("Sigfigs:", global_header.get_sigfigs())
        print("Snaplen:", global_header.get_snaplen())
        print("Network:", global_header.get_network())
        # Set pointer right
        
        packets = []
        #packets.append(PCAPPacket(b, global_header.byteorder))
        
        i = 0
        while True:
            #packets.append(PCAPPacket(b, global_header.byteorder))
            if not b.read(1):
                # EOF
                break

            # Move reading cursor one step back
            # seek(offset, whence)
            b.seek(-1, 1) # Whence = 1 => current position of cursor
            packet = PCAPPacket(b, global_header.byteorder)
            print(packet)
            #print(get_5_tuple(packet.payload, global_header.byteorder_inverse, global_header.get_network()))
            #packets.append(packet)

            i += 1
            print(i, end="\r")
        

        #for packet in packets:
        #    print(get_5_tuple(packet.payload, global_header.byteorder_inverse, global_header.get_network()))
        """
        print(len(packets))
        print(global_header.byteorder)
        #print(packets[0].string(global_header.byteorder_inverse))
        print(packets[0])
        
        eth = Ethernet(packets[0].payload, global_header.byteorder_inverse)
        ipv4= eth.get_next_layer_protocol(global_header.byteorder_inverse)
        tcp = ipv4.get_next_layer_protocol(global_header.byteorder_inverse)
        print(eth)
        print(ipv4)
        print(tcp)
        
        print(get_5_tuple(packets[0].payload, global_header.byteorder_inverse, global_header.get_network()))
        #data = binary_stream.write(f.read()).getbuffer()

        #print(buffer[0:1])
        """
