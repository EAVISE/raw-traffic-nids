"""
This module implements a lightweight parser for PCAPNG files (https://github.com/pcapng/pcapng/)
"""

from abc import ABC, ABCMeta, abstractmethod
from io import BytesIO
import numpy as np
from parsers.constants import *


def byteorder_to_numpy(byteorder):

    #order = ''
    #if byteorder == 'little':
    #    order = '<'
    #else:
    #    order = '>'

    # Byte-order is irrelevant for 8-bit numbers
    # https://numpy.org/doc/stable/reference/generated/numpy.dtype.byteorder.html
    return np.dtype('u1')


class OptionsBlock(object):
    def __init__(self, options, length):
        self.options = options
        self.length = length

        self.comments = []
        self.custom = []
        self.terminated = False

    def decode(self, byteorder):
        i = 0
        while i < self.length:
            opcode = self.options[i:i+2]
            oplen = self.options[i+2:i+4]
            oplen = int.from_bytes(oplen, byteorder=byteorder, signed=False)
            opval = self.options[i+4:i+4+oplen]

            self._store_opval(opcode, opval, oplen, byteorder)
            # Option Value fields are padded to multiples of 32-bits
            # If oplen is not a multiple of 4 bytes, account for the padding
            if oplen % 4 != 0:
                oplen_padded = oplen + (oplen % 4)
            else:
                oplen_padded = oplen
            i += oplen_padded + 4

    def _store_opval(self, opcode, opval, oplen, byteorder):
        if opcode == opt_comment:
            self.comments.append(int.from_bytes(opval, byteorder=byteorder, signed=False))
        elif opcode == opt_endofopt:
            self.terminated = True


class InterfaceDescriptionBlockOptions(OptionsBlock):
    def __init__(self, options, length):
        super().__init__(options, length)
        self.if_name = None             # Name of device used to capture data
        self.if_description = None      # Description of device used to capture data
        self.if_IPv4addr = None         # IPv4 address and netmasks for interface
        self.if_IPv6addr = None         # IPv6 address for the interface
        self.if_MACaddr = None          # Interface Hardware MAC address
        self.if_EUIaddr = None          # Interface Hardware EUI address
        self.if_speed = None            # Interface speed in bps
        self.if_tsresol = 0.000001      # Resolution of timestamps
        self.if_tzone = None            # Timezone
        self.if_filter = None           # Filter used on interface
        self.if_os = None               # Operating System of machine in which interface is installed
        self.if_fcslen = None           # Frame check sequence length
        self.if_tsoffset = 0            # Offset that must be added to each timestamp
        self.if_hardware = None         # Desciption of interface hardware

    def _store_opval(self, opcode, opval, oplen, byteorder):
        if opcode == opt_if_name:
            self.name = opval.decode('utf-8')
        elif opcode == opt_if_tsresol:
            number = int.from_bytes(opval, byteorder=byteorder, signed=False)
            msb = number & (1 << (oplen * 8 - 1))
            if msb == 1:
                self.if_tsresol = pow(2, -number)
            else:
                self.if_tsresol = pow(10, -number)
        elif opcode == opt_if_description:
            self.if_description = opval.decode('utf-8')
        elif opcode == opt_if_IPv4addr:
            self.if_IPv4addr = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_IPv6addr:
            self.if_IPv6addr = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_MACaddr:
            self.if_MACaddr = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_EUIaddr:
            self.if_EUIaddr = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_speed:
            self.if_speed = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_tzone:
            self.if_tzone = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_filter:
            self.if_filter = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_os:
            self.if_os = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_fcslen:
            self.if_fcslen = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_tsoffset:
            self.if_tsoffset = int.from_bytes(opval, byteorder=byteorder, signed=False)
        elif opcode == opt_if_hardware:
            self.if_hardware = opval.decode('utf-8')
        elif opcode == opt_comment:
            self.comments.append(int.from_bytes(opval, byteorder=byteorder, signed=False))
        elif opcode == opt_endofopt:
            self.terminated = True


class PCAPNGBlock(object):
    __metaclass__ = ABCMeta

    def __init__(self, bytestring):
        
        self.block_type = bytestring.read(4)
        self.block_total_length = bytestring.read(4)

    def get_block_total_length(self, byteorder):
        return int.from_bytes(self.block_total_length, byteorder, signed=False)

    def get_block_type(self):
        return self.block_type()


class SectionHeaderBlock(PCAPNGBlock):
    def __init__(self, bytestring):
        super().__init__(bytestring)
        self.byte_order_magic = bytestring.read(4) # Base: 0x1A2B3C4D

        if self.byte_order_magic == (b'\x1a\x2b\x3c\x4d'):
            self.byteorder = "big"
            self.byteorder_inverse = "little"
        else:
            self.byteorder = "little"
            self.byteorder_inverse = "big"

        # Magic number should be 0xa1b2c3d4 or 2712847316
        #print(self.byte_order_magic, hex(int.from_bytes(self.byte_order_magic, byteorder=self.byteorder)))

        # Version information
        self.version_major = bytestring.read(2) # Major version changes the way a file is read
        self.version_minor = bytestring.read(2) # Minor version consists of minor changes
        
        # Section length:
        # Section length of -1 (0xFFFFFFFFFFFFFFFF) indicates unspecified length
        self.section_length= bytestring.read(8) # Length in octets of the following section

        # Options
        if self.get_block_total_length(self.byteorder) > SHBSize:
            self.options = bytestring.read(self.get_block_total_length(self.byteorder) - SHBSize)
        else:
            self.options = None
        
        self.block_total_length_trailing = bytestring.read(4)

    def get_version_major(self):
        return int.from_bytes(self.version_major, byteorder=self.byteorder, signed=False)

    def get_version_minor(self):
        return int.from_bytes(self.version_minor, byteorder=self.byteorder, signed=False)
    
    def get_section_length(self):
        return int.from_bytes(self.section_length, byteorder=self.byteorder, signed=True)


class InterfaceDescriptionBlock(PCAPNGBlock):
    def __init__(self, bytestring, byteorder):
        super().__init__(bytestring)
        
        self.linktype = bytestring.read(2)  # Defines the link layer type (see constants.py)
        self.reserved = bytestring.read(2)

        self.snaplen = bytestring.read(4)  # Max number of octets stored per packet, 0 indicates "no limit"
        
        # Options
        if self.get_block_total_length(byteorder) > IDBSize:
            length = self.get_block_total_length(byteorder) - IDBSize
            options = bytestring.read(length)
            self.options = InterfaceDescriptionBlockOptions(options=options, length=length)
            self.options.decode(byteorder)
        else:
            self.options = InterfaceDescriptionBlockOptions(options=None, length=0)

        self.block_total_length_trailing = bytestring.read(4)

    def get_linktype(self, byteorder):
        return int.from_bytes(self.linktype, byteorder=byteorder, signed=False)
    
    def get_snaplen(self, byteorder):
        return int.from_bytes(self.snaplen, byteorder=byteorder, signed=False)

    def get_ts_parts(self, ts_high, ts_low):
        seconds = ((ts_high << 32) + ts_low) * self.options.if_tsresol
        sec = int(seconds)
        ns = int((seconds - sec) * 1000000000)
        return sec, ns


class EnhancedPacketBlock(PCAPNGBlock):
    def __init__(self, bytestring, byteorder):
        super().__init__(bytestring)

        # Data information
        self.interface_id = bytestring.read(4)  # Interface ID (0 = unknown)
        self.ts_high = bytestring.read(4)       # Timestamp (High)
        self.ts_low = bytestring.read(4)        # Timestamp (Low)
        self.caplen = bytestring.read(4)        # Captured Packet Length
        self.oglen = bytestring.read(4)         # Orignal Packet Length

        caplen_int = self.get_captured_packet_length(byteorder)
        # Actual data
        self.raw_data = bytestring.read(caplen_int)

        # Calculate zero-padding added:
        if caplen_int % 4 != 0:
            padding = 4 - (caplen_int % 4)
            self.padding = bytestring.read(padding)
        else:
            padding = 0
            self.padding = None

        # Options
        if self.get_block_total_length(byteorder) > (EPBSize + caplen_int + padding):
            self.options = bytestring.read(self.get_block_total_length(byteorder) - (EPBSize + caplen_int + padding))
        else:
            self.options = None
        
        self.block_total_length_trailing = bytestring.read(4)

    def get_interface_id(self, byteorder):
        return int.from_bytes(self.interface_id, byteorder=byteorder, signed=False)
    
    def get_timestamp_high(self, byteorder):
        return int.from_bytes(self.ts_high, byteorder=byteorder, signed=False)
    
    def get_timestamp_low(self, byteorder):
        return int.from_bytes(self.ts_low, byteorder=byteorder, signed=False)
    
    def get_timestamp(self, byteorder):
        return (self.get_timestamp_high(byteorder) << 32) | self.get_timestamp_low(byteorder)

    def get_captured_packet_length(self, byteorder):
        return int.from_bytes(self.caplen, byteorder, signed=False)
    
    def get_original_packet_length(self, byteorder):
        return int.from_bytes(self.oglen, byteorder, signed=False)

    def string(self, byteorder, byteorder_inverse):
        return "{:0{size}X}".format(int.from_bytes(self.raw_data, byteorder=byteorder_inverse),
                                    size=self.get_captured_packet_length(byteorder) * 2)

    def integer_array(self, byteorder='little', size=100):

        packet_size = self.get_captured_packet_length(byteorder)
        if packet_size > size:
            return np.frombuffer(self.raw_data, dtype=byteorder_to_numpy(None), count=size)
        else:
            data = np.zeros(100, dtype='u1')
            data[:packet_size] = np.frombuffer(self.raw_data, dtype=byteorder_to_numpy(None), count=-1)

            return data

    def print_raw_data(self, byteorder):
        print(hex(int.from_bytes(self.raw_data, byteorder, signed=False)))

    def __str__(self):
        return "iID: {}, ts: {}|{}| {} - {}|{}".format(self.get_interface_id("little"),\
            self.get_timestamp_high("little"), self.get_timestamp_low("little"),\
            self.get_captured_packet_length("little"), self.get_original_packet_length("little"),\
            self.options)

    def get_size(self, byteorder):
        return self.get_original_packet_length(byteorder=byteorder)


class SimplePacketBlock(PCAPNGBlock):
    pass


class NameResolutionBlock(PCAPNGBlock):
    pass


class InterfaceStatisticsBlock(PCAPNGBlock):
    pass


class JournalExportBlock(PCAPNGBlock):
    pass


class DecryptionSecretsBlock(PCAPNGBlock):
    pass


class CustomBlock(PCAPNGBlock):
    pass


def get_section_header_block_size(binaryfile):
    b = BytesIO(binaryfile)
    b.read(4)
    size = b.read(4)

    if b.read(4) == (b'\x1a\x2b\x3c\x4d'):
        return int.from_bytes(size, "big", signed=False)
    else:
        return int.from_bytes(size, "little", signed=False)


def get_next_block_size(bytestring, byteorder):
    bytestring.read(4)
    size = int.from_bytes(bytestring.read(4), byteorder, signed=False)
    bytestring.seek(-8, 1)
    return size


def get_next_block_type(bytestring, byteorder):
    block_type = int.from_bytes(bytestring.read(4), byteorder, signed=False)
    bytestring.seek(-4, 1)

    if block_type == 1:
        return InterfaceDescriptionBlock
    elif block_type == 6:
        return EnhancedPacketBlock
    elif block_type == 3:
        return SimplePacketBlock
    elif block_type == 4:
        return NameResolutionBlock
    elif block_type == 5:
        return InterfaceStatisticsBlock
    elif block_type == 9:
        return JournalExportBlock
    elif block_type == 10:
        return DecryptionSecretsBlock
    else:
        return None


if __name__ == '__main__':
    pass