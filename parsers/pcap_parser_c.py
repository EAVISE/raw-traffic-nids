"""
WARNING: This module is deprecated and should no longer be used.
"""

import ctypes
import os
from exceptions import CTypesInvalidArgument, CInternalError


def _get_parser():
    folder = os.path.dirname(os.path.abspath(__file__))
    so_file = os.path.join("..", "custom_libraries", "lib_pcap_parser.so")
    dll_path = os.path.join(folder, so_file)
    print(dll_path)
    return ctypes.CDLL(dll_path)


def pcap_to_text(input_file, output_file, debug_file=None, bran=0, eran=-1, batch_size=-1, verbose=False, include_ts=False):
    
    # First, the absolute path to the folder is necessary (because at that path the .so file is located)
    # Generate .so file using cc -fPIC -shared -o pcap_parser.so pcap_parser.c -lpcap
    parser = _get_parser()

    # Make sure the necessary files are present
    if input_file==None or output_file==None:
        if input_file == None:
            raise CTypesInvalidArgument("Input pcap file is undefined. Please define the input pcap file.")
        elif output_file == None:
            raise CTypesInvalidArgument("Output pcap file is undefined. Please define the output pcap file.")
    
    # Make sure the input file and output directory actually exist (output file will be created by c code)
    if not os.path.isfile(input_file):
        raise CTypesInvalidArgument("Input pcap file \"{}\" does not exist. Please provide an existing input pcap file.".format(input_file))
    
    output_path = os.path.split(output_file)[0]
    if not os.path.isdir(output_path):
        raise CTypesInvalidArgument("Output path \"{}\" does not exist. Please provide an existing output path.".format(output_path))

    # If the data will be processed in batches, make sure the output file name is suitable for use
    if batch_size > 0:
        output_file = output_file.rstrip(".csv")

    # Encode in/output file paths:
    input_file  = input_file.encode("utf-8")
    output_file = output_file.encode("utf-8")

    # The beginnen of the range of packets should 
    if bran < 0:
        raise CTypesInvalidArgument("The range of packets should at least start at 0. The given start is \"{}\" instead.".format(bran))
    
    if (eran < bran) and (eran != -1):
        raise CTypesInvalidArgument("The beginning of the range of packets (bran) is greater than the end of the range of packets (eran)."+\
            "If all packets are concerned, set eran=-1. Otherwise, either make eran ({}) greater or bran ({}) smaller.".format(eran, bran))
    
    has_debug = 0
    if debug_file != None:
        has_debug = 1
        debug_file = debug_file.encode("utf-8")  #flow_processing.
    if include_ts:
        include_ts_int = 1
    
    # By default, verbose mode is disabled
    verbose_int = 0
    if verbose:
        verbose_int = 1

    c_return = parser.pcap_to_text(1, input_file, 1, output_file, has_debug, debug_file,
                                    bran, eran, batch_size, verbose_int, include_ts_int)

    if c_return == 0:
        print("Done", c_return)
    
    else:
        raise CInternalError("Error internally in the C code: ", c_return)


def pcap_to_sorted_flows(input_file, root, debug_file=None, bran=0, eran=-1, batch_size=-1, verbose=False, include_ts=False):
    # First, the absolute path to the folder is necessary (because at that path the .so file is located)
    # Generate .so file using cc -fPIC -shared -o pcap_parser.so pcap_parser.c -lpcap
    parser = _get_parser()

    # Make sure the necessary files are present
    if input_file==None or root==None:
        if input_file == None:
            raise CTypesInvalidArgument("Input pcap file is undefined. Please define the input pcap file.")
        elif root == None:
            raise CTypesInvalidArgument("Output path is undefined. Please define the output path.")
    
    # Make sure the input file and output directory actually exist (output file will be created by c code)
    if not os.path.isfile(input_file):
        raise CTypesInvalidArgument("Input pcap file \"{}\" does not exist. Please provide an existing input pcap file.".format(input_file))
    
    if not os.path.isdir(root):
        raise CTypesInvalidArgument("Output path \"{}\" does not exist. Please provide an existing output path.".format(root))

    # If the data will be processed in batches, make sure the output file name is suitable for use
    if batch_size > 0:
        root = root.rstrip(".csv")

    # Encode in/output file paths:
    input_file  = input_file.encode("utf-8")
    root = root.encode("utf-8")

    # The beginnen of the range of packets should 
    if bran < 0:
        raise CTypesInvalidArgument("The range of packets should at least start at 0. The given start is \"{}\" instead.".format(bran))
    
    if (eran < bran) and (eran != -1):
        raise CTypesInvalidArgument("The beginning of the range of packets (bran) is greater than the end of the range of packets (eran)."+\
            "If all packets are concerned, set eran=-1. Otherwise, either make eran ({}) greater or bran ({}) smaller.".format(eran, bran))
    
    has_debug = 0
    if debug_file != None:
        has_debug = 1
        debug_file = debug_file.encode("utf-8")  
    
    # By default, verbose mode is disabled
    verbose_int = 0
    if verbose:
        verbose_int = 1

    # By default, time stamps are not included
    include_ts_int = 0
    if include_ts:
        include_ts_int = 1
    
    c_return = parser.extract_and_sort_pcap(1, input_file, 1, root, has_debug, debug_file,\
                                            bran, eran, batch_size, verbose_int, include_ts_int)

    if c_return == 0:
        print("Done", c_return)
    
    else:
        raise CInternalError("Error internally in the C code.")


def text_to_pcap(output_file, strings, seconds_ts=[], useconds_ts=[]):
    """
    Function to turn a list of strings representing the hexadecimal representation of packets (=FA826DCB52B48...) into a pcap file

    """

    if output_file is None:
        raise CTypesInvalidArgument("Target output PCAP file is not defined. Define a correct path to the output file that will be created.")
    
    if os.path.isfile(output_file):
        raise CTypesInvalidArgument("Target output PCAP file \"{}\" already exists.".format(output_file))
    
    n_strings = len(strings)

    include_ts = True
    if len(seconds_ts) != n_strings:
        include_ts = False
    
    if len(useconds_ts) != n_strings:
        include_ts = False
     
    parser = _get_parser()

    # Encode output file
    c_output_file = output_file.encode("utf-8")

    # Turn packets into C char pointers:
    # Important: first encode: https://stackoverflow.com/questions/23852311/different-behaviour-of-ctypes-c-char-p/24061473
    packets = []
    for packet in strings:
        packets.append(ctypes.c_char_p(packet.encode("utf-8")))
     
    
    # Turn lists into C arrays
    c_strings = (ctypes.c_char_p * n_strings)(*packets)
    c_seconds_ts = (ctypes.c_int * 1)(*[1]) # Ensure the array is initialized
    c_useconds_ts = (ctypes.c_int * 1)(*[1]) # Ensure the arrau is initialized
    if include_ts:
        c_seconds_ts = (ctypes.c_int * n_strings)(*seconds_ts)
        c_useconds_ts =(ctypes.c_int * n_strings)(*useconds_ts)
    
    # Turn boolean into int
    c_include_ts = 1 if(include_ts) else 0

    # c_return is the return value of the used function
    c_return = parser.text_to_pcap(c_output_file, c_strings, n_strings, c_include_ts, c_seconds_ts, c_useconds_ts)

    if c_return == 0:
        return
    else:
        raise CInternalError("Error internally in the C code.")
