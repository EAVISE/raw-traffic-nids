import os
from enums import Enum
import numpy as np
import json
from io import BytesIO

from parsers.pcapng_parser import get_next_block_type, SectionHeaderBlock, EnhancedPacketBlock
from parsers.pcap_parser import PCAPGlobalHeader, PCAPPacketHeader, PCAPPacket
from parsers.layer1_2_protocols import Ethernet, LinuxCookedCapture
from parsers.layer3_protocols import IPv4
from parsers.layer4_protocols import TCP, UDP
from parsers.constants import LINKTYPE_ETHERNET, LINKTYPE_LINUX_SLL, NETWORK_BYTE_ORDER
from flow_processing.flow_sorter import add2dict, sort_flows_from_dict
from flow_processing.expansion import FlowStreamSorter

from parsers.flow_parsing import get_5_tuple, get_flow_id, determine_file_type


class AnalyzerType(Enum):
    FlowBased = 1


def format_bitrate(bps, decimals=2):
    if bps > 10e12:
        return '{} Tbps'.format(round(bps / 10e12, decimals))
    elif bps > 10e9:
        return '{} Gbps'.format(round(bps / 10e9, decimals))
    elif bps > 10e6:
        return '{} Mbps'.format(round(bps / 10e6, decimals))
    elif bps > 10e6:
        return '{} kbps'.format(round(bps / 10e3, decimals))
    else:
        return '{} bps'.format(round(bps / 1, decimals))


def __calculate_section_RDR(section, starts_irrelevant):
    relevant = 0
    irrelevant = 0

    for i in range(len(section)):

        if starts_irrelevant:
            if i % 2 == 0:
                irrelevant += section[i]
            else:
                relevant += section[i]
        else:
            if i % 2 == 0:
                relevant += section[i]
            else:
                irrelevant += section[i]
    return relevant / (relevant + irrelevant)


def get_rel_flow_distribution(flow_distribution: dict):
    rel_flow_distribution = {}

    total_count = 0

    for idx, count in flow_distribution.items():
        rel_flow_distribution[idx] = count
        total_count += count

    for idx, count in flow_distribution.items():
        rel_flow_distribution[idx] = (count / total_count)

    return rel_flow_distribution


def density_distribution_from_list(datalist, window, starts_irrelevant=True):
    list_size = len(datalist)

    # Ensure a valid window
    # Windows either too large or too small will calculate
    if (window < 1) or (window > list_size):
        window = list_size

    density_distribution = []

    position = 0
    while (position + window) <= list_size:

        section = datalist[position:position+window]
        density_distribution.append(__calculate_section_RDR(section, starts_irrelevant=starts_irrelevant))
        position += 1

    return density_distribution


class TrafficAnalyzer:
    n_irrelevant_bytes = 0
    n_ignored_bytes = 0
    n_relevant_bytes = 0

    n_used_packets = 0
    n_skipped_packets = 0

    # Relevant Data Ratio for byte density
    window_b = 10  # Number of bytes
    PRDR_b = 0  # Peak Relevant Data Ratio for a given window
    LRDR_b = 0  # Lowest Relevant Data Ratio for a given window
    ARDR_b = 0  # Average Relevant Data Ratio for a given window
    b_list = [0]

    # Relevant Data Ratio for packet density
    window_p = 10  # Number of packets
    PRDR_p = 0  # Peak Relevant Data Ratio for a given window
    LRDR_p = 0  # Lowest Relevant Data Ratio for a given window
    ARDR_p = 0  # Average Relevant Data Ratio for a given window
    p_list = [0]

    currently_relevant = False

    __b_list_filename = 'b_list.npy'
    __p_list_filename = 'p_list.npy'

    def __init__(self, storage_path, byteorder):
        self.storage_path = storage_path
        self.byteorder = byteorder

    def _set_current_relevant(self, relevant):

        old_relevant = self.currently_relevant
        self.currently_relevant = relevant

        # Return False if the state of currently_relevant has changed
        return relevant == old_relevant

    def add_packet(self, packet: [PCAPPacket, EnhancedPacketBlock], **kwargs):
        """
        Add a packet to the traffic analysis

        :param packet: Packet to add to analysis
        :param keys: 5 tuple identifiers of the packet, if applicable
        :return: None
        """

        self.n_used_packets += 1
        self.n_relevant_bytes += packet.get_size(self.byteorder)

    def report(self):
        """
        Get a string report with the resulting analytics

        :return: A report of the analytics of all packets provided as input
        :rtype: ``str``
        """

        report = ""

        report += "Byte counts:\n"
        report += "# relevant bytes = {}\n".format(self.n_relevant_bytes)
        report += "# irrelevant bytes = {}\n".format(self.n_irrelevant_bytes)
        report += "# ignored bytes = {}\n".format(self.n_ignored_bytes)
        report += "\n"

        report += "Packet counts:\n"
        report += "# used packets = {}\n".format(self.n_used_packets)
        report += "# skipped packets = {}\n".format(self.n_skipped_packets)
        report += "\n"

        report += "Byte Relevant Data Ratio (RDR_b) counts:\n"
        report += "Window: {}\n".format(self.window_b)
        report += "Peak RDR = {}\n".format(self.PRDR_b)
        report += "Lowest RDR = {}\n".format(self.LRDR_b)
        report += "Average RDR = {}\n".format(self.ARDR_b)
        report += "\n"

        report += "Packet Relevant Data Ratio (RDR_p) counts:\n"
        report += "Window: {}\n".format(self.window_p)
        report += "Peak RDR = {}\n".format(self.PRDR_p)
        report += "Lowest RDR = {}\n".format(self.LRDR_p)
        report += "Average RDR = {}\n".format(self.ARDR_p)

        return report

    def calculate_byte_RDR(self, window=None):
        if window is None:
            window = self.window_b

        density_distribution = density_distribution_from_list(datalist=self.b_list, window=window)
        self.PRDR_b = max(density_distribution)
        self.LRDR_b = min(density_distribution)
        self.ARDR_b = sum(density_distribution) / len(density_distribution)

        return density_distribution

    def calculate_packet_RDR(self, window=None):
        if window is None:
            window = self.window_p

        density_distribution = density_distribution_from_list(datalist=self.p_list, window=window)
        self.PRDR_p = max(density_distribution)
        self.LRDR_p = min(density_distribution)
        self.ARDR_p = sum(density_distribution) / len(density_distribution)

        return density_distribution

    def _results_dict(self):
        results = {
            'n_irrelevant_bytes': self.n_irrelevant_bytes,
            'n_ignored_bytes': self.n_ignored_bytes,
            'n_relevant_bytes': self.n_relevant_bytes,

            'n_used_packets': self.n_used_packets,
            'n_skipped_packets': self.n_skipped_packets,

            'window_b': self.window_b,
            'PRDR_b': self.PRDR_b,
            'LRDR_b': self.LRDR_b,
            'ARDR_b': self.ARDR_b,

            'window_p': self.window_p,
            'PRDR_p': self.PRDR_p,
            'LRDR_p': self.LRDR_p,
            'ARDR_p': self.ARDR_p,

        }
        return results

    def _set_results_from_dict(self, results):

        self.n_irrelevant_bytes = results['n_irrelevant_bytes']
        self.n_ignored_bytes = results['n_ignored_bytes']
        self.n_relevant_bytes = results['n_relevant_bytes']

        self.n_used_packets = results['n_used_packets']
        self.n_skipped_packets = results['n_skipped_packets']

        self.window_b = results['window_b']
        self.PRDR_b = results['PRDR_b']
        self.LRDR_b = results['LRDR_b']
        self.ARDR_b = results['ARDR_b']

        self.window_p = results['window_p']
        self.PRDR_p = results['PRDR_p']
        self.LRDR_p = results['LRDR_p']
        self.ARDR_p = results['ARDR_p']

    def export(self, output_path=None, include_lists=True):

        if output_path is None:
            output_path = self.storage_path

        if not os.path.isdir(output_path):
            os.mkdir(output_path)

        # Export results
        results = self._results_dict()

        # Write results to the target dir
        output_file = os.path.join(output_path, 'results.json')
        with open(output_file, 'w') as f:
            json.dump(results, f)

        if include_lists:
            np.save(os.path.join(output_path, self.__b_list_filename), self.b_list)
            np.save(os.path.join(output_path, self.__p_list_filename), self.p_list)

    def import_results(self, input_path=None):

        if input_path is None:
            input_path = self.storage_path

        input_file = os.path.join(input_path, 'results.json')

        with open(input_file, 'r') as f:
            results = json.load(f)

        self._set_results_from_dict(results)

        # Check if lists can also be loaded
        b_target_path = os.path.join(input_path, self.__b_list_filename)
        p_target_path = os.path.join(input_path, self.__p_list_filename)

        if os.path.isfile(b_target_path):
            self.b_list = np.load(b_target_path)

        if os.path.isfile(p_target_path):
            self.b_list = np.load(p_target_path)

    def finish(self):


        #storage_data = np.array([self.b_list, self.p_list], dtype=np.int32)
        #np.save(self.storage_path, storage_data)

        # Calculate RDR
        self.calculate_byte_RDR()
        self.calculate_packet_RDR()

        # Store obtained data
        self.export(self.storage_path, True)

        return self.report()


class FlowBasedTrafficAnalyzer(TrafficAnalyzer):

    n_flows = 0
    bucket = {}
    flows_order = []

    # High-level metrics
    # Flows per bit
    bpf = 0  # Number bits per flow (average)

    def __init__(self, storage_path, byteorder, **kwargs):
        super(FlowBasedTrafficAnalyzer, self).__init__(storage_path, byteorder)

        self.bucket_size = kwargs.get('bucket_size', 5)
        self.bucket_depth = kwargs.get('bucket_depth', 5)

        self.flow_distribution = {x: 0 for x in range(self.bucket_depth)}

        self.dynamic_queue = kwargs.get('dynamic_queue', True)

        self.bidirectional = kwargs.get('bidirectional', False)

        # Counts of the number of bytes that will be used later on
        # This is the case for relevant flows, and either one other the other can be used
        # (either the first n_flow_bytes of a flow, or the first n_packet_bytes for each packet in a flow)
        self.n_flow_bytes = kwargs.get('n_flow_bytes', 0)
        self.n_packet_bytes = kwargs.get('n_packet_bytes', 100)

        assert not ((self.n_flow_bytes > 0) and (self.n_packet_bytes > 0)), \
            "Either n_flow_bytes ({}) or n_packet_bytes ({}) should be 0".format(self.n_flow_bytes, self.n_packet_bytes)

    def add_packet(self, packet, **kwargs):
        """
        Add a packet to the the object. This packet will to added to a current flow, if applicable, or otherwise a new
        flow will be added to store the packet into instead.


        :param packet:
        :param flow_id:
        :return:
        """

        flow_id = kwargs.get('flow_id', None)
        packet_size = packet.get_size(self.byteorder)

        if flow_id is None:
            # The provided packet is irrelevant (ARP, ping, ...)
            self.n_irrelevant_bytes += packet_size
            self.n_skipped_packets += 1

            self.__update_data_lists(packet_size, False)

        else:
            # The provided packet is relevant
            # First: Add the packet to the bucket
            self.__update_bucket(packet, flow_id)

            # Now we check which method is used to extract bytes flow the flow
            # We do this by either extracting the first n_flow_bytes bytes of a flow,
            # or by extracting the first n_packet_bytes bytes of every packet in a flow
            if self.n_flow_bytes > 0:
                self.__update_n_flow_bytes(packet_size, flow_id)
            else:
                self.__update_n_packet_bytes(packet_size)

    def report(self):
        report = super(FlowBasedTrafficAnalyzer, self).report()

        report += "\n"

        report += "Flow information:\n"
        report += "Used bucket with {} flows of {} packets\n".format(self.bucket_size, self.bucket_depth)
        report += "Captured a total of {} flows\n".format(self.n_flows)
        report += "\n"

        report += "High-level metrics\n"
        report += "Bits per flow: {}\n".format(self.bpf)
        report += "Necessary FPS for 200Gbps: {}\n".format(self.get_flowrate(200*10e9))
        report += "Supported bitrate (in Gbps) for 1000FPS: {}\n".format(self.get_bitrate(10000) / 10e9)
        report += "\n"

        report += "Flow distribution\n"
        for i in range(self.bucket_depth):
            report += "Number of flows with {} packets: {}\n".format(i + 1, self.flow_distribution[i])

        return report

    def determine_bpf(self):
        # Calculate the total number of bits:
        n_bytes = self.n_irrelevant_bytes + self.n_ignored_bytes + self.n_relevant_bytes
        n_bits = n_bytes * 8

        self.bpf = n_bits / self.n_flows

    def get_bitrate(self, fps):
        """
        For a given number of flows per second, determine the bitrate

        :param fps:
        :return:
        """
        return self.bpf * fps

    def get_flowrate(self, bitrate):
        """
        For a given number of bits per second, calculate the corresponding number of flows.
        :param bitrate:
        :return:
        """
        return 1 / self.bpf * bitrate

    def __update_data_lists(self, packet_size, is_relevant):

        if self._set_current_relevant(is_relevant):
            self.b_list[-1] += packet_size
            self.p_list[-1] += 1
        else:
            self.b_list.append(packet_size)
            self.p_list.append(1)

    def __update_n_packet_bytes(self, packet_size):
        self.n_used_packets += 1
        self.__update_data_lists(packet_size, True)

        rem_bytes = packet_size - self.n_packet_bytes

        if rem_bytes > 0:
            self.n_relevant_bytes += self.n_packet_bytes
            self.n_ignored_bytes += rem_bytes
        else:
            self.n_relevant_bytes += packet_size

    def __update_n_flow_bytes(self, packet_size, flow_id):
        """
        Update the analysis counters in the case that we only consider the first n_flow_bytes bytes in a flow as
        relevant

        :param packet:
        :param flow_id:
        :return:
        """
        # flow_size counts how many bytes the current flow already encompasses
        flow_size = 0
        # Iterate over all packets (excluding the one that was just added)
        for p in self.bucket[flow_id][:-1]:
            flow_size += p.get_size(self.byteorder)

        # We calculate how many bytes this packet would still have to add
        needed_bytes = self.n_flow_bytes - flow_size

        if needed_bytes > 0:
            # We will have to use the newest added packet
            self.n_used_packets += 1
            self.__update_data_lists(packet_size, False)

            rem_bytes = packet_size - needed_bytes

            if rem_bytes > 0:
                self.n_ignored_bytes += rem_bytes
                self.n_relevant_bytes += packet_size - rem_bytes
            else:
                self.n_relevant_bytes += packet_size
        else:
            self.n_skipped_packets += 1
            self.n_ignored_bytes = packet_size
            self.__update_data_lists(packet_size, False)

    def __is_in_bucket(self, flow_id):
        if isinstance(flow_id, tuple):
            return_flow_id = '{}-{}-{}-{}-{}'.format(flow_id[1], flow_id[0], flow_id[3], flow_id[2], flow_id[4])
            flow_id = '{}-{}-{}-{}-{}'.format(flow_id[0], flow_id[1], flow_id[2], flow_id[3], flow_id[4])
        else:
            raise NotImplementedError

        if self.bidirectional:

            if flow_id in self.flows_order:
                return True, flow_id
            elif return_flow_id in self.flows_order:
                return True, return_flow_id
            return False, flow_id
        else:
            return (flow_id in self.flows_order), flow_id

    def __update_bucket(self, packet, flow_id):

        in_bucket, flow_id2 = self.__is_in_bucket(flow_id)
        if in_bucket:
            if len(self.bucket[flow_id2]) < self.bucket_depth:
                self.__add_packet(packet=packet, flow_id=flow_id2)
                self.__update_queue_position(flow_id2)
            else:
                self.__remove_flow(flow_id2)
                self.__add_flow(flow_id=flow_id2, packet=packet)
        elif len(self.flows_order) < self.bucket_size:
            self.__add_flow(flow_id=flow_id2, packet=packet)
        else:
            self.__pop_flow()
            self.__add_flow(flow_id=flow_id2, packet=packet)

    def __add_packet(self, packet, flow_id):
        self.bucket[flow_id].append(packet)

    def __update_queue_position(self, flow_id):
        if self.dynamic_queue:
            self.flows_order.remove(flow_id)
            self.flows_order.append(flow_id)

    def __remove_flow(self, flow_id):
        """
        Remove a flow from the bucket

        :param flow_id: Identifier of the flow
        :type flow_id: ``str``
        :return: None
        """
        # Update flow distribution
        self.flow_distribution[len(self.bucket[flow_id]) - 1] += 1

        # Remove saved packets from storage
        self.bucket.pop(flow_id)
        # Remove flow id from queue
        self.flows_order.remove(flow_id)

    def __pop_flow(self):
        """
        Remove the flow that was added first from the bucket

        :return: None
        """
        # Get the flow ID that will be removed
        flow_out_id = self.flows_order.pop()

        # Update flow distribution
        self.flow_distribution[len(self.bucket[flow_out_id]) - 1] += 1

        # Empty the right bucket
        self.bucket.pop(flow_out_id)

    def __add_flow(self, packet, flow_id):
        """
        Add a flow to the bucket.

        :param packet: Packet data
        :type packet: ``str``
        :param flow_id: Identifier of the flow
        :type flow_id: ``str``
        :return: None
        """
        self.n_flows += 1
        self.flows_order.append(flow_id)
        self.bucket[flow_id] = [packet]

    def __empty_bucket(self):
        while len(self.flows_order) > 0:
            self.__pop_flow()

    def finish(self):
        self.__empty_bucket()
        self.determine_bpf()
        return super(FlowBasedTrafficAnalyzer, self).finish()

    def _results_dict(self):

        results = super(FlowBasedTrafficAnalyzer, self)._results_dict()
        results['n_flows'] = self.n_flows
        results['bpf'] = self.bpf
        results['bucket_size'] = self.bucket_size
        results['bucket_depth'] = self.bucket_depth
        results['dynamic_queue'] = self.dynamic_queue
        results['n_flow_bytes'] = self.n_flow_bytes
        results['n_packet_bytes'] = self.n_packet_bytes
        results['flow_distribution'] = self.flow_distribution
        results['bidirectional'] = self.bidirectional

        return results

    def _set_results_from_dict(self, results):

        self.n_flows = results['n_flows']
        self.bpf = results['bpf']
        self.bucket_size = results['bucket_size']
        self.bucket_depth = results['bucket_depth']
        self.dynamic_queue = results['dynamic_queue']
        self.n_flow_bytes = results['n_flow_bytes']
        self.n_packet_bytes = results['n_packet_bytes']
        self.flow_distribution = results['flow_distribution']
        self.bidirectional = results['bidirectional']

        super(FlowBasedTrafficAnalyzer, self)._set_results_from_dict(results)


class TrafficAnalyzerAggregator:

    analyzers = []

    n_irrelevant_bytes = 0
    n_ignored_bytes = 0
    n_relevant_bytes = 0

    n_used_packets = 0
    n_skipped_packets = 0

    # Relevant Data Ratio for byte density
    window_b = 10  # Number of bytes
    PRDR_b = 0  # Peak Relevant Data Ratio for a given window
    LRDR_b = np.inf  # Lowest Relevant Data Ratio for a given window
    ARDR_b = 0  # Average Relevant Data Ratio for a given window
    b_list_len = 0

    # Relevant Data Ratio for packet density
    window_p = 10  # Number of packets
    PRDR_p = 0  # Peak Relevant Data Ratio for a given window
    LRDR_p = np.inf  # Lowest Relevant Data Ratio for a given window
    ARDR_p = 0  # Average Relevant Data Ratio for a given window
    p_list_len = 0

    # Flow based traffic stats
    n_flows = 0

    # High-level metrics
    bpf = 0  # Number bytes per flow (average)

    # Aggregator stats
    n_analyzers = 0

    def __init__(self, *analyzers: [TrafficAnalyzer, FlowBasedTrafficAnalyzer], **kwargs):

        # bucket_config = (n_buckets, bucket_depth, n_bytes)
        self.bucket_size = kwargs.get('bucket_size', None)
        self.bucket_depth = kwargs.get('bucket_depth', None)
        self.n_bytes = kwargs.get('n_bytes', None)
        self.bidirectional = kwargs.get('bidirectional', False)

        if self.bucket_depth is not None:
            self.flow_distribution = {x: 0 for x in range(self.bucket_depth)}
        else:
            self.flow_distribution = {}

        for analyzer in analyzers:
            self.analyzers.append(analyzer)

            # General stats:
            self.n_irrelevant_bytes += analyzer.n_irrelevant_bytes
            self.n_ignored_bytes += analyzer.n_ignored_bytes
            self.n_relevant_bytes += analyzer.n_relevant_bytes

            self.n_used_packets += analyzer.n_used_packets
            self.n_skipped_packets += analyzer.n_skipped_packets

            self.PRDR_b = max(self.PRDR_b, analyzer.PRDR_b)
            self.PRDR_p = max(self.PRDR_p, analyzer.PRDR_p)

            self.LRDR_b = min(self.LRDR_b, analyzer.LRDR_b)
            self.LRDR_p = min(self.LRDR_p, analyzer.LRDR_p)

            b_len = len(analyzer.b_list)
            self.ARDR_b += analyzer.ARDR_b * b_len
            self.b_list_len += b_len

            p_len = len(analyzer.p_list)
            self.ARDR_p += analyzer.ARDR_p * p_len
            self.p_list_len += p_len

            # Flow based traffic specific stats
            if isinstance(analyzer, FlowBasedTrafficAnalyzer):
                self.n_flows += analyzer.n_flows
                for x in range(self.bucket_depth):
                    self.flow_distribution[x] += analyzer.flow_distribution[str(x)]

            # Aggregator stats
            self.n_analyzers += 1

        self.ARDR_b /= self.b_list_len
        self.ARDR_p /= self.p_list_len
        self.bpf = (self.n_relevant_bytes + self.n_ignored_bytes + self.n_irrelevant_bytes) * 8 / self.n_flows

    def report(self):
        report = ""

        report += "Byte counts:\n"
        report += "# relevant bytes = {}\n".format(self.n_relevant_bytes)
        report += "# irrelevant bytes = {}\n".format(self.n_irrelevant_bytes)
        report += "# ignored bytes = {}\n".format(self.n_ignored_bytes)
        report += "\n"

        report += "Packet counts:\n"
        report += "# used packets = {}\n".format(self.n_used_packets)
        report += "# skipped packets = {}\n".format(self.n_skipped_packets)
        report += "\n"

        report += "Byte Relevant Data Ratio (RDR_b) counts:\n"
        report += "Window: {}\n".format(self.window_b)
        report += "Peak RDR = {}\n".format(self.PRDR_b)
        report += "Lowest RDR = {}\n".format(self.LRDR_b)
        report += "Average RDR = {}\n".format(self.ARDR_b)
        report += "\n"

        report += "Packet Relevant Data Ratio (RDR_p) counts:\n"
        report += "Window: {}\n".format(self.window_p)
        report += "Peak RDR = {}\n".format(self.PRDR_p)
        report += "Lowest RDR = {}\n".format(self.LRDR_p)
        report += "Average RDR = {}\n".format(self.ARDR_p)
        report += "\n"

        report += "Flow information:\n"
        report += "Captured a total of {} flows\n".format(self.n_flows)
        report += "\n"

        report += "High-level metrics\n"
        report += "Average number of bits per flow: {}\n".format(round(self.bpf))
        report += "Necessary FPS for 200Gbps: {}\n".format(round(self.get_flowrate(200 * 10e9)))
        report += "Supported bitrate for 2500FPS: {}\n".format(format_bitrate(self.get_bitrate(2500)))
        report += "\n"

        report += "Flow distribution\n"
        for i in range(self.bucket_depth):
            report += "Number of flows with {} packets: {}\n".format(i + 1, self.flow_distribution[i])
        report += "\n"

        report += 'Aggregator stats:\n'
        report += 'Aggregation of {} analyzers\n'.format(self.n_analyzers)



        return report

    def get_byte_count(self):
        """
        Get number of bytes in the datasetm including non-flow bytes
        :return:
        """

        return self.n_relevant_bytes + self.n_ignored_bytes + self.n_irrelevant_bytes

    def get_bitrate(self, fps):
        """
        For a given number of flows per second, determine the bitrate

        :param fps:
        :return:
        """
        return self.bpf * fps

    def get_flowrate(self, bitrate):
        """
        For a given number of bits per second, calculate the corresponding number of flows.
        :param bitrate:
        :return:
        """
        return 1 / self.bpf * bitrate


def __get_analyzer(analyzer_type, storage_path, byteorder, **kwargs):

    if analyzer_type is None:
        return TrafficAnalyzer(storage_path, byteorder)
    elif analyzer_type == AnalyzerType.FlowBased:

        return FlowBasedTrafficAnalyzer(storage_path, byteorder, **kwargs)
    else:

        return None


def get_pcap_analytics(ifile, analyzer_type, storage_path, verbose=True, **kwargs):
    """
        Parse a PCAP file to extract flows in a stream-based fashion.

        :param ifile: Path to input PCAP file.
        :param config: Dataset configuration.
        :param storage_path: Path to root storage directory for the flows
        :param include_ts: Also extract packet timestamps
        :param verbose: Output progress information
        :type ifile: ``str``
        :type config: ``DatasetConfig``
        :type storage_path: ``str``
        :type include_ts: ``bool``
        :type verbose: ``bool``
        :return: None
        """

    # n_flows=config.n_stream_flows, n_packets=config.n_stream_packets dynamic_queue=True

    with open(ifile, "rb") as b:

        global_header = PCAPGlobalHeader(b)

        analyzer = __get_analyzer(analyzer_type, storage_path, byteorder=global_header.byteorder, **kwargs)

        i = 0
        while True:
            # Check for EOF
            if not b.read(1):
                # EOF
                break
            # Move reading cursor one step back
            # seek(offset, whence)
            b.seek(-1, 1)  # Whence = 1 => current position of cursor

            # Extract packet
            packet = PCAPPacket(b, global_header.byteorder)

            if analyzer_type == AnalyzerType.FlowBased:

                keys = get_5_tuple(packet.payload, NETWORK_BYTE_ORDER, global_header.get_network())
                #flow_id = get_flow_id(packet.payload, NETWORK_BYTE_ORDER, global_header.get_network())
                # Check that valid keys were returned

                analyzer.add_packet(packet, flow_id=keys)
            else:
                analyzer.add_packet(packet)
            i += 1
            if verbose:
                print(i, end="\r")

    analyzer.finish()
    return analyzer


def get_pcapng_analytics(ifile, analyzer_type, storage_path, verbose=True, **kwargs):
    """
        Parse a PCAPNG file for analysis.

        :param ifile: Path to input PCAP file.
        :param config: Dataset configuration.
        :param storage_path: Path to root storage directory for the flows
        :param include_ts: Also extract packet timestamps
        :param verbose: Output progress information
        :type ifile: ``str``
        :type config: ``DatasetConfig``
        :type storage_path: ``str``
        :type include_ts: ``bool``
        :type verbose: ``bool``
        :return: None
        """

    # n_flows=config.n_stream_flows, n_packets=config.n_stream_packets dynamic_queue=True

    with open(ifile, "rb") as b:

        shb = SectionHeaderBlock(b)
        ids = get_next_block_type(b, shb.byteorder)(b, shb.byteorder)

        current_position = shb.get_block_total_length(shb.byteorder) + ids.get_block_total_length(shb.byteorder)
        i = 0

        analyzer = __get_analyzer(analyzer_type, storage_path, byteorder=shb.byteorder, **kwargs)

        while True:
            # Check for EOF
            if not b.read(1):
                # EOF
                break
            # Move reading cursor one step back
            # seek(offset, whence)
            b.seek(-1, 1)  # Whence = 1 => current position of cursor

            # Extract packet
            packet = get_next_block_type(b, shb.byteorder)(b, shb.byteorder)
            # Update the position in the binary file
            current_position += packet.get_block_total_length(byteorder=shb.byteorder)

            if isinstance(packet, EnhancedPacketBlock):
                if analyzer_type == AnalyzerType.FlowBased:
                    # packet = packet(b, shb.byteorder)
                    #flow_id = get_flow_id(packet.raw_data, NETWORK_BYTE_ORDER, ids.get_linktype(shb.byteorder))
                    keys = get_5_tuple(packet.raw_data, NETWORK_BYTE_ORDER, ids.get_linktype(shb.byteorder))
                    analyzer.add_packet(packet, flow_id=keys)
                else:
                    analyzer.add_packet(packet)
                i += 1
                if verbose:
                    print(i, end="\r")
    analyzer.finish()
    return analyzer


def get_analytics(ifile, analyzer_type, storage_path, verbose=True, **kwargs):
    if determine_file_type(ifile) == "pcap":
        return get_pcap_analytics(ifile, analyzer_type, storage_path, verbose=verbose, **kwargs)
    else:
        return get_pcapng_analytics(ifile, analyzer_type, storage_path, verbose=verbose, **kwargs)
