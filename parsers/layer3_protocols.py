from parsers.protocols import Protocol
from parsers.layer4_protocols import TCP, UDP


class IPv4(Protocol):

    def __init__(self, raw_data, byteorder, *, name="IPv4"):
        
        super().__init__(raw_data, byteorder, name="IPv4")

        # Parse binary data for Ethernet-specific information:
        first_byte = int.from_bytes(raw_data[0:1], byteorder, signed=False)
        self.version = (first_byte & 240) >> 4  # should always be 4
        self.ihl = first_byte & 15              # Internet Header Length
        
        second_byte = int.from_bytes(raw_data[1:2], byteorder, signed=False)
        self.dscp = (second_byte >> 2) & 63     # Differentiated Services Code Point
        self.ecn = second_byte & 3              # Explicit Congestion Notification

        self.total_length = int.from_bytes(raw_data[2:4], byteorder, signed=False)  # Total length
        self.id = int.from_bytes(raw_data[4:6], byteorder, signed=False)            # Identification
        
        flag_frag = int.from_bytes(raw_data[6:8], byteorder, signed=False)
        self.flags = (flag_frag >> 14) & 3      # Flags
        self.frag = flag_frag & 16383           # Fragmentation offset

        self.ttl = int.from_bytes(raw_data[8:9], byteorder, signed=False)           # Time To Live
        self.proto = int.from_bytes(raw_data[9:10], byteorder, signed=False)        # Protocol
        self.chks = int.from_bytes(raw_data[10:12], byteorder, signed=False)        # Header Checksum
        self.src_addr = int.from_bytes(raw_data[12:16], byteorder, signed=False)    # Source address
        self.dst_addr = int.from_bytes(raw_data[16:20], byteorder, signed=False)    # Destination address

        if self.ihl > 5:
            #TODO
            max_range = self.ihl * 4
            self.options = int.from_bytes(raw_data[20:max_range], byteorder, signed=False)
            self.raw_data = raw_data[max_range:]
        else:
            self.options = 0
            self.raw_data = raw_data[20:]


    def __str__(self):
        return """{}: {}, {}, {}, {}
            \rTotal length: {}
            \rID: {}
            \rFlags: {}
            \rFragmentation offset: {}
            \rTTL: {}
            \rProtocol: {}
            \rChecksum: {}
            \rSRC: {}
            \rDST: {}
            \roptions: {}"""\
            .format(self.name, str(self.version),\
            str(self.ihl), str(self.dscp), str(self.ecn),\
            str(self.total_length), str(self.id),\
            str(self.flags), str(self.frag),\
            str(self.ttl), str(self.proto), str(self.chks),\
            str(self.src_addr), str(self.dst_addr),
            str(self.options))
    
    def __find_next_layer_protocol_header(self, byteorder):
        if self.proto == 6:
            # Transmission Control Protocol TCP
            self.__next_layer_protcol = TCP(self.raw_data, byteorder)
        elif self.proto == 17:
            # User Datagram Protocol UDP
            self.__next_layer_protcol = UDP(self.raw_data, byteorder)
        elif self.proto == 1:
            # Internet Control Message Protocol ICMP
            pass
        elif self.proto == 2:
            # Internet Group Management Protocol IGMP
            pass
        elif self.proto == 41:
            # IPc6 encapsulation ENCAP
            pass
        elif self.proto == 89:
            # Open Shortest Path First OSPF
            pass
        elif self.proto == 132:
            # Stream Control Transmission Protocol SCTP
            pass

    def get_next_layer_protocol(self, byteorder):
        self.__find_next_layer_protocol_header(byteorder)
        return self.__next_layer_protcol

    @staticmethod
    def format_address(address):
        return "{}.{}.{}.{}".format((address >> 24) & 255,
            (address >> 16) & 255, (address >> 8) & 255,
            (address >> 0) & 255)
    
