from parsers.protocols import Protocol
from parsers.layer3_protocols import IPv4
from parsers.constants import ARPHRD_IEEE80211_RADIOTAP, ARPHRD_IPGRE, ARPHRD_NETLINK
from exceptions import NotImplementedException


class Ethernet(Protocol):
    """
    Class implementing the Ethernet protcol for layers 1 and 2.
    """
    def __init__(self, raw_data, byteorder, *, name="Ethernet"):

        super().__init__(raw_data, byteorder, name="Ethernet")

        # Parse binary data for Ethernet-specific information:
        self.mac_dst = int.from_bytes(raw_data[0:6], byteorder, signed=False)
        self.mac_src = int.from_bytes(raw_data[6:12], byteorder, signed=False)
        self.ethertype = int.from_bytes(raw_data[12:14], byteorder, signed=False)
        self.raw_data = raw_data[14:]
    
    def __str__(self):
        return "{}: {:06X}-{:06X}| Type: {}".format(self.name, self.mac_dst,
                                                    self.mac_src, str(self.ethertype))

    def __find_next_layer_protocol_header(self, byteorder):
        if self.ethertype == 0x0800:
            self.__next_layer_protcol = IPv4(self.raw_data, byteorder)

    def get_next_layer_protocol(self, byteorder):
        self.__find_next_layer_protocol_header(byteorder)
        return self.__next_layer_protcol


class LinuxCookedCapture(Protocol):
    """
    Class implementing the Linux cooked-mode capture (SLL) pseudo-protocol. This is used when reliable
    link-layer headers are not available. LCC (or SLL) headers are added by libpcap (https://wiki.wireshark.org/SLL).
    """
    def __init__(self, raw_data, byteorder, *, name="LinuxCookedCapture"):
        
        super().__init__(raw_data, byteorder, name="LinuxCookedCapture")

        # Parse binary data for SLL-specific information
        # All information is based on the following source:
        # http://www.tcpdump.org/linktypes/LINKTYPE_LINUX_SLL.html
        # Packet type:
        self.packet_type = int.from_bytes(raw_data[0:2], byteorder, signed=False)

        # ARPHRD_ Type field contains Linux ARPHRD_ value for the link-layer device type
        self.ARPHRD_type = int.from_bytes(raw_data[2:4], byteorder, signed=False)

        # Link-layer address length: Length of the link-layer address of the sender of the packet, can be zero
        self.ll_addr_len = int.from_bytes(raw_data[4:6], byteorder, signed=False)

        # Link-layer address contains link-layer address of the sender of the packet. Relevant length is specified by ll_addr_len.
        # If number address bytes > 8, only first 8 bytes are present. Similarly, if the number of address bytes < 8, the remaining
        # bytes are padded.
        self.ll_addr = int.from_bytes(raw_data[6:14], byteorder, signed=False)

        # Protocol type
        self.protocol_type = int.from_bytes(raw_data[14:16], byteorder, signed=False)
        self.ethertype = self.protocol_type
        self.raw_data = raw_data[16:]
    
    def __str__(self):
        return "{}: {:08X} - {}| Type: {}".format(self.name, self.ll_addr,
                                                  self.ll_addr_len, str(self.protocol_type))

    def __find_next_layer_protocol_header(self, byteorder):
        if self.ARPHRD_type == ARPHRD_NETLINK:
            # Packet data is a LINKTYPE_NETLINK packet (http://www.tcpdump.org/linktypes/LINKTYPE_NETLINK.html)
            raise NotImplementedException()
        elif self.ARPHRD_type == ARPHRD_IPGRE:
            # Protocol type field contains a Generic Routing Encapsulation (GRE) protocol type (https://tools.ietf.org/html/rfc2784)
            raise NotImplementedException()
        elif self.ARPHRD_type == ARPHRD_IEEE80211_RADIOTAP:
            # Ignore ptocol type field, raw_data begins with Radiotap link-layer information (http://www.radiotap.org/)
            # followed by 802.11 header
            raise NotImplementedException()
        else:
            if self.protocol_type == 0x0800:
                self.__next_layer_protcol = IPv4(self.raw_data, byteorder)
            elif self.protocol_type == 0x0001:
                # Frame is Novell 802.3 frame without 802.2 LLC header
                raise NotImplementedException()
            elif self.protocol_type == 0x0003:
                # Mysterious case
                raise NotImplementedException("Mysterious case for SLL/LCC header", self.protocol_type)
            elif self.protocol_type == 0x0004:
                # The frame begins with an 802.2 LLC header
                raise NotImplementedException()
            elif self.protocol_type == 0x000C:
                # Frame is a CAN bus frame
                raise NotImplementedException()
            elif self.protocol_type == 0x000D:
                # Frame is a CAN FD (CAN with Flexible Data-Rate) frame
                raise NotImplementedException()
            else:
                raise NotImplementedException()

    def get_next_layer_protocol(self, byteorder):
        self.__find_next_layer_protocol_header(byteorder)
        return self.__next_layer_protcol