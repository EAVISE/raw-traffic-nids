# LINK-LAYER HEADER TYPE VALUES for PCAP files
# Source: http://www.tcpdump.org/linktypes.html
LINKTYPE_NULL = 0
LINKTYPE_ETHERNET = 1
LINKTYPE_LINUX_SLL= 113 # Linux Cooked Capture (SLL) (http://www.tcpdump.org/linktypes/LINKTYPE_LINUX_SLL.html)

# ARPHDR_ types:
ARPHRD_NETLINK = 824
ARPHRD_IPGRE = 778
ARPHRD_IEEE80211_RADIOTAP = 803

# PCAPNG block sizes
SHBSize = 28 # Section Header Block Size
IDBSize = 20 # Interface Description Block Size
EPBSize = 32 # Enhanced Packet Block

# Network byte order
NETWORK_BYTE_ORDER = "big"

# Opcodes
opt_endofopt = 0
opt_comment = 1
opt_custom_1 = 2988
opt_custom_2 = 2989
opt_custom_3 = 19372
opt_custom_4 = 19373

# Interface Description Block Options
opt_if_name = 2
opt_if_description = 3
opt_if_IPv4addr = 4
opt_if_IPv6addr = 5
opt_if_MACaddr = 6
opt_if_EUIaddr = 7
opt_if_speed = 8
opt_if_tsresol = 9
opt_if_tzone = 10
opt_if_filter = 11
opt_if_os = 12
opt_if_fcslen = 13
opt_if_tsoffset = 14
opt_if_hardware = 15
