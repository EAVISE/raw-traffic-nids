import os
import json
from enums import Dataset, FeatureStrategy
from config import configuration_exists, Config
from dataset_config import get_config_from_file as get_dataset_config_from_file
from exceptions import FeatureStrategyConfigurationConstructionException, NotImplementedException
import help_functions as hf
import numpy as np


def remove_configuration(name, root, verbose=False):
    config_file = os.path.join(root, "config", name + ".json")
    features_root = os.path.join(root, name)

    # Remove the configuration file
    os.remove(config_file)

    # Remove the directory containing all features
    # Only do this if this directory already existed:
    if os.path.isdir(features_root):
        hf.remove_dir(features_root, True, verbose)


def feature_strategy_config_from_path(path, export_mode=False):
    """

    :param path:
    :param export_mode: If True, operate in export mode
    :type export_mode: ``bool``
    :return:
    """

    if export_mode:
        export = path
    else:
        export = None

    with open(path, "r") as f:
        json_dict = json.load(f)
        feature_strategy = json_dict['featurestrategy']
        features_root = json_dict['storage']['features_root']
        name = json_dict['name']



        if feature_strategy == FeatureStrategy.PCCN.name:
            return PCCNConfig(features_root, name=name, use_existing=True, export_mode=export)
        elif feature_strategy == FeatureStrategy.HAST_I.name:
            return HAST1Config(features_root, name=name, use_existing=True, export_mode=export)
        elif feature_strategy == FeatureStrategy.HAST_II.name:
            return HAST2Config(features_root, name=name, use_existing=True, export_mode=export)
        elif feature_strategy == FeatureStrategy.SESSIONBASED.name:
            return SessionBasedConfig(features_root, name=name, use_existing=True, export_mode=export)
        elif feature_strategy == FeatureStrategy.SIMPLE_VECTORS.name:
            return SimpleVectorsConfig(features_root, name=name, use_existing=True, export_mode=export)
        else:
            raise NotImplementedException("Please implement this functionality")


def change_configuration_root(config_file, new_root):
    """
    Change the configuration root of the feature strategy inside the configuration file.

    :param config_file: Path to the configuration file to be edited
    :param new_root: New root that needs to be used inside the configuration file
    :return: None
    """
    with open(config_file, 'r') as f:
        data_dict = json.load(f)

    data_dict['storage']['features_root'] = os.path.join(new_root, 'features')
    data_dict['storage']['datasets_root'] = os.path.join(new_root, 'datasets')
    data_dict['storage']['config_root'] = os.path.join(new_root, 'features', 'config', data_dict['name'] + '.json')

    with open(config_file, 'w') as f:
        json.dump(data_dict, f)


class FeatureStrategyConfig(Config):
    """
    Base FeatureStrategyConfig class, from which specific classes can inherit.
    """

    def __init__(self, features_root, dataset_config=None, featurestrategy=None, use_existing=True,
                 name=None, export_mode=None, verbose=False, **kwargs):
        """
        This constructor can be used in three main different ways:

        Use an existing configuration with a fixed name:
        config = FeatureStrategyConfig(features_root, name)

        Use an existing configuration with a name derived from dataset + feature strategy:
        config = FeatureStrategyConfig(features_root, dataset, featurestrategy)

        Give all necessary information to create a new configuration. If applicable, an existing configuration will still be used.
        If the configuration must be created anew, consider setting use_existing as False.
        config = FeatureStrategyConfig(features_root, datasets_root, dataset, featurestrategy)

        Keep in mind that kwargs can be used to further finetune a new configuration.

        Params:
        features_root(str): Root directory the different generated features are be stored in
        datasets_root(str): Root directory the different (processed) datasets are stored in
        dataset(Dataset): Dataset to generate features from
        featurestrategy(FeatureStrategy): Strategy to generate features from a dataset
        use_existing(bool): If True, use an existing configuration if possible. If False, always remove overlapping preexisting configurations and create a new one.
        name(str): (custom) name of the targeted existing configuration, or of the target new configuration
        verbose(bool): If True, report on certain things during processing
        kwargs(dict): Optional parameters to finetune configurations, listed at ..
        """

        # Constructor can be used as:
        # FeatureStrategyConfig(features_root, name)

        # First, define the name of this configuration
        if not (name is None):
            self.name = name
        else:
            if featurestrategy is None:
                raise FeatureStrategyConfigurationConstructionException(
                    "Missing a feature strategy. Please specify a feature strategy.")
            if dataset_config.dataset is None:
                raise FeatureStrategyConfigurationConstructionException("Missing a dataset. Please specify a dataset.")

            self.name = featurestrategy.name + "-" + dataset_config.dataset

        # Features root: Root directory everything regarding the features will be stored into
        self.features_root = features_root

        if export_mode is not None:
            self.config_root = export_mode
            self.export_mode = True
            self._get_existing_config()
            return

        # Export mode: If True, all paths must be considered relative from the config files location, rather than
        # absolute
        self.export_mode = False

        # Root the configuration file will be stored at
        self.config_root = os.path.join(self.features_root, "config", self.name + ".json")

        # Check if a configuration for this name already exists
        if configuration_exists(self.name, self.features_root):
            if use_existing:
                self._get_existing_config()
                return
            else:
                remove_configuration(self.name, self.features_root, verbose=verbose)

        # If we did not find a configuration for the given name and root,
        # but if we only have a name and root provided,
        # we cannot continue execution, as we lack vital information.
        if use_existing and ((dataset_config is None) or (featurestrategy is None)):
            raise FeatureStrategyConfigurationConstructionException(
                "Insufficient information to create configuration, please provide dataset, feature strategy and dataset"
                "root")

        # If no configuration exists, create a new configuration
        # First extract vital information
        #self.dataset = dataset.name
        self.featurestrategy = featurestrategy.name

        #self.datasets_root = datasets_root

        # Get the dataset configuration object
        self.dataset_config = dataset_config
        self.days = self.dataset_config.days
        #self.days = DatasetConfig(Dataset[self.dataset], self.datasets_root).days

        # Then create the new configuration
        self._initial_configuration(**kwargs)

    def _get_config_dict(self):
        config = {}

        # config['dataset'] = self.dataset
        config['featurestrategy'] = self.featurestrategy
        config['name'] = self.name
        config['days'] = self.days

        config['progress'] = {
            'features_generated': self.features_generated
        }

        config['storage'] = {
            'datasets_root': self.dataset_config.config_root,
            'features_root': self.features_root,
            'config_root': self.config_root
        }

        config['compress'] = self.compress

        return config

    def _update_from_existing_config(self, config):
        """
        Update the data members of the object by reading their correct state
        from the stored configuration.

        Params:
        config(dict): Stored configuration dictionary
        """
        # self.dataset = config['dataset']
        self.featurestrategy = config['featurestrategy']
        self.name = config['name']
        self.days = config['days']
        self.features_generated = config['progress']['features_generated']

        #self.datasets_root =
        self.features_root = config['storage']['features_root']
        #self.config_root = config['storage']['config_root']

        self.compress = config['compress']

        if self.export_mode:
            export_dir = os.path.split(os.path.realpath(self.config_root))[0]
            dataset_config_path = os.path.join(export_dir, config['storage']['datasets_root'])
            self.dataset_config = get_dataset_config_from_file(dataset_config_path, self.export_mode)
        else:
            self.dataset_config = get_dataset_config_from_file(config['storage']['datasets_root'], self.export_mode)

    def _initial_configuration(self, **kwargs):
        # Processing progress
        self.features_generated = {}
        for day in self.days:
            self.features_generated[day] = False

        self.compress = kwargs.get('compress', False)
        # Store the current, initialized configuration
        self._store_configuration()

    def get_sample_shape(self):
        """
        Get the shape of a feature sample

        :return: Tuple with dimensions
        :rtype: ``tuple`` of ``int``
        """
        return 1,


class PCCNConfig(FeatureStrategyConfig):
    """
    Configuration for features generated following the PCCN approach.
    """

    def __init__(self, features_root, dataset_config=None, use_existing=True,
                 name=None, verbose=False, **kwargs):
        """
        Constructor for a PCCNConfig object
        """
        super().__init__(features_root=features_root, dataset_config=dataset_config,
                         featurestrategy=FeatureStrategy.PCCN,
                         use_existing=use_existing, name=name, verbose=verbose, **kwargs)

    def _initial_configuration(self, **kwargs):
        # Feature generation parameters
        self.header_min = kwargs.get('header_min', 0)
        self.header_max = kwargs.get('header_max', 100)
        self.payload_min = kwargs.get('payload_min', 100)
        self.payload_max = kwargs.get('payload_max', 200)
        self.hepa_min = kwargs.get('hepa_min', 0)
        self.hepa_max = kwargs.get('hepa_max', 192)
        self.num_packets = kwargs.get('num_packets', 5)

        self.start = min(self.header_min, self.payload_min, self.hepa_min)
        self.stop = max(self.header_max, self.payload_max, self.hepa_max)

        # Storage information:
        # Size of batches of numeric information to store
        self.batch_size = kwargs.get('batch_size', 10000)
        # If True, generate numeric features rather than text files
        self.numeric_features = kwargs.get('numeric_features', True)

        # Processing configuration
        self.use_c_libs = kwargs.get('use_c_libs', False)

        # Kind of features to be used later on:
        self.use_header = True
        self.use_payload = False
        self.use_hepa = False

        # Save this configuration
        super()._initial_configuration(**kwargs)
        # self._store_configuration()

    def _get_config_dict(self):
        config = super()._get_config_dict()

        config['use_header'] = self.use_header
        config['use_payload'] = self.use_payload
        config['use_hepa'] = self.use_hepa

        config['params'] = {
            'header_min': self.header_min,
            'header_max': self.header_max,
            'payload_min': self.payload_min,
            'payload_max': self.payload_max,
            'hepa_min': self.hepa_min,
            'hepa_max': self.hepa_max,
            'start': self.start,
            'stop': self.stop,
            'num_packets': self.num_packets
        }
        config['storage']['batch_size'] = self.batch_size
        config['storage']['numeric_features'] = self.numeric_features

        config['processing'] = {
            'use_c_libs': self.use_c_libs,
        }
        return config

    def _update_from_existing_config(self, config):
        super()._update_from_existing_config(config)

        self.header_min = config['params']['header_min']
        self.header_max = config['params']['header_max']
        self.payload_min = config['params']['payload_min']
        self.payload_max = config['params']['payload_max']
        self.hepa_min = config['params']['hepa_min']
        self.hepa_max = config['params']['hepa_max']
        self.num_packets = config['params']['num_packets']
        self.start = config['params']['start']
        self.stop = config['params']['stop']

        self.batch_size = config['storage']['batch_size']
        self.numeric_features = config['storage']['numeric_features']

        self.use_c_libs = config['processing']['use_c_libs']

        self.use_header = config['use_header']
        self.use_payload = config['use_payload']
        self.use_hepa = config['use_hepa']

    def set_feature_type(self, feature_type):
        if feature_type == "header":
            self.use_header = True
            self.use_payload = False
            self.use_hepa = False
        elif feature_type == "payload":
            self.use_header = False
            self.use_payload = True
            self.use_hepa = False
        elif feature_type == "hepa":
            self.use_header = False
            self.use_payload = False
            self.use_hepa = True

    def get_feature_type(self):
        if self.use_header:
            return 'header'
        elif self.use_payload:
            return 'payload'
        else:
            return 'hepa'

    def get_sample_shape(self):
        """

        :return:
        """
        if self.use_header:
            size = (self.header_max - self.header_min) // 2 * self.num_packets + self.num_packets + 1
            size = int(np.sqrt(size))
        elif self.use_payload:
            size = (self.payload_max - self.payload_min) // 2 * self.num_packets + self.num_packets + 1
            size = int(np.sqrt(size))
        else:
            size = (self.hepa_max - self.hepa_min) // 2 * self.num_packets + self.num_packets - 1
            size = int(np.sqrt(size))
        return size, size


class HAST1Config(FeatureStrategyConfig):
    """
    Configuration for features generated following the PCCN approach.
    """

    def __init__(self, features_root, dataset_config=None, use_existing=True,
                 name=None, verbose=False, **kwargs):
        """
        Constructor for a PCCNConfig object
        """
        self.axis = 0
        super().__init__(features_root=features_root, dataset_config=dataset_config,
                         featurestrategy=FeatureStrategy.HAST_I,
                         use_existing=use_existing, name=name, verbose=verbose, **kwargs)

    def _initial_configuration(self, **kwargs):
        # Feature generation parameters
        # n is the number of bytes from the flow to include 
        self.n = kwargs.get('n', 600)
        # m is the size of the one-hot encoded feature vector (typically 256, for each value of a byte)
        self.m = kwargs.get('m', 256)

        # Set the batch size, the number of of subsequent flow images that will be stored in one file
        self.batch_size = kwargs.get('batch_size', 10000)

        # Save this configuration
        super()._initial_configuration(**kwargs)

    def _get_config_dict(self):
        config = super()._get_config_dict()

        config['params'] = {
            'n': self.n,
            'm': self.m,
        }

        config['batch_size'] = self.batch_size

        return config

    def _update_from_existing_config(self, config):
        super()._update_from_existing_config(config)

        self.n = config['params']['n']
        self.m = config['params']['m']

        self.batch_size = config['batch_size']

    def decompress_features(self, array):
        """
        Decompress the provided n x 1 vector into a m x n vector.
        :param array: Input n x 1 vector to be decompressed
        :return: Decompressed 256 x n array
        """
        return hf.extract_hast_ohe_matrix(array)

    def get_compress_axis(self):
        return self.axis

    def get_sample_shape(self):
        return self.m, self.n


class HAST2Config(FeatureStrategyConfig):
    """
    Configuration for features generated following the HAST-II-IDS approach.
    """

    def __init__(self, features_root, dataset_config=None, use_existing=True,
                 name=None, verbose=False, **kwargs):
        """
        Constructor for a PCCNConfig object
        """
        self.axis = 1
        self.use_all_flow_packets = False
        super().__init__(features_root=features_root, dataset_config=dataset_config,
                         featurestrategy=FeatureStrategy.HAST_II,
                         use_existing=use_existing, name=name, verbose=verbose, **kwargs)

    def _initial_configuration(self, **kwargs):
        # Feature generation parameters
        # The number of flow packets to include
        self.r = kwargs.get('r', 6)
        # The number of bytes per packet to include
        self.q = kwargs.get('q', 100)
        # p is the size of the one-hot encoded feature vector (typically 256, for each value of a byte)
        self.p = kwargs.get('p', 256)

        self.batch_size = kwargs.get('batch_size', 10000)

        # Save this configuration
        super()._initial_configuration(**kwargs)

    def _get_config_dict(self):
        config = super()._get_config_dict()

        config['params'] = {
            'r': self.r,
            'q': self.q,
            'p': self.p,
        }

        config['batch_size'] = self.batch_size

        return config

    def _update_from_existing_config(self, config):
        super()._update_from_existing_config(config)

        self.r = config['params']['r']
        self.q = config['params']['q']
        self.p = config['params']['p']

        self.batch_size = config['batch_size']

    def get_compress_axis(self):
        return self.axis

    def decompress_features(self, array):
        """
        Decompress the provided r x q matrix into a r x p x q matrix.
        :param array: Input r x q matrix to be decompressed
        :return: Decompressed r x 256 x q array
        """
        output = np.zeros((self.r, self.p, self.q))
        for i in range(self.r):
            output[i] = hf.extract_hast_ohe_matrix(array[i])
        return output

    def get_sample_shape(self):
        return self.r, self.p, self.q

    def use_all_flow_packets(self):
        """
        Inquire wheter all packets in a flow must be used in the feature extraction process.

        :return: True if all packets need indeed to be used during extraction.
        """
        return self.use_all_flow_packets


class SimpleVectorsConfig(FeatureStrategyConfig):
    """
    Configuration to create Simple Vectors features.
    """
    def __init__(self, features_root, dataset_config=None, use_existing=True,
                 name=None, verbose=False, **kwargs):
        """
        Constructor for a SimpleVectorsConfig object
        """
        super().__init__(features_root=features_root, dataset_config=dataset_config,
                         featurestrategy=FeatureStrategy.SIMPLE_VECTORS,
                         use_existing=use_existing, name=name, verbose=verbose, **kwargs)

    def _initial_configuration(self, **kwargs):
        # Feature generation parameters
        # The number of flow packets to include
        self.n_packets = kwargs.get('n_packets', 6)
        # The number of bytes per packet to include
        self.n_bytes = kwargs.get('n_bytes', 100)

        # use_all_packets: For a flow, use all packets, divided into n_packets portions
        self.use_all_packets = kwargs.get('use_all_packets', False)
        # Padding to use when encountering insufficient bytes of packets
        # Default: Padding with zero-bytes
        self.padding = kwargs.get('padding', 0)
        # Batch size: Number of items that can be handled at once
        self.batch_size = kwargs.get('batch_size', 100000)

        # Save this configuration
        super()._initial_configuration(**kwargs)

    def _get_config_dict(self):
        config = super()._get_config_dict()

        config['params'] = {
            'n_packets': self.n_packets,
            'n_bytes': self.n_bytes,
        }
        config['use_all_packets'] = self.use_all_packets
        config['padding'] = self.padding
        config['batch_size'] = self.batch_size

        return config

    def _update_from_existing_config(self, config):
        super()._update_from_existing_config(config)

        self.n_packets = config['params']['n_packets']
        self.n_bytes = config['params']['n_bytes']

        self.use_all_packets = config['use_all_packets']
        self.padding = config['padding']
        self.batch_size = config['batch_size']

    def get_sample_shape(self):
        return self.n_packets, self.n_bytes

    def use_all_flow_packets(self):
        """
        Inquire wheter all packets in a flow must be used in the feature extraction process.

        :return: True if all packets need indeed to be used during extraction.
        """
        return self.use_all_flow_packets


class SessionBasedConfig(FeatureStrategyConfig):
    """
        Configuration to create Simple Vectors features.
        """

    def __init__(self, features_root, dataset_config=None, use_existing=True,
                 name=None, verbose=False, **kwargs):
        """
        Constructor for a SimpleVectorsConfig object
        """
        super().__init__(features_root=features_root, dataset_config=dataset_config,
                         featurestrategy=FeatureStrategy.SESSIONBASED,
                         use_existing=use_existing, name=name, verbose=verbose, **kwargs)

    def _initial_configuration(self, **kwargs):
        # Feature generation parameters
        # The number of features
        self.num_feats = kwargs.get('num_feats', 1000)

        self.include_icmp = kwargs.get('include_icmp', False)

        # Number of features reserved for session metadata
        if self.include_icmp:
            self.num_metadata_feats = 17
        else:
            # Without ICMP, we disregard one protocol field, and the type and value fields of ICMP
            self.num_metadata_feats = 14
        # Total number of payload bytes to be used
        self.payl_bytes = self.num_feats - self.num_metadata_feats


        # Padding to use when encountering insufficient bytes of packets
        # Default: Padding with zero-bytes
        self.padding = kwargs.get('padding', 0)
        # Batch size: Number of items that can be handled at once
        self.batch_size = kwargs.get('batch_size', 10000)

        # Save this configuration
        super()._initial_configuration(**kwargs)

    def _get_config_dict(self):
        config = super()._get_config_dict()

        config['params'] = {
            'num_feats': self.num_feats,
            'num_metadata_feats': self.num_metadata_feats,
            'payl_bytes': self.payl_bytes,
        }
        config['include_icmp'] = self.include_icmp
        config['padding'] = self.padding
        config['batch_size'] = self.batch_size

        return config

    def _update_from_existing_config(self, config):
        super()._update_from_existing_config(config)

        self.num_feats = config['params']['num_feats']
        # Number of features reserved for session metadata
        self.num_metadata_feats = config['params']['num_metadata_feats']
        # Total number of payload bytes to be used
        self.payl_bytes = config['params']['payl_bytes']

        self.include_icmp = config['include_icmp']

        self.padding = config['padding']
        self.batch_size = config['batch_size']

    def get_sample_shape(self):
        return self.num_feats,
