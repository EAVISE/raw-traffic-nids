import os
import shutil
import json
import re

from help_functions import mkdir, remove_dir
from dataset_config import change_configuration_root as ds_change_configuration_root
from features_config import change_configuration_root as feat_change_configuration_root
from ml_config import change_feat_configuration_root as ml_feat_change_configuration_root
from ml_config import change_experiment_configuration_root as ml_experiment_change_configuration_root


def export_memmaps_dataset(ml_features_config, target_directory):
    """
    Export generated memory mapped feature files, as well as corresponding configuration files, as a directory
    containing all relevant information.

    :param ml_features_config:
    :return:
    """

    # First prepare config files for export
    features_config = ml_features_config.features_config

    dataset_config_path = features_config.dataset_config.config_root
    features_config_path = features_config.config_root
    ml_config_path = ml_features_config.config_root

    # Change paths to relative, correct, paths in config files
    dataset_config_file_name = 'dataset_info.json'
    features_config_file_name = 'features_info.json'
    ml_config_file_name = 'ml_info.json'

    dataset_config_dict = __get_dataset_config_for_data_export(dataset_config_path,
                                                               future_name=dataset_config_file_name)
    features_config_dict = __get_features_config_for_data_export(features_config_path,
                                                                 future_name=features_config_file_name,
                                                                 future_dataset_name=dataset_config_file_name)
    ml_config_dict = __get_ml_config_for_data_export(ml_config_path,
                                                     future_name=ml_config_file_name,
                                                     future_features_name=features_config_file_name)

    # Ensure the target directory exists:
    mkdir(target_directory)

    # Export the config files to the target location
    with open(os.path.join(target_directory, dataset_config_file_name), 'w') as f:
        json.dump(dataset_config_dict, f)

    with open(os.path.join(target_directory, features_config_file_name), 'w') as f:
        json.dump(features_config_dict, f)

    with open(os.path.join(target_directory, ml_config_file_name), 'w') as f:
        json.dump(ml_config_dict, f)

    # Export the memory mapped features files
    memmaps_dir = os.path.join(target_directory, 'memmaps')

    mkdir(memmaps_dir)
    for usage in ['train', 'valid', 'test']:
        usage_dir = os.path.join(memmaps_dir, usage)
        mkdir(usage_dir)

        for label, metadata in ml_features_config.memmap_metadata[usage].items():
            shutil.copy(metadata[0], os.path.join(usage_dir, label + '.npy'))


def __get_ml_config_for_data_export(features_config_file, future_name='features_info',
                                    future_features_name='dataset_info'):
    """

    :param features_config_file:
    :rtype: ``dict``
    :return:
    """

    if (len(future_name) > 5) and (future_name[-5:] == '.json'):
        future_name = future_name[:-5]

    if (len(future_features_name) > 4) and (future_features_name[-5:] == '.json'):
        future_features_name = future_features_name[:-5]

    with open(features_config_file, 'r') as f:
        ml_config_dict = json.load(f)

        ml_config_dict['name'] = future_name
        ml_config_dict['root'] = '.'
        ml_config_dict['config_root'] = os.path.join('.', future_name + '.json')
        ml_config_dict['features_config'] = future_features_name + '.json'

        for usage in ['train', 'valid', 'test']:
            for label, metadata_instance in ml_config_dict['memmap_metadata'][usage].items():

                new_path = os.path.join('.', 'memmaps', usage, label + '.npy')
                ml_config_dict['memmap_metadata'][usage][label] = [new_path, metadata_instance[1]]

    return ml_config_dict


def __get_features_config_for_data_export(features_config_file, future_name='features_info',
                                          future_dataset_name='dataset_info'):
    """

    :param features_config_file:
    :rtype: ``dict``
    :return:
    """

    if (len(future_name) > 5) and (future_name[-5:] == '.json'):
        future_name = future_name[:-5]

    if (len(future_dataset_name) > 4) and (future_dataset_name[-5:] == '.json'):
        future_dataset_name = future_dataset_name[:-5]

    with open(features_config_file, 'r') as f:
        features_config_dict = json.load(f)

        features_config_dict['name'] = future_name
        features_config_dict['storage']['config_root'] = os.path.join('.', future_name + '.json')
        features_config_dict['storage']['datasets_root'] = future_dataset_name + '.json'

    return features_config_dict


def __get_dataset_config_for_data_export(dataset_config_file, future_name='dataset_info'):
    """

    :param dataset_config_file:
    :rtype: ``dict``
    :return:
    """

    if (len(future_name) > 5) and (future_name[-5:] == '.json'):
        future_name = future_name[:-5]

    with open(dataset_config_file, 'r') as f:
        dataset_config_dict = json.load(f)

        # For dataset config files, only the config root is relevant
        dataset_config_dict['root'] = '.'
        dataset_config_dict['name'] = future_name
        dataset_config_dict['storage']['config_root'] = os.path.join('.', future_name + '.json')

    return dataset_config_dict


def change_models_config_root(storage_root, target_root):
    """


    :param storage_root:
    :param target_root:
    :return:
    """
    for config_file in os.listdir(storage_root):
        ml_experiment_change_configuration_root(os.path.join(storage_root, config_file), target_root)


def change_configuration_internal_root(storage_root, target_root=None):
    """
    Function to change the root stored inside configuration files
    :return:
    """

    if target_root is None:
        target_root = storage_root

    for category in ['datasets', 'features', 'ml_data']:
        if category == 'ml_data':
            # ML features configuration
            config_dir = os.path.join(storage_root, category, 'features_config')
            for config_file in os.listdir(config_dir):
                ml_feat_change_configuration_root(os.path.join(config_dir, config_file), target_root)

            # ML models configuration (experiment configuration)
            config_dir = os.path.join(storage_root, category, 'models_config')
            for config_file in os.listdir(config_dir):
                ml_experiment_change_configuration_root(os.path.join(config_dir, config_file), target_root)

        else:
            config_dir = os.path.join(storage_root, category, 'config')
            for config_file in os.listdir(config_dir):
                config_file = os.path.join(config_dir, config_file)
                if category == 'datasets':
                    ds_change_configuration_root(config_file, target_root)
                else:
                    feat_change_configuration_root(config_file, target_root)


def export_relevant_data(old_root, new_root, clean=True, skip_existing=False, verbose=True):
    """
    This function exports relevant datasets as well as configuration files to a new location for training and testing
    without additional dataset generation.

    :param old_root: Path to the original root directory of the project
    :param new_root: Path to the new destination root directory
    :param clean: If True, all dataset files will be copied to overwrite any previously copied data
    :param skip_existing: If True, existing entities will not be copied
    :param verbose: If True, verbose output will describe the progress of the export
    :type old_root: ``str``
    :type new_root: ``str``
    :type clean: ``bool``print('transforming', item.shape)
    :type skip_existing: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    if verbose:
        print('Exporting relevant data from', old_root, 'to', new_root)
        print('Exporting dataset configuration data')
    _export_dataset_data(old_root, new_root, skip_existing=skip_existing, verbose=verbose)

    if verbose:
        print('Exporting feature extraction strategy configuration data')
    _export_features_data(old_root, new_root, skip_existing=skip_existing, verbose=verbose)

    if verbose:
        print('Exporting machine learning configuration and datasets')
    _export_ml_data(old_root, new_root, clean_copy=clean, skip_existing=skip_existing, verbose=verbose)


def _export_dataset_data(old_root, new_root, skip_existing=False, verbose=True):

    old_datasets = os.path.join(old_root, "datasets")
    old_datasets_config = os.path.join(old_datasets, "config")

    new_datasets = os.path.join(new_root, "datasets")
    new_datasets_config = os.path.join(new_datasets, "config")

    # Step 1: Create datasets dir and config dir
    mkdir(new_datasets)
    mkdir(new_datasets_config)

    # Step 2: Copy dataset data
    for config_file in os.listdir(old_datasets_config):
        # Copy the file
        old_file = os.path.join(old_datasets_config, config_file)
        target_file = os.path.join(new_datasets_config, config_file)

        if skip_existing and os.path.isfile(target_file):
            continue

        if verbose:
            print('Copying', old_file, 'to', target_file)

        shutil.copy(old_file, target_file)
        # Change path specification in the configuration file
        ds_change_configuration_root(os.path.abspath(target_file), os.path.abspath(new_root))


def _export_features_data(old_root, new_root, skip_existing=False, verbose=True):
    old_features = os.path.join(old_root, "features")
    old_features_config = os.path.join(old_features, "config")

    new_features = os.path.join(new_root, "features")
    new_features_config = os.path.join(new_features, "config")

    # Step 1: Create features dir and config dir
    mkdir(new_features)
    mkdir(new_features_config)

    # Step 2: Copy dataset data
    for config_file in os.listdir(old_features_config):
        # Copy the file
        old_file = os.path.join(old_features_config, config_file)
        target_file = os.path.join(new_features_config, config_file)
        shutil.copy(old_file, target_file)

        if skip_existing and os.path.isfile(target_file):
            continue

        if verbose:
            print('Copying', old_file, 'to', target_file)

        shutil.copy(old_file, target_file)
        # Change path specification in the configuration file
        feat_change_configuration_root(os.path.abspath(target_file), os.path.abspath(new_root))


def _export_ml_data(old_root, new_root, clean_copy=True, skip_existing=False, verbose=True):
    old_ml_data = os.path.join(old_root, "ml_data")
    new_ml_data = os.path.join(new_root, "ml_data")

    # If clean_copy = False, just copy the existing directory
    if not clean_copy:
        if os.path.isdir(new_ml_data):
            remove_dir(new_ml_data)
        shutil.copytree(old_ml_data, new_ml_data)
    else:

        old_ml_features_config = os.path.join(old_ml_data, "features_config")
        old_ml_models_config = os.path.join(old_ml_data, "models_config")
        old_ml_data_models = os.path.join(old_ml_data, "models")
        old_ml_data_features = os.path.join(old_ml_data, "memmaps")
        old_ml_data_results = os.path.join(old_ml_data, "results")
        old_ml_data_results_text = os.path.join(old_ml_data_results, 'text')
        old_ml_data_results_json = os.path.join(old_ml_data_results, 'json')

        new_ml_features_config = os.path.join(new_ml_data, "features_config")
        new_ml_models_config = os.path.join(new_ml_data, "models_config")
        new_ml_data_models = os.path.join(new_ml_data, "models")
        new_ml_data_features = os.path.join(new_ml_data, "memmaps")
        new_ml_data_results = os.path.join(new_ml_data, "results")
        new_ml_data_results_text = os.path.join(new_ml_data_results, 'text')
        new_ml_data_results_json = os.path.join(new_ml_data_results, 'json')

        mkdir(new_ml_data)
        mkdir(new_ml_features_config)
        mkdir(new_ml_models_config)
        mkdir(new_ml_data_features)
        mkdir(new_ml_data_models)
        mkdir(new_ml_data_results)
        mkdir(new_ml_data_results_text)
        mkdir(new_ml_data_results_json)

        # First copy the actual machine learning dataset files
        for ml_feat_config_file in os.listdir(old_ml_features_config):
            # Copy the file
            old_file = os.path.join(old_ml_features_config, ml_feat_config_file)
            target_file = os.path.join(new_ml_features_config, ml_feat_config_file)

            if skip_existing and os.path.isfile(target_file):
                continue

            if verbose:
                print('Copying', old_file, 'to', target_file)

            shutil.copy(old_file, target_file)
            ml_feat_change_configuration_root(os.path.abspath(target_file), os.path.abspath(new_root))

        # Then copy the datasets:
        for memmap_dir in os.listdir(old_ml_data_features):
            old_dir = os.path.join(old_ml_data_features, memmap_dir)
            new_dir = os.path.join(new_ml_data_features, memmap_dir)

            if skip_existing and os.path.isfile(new_dir):
                continue

            if os.path.isdir(new_dir):
                remove_dir(new_dir)

            if verbose:
                print('Copying', old_dir, 'to', new_dir)

            shutil.copytree(old_dir, new_dir)
