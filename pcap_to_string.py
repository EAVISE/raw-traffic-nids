"""
This module is used to extract packets from PCAP(NG) files and write them as strings in text files. This allows for
their use in prototyping or in the testing of specific algorithms.
"""

from parsers.flow_parsing import packet_file_to_raw_packets


def packet_capture_file_to_strings(pcap_filepath, output_filepath, bran=0, eran=-1, batch_size=1e5,
                                   include_ts=False, verbose=True):
    """
    Read provided pcap(ng) file and extract requested packets.
    :param pcap_filepath: Path to the input file, either PCAP or PCAPNG. Extension does not matter, the code will read
    the file and chooses whether it is pcap or pcapng.
    :param output_filepath: Path to the output file WITHOUT EXTRENSION. The selected batchsize might result in multple
    files being created.
    :param bran: Begin of the range of packets to extract. Default is zero.
    :param eran: End of the range of packets to extract. Default is all packets (= -1)
    :param batch_size: Number of packets to store at once in memory. Will determine how many out files will be created.
    Default is 100,000.
    :param include_ts: If True, PCAP timestamps will be included in the output file
    :param verbose: Enables or disables verbosity of the program.
    :return: None
    """
    packet_file_to_raw_packets(pcap_filepath, output_filepath,
                               bran=bran, eran=eran, batch_size=100000, include_ts=include_ts, verbose=verbose)


if __name__ == '__main__':
    ifile = '/media/laurens/Preprocessed datasets/Raw data/datasets/CICIDS2017/friday/pcap/Friday-WorkingHours.pcap'
    ofile = '/home/laurens/Experiment/Raw packet strings/'
    begin = 12345
    end = 12345 + 10
    bs = 1e5
    packet_capture_file_to_strings(ifile, ofile, begin, end, bs, False, True)
