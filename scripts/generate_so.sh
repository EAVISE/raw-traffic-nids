#!/usr/bin/env bash

CODE_DIR="c_code/src"
IDIR="c_code/include"
DIR="custom_libraries"

if [ ! -d "$DIR" ]; then
	mkdir "$DIR"
fi

cc -fPIC -shared -o "${DIR}/lib_pcap_parser.so" "${CODE_DIR}/pcap_parser.c" "${CODE_DIR}/hash_tree.c" "${CODE_DIR}/help_functions.c" "-I${IDIR}" -lpcap
cc -fPIC -shared -o "${DIR}/lib_pccn_features.so" "${CODE_DIR}/pccn_features.c" "-I${IDIR}"
