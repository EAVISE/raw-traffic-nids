#!/usr/bin/env bash
days=("monday" "tuesday" "wednesday" "thursday" "friday")

week1=("https://archive.ll.mit.edu/ideval/data/1998/training/week1/monday.tar" "https://archive.ll.mit.edu/ideval/data/1998/training/week1/tuesday.tar" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week1/wednesday.tar" "https://archive.ll.mit.edu/ideval/data/1998/training/week1/thursday.tar" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week1/friday.tar")

week2=("https://archive.ll.mit.edu/ideval/data/1998/training/week2/monday.tar" "https://archive.ll.mit.edu/ideval/data/1998/training/week2/tuesday.tar" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week2/wednesday.tar" "https://archive.ll.mit.edu/ideval/data/1998/training/week2/thursday.tar" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week2/friday.tar")

# Starting from week 3, suddenly two seperate downloads are needed

week3_tcpdump=("https://archive.ll.mit.edu/ideval/data/1998/training/week3/monday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week3/tuesday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week3/wednesday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week3/thursday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week3/friday/tcpdump.gz")

week3_list=("https://archive.ll.mit.edu/ideval/data/1998/training/week3/monday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week3/tuesday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week3/wednesday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week3/thursday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week3/friday/tcpdump.list.gz")

week4_tcpdump=("" "https://archive.ll.mit.edu/ideval/data/1998/training/week4/tuesday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week4/wednesday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week4/thursday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week4/friday/tcpdump.gz")

week4_list=("https://archive.ll.mit.edu/ideval/data/1998/training/week4/monday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week4/tuesday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week4/wednesday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week4/thursday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week4/friday/tcpdump.list.gz")

week5_tcpdump=("https://archive.ll.mit.edu/ideval/data/1998/training/week5/monday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week5/tuesday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week5/wednesday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week5/thursday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week5/friday/tcpdump.gz")

week5_list=("https://archive.ll.mit.edu/ideval/data/1998/training/week5/monday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week5/tuesday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week5/wednesday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week5/thursday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week5/friday/tcpdump.list.gz")

week6_tcpdump=("https://archive.ll.mit.edu/ideval/data/1998/training/week6/monday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week6/tuesday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week6/wednesday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week6/thursday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week6/friday/tcpdump.gz")

week6_list=("https://archive.ll.mit.edu/ideval/data/1998/training/week6/monday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week6/tuesday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week6/wednesday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week6/thursday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week6/friday/tcpdump.list.gz")

week7_tcpdump=("https://archive.ll.mit.edu/ideval/data/1998/training/week7/monday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week7/tuesday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week7/wednesday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week7/thursday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week7/friday/tcpdump.gz")

week7_list=("https://archive.ll.mit.edu/ideval/data/1998/training/week7/monday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week7/tuesday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week7/wednesday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/training/week7/thursday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/training/week7/friday/tcpdump.list.gz")

test1_tcpdump=("https://archive.ll.mit.edu/ideval/data/1998/testing/week1/monday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/testing/week1/tuesday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/testing/week1/wednesday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/testing/week1/thursday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/testing/week1/friday/tcpdump.gz")

test1_list=("https://archive.ll.mit.edu/ideval/data/1998/testing/week1/monday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/testing/week1/tuesday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/testing/week1/wednesday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/testing/week1/thursday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/testing/week1/friday/tcpdump.list.gz")

test2_tcpdump=("https://archive.ll.mit.edu/ideval/data/1998/testing/week2/monday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/testing/week2/tuesday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/testing/week2/wednesday/tcpdump.gz" "https://archive.ll.mit.edu/ideval/data/1998/testing/week2/thursday/tcpdump.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/testing/week2/friday/tcpdump.gz")

test2_list=("https://archive.ll.mit.edu/ideval/data/1998/testing/week2/monday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/testing/week2/tuesday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/testing/week2/wednesday/tcpdump.list.gz" "https://archive.ll.mit.edu/ideval/data/1998/testing/week2/thursday/tcpdump.list.gz" \
"https://archive.ll.mit.edu/ideval/data/1998/testing/week2/friday/tcpdump.list.gz")

DIR="testdir"
if [ ! -d "$DIR" ]; then
	mkdir "$DIR"
fi

#for i in 0 1
#do
#	day=${days[$i]}
#	url=${week1[$i]}
#	wget "$url" "${day}.tar" --no-check-certificate
#done

get_day () {
	day=$1
	case $day in
	0)
		echo "monday"
		;;
	1)
		echo "tuesday"
		;;
	2)
		echo "wednesday"
		;;
	3)
		echo "thursday"
		;;
	4)
		echo "friday"
		;;
	esac
}

get_url_for_week_and_day () {
	week=$1
	day=$2
	#echo "week= ${week} and day= ${day}"
	#echo "${week1[$day]}"
	case $week in
	0)
		echo "${week1[$day]}"
		;;
	1)
		echo "${week2[$day]}"
		;;
	esac
}

get_tcpdump_url_for_week_and_day () {
	week=$1
	day=$2
	
	case $week in
	2)
		echo "${week3_tcpdump[$day]}"
		;;
	3)
		echo "${week4_tcpdump[$day]}"
		;;
	4)
		echo "${week5_tcpdump[$day]}"
		;;
	5)
		echo "${week6_tcpdump[$day]}"
		;;
	6)
		echo "${week7_tcpdump[$day]}"
		;;
	7)
		# Testing 1
		echo "${test1_tcpdump[$day]}"
		;;
	8)
		# Testing 2
		echo "${test2_tcpdump[$day]}"
		;;
	esac
}

get_list_url_for_week_and_day () {
	week=$1
	day=$2
	
	case $week in
	2)
		echo "${week3_list[$day]}"
		;;
	3)
		echo "${week4_list[$day]}"
		;;
	4)
		echo "${week5_list[$day]}"
		;;
	5)
		echo "${week6_list[$day]}"
		;;
	6)
		echo "${week7_list[$day]}"
		;;
	7)
		# Testing 1
		echo "${test1_list[$day]}"
		;;
	8)
		# Testing 2
		echo "${test2_list[$day]}"
		;;
	esac
}

download_for_day_and_week_a () {
	let day=$2-1
	let week=$1-1
	
	pcap_path=$3
	csv_path=$4

	day_string="$(get_day "${day}")"
	# Get the download url
	url="$(get_url_for_week_and_day "${week}" "${day}")"
	#echo "${url}"
	
	# Download the archive
	downloaded_archive="target.tar"
	wget "$url" -O "${downloaded_archive}" --no-check-certificate -q
	
	# Extract archive:
	# 1) Set up temporary directory for processing data
	temporary_dir="tempp"
	if [ -d "$temporary_dir" ]; then
		rm -r "${temporary_dir}"
	fi
	mkdir "${temporary_dir}"
	
	# 2) Extract the data to the temporary dir
	tar xf "${downloaded_archive}" -C "${temporary_dir}" --strip-components 1
	
	
	# Get required data from archive
	tcpdump="tcpdump"
	csv="tcpdump.list"
	
	# Check the dir structure of the archive
	# This is specifically necessary for week 1 wednesday
	if [ -d "${temporary_dir}/${day_string}" ]; then
		new_temporary_dir="${temporary_dir}/${day_string}"
	else
		new_temporary_dir=temporary_dir
	fi
	
	gunzip "${new_temporary_dir}/${tcpdump}.gz"
	cp "${new_temporary_dir}/${tcpdump}" "${pcap_path}/${day_string}.tcpdump"
	
	gunzip "${new_temporary_dir}/${csv}.gz"
	cp "${new_temporary_dir}/${csv}" "${csv_path}/${day_string}.list"
	
	# Remove redundant files
	rm -r "${temporary_dir}"
	rm "${downloaded_archive}"
}

download_for_day_and_week_b () {
	let day=$2-1
	let week=$1-1
	
	pcap_path=$3
	csv_path=$4
	day_string="$(get_day "${day}")"
	# Get the download url
	url_tcpdump="$(get_tcpdump_url_for_week_and_day "${week}" "${day}")"
	url_list="$(get_list_url_for_week_and_day "${week}" "${day}")"
	
	# Set up temporary directory for processing data
	temporary_dir="tempp"
	if [ -d "$temporary_dir" ]; then
		rm -r "${temporary_dir}"
	fi
	mkdir "${temporary_dir}"
	
	# Download the archive
	downloaded_tcpdump="tcpdump.gz"
	downloaded_list="list.gz"
	# If verbose: add --show_progress
	wget "$url_tcpdump" -O "${temporary_dir}/${downloaded_tcpdump}" --no-check-certificate -q
	wget "$url_list" -O "${temporary_dir}/${downloaded_list}" --no-check-certificate -q
	
	
	# Get required data from archive
	tcpdump="tcpdump"
	csv="list"

	gunzip "${temporary_dir}/${downloaded_tcpdump}"
	cp "${temporary_dir}/${tcpdump}" "${pcap_path}/${day_string}.tcpdump"
	
	gunzip "${temporary_dir}/${downloaded_list}"
	cp "${temporary_dir}/${csv}" "${csv_path}/${day_string}.list"
	
	# Remove redundant files
	#rm -r "${temporary_dir}"
}

download_for_day_and_week () {
	let week=$1
	# The issue is that the data has to be downloaded in two separate ways
	# The first two weeks ($week < 3) have to be processed differently from the other weeks
	if test $week -lt 3
	then
		download_for_day_and_week_a $1 $2 $3 $4
	else
		download_for_day_and_week_b $1 $2 $3 $4
	fi
}

# input arguments
# $1 = week (1 - 7, 8 - 9)
# $2 = day (1 - 5)
# $3 = pcap/tcpdump destination
# $4 = .csv/.list destination

download_for_day_and_week $1 $2 $3 $4