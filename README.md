# General information
This repository will provide access to the code relevant for raw traffic-based network intrusion detection, as published in:

```
@ARTICLE{LEJEUNE2021,
  author={Le Jeune, Laurens and Goedemé, Toon and Mentens, Nele},
  journal={IEEE Access},
  title={Machine Learning for Misuse-Based Network Intrusion Detection: Overview, Unified Evaluation and Feature Choice Comparison Framework}, 
  year={2021},
  volume={9},
  number={},
  pages={63995-64015},
  doi={10.1109/ACCESS.2021.3075066}
}
```

# Usage
## Dataset prerequisites
There must be a directory containing the dataset (ISCX2012, UNSW-NB15 or CICIDS2017) with the following structure:
```
datasets/
├── CICIDS2017/
│   ├── friday/
│   │   ├── csv/
│   │   │   ├── Friday-WorkingHours-Afternoon-DDos.pcap_ISCX.csv
│   │   │   ├── Friday-WorkingHours-Afternoon-DDos.pcap_ISCX.csv
│   │   │   └── Friday-WorkingHours-Afternoon-DDos.pcap_ISCX.csv
│   │   ├── pcap/
│   │   │   └── Friday-WorkingHours.pcap
│   ├── monday
│   │   ├── csv/...
│   │   └── pcap/...
│   ...
│   └── wednesday
│       ├── csv/...
│       └── pcap/...
├── UNSW-NB15/
│   └── ...
└── ISCX2012/
    └── ...
```
One of either CICIDS2017, UNSW-NB15 or ISCX2012 should at least be present. For each day of that dataset, there should be a 
directory containing a *csv* and a *pcap* file. The *csv* should contain all label CSV files (one or multiple) for that day. Similarly 
the *pcap* directory should contain all PCAP and/or PCAPNG files for that day. As an example, this is demonstrated for the Friday
of CICIDS2017.

## Processing datasets
The *cli.py* file provides an interface to process a dataset. This can be done through either the command line (or shell script)
or directly as a line of code in the *cli.py* file itself.

A base example command is the following:
```shell
python cli.py --root /.../dir_to_store_results/ feature_extraction --dataset CICIDS2017 \
--dataset_dir /.../datasets/ \
--export_dir /.../export_dir/ \
--name example \
custom \
--n_packets 2 \
--n_bytes 64
```
This will cause the code to look for the required dataset files in /.../datasets/CICIDS2017/. The temporary working 
files will be stored in the /.../dir_to_store_results/ directory: Subdirectories will be identifiable by the *example* name.
The features extracted from the traffic will comprise 2 packets, of which the first 64 bytes will be used for each packet.
The feature extraction strategy is *custom*, as opposed to *PCCN*m *HAST1* or *HAST2*.

Finally, the resulting numpy memmap files will be stored inside the /.../export_dir/ directory under the form:
```
example/
├── memmaps/
├── dataset_info.json
├── features_info.json
└── ml_info.json
```

This /.../export_dir/example/ directory can then be used in a training loop to train a machine learning model.

## Interfacing the resulting machine learning features
The *dataset_interface* package provides all functionality that is necessary to interface the resulting memmaps with features. This can be used in any custom machine learning pipeline such as for example is done in the follow-up research following this paper: [Real-Time DL NIDS on FPGA](https://gitlab.com/EAVISE/real-time-dl-nids-on-fpga)

Use the [DatasetInterface](https://gitlab.com/EAVISE/raw-traffic-nids/-/blob/master/dataset_interface/dataset_interface.py#L8) class to interface with the data.
See for example [here](https://gitlab.com/EAVISE/real-time-dl-nids-on-fpga/-/blob/master/setup_dataset.py#L187) how you can use the interface (here a member of an experiment object) to construct a *torch.utils.data.Dataset*.

# Reproducibility information
## Hyperparameter settings
#### General
All datasets were split into a training set, a validation set and a test set. This split is achieved in a 75%/15%/10% manner for training/validation/testing.
The following sections provide the hyperparameters that were used to obtain the results as reported in the above publication.
These hyperparameters were obtained through trial-and-error and provided the best results for our experiments.

#### ISCX2012
|Experiment|Constraint|Initial LR|Number of epochs|Batch size|Optimizer|
|-|-|-|-|-|-|
|HAST-II|All|0.001|35|256|RMSProp|
|HAST-II|100000|0.001|35|256|RMSProp|
|HAST-II|120000|0.001|35|256|RMSProp|
|PCCN Header|All|0.001|35|256|SGD|
|PCCN Header|100000|0.001|35|256|SGD|
|PCCN HePa|All|0.001|35|256|SGD|
|PCCN HePa|100000|0.001|35|256|SGD|


#### UNWS-NB15
|Experiment|Constraint|Initial LR|Number of epochs|Batch size|Optimizer|
|-|-|-|-|-|-|
|HAST-II|All|0.001|50|256|SGD|
|HAST-II|100000|0.001|50|256|SGD|
|HAST-II|50000|0.001|50|256|SGD|
|PCCN Header|All|0.01|50|256|SGD|
|PCCN Header|100000|0.01|50|256|SGD|
|PCCN HePa|All|0.01|50|256|SGD|
|PCCN HePa|100000|0.01|50|256|SGD|

#### CICIDS2017
|Experiment|Constraint|Initial LR|Number of epochs|Batch size|Optimizer|
|-|-|-|-|-|-|
|HAST-II|200000|0.001|30|256|RMSProp|
|PCCN Header|All|0.1|60|256|SGD|
|PCCN Header|100000|0.1|92|256|SGD|
|PCCN HePa|All|0.01|55|256|SGD|
|PCCN HePa|100000|0.1|92|256|SGD|
