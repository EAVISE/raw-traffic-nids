import os
import sys
import numpy as np
from flow_processing.feature_extraction import FlowData, store_features_list
from flow_processing.stream_based_processing import FlowStreamSorter, label_stream_based_flow
from flow_processing.PCCN_feature_extraction import flow_file_to_numeric_features_PCCN, flow_data_to_numeric_features_PCCN
from help_functions import mkdir, remove_dir, process_flow_tree, shape_to_str, str_to_shape
from enums import Dataset

from random import randrange as randint


def generate_ip_address():
    """
    Generate random IP address

    :return:
    """
    return '{}.{}.{}.{}'.format(randint(256), randint(256), randint(256), randint(256))


def generate_port():
    """
    Generate random port

    :return:
    """
    return str(randint(65536))


def generate_flow_id():
    """
    Generate random flow id

    :return:
    """
    return '{}-{}-{}-{}-{}'.format(generate_ip_address(), generate_ip_address(), generate_port(), generate_port(), 6)


def generate_packet_array(instructions):

    flow_ids = {}
    packet_array = []

    for n_packets, flow_idx in instructions:
        if flow_idx not in list(flow_ids.keys()):
            flow_ids[flow_idx] = generate_flow_id()

        for _ in range(n_packets):
            packet_array.append(flow_ids[flow_idx])

    return packet_array, flow_ids


def generate_label_csv_line(dataset, flow_id, n_fwd, n_bwd, label):
    src_addr, dst_addr, src_port, dst_port, proto = flow_id.split('-')

    duration = '0'
    total_fwd = str(n_fwd)
    total_bwd = str(n_bwd)
    speed = '0'
    timestamp = '0'

    if dataset == Dataset.CICIDS2017:
        return ','.join([flow_id, label, src_addr, src_port, dst_addr, dst_port, proto, duration, total_fwd, total_bwd,
                         speed, timestamp]) + '\n'


def generate_label_csv(dataset, instructions, flow_ids, labels):

    csv_lines = ['Dummy header line\n']

    label_mapper = {}

    for flow_idx, flow_id in flow_ids.items():
        flow_label = labels[randint(len(labels))]
        label_mapper[flow_idx] = flow_label

    for n_fwd, flow_idx in instructions:
        label_file_line = generate_label_csv_line(dataset, flow_ids[flow_idx], n_fwd, 0, label_mapper[flow_idx])
        csv_lines.append(label_file_line)

    #print(''.join(csv_lines))
    return csv_lines, label_mapper


def __flow_file_to_flow_data(flow_file, current_path, features_config, features_lists, target_output_dir, dtype,
                             action):
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        return

    # Extract label
    label = "-".join(flow_file_parts[0].split("-")[:-1]).strip()
    if label == 'Backdoor':
        label = 'Backdoors'

    # Present the data of the flow file to the feature extraction function
    with open(os.path.join(current_path, flow_file), 'r') as opened_file:

        while True:
            flowdata = FlowData(opened_file)

            # Extract features from the flow file
            flow_features_list = action['function'](flowdata, **action['params'])
            features_lists[label] += flow_features_list

            if len(features_lists[label]) >= features_config.batch_size:
                store_features_list(features_lists=features_lists, target_output_dir=target_output_dir,
                                    label=label, dtype=dtype)

            # If the end of the flow file has been reached
            if flowdata.is_EOF():
                # Stop the while loop, we have nothing more to do
                break


def extract_PCCN_features(temp_test_dir, temp_feat_dir, labels):
    feature_arrays = {}

    for label in labels:
        os.mkdir(os.path.join(temp_feat_dir, label))
        feature_arrays[label] = []

    class DummyFeatConfig:
        batch_size = 100000
        header_min = 0
        header_max = 100
        payload_min = 100
        payload_max = 200
        hepa_min = 0
        hepa_max = 192
        num_packets = 5
        start = min(header_min, payload_min, hepa_min)
        stop = max(header_max, payload_max, hepa_max)
        use_payload = True
        feature_type = 'hepa'

        def __init__(self):
            pass

        def get_feature_type(self):
            return self.feature_type

    class DummyDatasetConfig:
        include_ts = True

        def __init__(self):
            pass

    process_flow_tree(
        root=temp_test_dir,
        action={
            "function": __flow_file_to_flow_data,
            "params": {
                "target_output_dir": temp_feat_dir,
                "features_config": DummyFeatConfig(),
                "features_lists": feature_arrays,
                "dtype": np.uint8,
                "action": {
                    'function': flow_data_to_numeric_features_PCCN,
                    'params': {'features_config': DummyFeatConfig(), 'dataset_config': DummyDatasetConfig()}
                }
            }
        })

    # Store the remaining features present in the "features_lists" dict:
    for label, feature_list in feature_arrays.items():

        if len(feature_list) == 0:
            continue

        store_features_list(feature_arrays, temp_feat_dir, label, dtype=np.uint8)


def calculate_expected_label_counts(flow_ids, label_mapper, packet_stream_instructions, label_csv_instructions,
                                    bucket_count, bucket_depth):
    # Create counter
    counter = {label: 0 for _, label in label_mapper.items()}

    # For each flow id:
    for flow_idx, flow_id in flow_ids.items():

        # Get packet distribution
        packet_distribution = __get_distribution(packet_stream_instructions, flow_idx, packet_array=True,
                                                 bucket_count=bucket_count, bucket_depth=bucket_depth)
        # Get CSV distribution
        csv_distribution = __get_distribution(label_csv_instructions, flow_idx)

        # Get label
        label = label_mapper[flow_idx]

        #print('For flow_idx: {}|{}'.format(flow_idx, label))
        #print('Packet stream distr: {} -> {}'.format(packet_stream_instructions, packet_distribution))
        #print('Csvdistr:            {} -> {}'.format(label_csv_instructions, csv_distribution))

        # Now start counting:
        csv_idx = 0
        for packet_idx in range(len(packet_distribution)):
            if csv_idx >= len(csv_distribution):
                break

            if packet_distribution[packet_idx] >= csv_distribution[csv_idx]:
                counter[label] += 1
                #packet_idx += 1
                csv_idx += 1
            else:
                if packet_idx == len(packet_distribution) - 1:
                    counter[label] += 1
                    #packet_idx += 1
                    csv_idx += 1
                else:
                    counter[label] += 1
                    #packet_idx += 1
                    csv_distribution[csv_idx] -= packet_distribution[packet_idx]

        #counter[label] += len(packet_distribution)
    return counter


class __InstructionDistribution:

    def __init__(self, width, depth):
        self.width = width
        self.depth = depth

        self.flows_map = {}
        self.flows_order = []
        self.__distribution = {}

    def get_distribution(self, flow_idx):
        # Empty the storage

        # Return the distribution
        try:
            return self.__distribution[flow_idx]
        except KeyError:
            return []

    def add_instruction(self, instruction):
        """
        To be documented

        :return:
        """
        n_packets, flow_idx = instruction

        for _ in range(n_packets):
            self.add_packet(flow_idx)

    def add_packet(self, flow_idx):
        if flow_idx in self.flows_order:
            if self.flows_map[flow_idx] < self.depth:
                self.__add_packet(flow_idx=flow_idx)
                self.__update_queue_position(flow_idx)
            else:
                self.__remove_flow(flow_idx)
                self._add_flow(flow_idx=flow_idx)
        elif len(self.flows_order) < self.width:
            self._add_flow(flow_idx=flow_idx)
        else:
            self.__pop_flow()
            self._add_flow(flow_idx=flow_idx)

    def __update_queue_position(self, flow_idx):
        """
        Update a flow's position in the queue: It will be put at the last place, removing it's previous occurence in
        that queue.

        :param flow_idx: Flow identifier
        :type flow_idx: ``str``
        :return: None
        """
        self.flows_order.remove(flow_idx)
        self.flows_order.append(flow_idx)

    def store_remaining_flows(self):
        """
        Write remaining flows to storage.

        :return: None
        """
        for _ in range(len(self.flows_order)):
            self.__pop_flow()

    def __del__(self):
        """
        Destructor, will write any remaining flows to the storage

        :return: None
        """
        self.store_remaining_flows()

    def __add_packet(self, flow_idx):
        """
        Add a packet to a flow currently in this FlowStreamSorter.

        :param flow_idx: Identifier of the flow
        :type flow_idx: ``str``
        :return: None
        """
        self.flows_map[flow_idx] += 1

    def _add_flow(self, flow_idx):
        """
        Add a flow to this FlowStreamSorter.

        :param packet_data: Packet data, with optional timestamps
        :type packet_data: ``str``
        :param flow_idx: Identifier of the flow
        :type flow_idx: ``str``
        :return: None
        """
        self.flows_order.append(flow_idx)
        self.flows_map[flow_idx] = 1

    def __remove_flow(self, flow_idx):
        """
        Remove a flow from this FlowStreamSorter by storing it on disk.

        :param flow_idx: Identifier of the flow
        :type flow_idx: ``str``
        :return: None
        """
        # Extract saved packets, while removing them from storage
        packets_out = self.flows_map.pop(flow_idx)
        # Remove flow id from queue
        self.flows_order.remove(flow_idx)

        # Then store the extracted packets
        self.__store_in_distribution(flow_idx=flow_idx, n_packets=packets_out)

    def __pop_flow(self):
        """
        Remove the flow was added first from this FlowStreamSorter

        :return: None
        """
        flow_out_id = self.flows_order.pop()

        packets_out = self.flows_map.pop(flow_out_id)

        self.__store_in_distribution(flow_idx=flow_out_id, n_packets=packets_out)

    def __store_in_distribution(self, flow_idx, n_packets):
        if flow_idx in self.__distribution.keys():
            self.__distribution[flow_idx].append(n_packets)
        else:
            self.__distribution[flow_idx] = [n_packets]


def __get_distribution(instructions, flow_idx, packet_array=False, bucket_count=1, bucket_depth=5):

    if packet_array:
        distributer = __InstructionDistribution(width=bucket_count, depth=bucket_depth)

        for instruction in instructions:
            distributer.add_instruction(instruction)

        distributer.store_remaining_flows()

        return distributer.get_distribution(flow_idx)
    else:
        distribution = []

        for n_packets, flow_idx_ in instructions:
            if flow_idx == flow_idx_:
                distribution.append(n_packets)

        return distribution


def generate_random_packet_array_instructions(n_flow_ids, n_packets, burst_limit):

    current_n_packets = 0
    instructions = []
    while current_n_packets < (n_packets - burst_limit):
        flow_idx = randint(n_flow_ids)
        flow_packets = randint(burst_limit) + 1

        instructions.append((flow_packets, flow_idx))

        current_n_packets += flow_packets

    instructions.append((n_packets-current_n_packets, randint(n_flow_ids)))

    return instructions


def generate_random_csv_instructions(flow_ids, n_packets, burst_limit):

    available_idx = list(flow_ids.keys())
    n_idx = len(available_idx)
    current_n_packets = 0
    instructions = []

    while current_n_packets < (n_packets - burst_limit):

        flow_idx = available_idx[randint(n_idx)]
        flow_packets = randint(burst_limit) + 1

        instructions.append((flow_packets, flow_idx))

        current_n_packets += flow_packets

    instructions.append((n_packets-current_n_packets, available_idx[randint(n_idx)]))

    return instructions


def test_flow_stream_sorter_auto(n_buckets, bucket_depth, n_flow_ids, n_packets, burst_limit, n_labels, limit=-1):

    # Burst limit = Max number of subsequent packets with same flow ID

    i = 0
    while True:
        # Generate dir where testing will take place
        temp_test_dir = 'tmp_test_dir'
        temp_feat_dir = 'tmp_features_dir'
        mkdir(temp_test_dir)

        # Generate generic packet data
        packet_data = '0,0,' + ''.join('{:02X}'.format(i) for i in range(256)) + '\n'

        # Define prerequisites
        dataset_ = Dataset.CICIDS2017
        labels = ['Label{}'.format(idx) for idx in range(n_labels)]

        # Initialize Flow Sorter
        stream_sorter = FlowStreamSorter(n_buckets, 5, temp_test_dir, True)

        # Generate random intructions

        # Generate packet array
        instructions = generate_random_packet_array_instructions(n_flow_ids, n_packets, burst_limit)

        with open('packet_array_instructions.txt', 'w') as f:
            f.writelines('\n'.join(['({},{})'.format(a, b) for a,b in instructions]))

        packet_array, flow_ids = generate_packet_array(instructions)

        # Execute flow sorting
        for flow_id in packet_array:
            stream_sorter.add_packet(packet_data, flow_id)

        stream_sorter.store_remaining_flows()

        # Generate label file:
        # Generate new random instructions
        instructions_csv = generate_random_csv_instructions(flow_ids, n_packets, burst_limit)
        csv_lines, label_mapper = generate_label_csv(dataset_, instructions_csv, flow_ids, labels)

        with open('csv_instructions.txt', 'w') as f:
            f.writelines('\n'.join(['({},{})'.format(a, b) for a, b in instructions_csv]))

        class DummyConfig:
            dataset = dataset_.name
            chronological = False
            use_ts_labelling = False
            bidirectional = True

            def __init__(self):
                pass

        # Temporarily generate CSV file
        csv_file_path = '.temporary_csv_file.csv'

        with open(csv_file_path, 'w') as f:
            f.writelines(csv_lines)

        # Label the extracted flows according to the generated label csv
        label_stream_based_flow(csv_file_path, temp_test_dir, None, DummyConfig(), verbose=False)

        # Extract features
        mkdir(temp_feat_dir)
        extract_PCCN_features(temp_test_dir, temp_feat_dir, labels)

        # Count packet counts in flow streams
        counter = {label: 0 for label in labels}

        base_path = temp_feat_dir
        for label in labels:
            label_path = os.path.join(base_path, label)

            for item in os.listdir(label_path):
                memmap_shape = str_to_shape(item.rstrip('.npy').split('--')[1])
                counter[label] += memmap_shape[0]

        # Check results
        sanity_check = calculate_expected_label_counts(flow_ids, label_mapper, instructions, instructions_csv,
                                                       bucket_count=n_buckets, bucket_depth=bucket_depth)
        for key, item in sanity_check.items():
            if counter[key] != item:
                print('For run i = {}:'.format(i))
                print('Discrepancy :')
                print(counter)
                print(sanity_check)
                print(instructions)
                sys.exit(0)

        # Cleanup
        os.remove(csv_file_path)
        remove_dir(temp_test_dir, remove_root=True, verbose=False)
        remove_dir(temp_feat_dir, remove_root=True, verbose=False)

        # Increment
        i += 1

        if limit != -1:
            if i >= limit:
                break


def test_flow_stream_sorter(n_buckets):
    # Generate dir where testing will take place
    temp_test_dir = 'tmp_test_dir'
    temp_feat_dir = 'tmp_features_dir'
    remove_dir(temp_test_dir, remove_root=True, verbose=False)
    remove_dir(temp_feat_dir, remove_root=True, verbose=False)
    mkdir(temp_test_dir)

    # Generate generic packet data
    packet_data = '0,0,' + ''.join('{:02X}'.format(i) for i in range(256)) + '\n'
    dataset_ = Dataset.CICIDS2017
    #labels = ['BENIGN', 'Attack1', 'Attack2']

    labels = ['BENIGN', 'Iets anders', 'Nog iets anders']
    # Initialize Flow Sorter
    stream_sorter = FlowStreamSorter(n_buckets, 5, temp_test_dir, True)

    # Generate packet array
    # Structure: (n_packets, flow_id)
    # instructions = [(3, 0), (2, 1), (2, 0), (1, 1), (1, 2), (1, 3), (2, 2), (3, 0)]
    #instructions = [(3, 0), (2, 1), (2, 0), (1, 1), (3, 0)]
    #instructions = [(1, 2), (1, 2), (4, 0), (4, 1)]
    #instructions = [(2, 0), (4, 0), (4, 2)]
    #instructions = [(3, 0), (3, 2), (4, 2)]
    instructions = [(1, 0), (5, 1), (4, 1)]
    instructions = [(2, 0), (4, 0), (5, 1), (5, 2), (2, 1), (4, 2), (2, 0), (2, 0), (4, 1), (5, 1), (2, 2), (1, 2),
                    (3, 0), (3, 0), (2, 2), (4, 1), (3, 1), (2, 0), (1, 2), (4, 0), (5, 1), (5, 0), (4, 2), (2, 1),
                    (1, 0), (2, 1), (3, 0), (1, 0), (1, 2), (3, 0), (4, 1), (1, 0), (2, 1), (1, 1), (5, 0)]
    packet_array, flow_ids = generate_packet_array(instructions)

    print('\n'.join(packet_array))

    # Execute flow sorting
    for flow_id in packet_array:
        stream_sorter.add_packet(packet_data, flow_id)

    stream_sorter.store_remaining_flows()

    # Generate label file:
    instructions_csv = [(4, 0), (5, 0), (1, 1)]
    instructions_csv = [(1, 0), (5, 1), (3, 2), (5, 0), (5, 2), (5, 1), (3, 2), (4, 2), (3, 2), (1, 2), (4, 2), (2, 0),
                        (1, 1), (2, 1), (5, 2), (4, 1), (2, 2), (4, 0), (5, 2), (2, 2), (2, 0), (5, 1), (4, 0), (3, 2),
                        (5, 0), (4, 1), (2, 1), (5, 1), (4, 1)]
    csv_lines, label_mapper = generate_label_csv(dataset_, instructions_csv, flow_ids, labels)

    class DummyConfig:
        dataset = dataset_.name
        chronological = False
        use_ts_labelling = False
        bidirectional = True

        def __init__(self):
            pass

    # Temporarily generate CSV file
    csv_file_path = '.temporary_csv_file.csv'

    with open(csv_file_path, 'w') as f:
        f.writelines(csv_lines)

    # Label the extracted flows according to the generated label csv
    label_stream_based_flow(csv_file_path, temp_test_dir, None, DummyConfig(), verbose=True)

    # Extract features
    mkdir(temp_feat_dir)
    extract_PCCN_features(temp_test_dir, temp_feat_dir, labels)

    # Count packet counts in flow streams
    counter = {label: 0 for label in labels}

    base_path = temp_feat_dir
    for label in labels:
        label_path = os.path.join(base_path, label)

        for item in os.listdir(label_path):
            memmap_shape = str_to_shape(item.rstrip('.npy').split('--')[1])
            counter[label] += memmap_shape[0]

    print(counter)
    print(calculate_expected_label_counts(flow_ids, label_mapper, instructions, instructions_csv,
                                          bucket_count=n_buckets, bucket_depth=5))

    # Cleanup
    os.remove(csv_file_path)

    # Remove temporary testing directory
    # remove_dir(temp_test_dir, remove_root=True)
