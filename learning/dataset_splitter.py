"""
Module for splitting traffic data into training, validation and testing
Author: Laurens Le Jeune
"""
import os
import numpy as np
from exceptions import DatasetSplitException


def split_dataset(indices, config):
    data_list = [i for i in range(indices[-1] + 1)]

    train, valid, test = split_dataset_list(data_list, train=config.train, valid=config.valid, test=config.test)

    return {'train': train, 'valid': valid, 'test': test}


def split_dataset_list(data_list, train=0.8, valid=0.0, test=0.2, shuffle=True):

    if train + test + valid != 1.0:
        raise DatasetSplitException("The sum of train, validation and test fraction should be 1, but is",
                                    train + test + valid, 'instead')

    n = len(data_list)

    # (optional) shuffle data
    if shuffle:
        np.random.shuffle(data_list)

    begin = 0
    end_1 = int(np.ceil(train * n))
    end_2 = int(np.ceil(end_1 + valid * n))
    end_3 = n

    return data_list[begin:end_1], data_list[end_1:end_2], data_list[end_2:end_3]

def split_dataset_arrays(array_list, output_dir, output_filename, train=0.8, valid=0.0, test=0.2, *, \
                         shuffle=True, replace_existing=False):
    """
    Split a provided dataset file into training, validation and testing data.
    Only use for files that can be loaded into memory entirely!

    Params:
    data_file (str): Path to the dataset file
    target_dir (str): Target directory to store generated dataset files. \
                      Defaults to directory of data_file.
    train (float, [0.0, .., 1.0]): Fraction of data to be assigned as training data. Default = 0.8
    valid (float, [0.0, .., 1.0]): Fraction of data to be assigned as validation data. Default = 0.0
    test  (float, [0.0, .., 1.0]): Fraction of data to be assigned as validation data. Default = 0.2
    Note that the sum of train, valid and test must be equal to 1.0.

    shuffle (bool): If True, the data will be shuffled before splitting. Default True.
    replace_existing (bool): If True, replace existing split dataset files for data_file.
    remove_source_file (bool): If True, data_file will be removed from storage. Default False.

    Return:
    list (str) of filenames of split dataset files
    """

    if train + test + valid != 1.0:
        DatasetSplitException("The sum of train, validation and test fraction should be 1, but is", train + test + valid)
        return None

    # Create resulting filenames and paths
    train_data_file = output_filename + "_{}_train.npz".format(int(train * 100))
    valid_data_file = output_filename + "_{}_valid.npz".format(int(valid * 100))
    test_data_file = output_filename + "_{}_test.npz".format(int(test * 100))

    train_data_path = os.path.join(output_dir, "train", train_data_file)
    valid_data_path = os.path.join(output_dir, "valid", valid_data_file)
    test_data_path = os.path.join(output_dir, "test", test_data_file)


    train_file_exists = os.path.isfile(train_data_path)
    valid_file_exists =  os.path.isfile(valid_data_path)
    test_file_exists = os.path.isfile(test_data_path)
    # If no new files will be made, terminate the function
    if not(replace_existing) and train_file_exists and valid_file_exists and test_file_exists:
        print("Split files already exist and will not be overwritten.")
        return [train_data_file, valid_data_file, test_data_file]

    # Generate dataset split files
    n = len(array_list)

    # (optional) shuffle data
    if shuffle:
        np.random.shuffle(array_list)

    # Determine splitting points
    begin = 0
    end_1 = int(np.ceil(train * n))
    end_2 = int(np.ceil(end_1 + valid * n))
    end_3 = n

    # Split and store data
    if train > 0.0 and (not train_file_exists or (replace_existing and train_file_exists)):
        np.savez_compressed(train_data_path, array_list[begin:end_1])
    else:
        train_data_file = None

    if valid > 0.0 and (not valid_file_exists or (replace_existing and valid_file_exists)):
        np.savez_compressed(valid_data_path, array_list[end_1:end_2])
    else:
        valid_data_file = None

    if test > 0.0 and (not test_file_exists or (replace_existing and test_file_exists)):
        np.savez_compressed(test_data_path, array_list[end_2:end_3])
    else:
        test_data_file = None

    return [train_data_file, valid_data_file, test_data_file]


def split_dataset_text_file(data_file, target_dir=None, train=0.8, valid=0.0, test=0.2, *, \
                  shuffle=True, replace_existing=False, remove_source_file=False):
    """
    Split a provided dataset file into training, validation and testing data.
    Only use for files that can be loaded into memory entirely!

    Params:
    data_file (str): Path to the dataset file
    target_dir (str): Target directory to store generated dataset files. \
                      Defaults to directory of data_file.
    train (float, [0.0, .., 1.0]): Fraction of data to be assigned as training data. Default = 0.8
    valid (float, [0.0, .., 1.0]): Fraction of data to be assigned as validation data. Default = 0.0
    test  (float, [0.0, .., 1.0]): Fraction of data to be assigned as validation data. Default = 0.2
    Note that the sum of train, valid and test must be equal to 1.0.

    shuffle (bool): If True, the data will be shuffled before splitting. Default True.
    replace_existing (bool): If True, replace existing split dataset files for data_file.
    remove_source_file (bool): If True, data_file will be removed from storage. Default False.

    Return:
    list (str) of filenames of split dataset files
    """

    if train + test + valid != 1.0:
        print("Hier moet nog een exception komen")
        return None

    # Extract relevant storage data for dataset file
    root_dir, data_file_name = os.path.split(data_file)

    # If no target directory is given, take the directory of the data file
    if target_dir is None:
        target_dir = root_dir

    # Create resulting filenames and paths
    train_data_file = data_file_name.rstrip(".txt") + "_{}_train.txt".format(int(train * 100))
    valid_data_file = data_file_name.rstrip(".txt") + "_{}_valid.txt".format(int(valid * 100))
    test_data_file = data_file_name.rstrip(".txt") + "_{}_test.txt".format(int(test * 100))

    train_data_path = os.path.join(target_dir, train_data_file)
    valid_data_path = os.path.join(target_dir, valid_data_file)
    test_data_path = os.path.join(target_dir, test_data_file)

    # If no new files will be made, terminate the function
    if not(replace_existing) and os.path.isfile(train_data_path) \
        and os.path.isfile(valid_data_path) and os.path.isfile(test_data_path):
        print("Split files already exist and will not be overwritten.")
        return [train_data_file, valid_data_file, test_data_file]

    # Generate dataset split files
    with open(data_file, "r") as f:
        # Extract data
        lines = f.readlines()
        n = len(lines)

        # (optional) shuffle data
        if shuffle:
            np.random.shuffle(lines)

        # Determine splitting points
        begin = 0
        end_1 = int(np.ceil(train * n))
        end_2 = int(np.ceil(end_1 + valid * n))
        end_3 = n

        # Split and store data
        if train > 0.0 and not (replace_existing and os.path.isfile(train_data_path)):
            with open(train_data_path, "w") as f_opened:
                f_opened.writelines(lines[begin:end_1])
        else:
            train_data_file = None

        if valid > 0.0 and not (replace_existing and os.path.isfile(valid_data_path)):
            with open(valid_data_path, "w") as f_opened:
                f_opened.writelines(lines[end_1:end_2])
        else:
            valid_data_file = None

        if test > 0.0 and not (replace_existing and os.path.isfile(test_data_path)):
            with open(test_data_path, "w") as f_opened:
                f_opened.writelines(lines[end_2:end_3])
        else:
            test_data_file = None

    # (optional) remove the source dataset file
    if remove_source_file:
        os.remove(data_file)

    return [train_data_file, valid_data_file, test_data_file]


if __name__ == '__main__':
    pass
