"""
Module that turns numpy files with extracted features into memmap files that can be accessed for machine learning
purposes.
"""
import os
import numpy as np
from enums import DataType
from ml_config import MLFeaturesConfig
from features_config import PCCNConfig
from help_functions import mkdir, str_to_shape
from learning.dataset_splitter import split_dataset


def _generate_index(config):

    index = {}

    root_dir = os.path.join(config.features_config.features_root, config.features_config.name)

    for day in config.features_config.days:
        day_dir = os.path.join(root_dir, day)

        # Only do this for days that actually have features
        if not config.features_config.features_generated[day]:
            continue

        for label in os.listdir(day_dir):
            label_dir = os.path.join(day_dir, label)

            if label not in index.keys():
                index[label] = []

            for data_file in os.listdir(label_dir):
                index[label].append(os.path.join(label_dir, data_file))

    return index


def get_item_from_memmaps(memmaps, indices, idx):
    """
    Get the requested item from the list of memory mapped files.
    Principle: memmaps is a list of memory mapped files:
    [memmap0, memmap1, memmap2, ...]
    Indices is a list built based on the number of elements in each memory map. If memmap0 has 10 elements, memmap1 has
    20 elements and memmap2 has 30 elements, indices would be the following:
    [9, 9 + 20, 29 + 30] -> [9, 29, 59]
    This means that, for the three memory maps together, the highest possible global index is 59.
    It is important to distinguish between global and local indices: The indices in the indices list are global.
    Therefore, an item with the global index 5 would be from memmap0, while 53 would correspond to memmap2.
    The responsibility of this function is to map the global provided index ``idx`` to it local equivalent in its
    corresponding memory map. For example, global ``idx``=53 would correspond to local index 23 in the third memory map,
    being the 24th element.

    :param memmaps: List of memory mapped files.
    :param indices: List of indices corresponding with the memory mapped files
    :param idx: Global index of the item requested

    :return: Requested item
    """
    for i in range(len(indices)):
        if idx <= indices[i]:
            if i == 0:
                correct_idx = idx
            else:
                correct_idx = idx - indices[i - 1] - 1

            return memmaps[i][correct_idx]

    return None


def _aggregate_memmaps_to_split_and_memmap(index, config, verbose=False):
    for label, filenames in index.items():
        indices = []
        memmaps = []
        if verbose:
            print("Processing data for label", label)

        n_filenames = len(filenames)
        i = 0

        base_shape = str_to_shape(os.path.split(filenames[0])[-1].rstrip('.npy').split('--')[1])[1:]
        if config.dtype == DataType.FLOAT32:
            dtype = np.float32
        else:
            dtype = np.uint8

        for filename in filenames:
            name = os.path.split(filename)[-1]
            # The memory mapped files have their shape in their filename:
            memmap_shape = str_to_shape(name.rstrip('.npy').split('--')[1])
            memmaps.append(np.memmap(filename=filename, mode='r', shape=memmap_shape, dtype=dtype))

            # Load the archive
            if len(indices) == 0:
                indices.append(memmap_shape[0] - 1)
            else:
                indices.append(indices[-1] + memmap_shape[0])

            i += 1
            if verbose:
                print('{}/{}'.format(i, n_filenames), end='\r')

        # Now, generate a list of indices that can be split into training, validation and testing
        splits = split_dataset(indices, config)

        # Write these features to their memory maps:
        for usage, items in splits.items():
            print('LEN ITEMS IS ', len(items))
            if config.memmap_metadata[usage][label][0] != "":
                continue

            if not config.byte_compression:
                output_file, shape = generate_numpy_memmap_from_memmaps(config=config, usage=usage, label=label,
                                                                        items=items, indices=indices,
                                                                        memmaps=memmaps,
                                                                        base_shape=base_shape,
                                                                        verbose=True)
            else:
                raise NotImplementedError()

            # Save memmap metadata:
            config.memmap_metadata[usage][label] = (output_file, shape)
            config.update()


def generate_numpy_memmap_from_memmaps(config, usage, label, items, indices, memmaps, base_shape, verbose=True):
    # 1) Generate memory map
    # Output file
    if verbose:
        print("Generating", label + '-memmap.npy')

    if isinstance(config.features_config, PCCNConfig):
        output_file = os.path.join(config.root, "memmaps", config.name,
                                   config.features_config.get_feature_type(), usage, label + '-memmap.npy')
    else:
        output_file = os.path.join(config.root, "memmaps", config.name, usage, label + '-memmap.npy')

    # Get shape from examples
    shape = tuple([len(items)] + list(base_shape))

    if verbose:
        print("Turning shape", base_shape, "into shape", shape)

    if config.dtype == DataType.FLOAT32:
        memmap = np.memmap(output_file, mode="w+", shape=shape, dtype=np.float32)
    else:
        memmap = np.memmap(output_file, mode="w+", shape=shape, dtype=np.uint8)

    # 2) Write all data to the memmap:
    for i in range(len(items)):
        if verbose and ((i % 10000) == 0):
            print("{}/{}".format(i, len(items)), end="\r")

        memmap[i, :] = get_item_from_memmaps(memmaps=memmaps, indices=indices, idx=items[i])

    return output_file, shape


def extract_features_metadata(config, batch_size=750000, verbose=True):
    """

    :param config:
    :param batch_size:
    :param verbose:
    :return:
    """
    label = list(config.memmap_metadata['train'].keys())[0]
    first_metadata_item = config.memmap_metadata['train'][label]
    base_shape = tuple(list(first_metadata_item[1])[1:])

    current_total = np.zeros(base_shape)
    current_max = np.zeros(base_shape)
    current_min = np.zeros(base_shape)
    current_total_std = np.zeros(base_shape)
    total_numel = 0

    if config.dtype == DataType.FLOAT32:
        dtype = np.float32
    else:
        dtype = np.uint8

    # Get min, max, mean
    for usage, item in config.memmap_metadata.items():
        for label, metadata in item.items():
            memmap = np.memmap(metadata[0], mode='r', shape=tuple(metadata[1]), dtype=dtype)
            numel = metadata[1][0]
            total_numel += numel
            i = 0
            print('{}-{}: {}'.format(usage, label, numel))

            while i < (numel - 1):
                if i + batch_size >= numel:
                    next_i = numel - 1
                else:
                    next_i = i + batch_size
                print('{}/{}'.format(i, numel), end='\r')
                current_total += np.sum(memmap[i:next_i], axis=0)
                current_max = np.max(np.concatenate((memmap[i:next_i], [current_max]), axis=0), axis=0)
                current_min = np.min(np.concatenate((memmap[i:next_i], [current_min]), axis=0), axis=0)

                i = next_i

    mean = current_total / total_numel

    # Now calculate the STD

    for usage, item in config.memmap_metadata.items():
        for label, metadata in item.items():
            memmap = np.memmap(metadata[0], mode='r', shape=tuple(metadata[1]), dtype=dtype)
            numel = metadata[1][0]
            total_numel += numel
            i = 0
            print('{}-{}: {}'.format(usage, label, numel))

            while i < (numel - 1):
                if i + batch_size >= numel:
                    next_i = numel - 1
                else:
                    next_i = i + batch_size
                print('{}/{}'.format(i, numel), end='\r')

                current_std = memmap[i:next_i] - mean
                current_total_std += np.sum(current_std, axis=0)

                i = next_i
    std = current_total_std / (total_numel - 1)

    return current_min.tolist(), current_max.tolist(), mean.tolist(), std.tolist()


def create_setup(root, name, use_existing, features_config, verbose=True, **kwargs):
    config = MLFeaturesConfig(name=name, root=root, features_config=features_config, use_existing=use_existing,
                              **kwargs)

    if not config.completed_setup:
        # Setup + cleanup
        if verbose:
            print("Removing previous incomplete work and creating required directories..")
        base_path = os.path.join(config.root, "memmaps", config.name)
        mkdir(base_path)
        if isinstance(config.features_config, PCCNConfig):

            base_path = os.path.join(base_path, config.features_config.get_feature_type())
            mkdir(base_path)

        for usage in ["train", "valid", "test"]:
            usage_path = os.path.join(base_path, usage)
            if os.path.isdir(usage_path):
                for filename in os.listdir(usage_path):
                    label = "-".join(filename.split("-")[:-1])
                    if config.memmap_metadata[usage][label][0] == "":
                        os.remove(os.path.join(usage_path, filename))
            else:
                mkdir(usage_path)

        # 1) Aggregate features:
        # Create a list of all filenames, sorted per label
        # Take into account the sizes of the files, create sets if necessary
        if verbose:
            print("Indexing extracted feature files..")
        index = _generate_index(config)
        if verbose:
            print("Index results:", index)

        # 2) For every set of filenames, load all numpy arrays into memory
        # Then, split the data and store each split in memory-mapped files.
        _aggregate_memmaps_to_split_and_memmap(index, config, verbose=verbose)
        config.update()

        # 3) Finally, calculate the metadata of the features for normalization:
        # min-max, mean and standard deciation
        maxi, mini, mean, std = extract_features_metadata(config, verbose=verbose)

        config.features_metadata['min'] = mini
        config.features_metadata['max'] = maxi
        config.features_metadata['mean'] = mean
        config.features_metadata['std'] = std

        config.completed_setup = True
        config.update()

    return config
