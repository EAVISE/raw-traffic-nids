import os
import numpy as np
import random
from .enums import DataType, FeatureStrategy, Dataset
from .ml_config import get_ml_features_config as ml_features_config_from_file


class DatasetInterface:

    root_directory = None
    __dataset_config = None
    __features_config = None
    __ml_features_config = None

    def __init__(self, root_directory):

        self.root_directory = root_directory

        ml_config_file = 'ml_info.json'

        self.__ml_features_config = ml_features_config_from_file(os.path.join(root_directory, ml_config_file), True)
        self.__features_config = self.__ml_features_config.features_config
        self.__dataset_config = self.__features_config.dataset_config

    def get_feature_strategy(self):
        """
        Get the feature extraction strategy that was used for the dataset.

        :rtype: ``FeatureStrategy``
        """
        return FeatureStrategy[self.__features_config.featurestrategy]

    def get_dataset(self):
        """
        Get the original dataset from which the data was extracted.

        :rtype: ``FeatureStrategy``
        """
        return Dataset[self.__dataset_config.dataset]

    def get_labelled_days(self):
        """
        Get all days in the dataset that were labelled

        :return: List with all days with labelled and processed data
        :rtype: ``list`` of ``str``
        """
        return self.__dataset_config.get_labelled_days()

    def get_day_for_label(self, label):
        """
        Get the days which contain data for the specified label, if applicable.

        :param label: Specified label
        :type label: ``str``
        :return: Days containing traffic for the label, if any.
        :rtype: ``list`` of ``str`` or None
        """
        labelled_days = self.get_labelled_days()
        relevant_days = []
        for day in labelled_days:
            if label in self.get_labels(day=day):
                relevant_days.append(day)

        if len(relevant_days) == 0:
            return None
        return relevant_days

    def get_normal_class(self):
        return self.__dataset_config.get_normal_class()

    def get_all_days(self):
        return self.__dataset_config.days

    def get_labels(self, day=None, only_labelled_days=False):
        return self.__dataset_config.get_labels(day=day, only_labelled_days=only_labelled_days)

    def get_attacks(self, day=None):
        return self.__dataset_config.get_attacks(day=day)

    def get_link_layer_protocol(self):
        return self.__dataset_config.get_link_layer()

    def get_sample_shape(self):
        """
        Get the shape of a feature sample

        :return: Tuple with dimensions
        :rtype: ``tuple`` of ``int``
        """
        return self.__features_config.get_sample_shape()

    def decompress_features(self, item):
        """
        Decompress the provided n x 1 vector into a m x n vector.
        WORKS ONLY FOR HAST-I and HAST-II generated features.

        :param array: Input n x 1 vector to be decompressed
        :return: Decompressed 256 x n array
        """
        return self.__features_config.decompress_features(item)

    def get_memmap_metadata(self, usage=None, label=None):

        # Modify paths to absolute paths:
        memmap_metadata = self.__ml_features_config.memmap_metadata

        for _usage, label_dicts in memmap_metadata.items():
            for _label, metadata in label_dicts.items():
                memmap_metadata[_usage][_label] = [os.path.join(self.root_directory, metadata[0]), metadata[1]]

        # Return the requested portion of the data
        if usage is None:
            return memmap_metadata
        else:
            if label is None:
                print(memmap_metadata[usage])
                return memmap_metadata[usage]
            else:
                return memmap_metadata[usage][label]

    def get_dataset_split(self, usage=None):
        """
        Provides insight in how the dataset was divided into a training, validation and test set.

        :param usage: Should be 'train', 'valid' or 'test'. Specifies the fraction of the dataset required. Optional.
        :type usage: ``str`` or None
        :return: If usage was specified, the fraction of the dataset attributed to that usage. If no usage was \
        specified, a tuple containing the fractions for all three usages is provided instead.
        :rtype: ``tuple of ``float`` or ``float
        """
        if usage is not None:
            usage = usage.lower()
            if (usage == 'train') or (usage == 'training'):
                return self.__ml_features_config.train
            elif (usage == 'valid') or (usage == 'validation'):
                return self.__ml_features_config.valid
            if (usage == 'test') or (usage == 'testing'):
                return self.__ml_features_config.test
        return self.__ml_features_config.train, self.__ml_features_config.valid, self.__ml_features_config.test

    def do_shuffle(self):
        """
        Check whether the dataset should be shuffled before use.

        :return: True if it should be shuffled.
        :rtype: ``bool``
        """
        return self.__ml_features_config.shuffle

    def get_label_mapping(self):
        """
        Get a dictionary containing the labels that have been mapped to another label, if any. The mapping is given in
        the following format: {'original label1': 'new label1', 'original label2': 'new label2'}. Note that this method
        can be used to map multiple different labels to one singular label.

        :return: Dictionary with the label mapping for the data.
        :rtype: ``dict``
        """
        return self.__ml_features_config.label_mapping

    def is_byte_compression_used(self):
        return self.__ml_features_config.byte_compression

    def get_features_metadata(self, kind=None):
        """
        Get metadata information for the generated features. Request either the maximum values, minimum values,
        mean values or standard deviation values for those features. If not specified, return a dictionary with all
        features instead.
        """
        if kind is not None:
            kind = kind.lower()
            if kind == 'min':
                return self.__ml_features_config.features_metadata['min']
            if kind == 'max':
                return self.__ml_features_config.features_metadata['max']
            if kind == 'mean':
                return self.__ml_features_config.features_metadata['mean']
            if kind == 'std':
                return self.__ml_features_config.features_metadata['std']

        return self.__ml_features_config.features_metadata

    def get_features_min(self):
        return self.get_features_metadata('min')

    def get_features_max(self):
        return self.get_features_metadata('max')

    def get_features_mean(self):
        return self.get_features_metadata('mean')

    def get_features_std(self):
        return self.get_features_metadata('std')

    def get_dtype(self):
        """
        Get the data type used to represent the stored data.

        :return: The Numpy datatype of the data
        """
        dtype = self.__ml_features_config.dtype

        if dtype == DataType.UINT8:
            return np.uint8
        elif dtype == DataType.FLOAT32:
            return np.float32

    def get_feature_type(self):
        """
        Header, Payload, Hepa for PCCN features
        """
        return self.__features_config.get_feature_type()

    def include_icmp(self):
        """
        Check if ICMP traffic is included.
        """
        if FeatureStrategy[self.__features_config.featurestrategy] == FeatureStrategy.SESSIONBASED:
            return self.__features_config.include_icmp
        else:
            return False

    def get_shape(self):
        """
        Get the shape of a data sample.

        :rtype: ``tuple`` of ``int``
        """
        return self.__features_config.get_sample_shape()

    def get_sample(self, label=None, seed=None, usage='valid'):
        """
        Get a random validation sample to sanity-check the functioning of a model.

        :param label: Label of the class the random sample should belong to
        :param seed: Seed to use in the random number generator
        :param usage: Determines the usage of the sample in the dataset. Can be 'train', 'valid' or 'test'.
        :type label: ``str`` or None
        :type seed: ``int`` or None
        :type usage: ``str`` or None
        :return: Random sample conforming the provided requirements
        :rtype: Numpy ndarray
        """

        # Set seed, if applicable
        if seed is not None:
            # Use a seed to get the same result each iteration
            np.random.seed(seed)
        elif not seed:
            pass

        if label is None:
            # Get a random label
            all_labels = self.get_labels(only_labelled_days=True)
            np.random.shuffle(all_labels)
            label = all_labels[0]

        # Now determine the index
        metadata = self.get_memmap_metadata(usage=usage, label=label)
        total_n_samples = metadata[1][0]

        index = np.random.randint(0, total_n_samples)

        # Now finally retrieve the sample:
        correct_memmap_path = os.path.join(self.root_directory, metadata[0])
        memmap = np.memmap(correct_memmap_path, mode="r", shape=tuple(metadata[1]), dtype=self.get_dtype())

        return memmap[index, :]

    def get_stream_params(self):
        return self.__dataset_config.n_stream_flows, self.__dataset_config.n_stream_packets


if __name__ == '__main__':
    path = os.path.join('/', 'home', 'laurens', 'Software', 'framework', 'experimentele_test')
    interface = DatasetInterface(root_directory=path)

    s1 = interface.get_sample(label='BENIGN')
    s2 = interface.get_sample(label='BENIGN')
    print(s1[0])
    print(s2[0])
