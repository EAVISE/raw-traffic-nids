"""
This module comprises the configuration class keeping track of the machine learning features and the machine learning
experiments. ``class:MLFeaturesConfig`` keeps track of sorting, splitting and shuffling features, preparing them for the
actual machine learning training. ``class:MLExperiment`` then keeps track of one specific experiment, and is responsible
for the exact configuration and storage of model and results.
"""
import os
import json
from .help_functions import remove_dir, split_path
from .features_config import feature_strategy_config_from_path
from .config import Config
from .exceptions import MLConfigurationConstructionException, NotImplementedException
from .enums import DataType


def remove_machine_learning_model_configuration(name, root, verbose=True):
    """
    Remove the configuration of a model for machine learning.
    :param name:
    :param root:
    :param verbose:
    :return:
    """
    config_file = os.path.join(root, "models_config", name + ".json")
    model_root = os.path.join(root, 'models', name)

    if verbose:
        print("Removing model configuration:", config_file)
    # Remove the configuration file
    os.remove(config_file)

    if verbose:
        print("Removing model:", model_root)
    # Remove the model
    os.remove(model_root)


def remove_machine_learning_features_configuration(name, root, verbose=True):
    """
    Remove the configuration of a set of features for machine learning.
    :param name:
    :param root:
    :param verbose:
    :return:
    """
    config_file = os.path.join(root, "features_config", name + ".json")
    dataset_root = os.path.join(root, 'features', name)

    # Remove the configuration file
    os.remove(config_file)

    # Remove the directory containing the memory maps
    remove_dir(dataset_root, True, verbose)



def get_ml_features_config(path, export_mode=False):
    """
    Get the ``class:MLFeaturesConfig`` object corresponding to the provided file path.

    :param path: Path to the json storage file containing a ``class:MLFeaturesConfig`` object.
    :type path: ``str``
    :param export_mode: If True, operate in export mode
    :type export_mode: ``bool``
    :return: The corresponding ``class:MLFeaturesConfig`` object.
    :rtype: ``MLFeaturesConfig``
    """

    if export_mode:
        export = path
    else:
        export = None

    with open(path, 'r') as f:
        data_dict = json.load(f)

    return MLFeaturesConfig(name=data_dict['name'], root=data_dict['root'], use_existing=True, export_mode=export)


def change_experiment_configuration_root(config_file, new_root):
    """
    Change the configuration root of the experiment described in the configuration file

    :param config_file: Path to the configuration file to be edited
    :param new_root: New root that needs to be used inside the configuration file
    :return: None
    """
    with open(config_file, 'r') as f:
        data_dict = json.load(f)

    # Change storage information
    data_dict['config_root'] = config_file
    data_dict['root'] = os.path.join(new_root)

    data_dict['storage']['model_path'] = data_dict['name'] + '.model'
    data_dict['storage']['model_path'] = data_dict['name'] + '-best.model'

    data_dict['storage']['model_path'] = os.path.join(new_root, 'ml_data', 'models', data_dict['name'] + '.model')
    data_dict['best']['model'] = os.path.join(new_root, 'ml_data', 'models', data_dict['name'] + '-best.model')

    with open(config_file, 'w') as f:
        json.dump(data_dict, f)


def change_feat_configuration_root(config_file, new_root):
    """
    Change the configuration root of the machine learning features inside the configuration file.

    :param config_file: Path to the configuration file to be edited
    :param new_root: New root that needs to be used inside the configuration file
    :return: None
    """
    with open(config_file, 'r') as f:
        data_dict = json.load(f)

    # Change base storage information
    data_dict['root'] = os.path.join(new_root, 'ml_data')
    feat_config_file = os.path.split(data_dict['features_config'])[1]
    data_dict['features_config'] = os.path.join(new_root, 'features', 'config', feat_config_file)
    data_dict['config_root'] = os.path.join(data_dict['root'], 'features_config', data_dict['name'] + '.json')

    # Change memmap storage information
    for mode in data_dict['memmap_metadata'].keys():
        for label in data_dict['memmap_metadata'][mode].keys():
            old_path = data_dict['memmap_metadata'][mode][label][0]
            # Check if the old path contains header/payload/hepa
            if feature_strategy_config_from_path(data_dict['features_config']).featurestrategy == 'PCCN':
                new_path = os.path.join(new_root, 'ml_data', 'memmaps', data_dict['name'],
                                        split_path(old_path)[-3], mode, label + '-memmap.npy')
            else:
                new_path = os.path.join(new_root, 'ml_data', 'memmaps', data_dict['name'], mode, label + '-memmap.npy')
            data_dict['memmap_metadata'][mode][label][0] = new_path

    with open(config_file, 'w') as f:
        json.dump(data_dict, f)


class MLFeaturesConfig(Config):
    """
    Target of this class:
    Create machine learning input features from the features generated of raw data. This class is responsible for the
    splitting and memory mapping of data into training, validation and testing subgroups.
    Storage:
    root/features_config/name.json
    root/features/name/*

    :param name: Unique name of the configuration
    :param root: Path to the root directory (see description).
    :param features_config: Configuration of the features extracted from raw data
    :param use_existing: Flag indicating whether to use an existing configuration, if possible
    :param kwargs: TODO
    :type name: ``str``
    :type root: ``str``
    :type features_config: ``FeatureStrategyConfig``
    :type use_existing: ``bool``
    :type kwargs: ``dict``
    """

    def __init__(self, name, root, features_config=None, use_existing=True, export_mode=None, **kwargs):
        self.name = name
        self.root = root

        if export_mode is not None:
            self.config_root = export_mode
            self.export_mode = True
            self._get_existing_config()
            return

        self.export_mode = False

        # Root the configuration file will be stored at
        self.config_root = os.path.join(self.root, "features_config", self.name + ".json")

        exists = os.path.isfile(self.config_root)

        if use_existing and exists:
            self._get_existing_config()
            return
        else:
            if features_config is None:
                raise MLConfigurationConstructionException("No features configuration specified.")
            # If a configuration actually exists, it will not be used (otherwise _get_existing_config would have been
            # called). Therefore remove the existing configuration.
            if exists:
                remove_machine_learning_features_configuration(self.name, self.root, verbose=True)

        self.features_config = features_config
        self._initial_configuration(**kwargs)

    def _update_from_existing_config(self, config):
        """
        Update the data members of the object by reading their correct state
        from the stored configuration.

        Params:
        config(dict): Stored configuration dictionary
        """

        # General

        if self.export_mode:
            export_dir = os.path.split(os.path.realpath(self.config_root))[0]
            features_config_path = os.path.join(export_dir, config['features_config'])
            self.features_config = feature_strategy_config_from_path(features_config_path, self.export_mode)
        else:
            self.features_config = feature_strategy_config_from_path(config['features_config'], self.export_mode)

        # Splitting
        self.train = config['splitting']['train']
        self.valid = config['splitting']['valid']
        self.test = config['splitting']['test']
        self.shuffle = config['splitting']['shuffle']
        self.remove_source_files = config['splitting']['remove_source_files']

        self.completed_setup = config['completed_setup']
        self.memmap_metadata = config['memmap_metadata']
        #self.completed_splitting = config['splitting']['completed']

        self.label_mapping = config['label_mapping']

        self.byte_compression = config['byte_compression']
        # Memory mapping
        self.dtype = DataType[config['dtype']]
        self.features_metadata = config['features_metadata']
        #self.completed_memmap = config['memmap']['completed']

    def _initial_configuration(self, **kwargs):

        # Dataset splitting configuration
        self.train = kwargs.get('train', 0.0)
        self.valid = kwargs.get('valid', 0.0)
        self.test = kwargs.get('test', 0.0)
        self.shuffle = kwargs.get('shuffle', True)
        self.remove_source_files = kwargs.get('remove_source_files', False)

        self.completed_setup = False
        self.memmap_metadata = {
            usage:  {
                label: ("", (0,)) for label in self.features_config.dataset_config.get_labels(only_labelled_days=True)
            }
            for usage in ["train", "valid", "test"]
        }

        self.label_mapping = kwargs.get('label_mapping', {})
        self.byte_compression = kwargs.get('byte_compression', False)
        self.dtype = kwargs.get('dtype', DataType.UINT8)

        self.features_metadata = {
            'min': kwargs.get('min', 0),
            'max': kwargs.get('max', 1),
            'mean': kwargs.get('mean', 0),
            'std': kwargs.get('std', 1)
        }

        # Store the current, initialized configuration
        self._store_configuration()

    def _get_config_dict(self):
        return {
            'config_root': self.config_root,
            'name': self.name,
            'root': self.root,
            'features_config': self.features_config.config_root,
            'splitting': {
                'train': self.train,
                'valid': self.valid,
                'test': self.test,
                'shuffle': self.shuffle,
                'remove_source_files': self.remove_source_files,
                        },
            'completed_setup': self.completed_setup,
            'memmap_metadata': self.memmap_metadata,
            'label_mapping': self.label_mapping,
            'byte_compression': self.byte_compression,
            'dtype': self.dtype.name,
            'features_metadata': self.features_metadata,
        }
