from enum import Enum


class FeatureStrategy(Enum):
    PCCN = 1
    HAST_I = 2
    HAST_II = 3
    SIMPLE_VECTORS = 4
    SESSIONBASED = 5


class Dataset(Enum):
    CICIDS2017 = 1
    DARPA1998 = 2
    UNSW_NB15 = 3
    ISCX2012 = 4


class Model(Enum):
    PCCN = 1
    HAST_I = 2
    HAST_II = 3
    ResNet50 = 4
    GoogLeNet = 5
    SimpleNet = 6
    TiedSDA = 7
    TCN = 8
    PCCNN = 9
    QPPCCN = 10
    SimpleQPPCCN = 11
    BaseCNN1D = 12
    QBaseCNN1D = 13
    BaseCNN2D = 14
    QBaseCNN2D = 15
    QTestCNN2D = 16

class Optimizer(Enum):
    RMSprop = 1
    SGD = 2


class PacketSource(Enum):
    BINARY = 1
    BYTEARR = 2
    STRING = 3


class DataType(Enum):
    UINT8 = 1,
    FLOAT32 = 2,


class LinkLayer(Enum):
    ETHERNET = 1
    LCC = 2


class LabelStatus(Enum):
    MISSING_DIR = 1     # When target directory is missing
    MISSING_FILE = 2    # When target unlabelled file is missing
    INCOMPLETE = 3      # When unlabelled file does not contain sufficient packets
    VALID = 4           # If labelling went right