"""
This module contains functions that are generally helpful and that can be/are used at multiple places in the framework.
"""

import os
import numpy as np
from torch import float64, float32, float16, int64, int32, int16, int8
from enums import FeatureStrategy, Dataset, LinkLayer, PacketSource


def remove_dir(root, remove_root=True, verbose=True):
    """
    Remove target root directory. If remove_root is True, remove the contents of the root directory,
    and then the directory itself. Otherwise, only remove the content.

    :param root: Target root directory.
    :param remove_root: If True, remove the root directory too.
    :param verbose: If True, output the removed files and directories
    :type root: ``str``
    :type remove_root: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    # Remove the content of the directory
    __remove_dir_recursively(root, verbose)

    # If required, remove the root itself
    if remove_root:
        os.rmdir(root)


def __remove_dir_recursively(root, verbose=True):
    """
    Recursive function used to remove files and directories following ``remove_dir``.
    :param root: Root of which contents need to be removed.
    :param verbose: If True, output the removed files and directories
    :type root: ``str``
    :type verbose: ``bool``
    :return: None
    """
    # Get the content of the 
    for target in os.listdir(root):
        target_path = os.path.join(root, target)
        # If the target is a file, remove the file
        if os.path.isfile(target_path):
            if verbose: print("Removing", target_path)
            os.remove(target_path)
        elif os.path.isdir(target_path):
            # Empty the directory
            __remove_dir_recursively(target_path, verbose=verbose)
            # If it is empty, remove it
            if len(os.listdir(target_path)) < 1:
                if verbose:
                    print("Removing", target_path)
                os.rmdir(target_path)


def get_all_files_with_extension(dir_path, extension):
    """
    Get the filenames for all files in the target directory with the target extension.

    :param dir_path: Path to target directory
    :type dir_path: ``str``
    :param extension: File extension (EXCLUDING THE ".")
    :type extension: ``str``

    :return: List of filenames in the target directory with the target file extension
    :rtype: ``list`` of ``str``
    """
    filenames = []
    for file in os.listdir(dir_path):
        if file.split('.')[-1] == extension:
            filenames.append(file)
    return filenames


def remove_empty_dirs(root):
    """
    Remove any empty (sub)directory in the root directory. If any removed directory causes another directory to be empty
    , those newly empty directories will be removed as well. If the root directory is empty, it will also be removed.

    :param root: Path to root directory that is checked if it is empty.
    :type root: ``str``
    :return: True if the root directory is empty and was removed.
    :rtype: ``bool``
    """
    # Assume the directory is empty
    is_empty = True

    # inspect any content of the directory
    for d in os.listdir(root):
        target = os.path.join(root, d)

        # For each directory, check if it is empty
        if os.path.isdir(target):
            # If it is empty, it will be removed and can be disregarded
            if not remove_empty_dirs(target):
                # If it is not empty, this directory also is not empty
                is_empty = False
        # If this directory contains a file, it is not empty
        else:
            is_empty = False

    # If this directory is empty, remove it
    if is_empty:
        os.rmdir(root)

    return is_empty


def check_preliminaries(root):
    """
    Function that generates all required directories that must be present before usage of the framework.

    :param root: Path to root directory where everything will be stored.
    :type root: ``str``
    :return: tuple contraining paths to the datasets directory, the features directory an the machine learning \
    directory
    :rtype: ``tuple`` of ``str``
    """
    datasets = os.path.join(root, "datasets")
    datasets_config = os.path.join(datasets, "config")

    features = os.path.join(root, "features")
    features_config = os.path.join(features, "config")

    ml_data = os.path.join(root, "ml_data")
    ml_features_config = os.path.join(ml_data, "features_config")
    ml_models_config = os.path.join(ml_data, "models_config")
    ml_experiments = os.path.join(ml_data, "experiments")
    ml_data_models = os.path.join(ml_data, "models")
    ml_data_features = os.path.join(ml_data, "memmaps")
    ml_data_results = os.path.join(ml_data, "results")
    ml_data_results_text = os.path.join(ml_data_results, 'text')
    ml_data_results_json = os.path.join(ml_data_results, 'json')
    ml_data_results_cm = os.path.join(ml_data_results, 'cm')

    mkdir(datasets)
    mkdir(datasets_config)

    mkdir(features)
    mkdir(features_config)

    mkdir(ml_data)
    mkdir(ml_features_config)
    mkdir(ml_models_config)
    mkdir(ml_experiments)
    mkdir(ml_data_features)
    mkdir(ml_data_models)
    mkdir(ml_data_results)
    mkdir(ml_data_results_text)
    mkdir(ml_data_results_json)
    mkdir(ml_data_results_cm)

    return datasets, features, ml_data


def remove_preprocessed_dataset(ml_config):
    # Dataset data to remove
    dataset_root = ml_config.features_config.dataset_config.data_root
    dataset_config = ml_config.features_config.dataset_config.config_root

    # Features data to remove
    features_root = os.path.join(ml_config.features_config.features_root, ml_config.features_config.name)
    features_config = ml_config.features_config.config_root

    # ML features config:
    ml_config_root = os.path.join(ml_config.root, 'memmaps', ml_config.name)
    ml_config_config = ml_config.config_root

    # Remove everything:
    if os.path.isdir(dataset_root):
        print('Removing {}'.format(dataset_root))
        remove_dir(root=dataset_root, remove_root=True, verbose=False)
    if os.path.isfile(dataset_config):
        print('Removing {}'.format(dataset_config))
        os.remove(dataset_config)
    if os.path.isdir(features_root):
        print('Removing {}'.format(features_root))
        remove_dir(root=features_root, remove_root=True, verbose=True)
    if os.path.isfile(features_config):
        print('Removing {}'.format(features_config))
        os.remove(features_config)
    if os.path.isdir(ml_config_root):
        print('Removing {}'.format(ml_config_root))
        remove_dir(root=ml_config_root, remove_root=True, verbose=True)
    if os.path.isfile(ml_config_config):
        print('Removing {}'.format(ml_config_config))
        os.remove(ml_config_config)

def mkdir(path):
    """
    Function that creates the given directory, if it does not already exist.
    Otherwise, nothing happens.
    :param path: Path to the directory that needs to be created.
    :return: None
    """
    if not os.path.isdir(path):
        os.mkdir(path)


def _split_path_recursive(path, results):
    """
    Split the provided path into its different parts.

    :param path: Path to split
    :param results: Temporary list containing the current results
    :return: List containing the different parts of the path
    :rtype: ``list`` of ``str``
    """
    if path == "":
        return results
    else:
        path, item = os.path.split(path)
        if path == '/':
            return [path, item] + results
        else:
            return _split_path_recursive(path, [item] + results)


def split_path(path):
    """
    Split a path into its different parts, e.g. 'alpha/beta/gamma/delta.txt' will become
    ['alpha', 'beta', 'gamma', 'delta.txt'].

    :param path: The path to split
    :type path: ``str``
    :return: A list containing the different parts of the path
    :rtype: ``list`` of ``str``
    """
    return _split_path_recursive(path, [])


def _extract_final_number(string, length):
    """
    Extract the complete number at the end of a string.
    Examples:
    "a15bc123" returns "123"
    "123" returns "123"
    "abc" returns ""
    "abc123de" returns ""
    "" returns ""

    Params:
    :param string: String to extract final number from
    :type string: ``str``
    :param length: Length of the string
    :type length: ``int``
    :return: Number at the end of a string
    :rtype: ``str``
    """
    if(length == 0):
        return ""
    elif(string[length - 1].isdigit()):
        return _extract_final_number(string[:length - 1], length - 1) + string[length - 1]
    else:
        return ""


def _extract_file_position(filename, trailer):
    """
    Extract the trailing digits in a filename after removing a certain signature.
    Examples:
    _extract_file_position("filename12.csv", ".csv") returns 12
    _extract_file_position("filename0.csv", ".csv") returns 0
    _extract_file_position("filename-12.csv", ".csv") returns 12
    _extract_file_position("filename12.csv", "csv") returns -1
    _extract_file_position("filename", "") returns -1

    :param filename: Filename to extract the trailing digits (position) from
    :param trailer: Signature to remove
    :type filename: ``str``
    :type trailer: ``str``
    :return: Positive integer position, or -1 if no digits are present in the remaining filename
    or the last character after removing the trailer is no digit.
    :rtype: ``int``
    """
    filename = filename.rstrip(trailer)
    position_string = _extract_final_number(filename, len(filename))
    try:
        return int(position_string)
    except:
        return -1


def list_files_ordered_from_path(path, trailer, verbose=False):
    """
    Create list of files in a given directory, sorted based a number in the filename. It is assumed that by removing a signature of trailing characters
    from the filename, the last characters will be the position of the file.
    For example: [abc-5.csv, abc-0.csv, abc-1.csv] should be sorted as [abc-0.csv, abc-1.csv, abc-5.csv]

    :param path: Path to the directory containing the files that need to be ordered
    :param trailer: Trailer to excluded from a filename in order to obtain its number
    :param verbose: If True, potential output will be printed to the file
    :type path: ``str``
    :type trailer: ``str``
    :type verbose: ``bool``
    :return: List containing the ordered filenames.
    :rtype: ``list`` of ``str``
    """
    files = []
    # Get all (unsorted) files
    for f in os.listdir(path):
        extracted = _extract_file_position(f, trailer)
        if extracted != -1:
            files.append({'position': extracted, 'name': f})
        elif verbose:
            print("File " + f + " is ignored.")
    
    # Sort the files:
    files.sort(key=lambda fileinfo: fileinfo['position'])
    
    # Return sorted files
    return [fileinfo['name'] for fileinfo in files]


def _process_flow_tree(current_path, depth, position, action, verbose=True):
    """
    Function used to recursively walk through the flow tree.

    :param current_path: Current path in the flow tree.
    :param depth: Current depth in the flow tree. Starts at 0, flow files are located at 5.
    :param position: Current position in the flow tree, to visualize progress.
    :param action: Action to execute on each flow file. Refer to ``process_flow_tree`` for more information.
    :param verbose: If True, current location in the tree will be given at each step in the tree
    :return:
    """
    # Use the position to report the progress
    [[ai, a], [bi, b], [ci, c], [di, d], [ei, e]] = position
    
    if verbose and (depth > 4):
        try:
            percentage = ((ai - 1) / a) + ((bi - 1) / b) * (1 / a) + (ci / c) * (1 / b) * (1 / a)
            percentage = round(percentage * 100, 2)
        except ZeroDivisionError:
            percentage = 0.00
        print(str(percentage) + "%|" + str(ai) + "/" + str(a) + " - " + str(bi) +
              "/" + str(b) + " - " + str(ci) + "/" + str(c) + " - " + str(di) +
              "/" + str(d) + " - " + str(ei) + "/" + str(e) + "                      ", end="\r")

    if depth > 4:
        flow_files = os.listdir(current_path)
        for ff in flow_files:
            action["function"](ff, current_path, **action["params"])
    else:
        all_items = os.listdir(current_path)
        total = 0
        count = 0
        if verbose:
            # Only calculate if usefull
            total = len(all_items)

        for item in all_items:
            count += 1
            item_path = os.path.join(current_path, item)
            if os.path.isdir(item_path):
                # Update the position
                position[depth] = [count, total]
                # Process the item
                _process_flow_tree(current_path=item_path, position=position,
                                   depth=depth + 1, action=action, verbose=verbose)


def process_flow_tree(root, action, verbose=True):
    """
    Walk through the directory containing all sorted flows, and execution a certain action on each flow file.

    :param root: Root of the directory tree (path)
    :param action: dict containing function to execute, as well as additional parameters:\
    {"function": function_name, "params": {"param1":param1, "param2":param2, ...}}\
    Note that the function should have two parameters as the first two parameters:\
    The flow file to be processed (ff), as well as the path to the directory containing that file. These parameters\
    do not have to be provided in the action dict, but are provided internally in this function.\
    An example is: example_function(ff, path_to_file_dir, param1, param2, ...).\
    Note that any additional parameters (param1, param2, ...) are optional.
    :param verbose: Print detailed information, default True
    :type root: ``str``
    :type action: ``dict`` of {'function':``func``, 'params': ``dict``}

    :return: None
    """
    _process_flow_tree(current_path=root, position=[[0,0],[0,0],[0,0],[0,0],[0,0]], depth=0,
                        action=action, verbose=verbose)


def _relabel(flow_file, path_to_flow_file, dataset_config):
    """
    Relabel the provided flow file, if necessary, in correspondence with the provided dataset configuration.

    :param flow_file: Filename of the file under review
    :param path_to_flow_file: Path to the directory containing the flow file, not including  the flow file.
    :param dataset_config: Configuration of the dataset.
    :type flow_file: ``str``
    :type path_to_flow_file: ``str``
    :type dataset_config: ``dataset_config.DatasetConfig``
    :return: None
    """
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid labelled
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        return

    # Extract the actual label from the label part
    label = "-".join(flow_file_parts[0].split("-")[:-1])

    # Check if the label is valid
    if not label in dataset_config.get_labels():
        # If not, search for a label with which it correspondents
        new_label = None
        for ds_label in dataset_config.get_labels():
            if label.strip() in ds_label:
                # If correspondence was found, relabel
                new_label = ds_label
        if new_label is None:
            print('No new label found for', label, flow_file)
        else:
            # Rename the original flow file
            new_filename = new_label + '-' + flow_file_parts[0].split("-")[-1] + '_' + flow_file_parts[1]
            #print('Relabel', flow_file, '->', new_filename)
            os.rename(os.path.join(path_to_flow_file, flow_file), os.path.join(path_to_flow_file, new_filename))


def relabel_files(root, config, verbose=True):
    """
    Relabel applicable flow files in the flow tree at the provided root, for the provided dataset configuration.

    :param root: Path to root directory containing the flow tree
    :type root: ``str``
    :param config: Dataset configuration
    :type config: ``dataset_config.DatasetConfig``
    :param verbose: Print progress to output if True
    :return: None
    """
    # Run the relabelling process through the directory tree
    process_flow_tree(root, action={'function': _relabel, 'params': {'dataset_config': config}}, verbose=verbose)


def precedes(ms1, ns1, ms2, ns2):
    """
    Check if the first timestamp (consisting of miliseconds and nanosecond) precedes the second timestamp.

    :param ms1: MSB of first timestamp (or milliseconds for PCAP files)
    :param ns1: LSB of first timestamp (or nanoseconds for PCAP files)
    :param ms2: MSB of second timestamp (or milliseconds for PCAP files)
    :param ns2: LSB of second timestamp (or nanoseconds for PCAP files)
    :type ms1: ``int``
    :type ns1: ``int``
    :type ms2: ``int``
    :type ns2: ``int``
    :return: True if the first timestamp precedes the second timestamp, e.g. the first timestamp occured BEFORE \
    the second timestamp.
    :rtype: ``bool``
    """
    if ms1 < ms2:
        return True
    elif ms1 > ms2:
        return False
    else:
        # ms1 == ms2
        return (ns1 <= ns2)


def get_filesize(ifile):
    """
    Alias for os.path.getsize(path_to_input_file).

    Params:
    :param ifile: Path to input file that needs to be measured.
    :type ifile: ``str``

    :return: Filesize of input file in bytes.
    :rtype: ``int``
    """
    return os.path.getsize(ifile)


def remove_front_not_numbers(string):
    """
    Remove all front characters from a string until a number is encountered.

    :param string: String to remove characters from
    :type str: ``str``

    :return: String with leading non-numbers removed.
    :rtype: ``str``
    """
    size = len(string)
    while(True):
        if size < 1:
            break
        try:
            number = int(string[0])
            break
        except:
            string = string[1:]
            size -= 1

    return string


def is_even(n):
    """
    Function to check if an integer is even.
    :param n: Number to check
    :type n: ``int``
    :return: True if even, False if odd
    :rtype: ``bool``
    """
    return n % 2 == 0


def _operate_on_bytes(packet, action):
    """
    Operate on the bytes in the packet, executing the action on every byte.

    :param packet: Packet that bytes will be extracted from.
    :param action: Action that will be executed on every byte.
    :type packet: ``str``
    :type action: ``dict`` of {'function':``function``, 'params': ``dict`` of {'param1':..., 'param2':....}}

    :return: The number of bytes operated on
    :rtype: ``int``
    """
    n_bytes = 0
    for i, _ in enumerate(packet):
        if not is_even(i):
            action["function"](packet[i - 1] + packet[i], **action["params"])
            n_bytes += 1
    return n_bytes


def _operate_on_bytes_with_max(packet, action, max_n):
    """
    Operate on the bytes in the packet, executing the action on the first max_n bytes. The parameter action can provide
    additional arguments to use in the executed function.

    :param packet: Packet that bytes will be extracted from.
    :param action: Action that will be executed on every byte.
    :param max_n: Maximum number of bytes that will be operated on, optional.
    :type packet: ``str``
    :type action: ``dict`` of {'function':``function``, 'params': ``dict`` of {'param1':..., 'param2':....}}
    :type max_n: ``int``

    :return: The number of bytes operated on
    :rtype: ``int``
    """

    n_bytes = 0
    for i, _ in enumerate(packet):
        if (i / 2) >= max_n:
            return n_bytes
        if not is_even(i):
            action["function"](packet[i - 1] + packet[i], **action["params"])
            n_bytes += 1
    return n_bytes


def operate_on_bytes(packet, action, max_n=None):
    """
    Function to operate specified action on each byte in the provided network traffic packet.

    :param packet: Packet that bytes will be extracted from.
    :param action: Action that will be executed on every byte.
    :param max_n: Maximum number of bytes that will be operated on, optional.
    :type packet: ``str``
    :type action: ``dict`` of {'function':``function``, 'params': ``dict`` of {'param1':..., 'param2':....}}
    :type max_n: ``int``

    :return: The number of bytes operated on
    :rtype: ``int``
    """
    if max_n is None:
        return _operate_on_bytes(packet, action)
    else:
        return _operate_on_bytes_with_max(packet, action, max_n)


_iana_protcol_numbers = {
    'icmp': 1,          # Internet Control Message Protocol
    'igmp': 2,          # Internet Group Management Protocol
    'ggp': 3,           # Gateway-to-Gateway Protocol
    'tcp': 6,           # Transmission Control Protocol
    'cbt': 7,           # CBT
    'egp': 8,           # Exterior Gateway Protocol
    'igp': 9,           # any private interior gateway (used by Cisco for their IGRP)
    'bbn-rcc': 10,      # BBN RCC Monitoring
    'bbn-rcc-': 10,     # BBN RCC Monitoring
    'nvp': 11,          # NVP-II Network Voice Protocol
    'pup': 12,          # PUP
    'argus': 13,        # ARGUS, deprecated
    'emcon': 14,        # EMCON
    'xnet': 15,         # Cross Net Debugger
    'chaos': 16,        # Chaos
    'udp': 17,          # User Datagram Protocol
    'mux': 18,          # Multiplexing
    'dcn-meas': 19,     # DCN Measurement Subsystems
    'hmp': 20,          # Host Monitoring
    'prm': 21,          # Packet Radio Measurement
    'xns-idp': 22,      # XEROX NS IDP
    'trunk-1': 23,      # Trunk-1
    'trunk-2': 24,      # Trunk-2
    'leaf-1': 25,       # Leaf-1
    'leaf-2': 26,       # Leaf-2
    'rdp': 27,          # Reliable Data Protocol
    'irtp': 28,         # Internet Reliable Transaction
    'iso-tp4': 29,      # ISO Transport Protocol Class 4
    'netblt': 30,       # NETBLT Bulk Data Transfer Protocol
    'mfe-nsp': 31,      # MFE Network Services Protocol
    'merit-inp': 32,    # MERIT Internodal Protocol
    '3pc': 34,          # Third Party Connect Protocol
    'idpr': 35,         # Inter-Domain Policy Routing Protocol
    'xtp': 36,          # XTP
    'ddp': 37,          # Datagram Delivery Protocol
    'idpr-cmtp': 38,    # IDPR Control Message Transport Protocol
    'tp++': 39,         # TP++ Transport Protocol
    'il': 40,           # IL Transport Protocol
    'ipv6': 41,         # IPv6 encapsulation
    'sdrp': 42,         # Source Demand Routing Protocol
    'ipv6-route': 43,   # Routing Header for IPv6
    'ipv6-frag': 44,    # Fragment Header for IPv6
    'idrp': 45,         # Inter-Domain Routing Protocol
    'rsvp': 46,         # Reservation Protocol
    'gre': 47,          # Generic Routing Encapsulation
    'bna': 49,          # BNA
    'esp': 50,          # Encap Security Payload
    'i-nlsp': 52,       # Integrated Net Layer Security TUBA
    'swipe': 53,        # IP with encryption (deprecated)
    'narp': 54,         # NBMA Address Resolution Protocol
    'mobile': 55,       # IP Mobility
    'tlsp': 56,         # Transport Layer Security Protocol using Kryptonet key management
    'skip': 57,         # SKIP
    'ipv6-no': 59,      # IPv6-NoNxt: No Next Header for IPv6
    'ipv6-opts': 60,    # Destination Options for IPv6
    'cftp': 62,         # CFTP
    'sat-expak': 64,    # SATNET and Backroom EXPAK
    'kryptolan': 65,    # Kryptolan
    'rvd': 66,          # MIT Remote Virtual Disk Protocol
    'ippc': 67,         # Internet Pluribus Packet Core
    'sat-mon': 69,      # SAT-MON
    'visa': 70,         # VISA protocol
    'ipcv': 71,         # Internet Packet Core Utility
    'cpnx': 72,         # Computer Protocol Network Executive
    'cphb': 73,         # Computer Protocol Heart Beat
    'wsn': 74,          # Wang Span Network
    'pvp': 75,          # Packet Video Protocol
    'br-sat-mon': 76,   # Backroom SATNET Monitoring
    'sun-nd': 77,       # SUN ND PROTOCOL - Temporary
    'wb-mon': 78,       # WIDEBAND Monitoring
    'wb-expak': 79,     # WIDEBAND EXPAK
    'iso-ip': 80,       # ISO Internet Protocol
    'vmtp': 81,         # VMTP
    'secure-vmtp': 82,  # SECURE-VMTP
    'vines': 83,        # VINES
    'ttp': 84,          # Transaction Transport Protocol
    'nsfnet-igp': 85,   # NSFNET-IGP
    'dgp': 86,          # Dissimilar Gateway Protocol
    'tcf': 87,          # ...
    'eigrp': 88,        # EIGRP
    'ospf': 89,         # OSPFIGP: Open Shortest Path First Interior Gateway Protocol
    'sprite-rpc': 90,   # Sprite RPC Protocol
    'mtp': 92,          # Multicast Transport Protocol
    'lapr': 91,         # Locus Address Resolution Protocol
    'ax.25': 93,        # AX.25 Frames
    'ipip': 94,         # Ip-within-IP Encapsulation Protocol
    'micp': 95,         # Mobile Internetworking Control Pro.
    'etherip': 97,      # Ethernet-within-IP Encapsulation
    'encap': 98,        # Encapsulation Header
    'gmtp': 100,        # GMTP
    'ifmp': 101,        # Ipsilon Flow Management Protocol
    'pnni': 102,        # PNNI over IP
    'pim': 103,         # Protocol Independent Multicast
    'aris': 104,        # ARIS
    'scps': 105,        # SCPS
    'qnx': 106,         # QNX
    'a/n': 107,         # Active Networks
    'ipcomp': 108,      # IP Payload Compression Protocol
    'snp': 109,         # Sitara Networks Protocol
    'compaq-peer': 110, # Compaq Peer Protocol
    'ipx-n-ip': 111,    # IPX in IP
    'vrrp': 112,        # Virtual Router Redundancy Protocol
    'pgm':  113,        # PGM Reliable Transport Protocol
    'l2tp': 115,        # Layer Two Tunneling Protocol
    'ddx': 116,         # D-II Data Exchange
    'iatp': 117,        # Interactive Agent Transfer Protcol
    'stp': 118,         # Schedule Transfer Protocol
    'srp': 119,         # SprectraLink Radio Protocol
    'uti': 120,         # UTI
    'smp': 121,         # Simple Message Protocol
    'sm': 122,          # Simple Multicast Protocol (deprecated)
    'ptp': 123,         # Performance Transparency Protocol
    'isis': 124,        # ISIS over IPv4
    'fire': 125,        # ?
    'crtp': 126,        # Combat Radio Transport Protocol
    'crudp': 127,       # Combat Radio User Datagram
    'iplt': 129,        # IPLT
    'sps': 130,         # Secure Packet Shield
    'pipe': 131,        # Private IP Encapsulation within IP
    'sctp': 132,        # Stream Control Transmission Protocol
    'fc': 133,          # Fibre channel
    'unas': 144,        # UNASSIGNED: NUMBERS 144-252
    'aes-sd3-d': 256,   # ?, custom number, is not on IANA list
    'any': 257,         # ?, custom number, is not on IANA list
    'arp': 258,         # Address resolution protocol, is not on IANA list
    'dcn': 259,         # ?, custom number, is not on IANA list
    'ib': 260,          # ?, custom number, is not on IANA list
    'ip': 261,          # ?, custom number, is not on IANA list
    'ipnip': 262,       # ?, custom number, is not on IANA list
    'mhrp': 263,        # Multi-Path Hybrid Routing Protocol, not on IANA list
    'pri-enc': 264,     # ?, custom number, is not on IANA list
    'rtp': 265,         # ?, ...
    'sccopmce': 266,    # ?, ...
    'sep': 267,         # ?, ...
    'st2': 268,         # ?, ...
    'udt': 269,         # ?, ...
    'zero': 270,        # ?, ...

}


def get_protcol_number(protocol_name):
    """
    For a given protocol name, get the corresponding IANA number. Some protocols not listed in IANA
    have been added with custom numbers.

    :param protocol_name: Protocol name in lowercase.
    :type protocol_name: ``str``
    :return: Corresponding protocol number
    :rtype: ``str``
    """
    return str(_iana_protcol_numbers[protocol_name])


def is_in_interval(packet_string, flow):
    """
    For a given packet string and a given flow, check if the packet was transmitted in the timeframe of the flow.

    :param packet_string: Packet string containing the seconds and nanoseconds timestamp as well as the hexadecimal \
    packet string. Must be of the form 'ts_seconds,ts_nanoseconds,packet'
    :param flow: Flow for which the timeframe must be checked
    :type packet_string: ``str``
    :type flow: subclass of ``Flow``
    :return: True if the packet lies in the timeframe
    :rtype: bool
    """
    ts1, ts2, _ = packet_string.split(',')
    ts1 = int(ts1)
    ts2 = int(float(ts2))
    return flow.is_in_timeframe(ts_sec=ts1, ts_ns=ts2)


def dict_numpy_to_python(dictionary):
    """
    For the given dictionary, turn each numpy data type into its corresponding Python data type.
    :param dictionary: Input dictionary for which the data types will be transformed
    :rtype dictionary: ``dict``
    :return: None
    """
    for key, item in dictionary.items():
        _dict_numpy_to_python(dictionary, key, item)


def _dict_numpy_to_python(base, key, item):
    """
    Turn the numpy numbers in the provided item of the provided base dictionary with the corresponding provided key into
     Python numbers.
    :param base: Base dictionary containing the provided item for the provided key.
    :param key: Key corresponding with the provided item
    :param item: Provided item containing (a) number with a Numpy type.
    :type base: ``dict``
    :type key: ``str``
    :type item: ``dict`` or ``number``
    :return: None
    """
    if isinstance(item, dict):
        for next_key, next_item in item.items():
            _dict_numpy_to_python(item, next_key, next_item)
    else:
        base[key] = numpy_to_python_data_type(item)


def numpy_to_python_data_type(data):
    """
    Transform a Numpy data type into a regular data type serializable by JSON.

    :param data: Data to have its type transformed if possible
    :type data: ``numpy.int64`` or ``numpy.float64`` will be transformed
    :return: Transformed data if applicable, unchanged input data otherwise
    :rtype: ``int`` for ``numpy.int64``, ``float`` for ``numpy.float64``
    """
    if isinstance(data, np.int64):
        return int(data)
    elif isinstance(data, np.float64):
        return float(data)
    else:
        return data


def ohe_vector_to_byte(array, axis):
    """
    Turn ... x 256 x ... matrix into ... x 1 x .... matrix.\
    The assumption is made that the provided axis is one-hot encoded, implying that\
    all elements of a 1D-vector along that axis are 0 except for one ([0, 0, 0, ..., 1, ..., 0]).\
    This one-hot encoded vector is compressed to its representing integer, e.g.:\
    [1, 0, 0, 0] => [0]\
    [0, 1, 0, 0] => [1]\
    [0, 0, 1, 0] => [2]\
    [0, 0, 0, 1] => [3]\

    :param array: Matrix containing one-hot encoded byte vectors that should be compressed to their representing byte.
    :param axis: The axis along which the one-hot encoded vectors exists.
    :type array: ``numpy.ndarray``
    :type axis: ``int``
    :return: The provided matrix with each one-hot encoded vector along the provided axis compressed to a scalar \
    between 0 and 256.
    :rtype: ``numpy.ndarray``
    """

    return np.apply_along_axis(func1d=_compress_ohe_to_scalar, axis=axis, arr=array)


def _compress_ohe_to_scalar(array1d):
    """
    Compress the provided One-Hot Encoded vector to a scalar value.
    The one-hot encoded vector is compressed to its representing integer, e.g.:
    [1, 0, 0, 0] => [0]
    [0, 1, 0, 0] => [1]
    [0, 0, 1, 0] => [2]
    [0, 0, 0, 1] => [3]

    :param array1d: One-Hot Encoded vector.
    :type array1d: ``numpy.ndarray``
    :return: 1 x 1 array containing the scalar value
    :rtype: ``numpy.ndarray``
    """
    # This works because matrix multiplication with dimension (1 x n ) * (n x 1) results in a (1 x 1) matrix.
    return np.matmul(array1d, np.arange(256))


def extract_hast_ohe_matrix(vector):
    """
    Turn the provided vector with values between 0 and 255 into a One-Hot Encoded matrix containing the OHE-vectors
    corresponding with each value in the provided vector.

    :param vector: Provided (n x 1) vector.
    :type vector: ``numpy.ndarray``
    :return: Corresponding (256 x n) OHE array
    :rtype: ``numpy.ndarray``
    """
    # input: vector of size n
    # output: matrix of size 256 x n
    output = np.zeros((256, vector.shape[0]))

    for i in range(vector.shape[0]):
        output[:, i] = ohe_byte(vector[i])

    return output


def ohe_byte(byte):
    """
    One-hot encode the input byte into a 256-dimensional vector

    :param byte: Byte to be one-hot encoded
    :type byte: ``int``
    :return: one-hot encoded 256D vector with '1' corresponding to the value of the byte
    :rtype: ``Numpy Array``
    """
    vector = np.zeros(256)
    vector[byte] = 1
    return vector


def num_parameters_to_storage_size(numel, dtype):
    """
    For the given number of elements of the given data type, calculate the number of storage bytes required.

    :param numel: Number of parameters.
    :param dtype: Datatype of the parameters
    :type numel: ``int``
    :type dtype: ``torch.dtype``
    :return: Required number of storage bytes for the specified number of elements of the specified data type.
    :rtype: ``int``
    """
    if (dtype == float64) or (dtype == int64):
        nbpp = 8
    elif (dtype == float32) or (dtype == int32):
        nbpp = 4
    elif (dtype == float16) or (dtype == int16):
        nbpp = 2
    elif dtype == int8:
        nbpp = 1
    else:
        raise NotImplementedError('Unrecognized data type', dtype)

    return nbpp * numel


def format_storage_size(n_bytes, base=1024):
    """
    Express the provided number of bytes in a readable format.

    :param n_bytes: Number of bytes.
    :param base: Base that will be used for size calculation, typically 1000 or 1024. Default is binary base (1024).
    :type n_bytes: ``int``
    :type base: ``int``
    :return: String giving the number of GB, MB, kB, B, depending on what is most suitable
    :rtype: ``str``
    """
    if n_bytes > pow(base, 3):
        label = 'GB'
        size = n_bytes / pow(base, 3)
    elif n_bytes > pow(base, 2):
        label = 'MB'
        size = n_bytes / pow(base, 2)
    elif n_bytes > pow(base, 1):
        label = 'kB'
        size = n_bytes / pow(base, 1)
    else:
        label = 'B'
        size = n_bytes
    return '{} {}'.format(np.round(size, 3), label)


def format_macs(macs):
    """
    Express the provided number of Multiply-and-ACcumulate operations more legible.

    :param macs: Number of bytes.
    :type macs: ``int``
    :return: String giving the number of GMACs, MMACs, kMACs, MACs, depending on what is most suitable
    :rtype: ``str``
    """
    base = 1000
    if macs > pow(base, 3):
        label = 'GMACs'
        size = macs / pow(base, 3)
    elif macs > pow(base, 2):
        label = 'MMACs'
        size = macs / pow(base, 2)
    elif macs > pow(base, 1):
        label = 'kMACs'
        size = macs / pow(base, 1)
    else:
        label = 'MACs'
        size = macs
    return '{} {}'.format(np.round(size, 3), label)


def decode_tensor(tensor, feature_strategy):
    """
    UNDER CONSTRUCTION
    Decode a provided tensor and extract the packet(s) information that was used to construct this vector. The result is
    printed as output.

    :param tensor: PyTorch tensor that containing features generated by the specified feature extraction strategy.
    :param feature_strategy: Feature extraction strategy used to generate the features contained in the tensor.
    :type tensor: ``torch.Tensor``
    :type feature_strategy: ``FeatureStrategy``
    :return: None
    """
    if feature_strategy == FeatureStrategy.HAST_II:
        _decode_HASTII(tensor)


def _decode_HASTII(tensor):
    """
    Decode a tensor containing features generated by the HAST-II feature extraction strategy and print the result as
    output.
    :param tensor: PyTorch tensor.
    :type: ``torch.tensor``
    :return: None
    """
    # We only process the first flow in the batch
    flow = tensor[0].numpy()
    # Display each 256 x 100 image as its corresponding bytes
    packets = []
    # sums = []
    # for vector in ohe_vector_to_byte(flow, axis=1):
    #     if np.sum(vector) > 0:
    #         sums.append(vector)
    # if len(sums) > 1:
    #     print('Ik heb er eentje\nIk heb ene gevonden',vector)

    for vector in ohe_vector_to_byte(flow, axis=1):
        # Vector is 100x1 vector, containing the first 100 bytes of a network packet
        # Cast vector to int
        vector = vector.astype('int')
        packets.append(''.join(["{:0{size}X}".format(vector_el, size=2) for vector_el in vector]))

    print('\n'.join([packet for packet in packets]), end='')


def shape_to_str(shape):
    """
    Turn a shape tuple into a string representation.

    :param shape: The shape tuple
    :type shape: ``tuple`` of ``int``
    :return: String representation
    :rtype: ``str``
    """
    return '_'.join([str(i) for i in shape])


def str_to_shape(shape_str):
    """
    Turn a string representing a shape back into the original shape.

    :param shape_str: String representing a shape tuple
    :type shape_str: ``str``
    :return: Original shape tuple
    :rtype: ``tuple`` of ``int``
    """
    return tuple([int(i) for i in shape_str.split('_')])


def get_bytes_from_string(string):
    for position in range(0, len(string) - 1, 2):
        yield int(string[position:position+2], 16)


def npz_to_memmap(in_path, out_path):
    with np.load(in_path) as archive:
        arrays = archive.files
        n = len(arrays)
        base_shape = archive[arrays[0]].shape
        new_arr = np.zeros(tuple([n] + list(base_shape)))

        for i in range(n):
            new_arr[i] = archive[arrays[i]]
        memmap = np.memmap(out_path, mode="w+", shape=tuple([n] + list(base_shape)), dtype=np.uint8)
        memmap[:] = new_arr


def npz_to_memmap_tree(root, reporting='', verbose=True):
    dir_items = os.listdir(root)
    n = len(dir_items)

    for i in range(n):
        item = dir_items[i]
        target_path = os.path.join(root, item)

        # If the item is a directory, progress down the tree
        if os.path.isdir(target_path):
            if verbose:
                reporting += '{}/{} - '.format(i, n)
            npz_to_memmap_tree(target_path, reporting=reporting, verbose=verbose)

        # If the item is a file, process the file
        elif os.path.isfile(target_path):
            if verbose:
                print(reporting + '{}/{}'.format(i, n), end='\r')
            if item[-3:] == 'npz':
                new_item = item.rstrip('npz') + 'npy'
                new_path = os.path.join(root, new_item)
                npz_to_memmap(target_path, new_path)
                os.remove(target_path)


def conv1d_output_size(input_size, filter_size, padding, stride):
    """
    Calculate the output size of a feature vector going through a 1D convolutional layer

    :param input_size: Size of the input feature vector
    :type input_size: ``int``
    :param filter_size: Size of the filter
    :type filter_size: ``int``
    :param padding: Number of padded zeros on both sides of the feature vector
    :type padding: ``int``
    :param stride:Stride of the filter
    :type stride: ``int``
    :return: Output size of the feature vector
    :rtype: ``int``
    """
    return int(np.floor((input_size - filter_size + 2 * padding) / stride) + 1)


def latexify(string):
    """
    Format the input string for latex.

    :param string: ``str``
    :return:
    """
    output = ''
    for char in string:
        if char == '_':
            output += '\\_'
        else:
            output += char

    return output


class StringBytes:
    """
    This class implements iterator and slicing capabilities for a string representing bytes. When slicing or iterating,\
    every two characters in the string will be treated as a single byte and returned as an integer. While a string is \
    required as initial input, the output is always a number. This allows for easy interfacing the string as if it were\
     a bytearray. While interfacing, use indices as if this is a bytearray, or an array of integers.
    Note that stepsizes different from 1 are not supported for slicing.

    """

    def __init__(self, string):
        self.string = string
        self.size = len(string)
        self.position = 0

    def __iter__(self):
        return self

    def __len__(self):
        return self.size / 2

    def __next__(self):
        if self.position < (self.size - 1):
            val = int(self.string[self.position:self.position + 2], 16)
            self.position += 2
            return val
        else:
            raise StopIteration

    def __getitem__(self, item):
        # If the provided item is a slice-object, we implement slicing
        if isinstance(item, slice):
            # Step sizes different from 1 are not supported
            if item.step is not None:
                raise NotImplementedError()
            # If no starting index is given, assume 0
            if item.start is None:
                start = 0
                stop = item.stop
            # If no stopping index is given, assume the the end of the array
            elif item.stop is None:
                start = item.start
                stop = self.size // 2
            else:
                start = item.start
                stop = item.stop
            # Account for negative indices
            if start < 0:
                start = (self.size + 2 * start) // 2  # Start is negative
            if stop < 0:
                stop = (self.size + 2 * start) // 2   # Stop is negative
            # print(start, stop)
            # Calculate slicing range, required for bit shifting if multiple bytes are involved
            slice_range = stop - start - 1
            total = 0
            for i in range(start*2, stop*2, 2):
                # Shifting:
                # If more than one bytes is required, the slice_range is > 0.
                # In that case, the bytes other than the least significant byte need to be shifted
                # Example
                # If the selected bytes are [01, 01, 01], the slicing range is 2
                # The result then is: (1 << 2 * 8) + (1 << 1 * 8) + (1 << 0 * 8) = 65793 (= 0b000000010000000100000001)
                # This effect is obtained in the for-loop, decrementing slice_range each time
                total += int(self.string[i: i + 2], 16) << (slice_range * 8)
                slice_range -= 1
            return total
        # Otherwise, the item is a singular index to get one item
        else:
            # Account for negative indices
            if item < 0:
                item = (self.size + 2 * item) // 2
            return int(self.string[item*2: item*2+2], 16)

    def next(self, numbytes):
        """
        Get the next n bytes as a single value.

        :param numbytes: The number of bytes requested
        :type numbytes: ``int``
        :return: Single number representing the bytes
        :rtype: ``int``

        """
        if numbytes < 1:
            raise IndexError("Number of bytes must be a positive integer, not {}".format(numbytes))

        elif (self.position + 2 * numbytes) <= self.size:
            total = 0
            for i in range(numbytes - 1, -1, -1):
                # Get the value of the byte
                val = int(self.string[self.position:self.position + 2], 16)
                self.position += 2
                # Add the byte to the total
                total += (val << (8 * i))
            return total
        else:
            raise IndexError

    def seek(self, offset, whence=0):
        """
        Move position of the position pointer for `offset` bytes.

        :param offset: Offset that the pointer needs to be moved.
        :param whence: If 0, move relative from the start. If 1, move relative from the current position.\
        If 2, move relative from the end. Default 0.
        :type offset: ``int``
        :type whence: ``int``
        :return: None
        """
        if whence == 0:
            self.position = 2 * offset
        elif whence == 1:
            self.position += (offset * 2)
        elif whence == 3:
            self.position = ((self.size - 1) + (offset * 2))
        else:
            raise NotImplementedError

    def rem_bytes(self):
        """
        Get the number of remaining bytes based on the current value of the position pointer.

        :return: The number of remaining bytes
        :rtype: ``int``
        """
        return (self.size - self.position) // 2


if __name__ == '__main__':
    pass
