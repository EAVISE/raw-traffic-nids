from protocol_stack.base import Protocol
from protocol_stack.transport_layer_protocols import TCP, UDP

from parsers.layer3_protocols import IPv4 as IPv4Parser

from enums import PacketSource


class IPv4(Protocol):

    name = 'IPv4'

    def __init__(self, source=PacketSource.BINARY, **kwargs):
        if source == PacketSource.BINARY:
            raw_data = kwargs.get('raw_data')
            byteorder = kwargs.get('byteorder')
            parser = IPv4Parser(raw_data, byteorder)

            self.version = parser.version  # should always be 4
            self.ihl = parser.ihl  # Internet Header Length

            self.dscp = parser.dscp
            self.ecn = parser.ecn

            self.total_length = parser.total_length
            self.id = parser.id

            self.flags = parser.flags
            self.res = (self.flags >> 2) & 1  # Reserved, must be 0
            self.df = (self.flags >> 1) & 1  # Don't Fragment
            self.mf = (self.flags >> 0) & 1  # More Fragment
            self.frag = parser.frag

            self.ttl = parser.ttl
            self.proto = parser.proto
            self.chks = parser.chks
            self.src_addr = parser.src_addr
            self.dst_addr = parser.dst_addr

            self.options = parser.options

            if self.options == 0:
                raw_data = raw_data[20:]
            else:
                raw_data = raw_data[self.ihl * 4:]

            self.next_layer = self._next_layer_protocol()(source=source, raw_data=raw_data, byteorder=byteorder)

        elif source == PacketSource.BYTEARR:
            stringbytes = kwargs.get('stringbytes')

            first_byte = next(stringbytes)
            self.version = (first_byte & 240) >> 4  # should always be 4
            self.ihl = first_byte & 15              # Internet Header Length

            second_byte = next(stringbytes)
            self.dscp = (second_byte >> 2) & 63  # Differentiated Services Code Point
            self.ecn = second_byte & 3           # Explicit Congestion Notification

            self.total_length = stringbytes.next(2)  # Total length
            self.id = stringbytes.next(2)            # Identification

            flag_frag = stringbytes.next(2)
            self.flags = (flag_frag >> 14) & 3  # Flags
            self.res = (self.flags >> 2) & 1    # Reserved, must be 0
            self.df = (self.flags >> 1) & 1     # Don't Fragment
            self.mf = (self.flags >> 0) & 1     # More Fragment
            self.frag = flag_frag & 16383  # Fragmentation offset

            self.ttl = next(stringbytes)         # Time To Live
            self.proto = next(stringbytes)       # Protocol
            self.chks = stringbytes.next(2)      # Header Checksum

            self.src_addr = stringbytes.next(4)  # Source address
            self.dst_addr = stringbytes.next(4)  # Destination address

            if self.ihl > 5:
                self.options = stringbytes.next((self.ihl - 5) * 4)
            else:
                self.options = None

            self.next_layer = self._next_layer_protocol()(source=source, stringbytes=stringbytes)

    def _next_layer_protocol(self):
        if self.proto == 6:
            return TCP
        elif self.proto == 17:
            return UDP
        else:
            return None

    @staticmethod
    def format_address(address):
        return "{}.{}.{}.{}".format((address >> 24) & 255,
                                    (address >> 16) & 255,
                                    (address >> 8) & 255,
                                    (address >> 0) & 255)

    def __str__(self):
        first_line = '{}-layer: : {} -> {}'.format(self.name, self.format_address(self.src_addr),
                                                   self.format_address(self.dst_addr))
        second_line = '   Protocol: {} ({})'.format(self.proto, self.next_layer.name)
        third_line = '   Version: {}, Header Length: {}, DSCP: {}, ECN: {}'.format(self.version, self.ihl,
                                                                                   self.dscp, self.ecn)
        fourth_line = '   Total length: {}, Identification: {}, Fragmentation offset: {}'.format(self.total_length,
                                                                                                 self.id, self.frag)
        fifth_line = '   FLAGS: Reserved: {}, Don\'t Fragment: {}, More Fragments: {}'.format(self.res,
                                                                                              self.df,
                                                                                              self.mf)
        sixth_line = '   Time To Live: {}, Header Checksum: {}'.format(self.ttl, self.chks)
        seventh_line = '   Options: {}'.format(self.options)
        return '\n'.join([first_line, second_line, third_line, fourth_line, fifth_line, sixth_line, seventh_line])


class IPv6(Protocol):
    pass


class ICMP(Protocol):
    def __init__(self, source=PacketSource.BINARY, **kwargs):
        if source == PacketSource.BINARY:
            raw_data = kwargs.get('raw_data')
            byteorder = kwargs.get('byteorder')
            #TODO
            #parser = IPv4Parser(raw_data, byteorder)

            self.source = source
            self.data = raw_data[8:]

        elif source == PacketSource.BYTEARR:
            stringbytes = kwargs.get('stringbytes')

            self.type = next(stringbytes)
            self.code = next(stringbytes)
            self.chks = stringbytes.next(2)
            self.rest = stringbytes.next(4)

            self.source = source
            self.data = stringbytes
