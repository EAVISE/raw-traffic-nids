from enums import LinkLayer
from protocol_stack.link_layer_protocols import Ethernet, LinuxCookedCapture
from protocol_stack.internet_layer_protocols import IPv4, IPv6, ICMP
from protocol_stack.transport_layer_protocols import TCP, UDP


class Packet:

    def __init__(self, source, link_layer, **kwargs):
        if link_layer == LinkLayer.ETHERNET:
            self.ll = Ethernet(source=source, **kwargs)
        elif link_layer == LinkLayer.LCC:
            self.ll = LinuxCookedCapture(source=source, **kwargs)

    def get_layer(self, num=0):
        """
        Get the protocol of the specified layer for this packet, if applicable.
        Current supported protocols:

        Layer 0:
        - Ethernet
        - LCC/SSL (Linux Cooked Capture)

        Layer 1:
        - IPv4
        - IPv6
        - ICMP

        Layer 3:
        - TCP
        - UDP

        :param num: The number of the layer. For example, 0 will return the Ethernet/LCC layer.
        :type num: ``int``
        :return: The protocol of the specified layer.
        :rtype: ``protocol_stack.base.Protocol`` or ``Ethernet`` or ``LinuxCookedCapture`` or ``IPv4`` or ``IPv6`` or \
        ``ICMP`` or ``TCP`` or ``UDP``
        """
        return self.__get_layer(num=num, layer=self.ll)

    def __get_layer(self, num, layer):
        if num == 0:
            return layer
        else:
            return self.__get_layer(num=num - 1, layer=layer.get_next_layer())

    def contains_layer_type(self, name):
        """
        Check if the packet contains the specified protocol. Currently supported protocols:

        Layer 0:
        - Ethernet
        - LCC/SSL (Linux Cooked Capture)

        Layer 1:
        - IPv4 (aka IP)
        - IPv6
        - ICMP

        Layer 3:
        - TCP
        - UDP

        The names can be provided in any combination of uppercase and lowercase characters

        :param name: The name of the specified protocol in lowercase or uppercase.
        :type name: ``str``

        :return: True if the packet contains the protocol. If an unsupported protocol is specified, the result will \
        always be False.
        :rtype: ``bool``
        """
        if name.lower() == 'tcp':
            return isinstance(self.get_layer(2), TCP)
        elif name.lower() == 'udp':
            return isinstance(self.get_layer(2), UDP)
        elif (name.lower() == 'ipv4') or (name.lower() == 'ip'):
            return isinstance(self.get_layer(1), IPv4)
        elif name.lower() == 'ipv6':
            return isinstance(self.get_layer(1), IPv6)
        elif name.lower() == 'icmp':
            return isinstance(self.get_layer(1), ICMP)
        elif (name.lower() == 'lcc') or (name.lower() == 'ssl'):
            return isinstance(self.get_layer(0), LinuxCookedCapture)
        elif name.lower() == 'ethernet':
            return isinstance(self.get_layer(0), Ethernet)
        else:
            return False

    def get_transport_layer_protocol(self):
        """
        Get the transport layer protocol used in this packet.

        :return: 'tcp' or 'udp'
        :rtype: ``str``
        """
        if isinstance(self.get_layer(2), TCP):
            return 'tcp'
        elif isinstance(self.get_layer(2), UDP):
            return 'udp'

    def get_source_port(self, protocol=None):
        """
        If the transport layer protcol is TCP or UDP, get the source port of the packet.

        :arg protocol: Transport layer protocol used in the packet in LOWERCASE. If not specified, it will be looked up\
        inside the packet.
        :type protocol: ``str``
        :return: Source port of the packet if TCP or UDP, or 'None' otherwise
        :rtype: ``int``
        """
        if protocol is None:
            protocol = self.get_transport_layer_protocol()
        if protocol == 'tcp' or protocol == 'udp':
            return self.get_layer(2).src_port
        else:
            return None

    def get_destination_port(self, protocol=None):
        """
        If the transport layer protcol is TCP or UDP, get the destination port of the packet.

        :arg protocol: Transport layer protocol used in the packet in LOWERCASE. If not specified, it will be looked up\
        inside the packet.
        :type protocol: ``str``
        :return: Destination port of the packet if TCP or UDP, or 'None' otherwise
        :rtype: ``int``
        """
        if protocol is None:
            protocol = self.get_transport_layer_protocol()
        if protocol == 'tcp' or protocol == 'udp':
            return self.get_layer(2).dst_port
        else:
            return None

    def get_icmp_metadata(self):
        """
        If applicable, get the ICMP type and code of the packet. If the packet does not contain an ICMP header, return
        None.

        :return: ICMP type and code if applicable, or twice 'None' otherwise
        :rtype: ``tuple`` of (``int``, ``int``) or `tuple`` of (None, None)
        """
        if self.contains_layer_type('icmp'):
            icmp = self.get_layer(1)
            return icmp.type, icmp.code
        else:
            return None, None

    def get_num_payload_bytes(self):
        """
        Get the number of bytes in the payload of the packet. The payload in this case is defined as the application
        layer of the packet, namely everything beyond the transportation layer.

        :return: The number of payload bytes.
        :rtype: ``int``
        """
        return self.get_layer(2).get_application_layer_size()

    def get_payload(self):
        """
        Get the the application layer following the transport layer.

        :return: The number of payload bytes, as well as a list containing all payload bytes
        :rtype: (``int``, ``list``)
        """
        return self.get_layer(2).get_application_layer()
