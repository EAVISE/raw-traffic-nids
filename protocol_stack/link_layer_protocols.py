from protocol_stack.base import Protocol
from parsers.layer1_2_protocols import Ethernet as EthernetParser
from parsers.layer1_2_protocols import LinuxCookedCapture as LCCParser
from parsers.constants import ARPHRD_IEEE80211_RADIOTAP, ARPHRD_IPGRE, ARPHRD_NETLINK
from exceptions import NotImplementedException
from enums import PacketSource

from protocol_stack.internet_layer_protocols import IPv4

from help_functions import StringBytes


class Ethernet(Protocol):

    name = 'Ethernet'

    def __init__(self, source=PacketSource.BINARY, **kwargs):
        if source == PacketSource.BINARY:
            byteorder = kwargs.get('byteorder')
            parser = EthernetParser(kwargs.get('raw_data'), byteorder)
            self.mac_dst = parser.mac_dst
            self.mac_src = parser.mac_src
            self.ethertype = parser.ethertype
            raw_data = parser.raw_data[14:]

            self.next_layer = self._next_layer_protocol()(source=source, raw_data=raw_data, byteorder=byteorder)

        elif source == PacketSource.STRING:
            stringbytes = StringBytes(kwargs.get('packet_string'))
            self.mac_dst = stringbytes.next(6)
            self.mac_src = stringbytes.next(6)
            self.ethertype = stringbytes.next(2)

            #print(self._next_layer_protocol())
            self.next_layer = self._next_layer_protocol()(source=PacketSource.BYTEARR, stringbytes=stringbytes)

        elif source == PacketSource.BYTEARR:
            stringbytes = kwargs.get('stringbytes')
            self.mac_dst = stringbytes.next(6)
            self.mac_src = stringbytes.next(6)
            self.ethertype = stringbytes.next(2)

            self.next_layer = self._next_layer_protocol()(source=source, stringbytes=stringbytes)

    def __str__(self):
        l_name = super().__str__()
        return '{}: {:012X} -> {:012X}, type: {}'.format(l_name, self.mac_src, self.mac_dst, self.ethertype)

    def _next_layer_protocol(self):
        if self.ethertype == 0x0800:
            return IPv4
        elif self.ethertype == 0x0806:
            # ARP
            return None
        elif self.ethertype == 0x86DD:
            # IPv6
            return None
        else:
            return None


class LinuxCookedCapture(Protocol):

    name = 'LinuxCookedCapture'

    def __init__(self, source=PacketSource.BINARY, **kwargs):
        if source == PacketSource.BINARY:
            byteorder = kwargs.get('byteorder')
            parser = LCCParser(kwargs.get('raw_data'), byteorder)
            self.packet_type = parser.packet_type
            self.ARPHRD_type = parser.ARPHRD_type
            self.ll_addr_len = parser.ll_addr_len
            self.ll_addr = parser.ll_addr

            # Protocol type
            self.protocol_type = parser.protocol_type
            self.ethertype = self.protocol_type

            raw_data = parser.raw_data[16:]

            self.next_layer = self._next_layer_protocol()(source=source, raw_data=raw_data, byteorder=byteorder)

        elif source == PacketSource.STRING:
            stringbytes = StringBytes(kwargs.get('packet_string'))
            self.packet_type = stringbytes.next(2)
            self.ARPHRD_type = stringbytes.next(2)
            self.ll_addr_len = stringbytes.next(2)
            self.ll_addr = stringbytes.next(8)

            # Protocol type
            self.protocol_type = stringbytes.next(2)
            self.ethertype = self.protocol_type

            # print(self._next_layer_protocol())
            self.next_layer = self._next_layer_protocol()(source=PacketSource.BYTEARR, stringbytes=stringbytes)

        elif source == PacketSource.BYTEARR:
            stringbytes = kwargs.get('stringbytes')
            self.packet_type = stringbytes.next(2)
            self.ARPHRD_type = stringbytes.next(2)
            self.ll_addr_len = stringbytes.next(2)
            self.ll_addr = stringbytes.next(8)

            # Protocol type
            self.protocol_type = stringbytes.next(2)
            self.ethertype = self.protocol_type

            self.next_layer = self._next_layer_protocol()(source=source, stringbytes=stringbytes)

    def __str__(self):
        return "{}: {:08X} - {}| Type: {}".format(self.name, self.ll_addr, self.ll_addr_len, str(self.protocol_type))

    def _next_layer_protocol(self):
        if self.ARPHRD_type == ARPHRD_NETLINK:
            # Packet data is a LINKTYPE_NETLINK packet (http://www.tcpdump.org/linktypes/LINKTYPE_NETLINK.html)
            raise NotImplementedException()
        elif self.ARPHRD_type == ARPHRD_IPGRE:
            # Protocol type field contains a Generic Routing Encapsulation (GRE) protocol type (https://tools.ietf.org/html/rfc2784)
            raise NotImplementedException()
        elif self.ARPHRD_type == ARPHRD_IEEE80211_RADIOTAP:
            # Ignore ptocol type field, raw_data begins with Radiotap link-layer information (http://www.radiotap.org/)
            # followed by 802.11 header
            raise NotImplementedException()
        else:
            if self.protocol_type == 0x0800:
                return IPv4
            elif self.protocol_type == 0x0001:
                # Frame is Novell 802.3 frame without 802.2 LLC header
                raise NotImplementedException()
            elif self.protocol_type == 0x0003:
                # Mysterious case
                raise NotImplementedException("Mysterious case for SLL/LCC header", self.protocol_type)
            elif self.protocol_type == 0x0004:
                # The frame begins with an 802.2 LLC header
                raise NotImplementedException()
            elif self.protocol_type == 0x000C:
                # Frame is a CAN bus frame
                raise NotImplementedException()
            elif self.protocol_type == 0x000D:
                # Frame is a CAN FD (CAN with Flexible Data-Rate) frame
                raise NotImplementedException()
            else:
                raise NotImplementedException()