from protocol_stack.base import Protocol

from parsers.layer4_protocols import UDP as UDPParser
from parsers.layer4_protocols import TCP as TCPParser

from enums import PacketSource


class TransportLayerProtocol(Protocol):

    def __init__(self, source=PacketSource.BINARY, **kwargs):
        super().__init__(source)

        self.source = source
        self.byteorder = kwargs.get('byteorder', None)
        self.data = None

    def get_application_layer(self, numbytes=None):
        """
        Get the the application layer following the transport layer.
        :param numbytes: Number of bytes of the application layer to include, optional. By default, all bytes will be
        included.
        :return: The number of returned bytes, as well as the total value of those bytes if numbytes was specified, or
        a list of singular bytes otherwise.
        :rtype: (``int``, ``int``) or (``int``. ``list``)
        """
        if self.source == PacketSource.BINARY:
            if numbytes is not None:
                return numbytes, int.from_bytes(self.data[:numbytes], self.byteorder, signed=False)
            else:
                application_layer = []
                i = 0
                while True:
                    try:
                        application_layer.append(int.from_bytes(self.data[i:i+1], self.byteorder, signed=False))
                        i += 1
                    except:
                        break
                    return i, application_layer
        else:
            if numbytes is not None:
                return numbytes, self.data.next(numbytes)
            else:
                numbytes = self.get_application_layer_size()
                return numbytes, [i for i in self.data]

    def get_application_layer_size(self):
        """
        Get the size of the application layer or, more concretely, the number of application layer bytes.

        :return: The number of application layer bytes
        :rtype: ``int``
        """
        if self.source == PacketSource.BYTEARR:
            return self.data.rem_bytes()
        else:
            i = 0
            while True:
                try:
                    x = self.data[i:i + 1]
                    i += 1
                except:
                    return i


class TCP(TransportLayerProtocol):
    name = 'TCP'

    def __init__(self, source=PacketSource.BINARY, **kwargs):

        super().__init__(source)

        if source == PacketSource.BINARY:
            raw_data = kwargs.get('raw_data')
            parser = TCPParser(raw_data, self.byteorder)

            # Parse binary data for Ethernet-specific information:
            self.src_port = parser.src_port
            self.dst_port = parser.dst_port
            self.seq = parser.seq                   # Sequence number
            self.ack = parser.ack                   # Acknowledgement number
            self.flags = parser.flags               # Flags

            # Decode flags:
            self.ns = (self.flags >> 8) & 1
            self.cwr = (self.flags >> 7) & 1
            self.ece = (self.flags >> 6) & 1
            self.urg = (self.flags >> 5) & 1
            self.ack = (self.flags >> 4) & 1
            self.psh = (self.flags >> 3) & 1
            self.rst = (self.flags >> 2) & 1
            self.syn = (self.flags >> 1) & 1
            self.fin = (self.flags >> 0) & 1

            self.data_offset = parser.data_offset   # Data offset
            self.wsize = parser.wsize               # Window size
            self.chks = parser.chks                 # checksum
            self.urg = parser.urg                   # Urgent pointer

            self.options = parser.options

            if self.options > 5:
                raw_data = raw_data[self.data_offset * 8:]
            else:
                raw_data = raw_data[20:]

            self.data = raw_data

        elif source == PacketSource.BYTEARR:
            stringbytes = kwargs.get('stringbytes')

            # Parse binary data for Ethernet-specific information:
            self.src_port = stringbytes.next(2)
            self.dst_port = stringbytes.next(2)
            self.seq = stringbytes.next(4)  # Sequence number
            self.ack = stringbytes.next(4)  # Acknowledgement number
            self.flags = stringbytes.next(2)  # Flags

            # Decode flags:
            self.ns = (self.flags >> 8) & 1
            self.cwr = (self.flags >> 7) & 1
            self.ece = (self.flags >> 6) & 1
            self.urg = (self.flags >> 5) & 1
            self.ack = (self.flags >> 4) & 1
            self.psh = (self.flags >> 3) & 1
            self.rst = (self.flags >> 2) & 1
            self.syn = (self.flags >> 1) & 1
            self.fin = (self.flags >> 0) & 1

            self.data_offset = (self.flags >> 12) & 15  # Data offset
            self.wsize = stringbytes.next(2)  # Window size
            self.chks = stringbytes.next(2)  # checksum
            self.urg = stringbytes.next(2)  # Urgent pointer

            if self.data_offset > 5:
                self.options = stringbytes.next(self.data_offset * 4 - 20)
            else:
                self.options = None

            self.data = stringbytes

    def __str__(self):
        first_line = '{}-layer: : {} -> {}'.format(self.name, self.src_port, self.dst_port)
        second_line = '   Sequence number: {}, Acknowledgement number: {}'.format(self.seq, self.ack)
        third_line = '   FLAGS: NS: {}, CWR: {}, ECE: {}, URG: {}'.format(self.ns, self.cwr, self.ece, self.urg)
        third_line_bis = '         ACK: {}, PSH: {}, RST: {}, SYN: {}, FIN: {}'.format(self.ack, self.psh, self.rst,
                                                                                       self.syn, self.fin)
        fourth_line = '   Data offset: {}, Window size: {}'.format(self.data_offset, self.wsize)
        fith_line = '   Checksum: {}, Urgent Pointer: {}'.format(self.chks, self.urg)
        sixth_line = '   Options: {}'.format(self.options)

        return '\n'.join([first_line, second_line, third_line, third_line_bis, fourth_line, fith_line, sixth_line])


class UDP(TransportLayerProtocol):
    name = 'UDP'

    def __init__(self, source=PacketSource.BINARY, **kwargs):
        super().__init__(source)

        if source == PacketSource.BINARY:
            raw_data = kwargs.get('raw_data')
            parser = UDPParser(raw_data, self.byteorder)

            # Parse binary data for Ethernet-specific information:
            self.src_port = parser.src_port
            self.dst_port = parser.dst_port
            self.length = parser.length  # Length
            self.chks = parser.chks  # checksum

            self.source = source
            self.data = raw_data[8:]

        elif source == PacketSource.BYTEARR:
            stringbytes = kwargs.get('stringbytes')

            # Parse binary data for Ethernet-specific information:
            self.src_port = stringbytes.next(2)
            self.dst_port = stringbytes.next(2)
            self.length = stringbytes.next(2)  # Length
            self.chks = stringbytes.next(2)  # checksum

            self.source = source
            self.data = stringbytes

    def __str__(self):
        first_line = '{}-layer: : {} -> {}'.format(self.name, self.src_port, self.dst_port)
        second_line = '   Length: {}, Checksum: {}'.format(self.length, self.chks)

        return '\n'.join([first_line, second_line])
