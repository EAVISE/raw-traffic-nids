from enums import LinkLayer

class Protocol:
    name = 'protocol'

    def __init__(self, source):
        self.next_layer = None

    def __str__(self):
        return self.name + '-layer'

    def get_next_layer(self):
        return self.next_layer

    def _next_layer_protocol(self):
        pass


if __name__ == '__main__':

    #for a in get_bytes_from_string('000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F'):
    #    print(a)
    pass
