"""
Module responsible for all things related to the extraction and processing of the ISCX2012 dataset.
"""
import os
import sys
from lxml import etree

import help_functions as hf
from dataset_config import get_dataset_days, ISCX2012Config
from enums import Dataset
from flow_processing import flow_sorter, flow_labeler, preprocess
from flow_processing.flow import ISCX2012Flow


def setup_preliminaries(root, xml_root, use_existing=True, verbose=True, **kwargs):
    """
    Generate all directories required for the preprocessing of a dataset at the given location.

    :param root: Path to root directory where the dataset will be stored and processed.
    :param xml_root: Directory containing the XML flow information
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :param use_existing: If True, try to use an existing ISCX2012 configuration
    :param kwargs: Consult ``dataset_config.ISCX2012Config`` for available keyword arguments.
    :type root: ``str``
    :type xml_root: ``str``
    :type verbose: ``bool``
    :type use_existing: ``bool``

    :return: Dataset configuration, list of days with data, \
    list of days without data (lacking either CSV data or PCAP data)
    :rtype: ``ISCX2012Config``, ``list``, ``list``

    """
    # Necessary structure
    # Days in ISCX2012:
    # Friday    11/6/2010
    # Saturday  12/6/2010
    # Sunday    13/6/2010
    # Monday    14/6/2010
    # Tuesday   15/6/2010
    # Wednesday 16/6/2010
    # Thursday  17/6/2010
    # Note that no XML or CSV data is present for ISCX, as all behaviour for this day is "normal"

    # days = ["11-friday", "12-saturday", "13-sunday", "14-monday", "15-tuesday", "16-wednesday", "17-thursday"]
    days = get_dataset_days(Dataset.ISCX2012)
    dataset_dirs = ["csv_data", "pcap_data", "raw_packets", "sorted_flows", "debug"]

    config = ISCX2012Config(root, days=days, use_existing=use_existing, verbose=verbose, **kwargs)

    # Check if the configuration has already been completed
    if config.is_completed():
        return config, None, None

    # Create roots
    data_root = config.data_root

    if not os.path.isdir(data_root):
        os.mkdir(data_root)

    # Create necessary directories for dataset dir, if applicable
    if verbose: print("Checking presence of necessary directories")
    for d in dataset_dirs:
        dir_path = os.path.join(data_root, d)
        if not os.path.isdir(dir_path):
            if verbose: print("Creating directory " + dir_path)
            os.mkdir(dir_path)
        for day in days:
            if not os.path.isdir(os.path.join(dir_path, day)):
                if verbose: print("Creating directory " + os.path.join(dir_path,day))
                os.mkdir(os.path.join(dir_path, day))

    # Generate CSVs from XML files
    for day in days:
        xml_files = config.get_xml_filenames(day)
        if not config.xml_processed[day]:
            # First, check if the XML file has been preprocessed
            # This is necessary to remove corrupted file lines
            if not config.xml_preprocessed[day]:
                for xml_file in xml_files:
                    _preprocess_xml(xml_file, xml_root, config, verbose=verbose)
                config.xml_preprocessed[day] = True
                config.update()

            # Remove previous CSV files
            output_path = os.path.join(data_root, dataset_dirs[0], day, day + ".csv")
            if os.path.isfile(output_path):
                    os.remove(output_path)
            # Then, process the XML files:
            for xml_file in xml_files:
                input_path = os.path.join(xml_root, xml_file)
                flow_xml_to_csv(input_path, output_path, day=day, verbose=verbose)

            # Update the configuration
            config.xml_processed[day] = True
            config.update()

    # Check that the dataset directories are not empty
    empty_dirs = [] # List where all empty dirs will be put into

    # We check beginning from the csv files, but for each csv directory the corresponding pcap directory is checked
    for day in os.listdir(os.path.join(data_root, dataset_dirs[0])):
        if os.path.isdir(os.path.join(data_root, dataset_dirs[0], day)):

            # Check if this directory is empty
            csv_dir_empty = len(os.listdir(os.path.join(data_root, dataset_dirs[0], day))) < 1
            # Check if the corresponding pcap directory is empty
            pcap_dir_empty = len(os.listdir(os.path.join(data_root, dataset_dirs[1], day))) < 1

            if (day == days[0]) and (not pcap_dir_empty):
                # For friday 11, we only need the PCAP as no CSV exists
                continue

            if csv_dir_empty or pcap_dir_empty:
                empty_dirs.append(day)

    if verbose: print("Dataset directories for " + ", ".join(empty_dirs) +
                      " are empty. A dataset consists of csv-files and pcap(ng) files for a certain day.")

    if len(empty_dirs) >= len(days):
        print("All dataset directories are empty. No dataset available. Please add a dataset.")
        config.update()
        return config, None, None

    # Find all days with datasets:
    full_dirs = [day for day in days if day not in empty_dirs]
    for day in full_dirs:
        config.dataset_present[day] = True

    config.update()

    return config, full_dirs, dataset_dirs


def _preprocess_xml(xml_file, xml_root, config, verbose=False):
    """
    For ISCX2012, the XML files may contain corrupted lines (that cannot be read by our xml parsers).
    This function removes those lines.

    :param xml_file: Filename of xml file to preprocess
    :param xml_root: Path to directory containing the XML file in question
    :param config: ISCX2012 dataset configuration
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type xml_file: ``str``
    :type xml_root: ``str``
    :type config: ``ISCX2012Config``
    :type verbose: ``bool``
    :return: None
    """
    corrupted_lines = config.get_corrupted_xml_lines(xml_file)
    if len(corrupted_lines) > 0:
        path_to_file = os.path.join(xml_root, xml_file)
        temp_file = os.path.join(xml_root, "tempfile")

        with open(path_to_file, "r") as readfile:
            with open(temp_file, "w") as tempfile:
                for idx, line in enumerate(readfile):
                    if idx not in corrupted_lines:
                        tempfile.write(line)
                    else:
                        if verbose:
                            print("Removing line", idx, "from", xml_file)

        os.remove(path_to_file)
        os.rename(temp_file, path_to_file)


def flow_xml_to_csv(input_path, output_path, day, verbose=True):
    """
    Append the provided xml file transformed to an existing CSV file, or create a new one.

    :param input_path: Path to the XML file that needs to be transformed to the CSV format. Only relevant ISCX2012 \
    information will be gathered.
    :param output_path: Path to the output CSV file that will be appended to, or, if it does not exist, that will be \
    created.
    :param day: Day for which the XML files are transformed.
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type input_path: ``str``
    :type output_path: ``str``
    :type day: ``str``
    :type verbose: ``bool``

    :return: None
    """
    if verbose:
        print("Converting", input_path, "to", output_path)

    output_path_existed = os.path.isfile(output_path)
    with open(output_path, "a+") as ofile:
        if not output_path_existed:
            ofile.write("src_addr,dst_addr,src_port,dst_port,protocol,start,stop,n_fwd,n_bwd,label\n")
        tree = etree.parse(input_path)
        root = tree.getroot()

        for child in root:
            src_addr = child.findtext('source')
            dst_addr = child.findtext('destination')
            src_port = child.findtext('sourcePort')
            dst_port = child.findtext('destinationPort')
            protocol = child.findtext('protocolName')
            start = child.findtext('startDateTime')
            stop = child.findtext('stopDateTime')
            n_fwd = child.findtext('totalSourcePackets')
            n_bwd = child.findtext('totalDestinationPackets')
            label = child.findtext('Tag')

            if label == "Attack":
                label = ISCX2012Config.get_attack_label(None, day)
            ofile.write(",".join([src_addr, dst_addr, src_port, dst_port, protocol, start, stop, n_fwd, n_bwd, label]) + "\n")


def label_csv_line_to_flow(line, use_ts=True):
    """
    Extract the flow information from a line of a light label CSV file for ISCX2012 to create the corresponding flow.

    :param line: Line CSV file line containing flow information.
    :param use_ts: If True, use the timestamps provided in the label CSV file.
    :type line: ``str``
    :type use_ts: ``bool``

    :return: The CICIDS2017 flow corresponding to the line.
    :rtype: ``ISCX2012Flow``
    """
    [src_addr, dst_addr, src_port, dst_port, proto, starttime, stoptime, total_fwd, total_bwd, label] = line.split(",")

    label = label.rstrip("\n")

    if proto == 'tcp_ip':
        proto = '6'
    elif proto == 'udp_ip':
        proto = '17'
    else:
        proto = '-1'

    # Sometimes, the int values are stored as float strings ("48.0" instead of "48")
    # These have to be converted to int strings: "48.0" -> 48.0 -> 48 -> "48"
    src_port = str(int(float(src_port)))  # Used
    dst_port = str(int(float(dst_port)))  # Used
    # duration = str(int(float(duration))) # Unused
    # proto = str(int(float(proto))) # Used

    # Are not used as strings
    total_fwd = int(float(total_fwd))  # Used
    total_bwd = int(float(total_bwd))  # Used if bidirectional == True

    return ISCX2012Flow(src_addr, dst_addr, src_port, dst_port, proto, label, total_fwd, total_bwd,
                        start_ts=starttime, end_ts=stoptime, use_ts=use_ts)


def flow_sorting(raw_packets_storage_path, flows_storage_path, ts_included, verbose=True):
    """
    Sort extracted network packets in flows according to their flow identifier (flow ID).

    :param raw_packets_storage_path: Path to the directory containing the extracted packets to sort.
    :param flows_storage_path: Path to the directory the sorted flows will be stored into.
    :param ts_included: If timestamps were extracted along with packets, set True
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type raw_packets_storage_path: ``str``
    :type flows_storage_path: ``str``
    :type ts_included: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    # Put the raw packet files in the right order:
    filelist = hf.list_files_ordered_from_path(path=raw_packets_storage_path, trailer=".csv", verbose=verbose)
    # Sort the packets inside the generated csv-files
    for raw_packets_file in filelist:
        # Check if file is a csv file:
        if raw_packets_file.split(".")[-1] == "csv":
            # Calculate correct file path
            raw_packets_file_path = os.path.join(raw_packets_storage_path, raw_packets_file)
        
            if verbose: print("Sorting flows for " + raw_packets_file)
            flow_sorter.sort_flows_1_file(raw_packets_file_path, flows_storage_path, ts_included=ts_included,
                                          verbose=verbose)


def csv_preprocess(csv_data_dir, verbose=True):
    """
    Preprocess the ISCX2012 CSV label files before using them for labelling.

    :param csv_data_dir: Path to directory containing all CSV label files.
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_data_dir: ``str``
    :type verbose: ``bool``
    :return: None
    """
    # Process each CSV file
    # No actual preprocessing is required
    for labels_csv in os.listdir(csv_data_dir):
        # Clean and minimize the label CSVs
        preprocess.preprocess_dataset_csv(Dataset.ISCX2012, old_file=labels_csv, path_to_csv_dir=csv_data_dir,
                                          verbose=verbose)


def flow_labelling(csv_data_dir, flows_root_dir, error_file, day, config, verbose=True):
    """
    Label the ISCX2012 flows.

    :param csv_data_dir: Path to directory containing the CSV label files.
    :param flows_root_dir: Path to the root directory containing al sorted flows.
    :param error_file: Path to file to store error messages into
    :param day: Day for which the flows will be labelled
    :param config: ISCX2012 dataset configuration
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_data_dir: ``str``
    :type flows_root_dir: ``str``
    :type error_file: ``str``
    :type day: ``str``
    :type config: ``ISCX2017Configuration``
    :type verbose: ``bool``
    :return: None
    """
    if day == config.days[0]:
        # In case of Friday, no label files are present, as each flow is "normal" or benign
        flow_labeler.label_sorted_flows_no_csv('Normal', flows_root_dir, verbose=verbose)
    else:
        # Process each item in the csv directory (which should only be one item)
        for labels_csv in os.listdir(csv_data_dir):
            if verbose:
                print("Labelling according to " + labels_csv)
            
            flow_labeler.label_sorted_flows(label_csv=os.path.join(csv_data_dir, labels_csv),
                                            flows_root_dir=flows_root_dir,
                                            error_file=error_file,
                                            config=config,
                                            verbose=verbose)


if __name__ == "__main__":
    pass
    # xml_root = os.path.join("~", "home", "laurens", "Data", "ISCX2012")
    # out = os.path.join("..", "..", "ISCX2012", "test.csv")
    # flow_xml_to_csv(target, out)
