import os
import numpy as np

from exceptions import MissingTimestampsException
from features_config import SessionBasedConfig
from help_functions import remove_dir, shape_to_str, process_flow_tree
from protocol_stack.packet import Packet
from enums import PacketSource


def __insert_session_metadata(flag_counter, timestamps, features, feature_config):

    offset = 4
    if feature_config.include_icmp:
        offset += 3

    # Add TCP flags
    features[offset + 0] = flag_counter['fin']
    features[offset + 1] = flag_counter['syn']
    features[offset + 2] = flag_counter['rst']
    features[offset + 3] = flag_counter['psh']
    features[offset + 4] = flag_counter['ack']
    features[offset + 5] = flag_counter['urg']
    features[offset + 6] = flag_counter['ece']
    features[offset + 7] = flag_counter['cwr']

    # Add timing features
    # First, calculate the time intervals:
    if len(timestamps) <= 1:
        features[offset + 8] = 0
        features[offset + 9] = 0
    else:
        intervals = []
        for i in range(1, len(timestamps)):
            intervals.append(timestamps[i] - timestamps[i - 1])

        # Then calculate the mean and std:
        features[offset + 8] = np.mean(intervals)
        features[offset + 9] = np.std(intervals)


def __insert_protocol_metadata(features, decoded_packet, feature_config):

    # Extract all metadata features
    protocol = decoded_packet.get_transport_layer_protocol()
    src_port = decoded_packet.get_source_port(protocol)
    dst_port = decoded_packet.get_destination_port(protocol)

    if feature_config.include_icmp:
        if protocol == 'tcp':
            features[0] = 1
            features[3] = src_port
            features[4] = dst_port
        elif protocol == 'udp':
            features[1] = 1
            features[3] = src_port
            features[4] = dst_port
        elif protocol == 'icmp':
            icmp_type, icmp_code = decoded_packet.get_icmp_metadata()
            features[2] = 1
            features[5] = icmp_type
            features[6] = icmp_code
        else:
            raise NotImplementedError
    else:
        if protocol == 'tcp':
            features[0] = 1
            features[2] = src_port
            features[3] = dst_port
        elif protocol == 'udp':
            features[1] = 1
            features[2] = src_port
            features[3] = dst_port
        else:
            raise NotImplementedError
    return protocol


def __update_flag_counter(decoded_packet, flag_counter):
    tcp_layer = decoded_packet.get_layer(2)

    flag_counter['psh'] += tcp_layer.psh
    flag_counter['ack'] += tcp_layer.ack
    flag_counter['fin'] += tcp_layer.fin
    flag_counter['syn'] += tcp_layer.syn
    flag_counter['rst'] += tcp_layer.rst
    flag_counter['urg'] += tcp_layer.urg
    flag_counter['ece'] += tcp_layer.ece
    flag_counter['cwr'] += tcp_layer.cwr


def __extract_packet_features(decoded_packet, flag_counter, current_payload_bytes, features, feature_config, protocol):

    # Only collect flags if the protocol is TCP
    if protocol == 'tcp':
        # Collect flag information
        __update_flag_counter(decoded_packet, flag_counter)

    # If applicable, extract application layer payload bytes
    if current_payload_bytes < feature_config.payl_bytes:
        required_bytes = feature_config.payl_bytes - current_payload_bytes
        num_bytes, payload = decoded_packet.get_payload()

        for i in range(required_bytes):
            if (i >= num_bytes) or (current_payload_bytes >= feature_config.payl_bytes):
                break
            # print(i, num_bytes, len(payload))

            features[feature_config.num_metadata_feats + current_payload_bytes - 1] = payload[i]
            current_payload_bytes += 1

    return current_payload_bytes


def extract_session_based_features(flow_file, feature_config, dataset_config):

    with open(flow_file, "r") as ff:
        features = np.zeros(feature_config.num_feats, dtype=np.float32)

        # Preliminaries:
        # List of timestamps per session packet
        timestamps = []

        # Current amount of occupied payload bytes
        current_payload_bytes = 0

        # Flag counters
        flag_counter = {'fin': 0, 'syn': 0, 'rst': 0, 'psh': 0, 'ack': 0, 'urg': 0, 'ece': 0, 'cwr': 0}

        # Use the first line to get address information:
        line = ff.readline()

        # Get the timestamp and packet information from the line
        ts_sec, ts_nano, packet = line.rstrip("\n").split(",")

        # Add the timestamp to the list
        timestamps.append((int(ts_sec) * pow(10, 9)) + int(ts_nano))

        # Get the packet
        decoded_packet = Packet(source=PacketSource.STRING, link_layer=dataset_config.get_link_layer(),
                                packet_string=packet)

        protocol = __insert_protocol_metadata(features=features,
                                              decoded_packet=decoded_packet,
                                              feature_config=feature_config)

        current_payload_bytes = __extract_packet_features(decoded_packet=decoded_packet,
                                                          flag_counter=flag_counter,
                                                          current_payload_bytes=current_payload_bytes,
                                                          protocol=protocol,
                                                          feature_config=feature_config,
                                                          features=features)

        # Then start looping over all other lines
        line = ff.readline()
        while line:
            # Get the timestamp and packet information from the line
            ts_sec, ts_nano, packet = line.rstrip("\n").split(",")

            # Add the timestamp to the list
            timestamps.append((int(ts_sec) * pow(10, 9)) + int(ts_nano))

            # Get the packet
            print('{}\n'.format(flow_file))
            decoded_packet = Packet(source=PacketSource.STRING, link_layer=dataset_config.get_link_layer(),
                                    packet_string=packet)

            # Fill in the features:
            current_payload_bytes = __extract_packet_features(decoded_packet=decoded_packet,
                                                              flag_counter=flag_counter,
                                                              feature_config=feature_config,
                                                              current_payload_bytes=current_payload_bytes,
                                                              features=features,
                                                              protocol=protocol)

            line = ff.readline()
    __insert_session_metadata(flag_counter=flag_counter, timestamps=timestamps,
                              features=features, feature_config=feature_config)

    return features


def flow_file_to_session_based_features(flow_file, current_path, target_output_dir,
                                        feature_config, dataset_config, flow_features_dict):
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        return

    #   - Generate flow image
    label = "-".join(flow_file_parts[0].split("-")[:-1])

    flow_features = extract_session_based_features(os.path.join(current_path, flow_file),
                                                   feature_config, dataset_config)

    #   - Add flow image to list of flow images of the same traffic class
    flow_features_dict[label] += [flow_features]
    #   - If a "batch_size" number of images are present in the list, save it to a .npz file
    if len(flow_features_dict[label]) >= feature_config.batch_size:
        memmap_shape = tuple([len(flow_features_dict[label])] + list(flow_features_dict[label][0].shape))
        output_dir = os.path.join(target_output_dir, label)

        output_file = "arrays_" + str(len(os.listdir(output_dir))) + '--' + shape_to_str(memmap_shape) + '.npy'

        # Instead of saving in a compressed file, we use a memory mapped file with dtype=float32
        # np.savez_compressed(os.path.join(output_dir, output_file), *simple_vectors_dict[label])
        memmap = np.memmap(os.path.join(output_dir, output_file),
                           mode="w+",
                           shape=memmap_shape,
                           dtype=np.float32)
        memmap[:] = np.array(flow_features_dict[label])
        # - Empty list
        flow_features_dict[label] = []


def generate_session_based_features(datasets_root, features_root, dataset,
                                    use_existing=True, verbose=True, name=None, **kwargs):

    # Generate the configuration
    config = SessionBasedConfig(features_root=features_root, datasets_root=datasets_root, dataset=dataset,
                                use_existing=use_existing, name=name, verbose=verbose, **kwargs)

    # Generate the target output directory:
    output_dir = os.path.join(config.features_root, config.name)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    # Use the configuration to traverse the flow trees for the different days of the dataset
    dataset_config = config.dataset_config

    if not dataset_config.include_ts:
        raise MissingTimestampsException("Timestamps are necessary for Session Based features.")

    for day in dataset_config.get_labelled_days():
        if not config.features_generated[day]:

            target_output_dir = os.path.join(output_dir, day)
            # 1) Clean previous feature data
            if os.path.isdir(target_output_dir):
                if verbose:
                    print("Removing previous generated features:", target_output_dir)
                remove_dir(target_output_dir, remove_root=True, verbose=verbose)
            # Create clean directory for the day
            os.mkdir(target_output_dir)

            # 2) Make a directory for each traffic class in that day
            for label in dataset_config.get_labels(day):
                os.mkdir(os.path.join(target_output_dir, label))

            # Get root directory containing sorted flows to extract features from
            sorted_flows_root = dataset_config.get_sorted_flows_root(day)

            # 3) "flow_features" keeps a list of the flow images per label
            flow_features = {}
            for label in dataset_config.get_labels(day):
                flow_features[label] = []

            # Process the sorted flows to get the features
            process_flow_tree(root=sorted_flows_root, action={"function": flow_file_to_session_based_features,
                                                              "params": {"target_output_dir": target_output_dir,
                                                                         "feature_config": config,
                                                                         "dataset_config": dataset_config,
                                                                         "flow_features_dict": flow_features}
                                                              }, verbose=verbose)

            # Store the remaining features present in the "flow_images" dict:
            for key, item in flow_features.items():

                if len(item) == 0:
                    continue

                remaining_output_dir = os.path.join(target_output_dir, key)

                memmap_shape = tuple([len(flow_features[key])] + list(flow_features[key][0].shape))

                remainder_output_file = "arrays_" + str(len(os.listdir(remaining_output_dir))) + '--' + \
                                        shape_to_str(memmap_shape) + '.npy'

                # Instead of saving in a compressed file, we use a memory mapped file with dtype=float32
                memmap = np.memmap(os.path.join(remaining_output_dir, remainder_output_file),
                                   mode="w+",
                                   shape=memmap_shape,
                                   dtype=np.float32)
                memmap[:] = np.array(flow_features[key])

            if verbose:
                print("\nFinished generating features for", day)

            # Save progress
            config.features_generated[day] = True
            config.update()

    return config
