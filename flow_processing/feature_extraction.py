import os
import numpy as np
from enum import Enum

from features_config import PCCNConfig, HAST1Config, HAST2Config, SessionBasedConfig, SimpleVectorsConfig

from flow_processing.PCCN_feature_extraction import flow_data_to_numeric_features_PCCN
from flow_processing.HAST_feature_extraction import flow_data_to_features_HAST2
from flow_processing.simplevectors_feature_extraction import flow_data_to_simple_vectors

from enums import FeatureStrategy, Dataset
from help_functions import remove_dir, shape_to_str, process_flow_tree


class StoppingCondition(Enum):
    NEWLINE = 1
    EOF = 2


class FlowData:
    """
    Structure that allows for the controlled extraction of flow data from their storage files. This way, differently
    generated flow files can be represented with a uniform interface for feature extraction methods.

    FlowData objects should be used as an iterator, for example in a for loop: 'for line in flow_data', where flow_data
    is a FlowData object, iterates over every line of the first flow in an opened file. By doing this a second time,
    flow_data will now provide the lines of the second flow in the opened file. Note that before doing this, the
    FlowData object should be consulted whether the StopIteration exception was raised due to the end-of-file, or due
    to the end of the first flow. This can be done by calling is_EOF()
    """

    def __init__(self, opened_file):
        """
        The constructor for the FlowData object. The provided opened file contains one of more flows, separated by
        '\n', that will be presented uniformly by this class.

        :param opened_file: File descriptor of an already opened flow file.
        :type opened_file: ``file``
        """
        self.opened_file = opened_file
        self.stopping_condition = None

    def __next__(self):
        line = self.opened_file.readline()

        if line:
            if line != '\n':
                return line
            else:
                self.stopping_condition = StoppingCondition.NEWLINE
        else:
            self.stopping_condition = StoppingCondition.EOF

        #raise StopIteration
        return

    def has_stopped(self):
        """
        Check whether the FlowData object has raised a StopIteration.

        :return: True of the FlowData iterator has been stopped during the last next() call
        :rtype: ``bool``
        """
        if self.stopping_condition is not None:
            if self.stopping_condition == StoppingCondition.NEWLINE:
                self.stopping_condition = None
            return True
        else:
            return False

    def is_EOF(self):
        """
        Check wheter the FlowData object has reached the End-Of-File.

        :return: True if EOF was reached.
        :rtype: ``bool``
        """
        return self.stopping_condition == StoppingCondition.EOF

    def continue_to_end_flow(self):
        """
        Skip all packets of the current flow until a StopIteration exception is raised. In case of iterating again, the
        object will start with the first packet of the next flow in the file rather than the current packet in the
        current flow.

        :return:
        """
        for _ in self:
            pass

    def __iter__(self):
        return self


def get_feature_generation_action(features_config, dataset_config, **kwargs):
    if features_config.featurestrategy == FeatureStrategy.PCCN.name:

        features_config.set_feature_type(kwargs.get('type', 'header'))
        return {'function': flow_data_to_numeric_features_PCCN,
                'params': {'features_config': features_config, 'dataset_config': dataset_config}}
    elif features_config.featurestrategy == FeatureStrategy.HAST_I.name:
        return

    elif features_config.featurestrategy == FeatureStrategy.HAST_II.name:

        # For HAST-II, it is more convenient to also use a simple vectors representation
        return {'function': flow_data_to_simple_vectors,
                'params': {'features_config': features_config, 'dataset_config': dataset_config}}

    elif features_config.featurestrategy == FeatureStrategy.SESSIONBASED.name:
        return

    elif features_config.featurestrategy == FeatureStrategy.SIMPLE_VECTORS.name:
        return {'function': flow_data_to_simple_vectors,
                'params': {'features_config': features_config, 'dataset_config': dataset_config}}


def __get_dtype(features_config):
    if features_config.featurestrategy == FeatureStrategy.SESSIONBASED:
        return np.float32
    else:
        return np.uint8


def store_features_list(features_lists, target_output_dir, label, dtype):
    memmap_shape = tuple([len(features_lists[label])] + list(features_lists[label][0].shape))
    output_dir = os.path.join(target_output_dir, label)

    output_file = "arrays_" + str(len(os.listdir(output_dir))) + '--' + shape_to_str(memmap_shape) + '.npy'

    # Instead of saving in a compressed file, we use a memory mapped file with dtype=uint8
    # np.savez_compressed(os.path.join(output_dir, output_file), *simple_vectors_dict[label])
    memmap = np.memmap(os.path.join(output_dir, output_file),
                       mode="w+",
                       shape=memmap_shape,
                       dtype=dtype)
    memmap[:] = np.array(features_lists[label])
    # - Empty list
    features_lists[label] = []


def generate_configuration(dataset_config, features_root, featurestrategy, use_existing, verbose=True, **kwargs):

    if featurestrategy == FeatureStrategy.PCCN:
        return PCCNConfig(features_root=features_root, dataset_config=dataset_config,
                          use_existing=use_existing, verbose=verbose, **kwargs)
    elif featurestrategy == FeatureStrategy.HAST_I:
        pass  # HAST1Config
    elif featurestrategy == FeatureStrategy.HAST_II:
        return HAST2Config(features_root=features_root, dataset_config=dataset_config,
                           use_existing=use_existing, verbose=verbose, **kwargs)
    elif featurestrategy == FeatureStrategy.SESSIONBASED:
        pass  # SessionBasedConfig
    elif featurestrategy == FeatureStrategy.SIMPLE_VECTORS:
        return SimpleVectorsConfig(features_root=features_root, dataset_config=dataset_config,
                                   use_existing=use_existing, verbose=verbose, **kwargs)
    else:
        raise NotImplemented('Specified feature extraction strategy ' + featurestrategy.name + ' is not currently ' +
                             'implemented.')


def flow_file_to_flow_data(flow_file, current_path, features_config, features_lists, target_output_dir, dtype, action):
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        return

    # Extract label
    label = "-".join(flow_file_parts[0].split("-")[:-1]).strip()
    if label == 'Backdoor':
        label = 'Backdoors'

    # Present the data of the flow file to the feature extraction function
    with open(os.path.join(current_path, flow_file), 'r') as opened_file:

        while True:
            flowdata = FlowData(opened_file)

            # Extract features from the flow file
            flow_features_list = action['function'](flowdata, **action['params'])
            features_lists[label] += flow_features_list

            if len(features_lists[label]) >= features_config.batch_size:
                store_features_list(features_lists=features_lists, target_output_dir=target_output_dir,
                                    label=label, dtype=dtype)

            # If the end of the flow file has been reached
            if flowdata.is_EOF():
                # Stop the while loop, we have nothing more to do
                break


def generate_features(dataset_config, features_root, featurestrategy, use_existing=True, verbose=True, **kwargs):
    """
    :param features_root: Root directory where all extracted features are stored
    :param use_existing: If True and applicable, use an existing configuration
    :param verbose: If True, print additional informative text as output
    :param kwargs: See ``FeatureStrategyConfig`` and ``PCCNConfig`` for more information
    :type features_root: ``str``
    :type use_existing: ``bool``
    :return: PCC Configuration
    :rtype: ``PCCNConfig``
    """
    # Generate the configuration
    config = generate_configuration(dataset_config=dataset_config, features_root=features_root,
                                    featurestrategy=featurestrategy, use_existing=use_existing,
                                    verbose=verbose, **kwargs)

    # Generate the target output directory:
    output_dir = os.path.join(config.features_root, config.name)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    # Use the configuration to traverse the flow trees for the different days of the dataset
    for day in dataset_config.get_labelled_days():
        # Only use days for which features have not been generated
        if not config.features_generated[day]:
            if verbose:
                print('Generating features for', day)
            target_output_dir = os.path.join(output_dir, day)

            # Clean previous feature data
            if os.path.isdir(target_output_dir):
                if verbose:
                    print("Removing previous generated features:", target_output_dir)
                remove_dir(target_output_dir, remove_root=True, verbose=verbose)
            # Create clean directory
            os.mkdir(target_output_dir)

            # Get root directory containing sorted flows to extract features from
            sorted_flows_root = dataset_config.get_sorted_flows_root(day)

            features_lists = {}
            for label in dataset_config.get_labels(day):
                os.mkdir(os.path.join(target_output_dir, label))
                features_lists[label] = []

            # Process the sorted flows to get the features
            sub_action = get_feature_generation_action(config, dataset_config, type='hepa')
            process_flow_tree(root=sorted_flows_root, action={"function": flow_file_to_flow_data,
                                                              "params": {"target_output_dir": target_output_dir,
                                                                         "features_config": config,
                                                                         "features_lists": features_lists,
                                                                         "dtype": __get_dtype(config),
                                                                         "action": sub_action}
                                                              }, verbose=verbose)

            # Store the remaining features present in the "features_lists" dict:
            for label, feature_list in features_lists.items():

                if len(feature_list) == 0:
                    continue

                store_features_list(features_lists, target_output_dir, label, dtype=__get_dtype(config))

            # Report on the finished day
            if verbose:
                print("\nFinished generating features for {}!".format(day))

            # Save progress
            config.features_generated[day] = True
            config.update()

    return config
