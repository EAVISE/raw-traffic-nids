import h5py
import os
import numpy as np

from help_functions import process_flow_tree
from flow_processing.feature_extraction import FlowData
from dataset_config import get_config_from_file
from flow_processing.simplevectors_feature_extraction import packet_to_bytes
from flow_processing.flow import Flow
#from flow_processing.stream_based_processing import LabelStatus
from enums import LabelStatus

LABELS_GROUP_NAME = 'labels'
FLOW_REFERENCES_GROUP_NAME = 'flow_references'
FLOWS_GROUP = 'flows'


class HDF5File:

    def __init__(self, filename, mode, packet_size=100):
        self.filename = filename
        self.packet_size = packet_size

        self.file = h5py.File(name=self.filename, mode=mode)
        self.is_open = True

    def close(self):
        self.file.close()
        self.is_open = False

    def open(self, mode='a'):
        self.file = h5py.File(name=self.filename, mode=mode)
        self.is_open = True

    def is_open(self):
        return self.is_open


class HDF5FlowStorage(HDF5File):
    """
    Store an entire dataset in this HDF5, taking chronology and labelling into account,

    The HDF5 internal hierachy looks like this:

    root/
        sorted_flows/
            src_addr/
                dst_addr/
                    ...
                        protocol/
                            dset -> Contains actual packets
                            refs -> References groups of individual packets, alongside their labels
        labels/ -> Attributes: Mapping between labels and indices
        chronologic/ -> One big dataset with references to groups op pakcets, in chronologische volgorde

    """

    root_group_name = 'root'
    sorted_flows_group_name = 'sorted_flows'
    labels_group_name = 'labels'
    chronologic_group_name = 'chronologic'

    dataset_name = 'dset'
    reference_dset_name = 'refs'

    # Attribute_names
    numel_name = 'numel'
    batch_size_name = 'batch_size'
    attr_n_packets_name = 'n_packets'
    attr_n_labelled_packets_name = 'n_labelled_packets'
    attr_n_labelled_references_name = 'n_labelled_references'

    chronologic_batch_size = 10e6
    labels_batch_size = 10e4

    def __init__(self, filename, mode, packet_size, **kwargs):
        file_already_exists = os.path.isfile(filename)

        super(HDF5FlowStorage, self).__init__(filename, mode, packet_size)

        self.packet_datatype = np.dtype(
            {
                'names': ['ts_s', 'ts_ns', 'packet'],
                'formats': ['i4', 'i4', ('u1', self.packet_size)]}
        )

        self.ref_datatype = np.dtype(
            {
                'names': ['ref', 'label'],
                'formats': [h5py.regionref_dtype, 'i4']}
        )

        self.default_dataset_batch_size = kwargs.get('packet_bs', 1)
        self.default_dataset_chunk_size = kwargs.get('packet_chs', 1)

        if file_already_exists:
            # Just load in the prerequisite groups
            self.root_group = self.file[self.root_group_name]

            self.sorted_flows_group = self.root_group[self.sorted_flows_group_name]
            self.labels_group = self.root_group[self.labels_group_name]
            self.chronologic_group = self.root_group[self.chronologic_group_name]

            self.stream_based = self.root_group.attrs['stream_based']
        else:
            # Create the prerequisite groups
            self.root_group = self.file.create_group(self.root_group_name)

            self.sorted_flows_group = self.root_group.create_group(self.sorted_flows_group_name)
            self.labels_group = self.root_group.create_group(self.labels_group_name)
            self.chronologic_group = self.root_group.create_group(self.chronologic_group_name)

            # Arrange attributes and create datasets
            # Stream-based
            self.stream_based = kwargs.get('stream_based', False)
            self.root_group.attrs['stream_based'] = self.stream_based

            # Label information
            labels = kwargs.get('labels')
            self.labels_group.attrs['labels'] = labels

            for i in range(len(labels)):
                self.labels_group.attrs[labels[i]] = i

                label_dset = self.labels_group.create_dataset(name=labels[i],
                                                              shape=(self.labels_batch_size, 1),
                                                              maxshape=(None, 1),
                                                              dtype=h5py.regionref_dtype)

                label_dset.attrs[self.batch_size_name] = self.labels_batch_size
                label_dset.attrs[self.numel_name] = 0

            # Chronologic information and dataset
            chronoset = self.chronologic_group.create_dataset(
                            name=self.dataset_name,
                            shape=(self.chronologic_batch_size, 1),
                            maxshape=(None, 1),
                            dtype=h5py.regionref_dtype,
            )

            chronoset.attrs[self.batch_size_name] = self.chronologic_batch_size
            chronoset.attrs[self.numel_name] = 0

    def flow_root_exists(self, flow: Flow):
        """
        Check if the flow exists in the HDF5-file
        :arg flow: Flow object containing the
        :return: True if the flow exists, and the path to that flow
        :rtype: tuple of ``bool`` and ``str``
        """
        path = self.root_group_name + '/' + self.sorted_flows_group_name + '/' + flow.src_addr + '/' + \
               flow.dst_addr + '/' + flow.src_port + '/' + flow.dst_port + '/' + flow.proto

        return path in self.file, path

    def check_for_unlabelled_packets(self, path):
        """
        Check whether the flows at the provided path still contain unlabelled packets.

        :param path: Path to group containing the reference dataset of the specific flow.
        :return: True if there are unlablled packets
        """
        reference_dset = self.file[path + '/' + self.reference_dset_name]

        if self.stream_based:
            return reference_dset.shape[-1, 0][1] == -1
        else:
            return reference_dset.attrs[self.attr_n_packets_name] < self.file[path + '/' + self.dataset_name].shape[0]

    def label_flow(self, path, flow):
        reference_dset = self.file[path + '/' + self.reference_dset_name]
        packets_dset = self.file[path + '/' + self.dataset_name]
        label_idx = self.labels_group.attrs[flow.label]

        if self.stream_based:
            # First, get the index of the first unlabelled region reference
            # Both the current index and the start index are necessary for the 'regref' variable down the line
            current_idx = start_idx = reference_dset.attrs[self.attr_n_labelled_references_name]

            # Current packet count to keep track of when we surpass the required number of forward packets
            current_packet_count = 0

            # We assume the labelstatus will be VALID, unless proven otherwise through calculation
            label_status = LabelStatus.VALID

            while current_packet_count < flow.fwd_packets:
                if current_idx == reference_dset.shape[0]:
                    label_status = LabelStatus.INCOMPLETE
                    break

                # Add label
                reference_dset[current_idx, 0][1] = label_idx

                # Update loop variables
                current_packet_count += packets_dset[reference_dset[current_idx, 0]].shape[0]
                current_idx += 1

            regref = reference_dset.regionref[start_idx:current_idx]

        else:

            # Only the first time, we do not want to actually resize the dataset, as we need to fill the first row still
            if not reference_dset[0, 0][1] == -1:
                self.__resize_refset(reference_dset)

            n_labelled_packets = reference_dset.attrs[self.attr_n_packets_name]
            unlabelled_packets = packets_dset.shape[0] - n_labelled_packets
            packets_to_extract = min(flow.fwd_packets, unlabelled_packets)

            reference_dset[reference_dset.shape[0] - 1] = \
                packets_dset.regionref[n_labelled_packets:n_labelled_packets + packets_to_extract, 0], label_idx

            regref = reference_dset.regionref[reference_dset.shape[0] - 1]



            label_status = LabelStatus.VALID if packets_to_extract >= flow.fwd_packets else LabelStatus.INCOMPLETE

        # Then update the reference list for the labels dataset, and the chronology dataset
        self.__update_batched_dataset(self.labels_group[flow.label], regref)
        self.__update_batched_dataset(self.chronologic_group[self.dataset_name], regref)

        # The return value is the labelstatus indicating how well the labelling went
        return label_status

    def __update_batched_dataset(self, dataset, item):
        self.__dataset_resize_check(dataset)
        idx = dataset.attrs[self.numel_name]

        dataset[idx] = item
        dataset.attrs[self.numel_name] = idx + 1

        return dataset

    def __dataset_resize_check(self, dataset):
        """
        Check whether a dataset needs to be resized, as a result of it being completely filled.

        :param dataset:
        :return:
        """
        cur_idx = dataset.attrs[self.numel_name]
        bs = dataset.attrs[self.batch_size_name]

        if ((cur_idx % bs) == 0) and (cur_idx > 0):
            dataset.resize(dataset.shape[0] + bs, axis=0)

    def store_packets_from_root(self, src_addr, dst_addr, src_port, dst_port, proto, packets):
        group, path = self.__get_protocol_group(src_addr, dst_addr, src_port, dst_port, proto)
        self.store_packets(group, path, packets)

    def store_packets(self, group, path, packets):
        n_packets = len(packets)

        if not path + '/' + self.dataset_name in self.file:
            chunk_shape = (self.__calculate_n_chunks(n_packets), 1)

            dset = group.create_dataset(name=self.dataset_name,
                                        chunks=chunk_shape,
                                        shape=(n_packets, 1),
                                        maxshape=(None, 1),
                                        dtype=self.packet_datatype)

            dset[:, 0] = packets

            # Intialize reference dataset
            regset = group.create_dataset(name=self.reference_dset_name,
                                          chunks=(1, 1),
                                          shape=(1, 1),
                                          maxshape=(None, 1),
                                          dtype=self.ref_datatype)
            #regset.attrs[self.attr_n_packets_name] = len(packets)
            regset.attrs[self.attr_n_packets_name] = 0
            regset[0, 0] = dset.regionref[:, 0], -1

            if self.stream_based:
                regset[0, 0] = dset.regionref[:, 0], -1

                regset.attrs[self.attr_n_labelled_packets_name] = 0
                regset.attrs[self.attr_n_labelled_references_name] = 0

        else:
            dset = group[self.dataset_name]

            # Resize the dataset to create space for the new packets
            old_numel = dset.shape[0]
            self.__resize_packet(dset, n_packets)

            dset[old_numel:, 0] = packets

            if self.stream_based:
                regset = group[self.reference_dset_name]
                self.__resize_refset(regset)
                regset[regset.shape[0] - 1] = dset.regionref[old_numel:, 0], -1

    def __resize_packet(self, dataset, n_packets):
        dataset.resize(dataset.shape[0] + n_packets, axis=0)

    def __resize_refset(self, dataset):
        dataset.resize(dataset.shape[0] + 1, axis=0)

    def __calculate_n_chunks(self, n_packets):
        return max((n_packets // 10) * 10, self.default_dataset_chunk_size)

    def __get_protocol_group(self, src_addr, dst_addr, src_port, dst_port, proto):
        """

        :param src_addr:
        :param dst_addr:
        :param src_port:
        :param dst_port:
        :param proto:
        :return: The protocol group, the path to that group
        :rtype: ``tuple`` of ``Group``, ``str``
        """
        src_group = self.get_subgroup(self.sorted_flows_group,
                                      path_to_group=self.root_group_name + '/' + self.sorted_flows_group_name,
                                      subgroup_name=src_addr)
        path = self.root_group_name + '/' + self.sorted_flows_group_name + '/' + src_addr

        dst_group = self.get_subgroup(src_group, path, dst_addr)
        path += '/' + dst_addr

        sport_group = self.get_subgroup(dst_group, path, src_port)
        path += '/' + src_port

        dport_group = self.get_subgroup(sport_group, path, dst_port)
        path += '/' + dst_port

        proto_group = self.get_subgroup(dport_group, path, proto)
        path += '/' + proto

        return proto_group, path

    def get_subgroup(self, group, path_to_group, subgroup_name):
        """ Returns a subgroup, creating it if it does not exist. """
        if not path_to_group + '/' + subgroup_name in self.file:
            return group.create_group(subgroup_name)
        else:
            return group[subgroup_name]

    def remove_redundant_records(self):
        """
        From every dataset that increase in size using batches, remove redundant records.

        :return: None
        """
        # Remove redundant rows for every label dataset:
        for label in self.labels_group.attrs['labels']:
            self.__remove_redundant_records_from_dataset(self.labels_group[label])

        # Remove redundant rows from the chronological dataset
        self.__remove_redundant_records_from_dataset(self.chronologic_group[self.dataset_name])

    def __remove_redundant_records_from_dataset(self, dataset):
        """
        Use dataset-accompanying attributes to resize the dataset to its actual size, removing any redundant (empty)
        rows.

        :param dataset: Dataset to resize.
        :return: None
        """
        cur_idx = dataset.attrs[self.numel_name]
        bs = dataset.attrs[self.batch_size_name]

        n_to_remove = bs - (cur_idx % bs)

        if n_to_remove > 0:
            dataset.resize(dataset.shape[0] - n_to_remove, axis=0)


class HDF5SortedDataset(HDF5File):

    root_group_name = 'root'
    dataset_name = 'dset'

    def __init__(self, filename, mode, packet_size, **kwargs):
        super(HDF5SortedDataset, self).__init__(filename, mode, packet_size)

        self.root_group = self.file.create_group(self.root_group_name)

        self.packet_datatype = np.dtype(
            {
                'names': ['ts_s', 'ts_ns', 'packet'],
                'formats': ['i4', 'i4', ('u1', self.packet_size)]}
        )

        self.default_dataset_batch_size = kwargs.get('packet_bs', 1)
        self.default_dataset_chunk_size = kwargs.get('packet_chs', 1)

    def store_packets_from_root(self, src_addr, dst_addr, src_port, dst_port, proto, packets):
        group, path = self.__get_protocol_group(src_addr, dst_addr, src_port, dst_port, proto)
        self.store_packets(group, path, packets)

    def store_packets(self, group, path, packets):
        n_packets = len(packets)

        if not path + '/' + self.dataset_name in self.file:
            chunk_shape = (self.__calculate_n_chunks(n_packets), 1)

            dset = group.create_dataset(name=self.dataset_name,
                                        chunks=chunk_shape,
                                        shape=(n_packets, 1),
                                        maxshape=(None, 1),
                                        dtype=self.packet_datatype)

            dset[:, 0] = packets

        else:
            dset = group[self.dataset_name]

            # Resize the dataset to create space for the new packets
            old_numel = dset.shape[0]
            self.__resize(dset, n_packets)

            dset[old_numel:, 0] = packets

    def __resize(self, dataset, n_packets):
        dataset.resize(dataset.shape[0] + n_packets, axis=0)

    def __calculate_n_chunks(self, n_packets):
        return max((n_packets // 10) * 10, self.default_dataset_chunk_size)

    def __get_protocol_group(self, src_addr, dst_addr, src_port, dst_port, proto):
        """

        :param src_addr:
        :param dst_addr:
        :param src_port:
        :param dst_port:
        :param proto:
        :return: The protocol group, the path to that group
        :rtype: ``tuple`` of ``Group``, ``str``
        """
        src_group = self.get_subgroup(self.root_group, path_to_group=self.root_group_name, subgroup_name=src_addr)
        path = self.root_group_name + '/' + src_addr

        dst_group = self.get_subgroup(src_group, path, dst_addr)
        path += '/' + dst_addr

        sport_group = self.get_subgroup(dst_group, path, src_port)
        path += '/' + src_port

        dport_group = self.get_subgroup(sport_group, path, dst_port)
        path += '/' + dst_port

        proto_group = self.get_subgroup(dport_group, path, proto)
        path += '/' + proto

        return proto_group, path

    def get_subgroup(self, group, path_to_group, subgroup_name):
        """ Returns a subgroup, creating it if it does not exist. """
        if not path_to_group + '/' + subgroup_name in self.file:
            return group.create_group(subgroup_name)
        else:
            return group[subgroup_name]

    def open(self, mode='a'):
        super(HDF5SortedDataset, self).open(mode=mode)
        self.root_group = self.file.create_group(self.root_group_name)

    def close(self):
        super(HDF5SortedDataset, self).close()
        self.root_group = None


def flow_file_to_flow_data(flow_file, current_path, hdf5_file, action):
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        return

    # Extract label
    label = "-".join(flow_file_parts[0].split("-")[:-1]).strip()
    if label == 'Backdoor':
        label = 'Backdoors'

    label_index = __get_label_index(hdf5_file['/' + LABELS_GROUP_NAME], label)

    # Present the data of the flow file to the feature extraction function
    with open(os.path.join(current_path, flow_file), 'r') as opened_file:

        while True:
            flowdata = FlowData(opened_file)

            # Extract features from the flow file
            flow_features_list = action['function'](flowdata, **action['params'])

            for packet in flow_features_list:
                __add_to_hdf5(hdf5_file, label, label_index, array_to_store=packet)

            # If the end of the flow file has been reached
            if flowdata.is_EOF():
                # Stop the while loop, we have nothing more to do
                break


def flow_data_to_array(flow_data, n_bytes=100):
    """
        Extract all simple vectors from the given flow file, sorting them in batches of n_packets, where n_batches is
        specified in feature_config.

        :param flow_data: Path to flow file to extract from
        :param dataset_config: Configuration of dataset to extract features from
        :type flow_data: ``FlowData``
        :type dataset_config: Object inherited from ``DatasetConfig``
        :return: List containing one extracted simple vectors array
        :rtype: ``list`` of ``numpy.array``
        """
    #n_packets = 1
    return_list = []

    line = next(flow_data)

    while not flow_data.has_stopped():
        # Take the possible presence of timestamps into account
        packet = line.split(",")[-1].rstrip("\n")

        # Calculate the image from the packet
        simple_vector, _ = packet_to_bytes(packet, max_n=n_bytes)

        # Prepare next line/packet
        return_list.append(simple_vector)
        line = next(flow_data)

    return return_list


def __get_label_index(labels_group, label):
    mapping = labels_group.attrs['label_indices']

    for lab, i in mapping:
        if lab == label:
            return i

    return -1


def __add_to_hdf5(hdf5_file, label, label_index, array_to_store):

    flows_dataset = hdf5_file['/' + FLOWS_GROUP + '/flows']
    __dataset_resize_check(flows_dataset)

    idx = flows_dataset.attrs['numel']
    last_label, corresp_idx = flows_dataset.attrs['last_label']

    # Check label requirements
    if not last_label == label_index:
        if not last_label == 'None':
            corresp_idx = int(corresp_idx)
            regref = flows_dataset.regionref[corresp_idx:idx]
            __store_label_regref(hdf5_file, label, regref)

        flows_dataset.attrs['last_label'] = label, idx

    flows_dataset[idx] = (array_to_store, label_index)

    flows_dataset.attrs['numel'] = idx + 1


def __store_label_regref(hdf5_file, label, regref):
    label_dataset = hdf5_file['/' + LABELS_GROUP_NAME + '/' + label]
    __dataset_resize_check(label_dataset)

    idx = label_dataset.attrs['numel']

    label_dataset[idx] = regref

    label_dataset.attrs['numel'] = idx + 1


def __dataset_resize_check(dataset):
    """
    Check whether a dataset needs to be resized, as a result of it being completely filled.

    :param dataset:
    :return:
    """
    cur_idx = dataset.attrs['numel']
    bs = dataset.attrs['batch_size']

    if (cur_idx % bs) == 0:
        dataset.resize(dataset.shape[0] + bs, axis=0)


def __remove_redundant_records(hdf5_file):
    def visitor_func(name, node):
        # node is a dataset
        if isinstance(node, h5py.Dataset):
            __remove_redundant_rows(node)
        # node is a group
        else:
            pass

    hdf5_file.visititems(visitor_func)


def __remove_redundant_rows(dataset):
    """
    Remove redundant (empty) rows from a dataset

    :param dataset:
    :return:
    """
    cur_idx = dataset.attrs['numel']
    bs = dataset.attrs['batch_size']

    n_to_remove = bs - (cur_idx % bs)

    if n_to_remove > 0:
        dataset.resize(dataset.shape[0] - n_to_remove, axis=0)


def start_hdf5_file(filename, dataset_labels):
    file = h5py.File(name=filename, mode='w')

    #flow_references_group = file.create_group(FLOW_REFERENCES_GROUP_NAME)
    labels_group = file.create_group(LABELS_GROUP_NAME)
    flows_group = file.create_group(FLOWS_GROUP, track_order=True)

    # Initialize datasets:
    # We increase dataset sizes in batches
    # After storing all data inside the dataset, the dataset is resized to fit the final shape
    flows_batch_size = 10e6
    labels_batch_size = 10e4

    # Flows dataset
    # 'u1' = Unsigned integer ('u'), 1 byte => 8-bit
    # 'i4' = Signed integer ('i'), 4 bytes => 32-bit
    ds_dt = np.dtype({'names': ['packet', 'label'],
                      'formats': [('u1', 100), 'i4']})

    flows_dataset = flows_group.create_dataset(chunks=(1000, 1),
                                               name='flows',
                                               shape=(flows_batch_size, 1),
                                               maxshape=(None, 1),
                                               dtype=ds_dt,
                                               track_order=False)

    # Note:
    # Enabling track_order will track the order of the attributes
    # If you then update an attribute for each data member, this will create a new attribute
    # The current limit of attributes appears to be 2 ^ 15 = 32768 (probably signed 16-bit integer).
    # Therefore, if track_order is True, it is unwise to update attributes too often.
    # However, for this application, the order of attributes should not be retained.

    flows_dataset.attrs['batch_size'] = flows_batch_size
    flows_dataset.attrs['numel'] = 0
    flows_dataset.attrs['last_label'] = ('None', 0)

    label_indices = {}
    # Label datasets contain region references to regions in the flows dataset that contain samples of the chosen type
    for i in range(len(dataset_labels)):
        label_indices[dataset_labels[i]] = i
        label_dataset = labels_group.create_dataset(chunks=(labels_batch_size, 1),
                                                    name=dataset_labels[i],
                                                    shape=(labels_batch_size, 1),
                                                    maxshape=(None, 1),
                                                    dtype=h5py.regionref_dtype)
        label_dataset.attrs['batch_size'] = labels_batch_size
        label_dataset.attrs['numel'] = 0

    labels_group.attrs['label_indices'] = [(key, item) for key, item in label_indices.items()]

    return file


def test_fun(config_file, hdf5_filename):
    dataset_configuration = get_config_from_file(config_file)

    labels_list = dataset_configuration.get_labels()

    hdf5_file = start_hdf5_file(hdf5_filename, labels_list)

    sorted_flows_root = dataset_configuration.get_sorted_flows_root('friday')

    process_flow_tree(root=sorted_flows_root, action={"function": flow_file_to_flow_data,
                                                      "params": {"hdf5_file": hdf5_file,
                                                                 "action": {'function': flow_data_to_array,
                                                                            'params': {}}}
                                                      }, verbose=True)

    __remove_redundant_records(hdf5_file)

    print("Finished!")


if __name__ == '__main__':
    pass