import os
from datetime import datetime
import numpy as np


class Flow(object):

    def __init__(self, src_addr, dst_addr, src_port, dst_port, proto, label, fwd_packets=0, bwd_packets=0,
                 label_idx=-1):
        """
        Data members:
        src_addr = string (source address of flow)
        dst_addr = string (destination address of flow)
        src_port = string (source port of flow)
        dst_port = string (destination port of flow)
        proto = string    (transport layer protocol)

        fwd_packets = int (number of packets going from src -> dst)
        bwd_packets = int (number of packets going from dst -> src)
        """
        self.src_addr = src_addr
        self.dst_addr = dst_addr
        self.src_port = src_port
        self.dst_port = dst_port
        self.proto = proto
        self.label = label

        self.fwd_packets = fwd_packets
        self.bwd_packets = bwd_packets

        self.label_idx = label_idx

    def __str__(self):
        return self.src_addr + "-" + self.dst_addr + "-" + self.src_port + "-" + self.dst_port + "-" + self.proto +\
            "|fwd:" + str(self.fwd_packets) + "|bwd:" + str(self.bwd_packets) + "|" + self.label

    def __eq__(self, flow):
        """

        """
        return (self.src_addr == flow.src_addr) and \
               (self.dst_addr == flow.dst_addr) and \
               (self.src_port == flow.src_port) and \
               (self.dst_port == flow.dst_port) and \
               (self.label == flow.label) and \
               (self.fwd_packets == flow.fwd_packets) and \
               (self.bwd_packets == flow.bwd_packets)

    def equal_id(self, flow):
        return (self.src_addr == flow.src_addr) and \
               (self.dst_addr == flow.dst_addr) and \
               (self.src_port == flow.src_port) and \
               (self.dst_port == flow.dst_port) and \
               (self.proto    == flow.proto)

    def get_flow_size(self):
        """
        Get the total number of packets transported in this flow (forward and backward traffic).
        """
        return self.fwd_packets + self.bwd_packets

    def get_storage_dir(self, root):
        return os.path.join(root, self.src_addr, self.dst_addr, self.src_port, self.dst_port, self.proto)

    def get_filename(self):
        return self.src_addr + "-" + self.dst_addr + "-" + self.src_port + "-" + self.dst_port + ".txt"

    def get_filename_v2(self):
        return '{}-{}-{}-{}-{}.txt'.format(self.src_addr, self.dst_addr, self.src_port, self.dst_port, self.proto)

    def get_return_flow(self):
        return Flow(src_addr=self.dst_addr, dst_addr=self.src_addr, src_port=self.dst_port, dst_port=self.src_port,\
                    proto=self.proto, label=self.label, fwd_packets=self.bwd_packets, bwd_packets=self.fwd_packets)

    def get_flow_id(self):
        return '{}-{}-{}-{}-{}'.format(self.src_addr, self.dst_addr, self.src_port, self.dst_port, self.proto)

    def is_in_timeframe(self, ts_sec, ts_ns):
        return True


class CICIDS2017Flow(Flow):
    def __init__(self, src_addr, dst_addr, src_port, dst_port, proto, label, fwd_packets=0, bwd_packets=0,
                 timestamp=None, duration=-1, use_ts=False):
        """
        Data members:
        src_addr = string (source address of flow)
        dst_addr = string (destination address of flow)
        src_port = string (source port of flow)
        dst_port = string (destination port of flow)
        proto = string    (transport layer protocol)

        fwd_packets = int (number of packets going from src -> dst)
        bwd_packets = int (number of packets going from dst -> src)
        """
        super().__init__(src_addr, dst_addr, src_port, dst_port, proto, label,
                         fwd_packets=fwd_packets, bwd_packets=bwd_packets)

        if use_ts:
            if len(timestamp) >= 19:
                # Monday for some reason provides differently formatted timestamps
                dt = np.datetime64(datetime.strptime(timestamp, '%d/%m/%Y %H:%M:%S')) + np.timedelta64(3, 'h')
            else:
                # Timestamps provided in the CICIDS2017 label files are accurate up to the minute
                # Provide one minute offset to including everything in this range
                # Also provide +3 hours to account for time zone
                dt = np.datetime64(datetime.strptime(timestamp, '%d/%m/%Y %H:%M')) + np.timedelta64(3, 'h')

            self.timestamp_begin = dt
            self.timestamp_end = self.timestamp_begin + np.timedelta64(duration, 'us') + np.timedelta64(1, 'm')
        else:
            self.timestamp_begin = None
            self.timestamp_end = None

    def is_in_timeframe(self, ts_sec, ts_ns):
        timestamp = np.datetime64(ts_sec, 's') + np.timedelta64(ts_ns, 'ns')

        return self.timestamp_begin < timestamp < self.timestamp_end


class ISCX2012Flow(Flow):
    def __init__(self, src_addr, dst_addr, src_port, dst_port, proto, label, fwd_packets=0, bwd_packets=0,
                 start_ts=None, end_ts=None, use_ts=False):
        """
        Data members:
        src_addr = string (source address of flow)
        dst_addr = string (destination address of flow)
        src_port = string (source port of flow)
        dst_port = string (destination port of flow)
        proto = string    (transport layer protocol)

        fwd_packets = int (number of packets going from src -> dst)
        bwd_packets = int (number of packets going from dst -> src)

        start_ts = string like '2010-06-12T23:57:24'
        """
        super().__init__(src_addr, dst_addr, src_port, dst_port, proto, label,
                         fwd_packets=fwd_packets, bwd_packets=bwd_packets)

        if use_ts:
            # Timestamps provided in the ISCX2012 label files are accurate up to the second
            # Provide 999 ms offset to including everything in this range
            self.starttime= np.datetime64(start_ts)
            self.stoptime = np.datetime64(end_ts) + np.timedelta64(999, 'ms')
        else:
            self.starttime = None
            self.stoptime = None

    def is_in_timeframe(self, ts_sec, ts_ns):
        #timestamp = np.datetime64((ts_high << 32) + ts_low, 'us')
        timestamp = np.datetime64(ts_sec, 's') + np.timedelta64(ts_ns, 'ns')
        return self.starttime < timestamp < self.stoptime
