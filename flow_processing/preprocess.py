"""
This module contains generic functions that are used in the preprocessing of all datasets as well as the extraction of
their features.
"""

import os

from enums import Dataset, FeatureStrategy
from flow_processing import csv_cleaner
from flow_processing.preprocess_dataset import preprocess_dataset, create_symbolic_links
from flow_processing.feature_extraction import generate_features as extract_features


def _preprocess_dataset_csvs_params(dataset, verbose=True):
    """
    Gives the settings for preprocessing the CSV files of a dataset.

    :param dataset: Dataset to process
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type dataset: ``Dataset``
    :type verbose: ``bool``
    :return: Tuple "(clean, minimize, columns)" containing information regarding the preprocessing of the data CSVs. \
    "clean" indicates whether the CSV should be cleaned (set to utf-8, remove redundant NaN records). \
    "minimize" indicates whether the only the specified columns of a CSV should be used. \
    "columns" is a list of the names of the columns that should be used in case of minimization.
    :rtype: ``tuple`` of ``(``bool``, ``bool``, ``str``)``
    """
    # CSV preprocessing for CICIDS2017
    if dataset == Dataset.CICIDS2017:
        if verbose:
            print("The data csvs will be cleaned and minimalized before preprocessing.")
        return (True, True,
                [" Label", " Source IP", " Source Port", " Destination IP", " Destination Port",
                 " Protocol", " Flow Duration", " Total Fwd Packets", " Total Backward Packets",
                 " Flow Packets/s", " Timestamp"])
    # CSV preprocessing for ISCX2012
    elif dataset == Dataset.ISCX2012:
        if verbose:
            print("Nothing will be done with the data csvs.")
        return False, False, []

    elif dataset == Dataset.UNSW_NB15:
        if verbose:
            print("The data csvs will be minimized")
        return (False, True,
                ["srcip", "sport", "dstip", "dsport", "proto",
                 "spkts", "dpkts", "stime", "ltime", "attack_cat", "Label"])

    # CSV preprocessing for DARPA1998
    elif dataset == Dataset.DARPA1998:
        if verbose:
            print("The data csvs will not be cleaned, but will be minimalized before preprocessing.")
        # Should not be necessary to minimize
        return (False, False,
                ["idx", "date", "start_time", "duration", "src_port",
                 "dst_port", "src_addr", "dst_addr", "attack_name"])


def preprocess_dataset_csv(dataset, old_file, path_to_csv_dir, verbose=True):
    """
    Function to preprocess a dataset CSV (containing the labelled data).

    :param dataset: Dataset to process
    :param old_file: Name of the dataset CSV file to preprocess
    :param path_to_csv_dir: Path to the directory containing the dataset CSV file
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type dataset: ``Dataset``
    :type old_file: ``str``
    :type path_to_csv_dir: ``str``
    :type verbose: ``bool``
    :return: None
    """
    clean, minimize, columns = _preprocess_dataset_csvs_params(dataset, verbose=verbose)

    # If the csv needs to be minimized or cleaned, a new file will be generated:
    # Determine the name of that new file
    new_file = ""
    if clean or minimize:
        new_file = old_file.strip(".csv") + "-light.csv"
    
    old_csv = os.path.join(path_to_csv_dir, old_file)
    new_csv = os.path.join(path_to_csv_dir, new_file)
    _preprocess_csv(clean, minimize, columns, old_csv, new_csv, verbose=verbose)


def _preprocess_csv(clean, minimalize, columns, csv_file, new_csv_file, verbose=True):
    """
    Function to preprocess a dataset CSV (containing the labelled data). Here the actual preprocessing functions are
    called.

    Parameters:
    clean (bool): True if the CSV needs to be cleaned (turned to utf-8, NaN records removed)
    minimalize (bool): True if the CSV needs to be minimized (reduced number of columns)
    columns (list): List of the columns that will be used (in case of minimalization)
    csv_file (string): Path to the the dataset CSV file to preprocess
    new_csv_file (string): Path of the new dataset CSV file that will be generated
    verbose (bool): If True, the algorithm will output relevant information at each preprocess step

    Returns:
    None
    """
    if clean and minimalize:
        csv_cleaner.clean_and_minimalize(csv_file, new_csv_file, columns, encoding="utf-8", verbose=verbose)
    elif clean:
        csv_cleaner.clean(csv_file, new_csv_file, encoding="utf-8", verbose=verbose)
    elif minimalize:
        csv_cleaner.minimalize(csv_file, new_csv_file, columns, verbose=verbose)
    else:
        if verbose:
            print("No cleaning or minimalization occurred.")


def preprocess(dataset, root, xml_root=None, verbose=True, **kwargs):
    """
    Preprocess the selected dataset.

    :param dataset: Selected dataset
    :param root: Path to the root of the project
    :param xml_root: Path to the directory containing xml label files. Optional, only for ISCX2012
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :param kwargs: Additional arguments for the dataset configuration. Consult ``dataset_config.DatasetConfig`` for \
    more information.
    :type xml_root: ``str``
    :type dataset: ``Dataset``
    :type root: ``str``
    :type verbose: ``bool``

    :return: None
    """
    if verbose:
        print("Checking the provided " + dataset.name + " dataset")

    return preprocess_dataset(dataset=dataset, root=root, verbose=verbose, xml_root=xml_root, **kwargs)


def generate_features(dataset_config, features_root, featurestrategy,
                      use_existing=True, verbose=True, **kwargs):
    """
    Preprocess the selected dataset for the selected feature extraction strategy.

    :param datasets_root: Root directory where all datasets are stored
    :param features_root: Root directory where all extracted features are stored
    :param dataset: Selected dataset
    :param featurestrategy: Selected feature extraction strategy
    :param use_existing: If True, use
    :param verbose: If True, print additional informative text as output
    :param kwargs: See ``FeatureStrategyConfig`` of the desired feature extraction strategy
    :type datasets_root: ``str``
    :type features_root: ``str``
    :type dataset: ``Dataset``
    :type featurestrategy: ``FeatureStrategy``
    :type verbose: ``bool``
    :return: Feature extraction configuration object
    """
    return extract_features(dataset_config=dataset_config, features_root=features_root,
                            featurestrategy=featurestrategy, use_existing=use_existing, verbose=verbose, **kwargs)


def generate_symbolic_links(dataset_config, datasets_storage):
    """
    Create symbolic link to dataset files to avoid copying existing dataset files and wasting storage.

    :param dataset_config: Dataset configuration for which the symbolic links need to be created
    :param datasets_storage: Path to storage directory containing the actual dataset files.
    :return: None
    """
    create_symbolic_links(dataset_config, datasets_storage)


if __name__ == "__main__":
    # Remark: do not run this module as the main, for this reason: 
    # https://stackoverflow.com/questions/26589805/python-enums-across-modules
    
    pass
