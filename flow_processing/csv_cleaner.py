import pandas as pd

# This file allow the generation of correctly encoded csv files, if the files are not correctly encoded
# (contain many nan appended to end of file)


def _to_clean_and_minimalized_utf8(csv_file, new_file, columns, verbose = False):
    df = pd.read_csv(csv_file, encoding="latin1", low_memory=False)
    # print(df.loc[0:1,["Flow ID","Label"]])
    # print(df.columns)
    # print(df.loc[:,"Label"])
    
    # Count the number of correct rows
    idx = -1
    for _, row in df.iterrows():
        #print("\n\n\n")
        #print(row[1])
        #print("\n\n\n")
        if pd.isna(row[0]):
            break
        idx += 1
    if verbose: print("There are " + str(idx) + " correct rows")
    
    # Write the legitimate rows, as well as the relevant columns, to a new file
    df.loc[:idx, columns].to_csv(new_file, mode="w+")

def _to_clean_utf8(csv_file, new_file, verbose = False):
    df = pd.read_csv(csv_file, encoding="latin1", low_memory=False)

    # Count the number of correct rows
    idx = -1
    for item in df["Flow ID"]:
        if pd.isna(item):
            break
        idx += 1
    if verbose: print("There are " + str(idx) + " correct rows")
    
    # Write the legitimate rows, as well as the relevant columns, to a new file
    df.loc[:idx].to_csv(new_file, mode="w+")

def _to_minimalized(csv_file, new_file, columns, verbose = False):
    df = pd.read_csv(csv_file, encoding="utf-8", low_memory=False)

    # Write relevant columns to new csv file
    if verbose: print("Retaining the " + ", ".join(columns) + " columns.")
    df.loc[:, columns].to_csv(new_file, mode="w+")

def clean(csv_file, new_file, encoding = "utf-8", verbose=False):
    if encoding == "utf-8":
        if verbose: print("Cleaning csv and encoding to utf-8..")
        _to_clean_utf8(csv_file=csv_file, new_file=new_file, verbose=verbose)
        return True

    return False

def minimalize(csv_file, new_file, columns, verbose=False):
    if verbose: print("Minimalizing the csv file...")
    _to_minimalized(csv_file,new_file,columns, verbose)

def clean_and_minimalize(csv_file, new_file, columns, encoding = "utf-8", verbose=False):
    if encoding == "utf-8":
        if verbose: print("Cleaning csv and encoding to utf8. Minimalizing too..")
        _to_clean_and_minimalized_utf8(csv_file=csv_file, new_file=new_file, columns=columns, verbose=verbose)
        return True

    return False

if __name__ == "__main__":    
    csv_file = "Thursday-WorkingHours-Morning-WebAttacks.pcap_ISCX.csv"
    new_file = "Thursday-WorkingHours-Morning-WebAttacks-light.csv"
    _to_clean_utf8(csv_file=csv_file, new_file=new_file, verbose=True)