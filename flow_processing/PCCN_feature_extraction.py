import ctypes
import os
import numpy as np
from exceptions import CTypesInvalidArgument, NotImplementedException
from enums import FeatureStrategy
from features_config import PCCNConfig
from help_functions import process_flow_tree, remove_dir, operate_on_bytes, shape_to_str
from enums import Dataset
# Constants for feature extraction
# header_min = 0
# header_max = 100
# payload_min = 100
# payload_max = 200
# hepa_min = 0
# hepa_max = 192

# First, the absolute path to the folder is necessary (because at that path the .so file is located)
# Generate .so file using cc -fPIC -shared -o pcap_parser.so pcap_parser.c -lpcap
folder = os.path.dirname(os.path.abspath(__file__))
so_file = os.path.join("custom_libraries", "lib_pccn_features.so")
dll_path = os.path.join(folder, "..", so_file)
pccn = None
#pccn = ctypes.CDLL(dll_path)


def move_and_aggregate_features(features_root, target_dir):
    """
    Move and aggregate the features, spread over multiple days, into one folder for machine learning.

    """
    features = {}
    features['header'] = {}
    features['hepa'] = {}
    features['payload'] = {}
    for subdir in os.listdir(features_root):
        subdir_path = os.path.join(features_root, subdir)
        
        for feature_file in os.listdir(subdir_path):
            traffic_class = feature_file[:-len(feature_file.split('-')[-1]) - 1]
            approach = feature_file.strip('.txt').split('-')[-1]
            
            path_to_file = os.path.join(subdir_path, feature_file)
            try:
                features[approach][traffic_class].append(path_to_file)
            except:
                features[approach][traffic_class] = [path_to_file]
    #print(features)

    header_target = os.path.join(target_dir, "header")
    hepa_target = os.path.join(target_dir, "hepa")
    payload_target = os.path.join(target_dir, "payload")
    
    if not os.path.isdir(header_target):
        os.mkdir(header_target)
    if not os.path.isdir(hepa_target):
        os.mkdir(hepa_target)
    if not os.path.isdir(payload_target):
        os.mkdir(payload_target)

    for key, value in features.items():
        for traffic_class, file_paths in value.items():
            with open(os.path.join(target_dir, key, traffic_class + ".txt"), "w") as ofile:
                for file_path in file_paths:
                    with open(file_path, "r") as ifile:
                        for line in ifile:
                            ofile.write(line)


def __extract_chars_PCCN_header(line, header_string, config):
    i = 0
    for char in line:
        if i < config.header_min:
            pass
        elif i > config.header_max:
            break
        else:
            if config.header_min <= i < config.header_max:
                header_string += char
            i += 1
    # If not all strings have been sufficiently filled
    # Append with '0' until requirements have been met
    while i < config.stop:
        if config.header_min <= i < config.header_max:
            header_string += '0'
        i += 1

    return header_string


def __extract_chars_PCCN_payload(line, payload_string, config):
    i = 0
    for char in line:
        if i < config.payload_min:
            pass
        elif i > config.payload_max:
            break
        else:
            if config.payload_min <= i < config.payload_max:
                payload_string += char
            i += 1
    # If not all strings have been sufficiently filled
    # Append with '0' until requirements have been met
    while i < config.stop:
        if config.payload_min <= i < config.payload_max:
            payload_string += '0'
        i += 1

    return payload_string


def __extract_chars_PCCN_hepa(line, hepa_string, config):
    i = 0
    for char in line:
        if i < config.start:
            pass
        elif i > config.stop:
            break
        else:
            if config.hepa_min <= i < config.hepa_max:
                hepa_string += char
            i += 1
    # If not all strings have been sufficiently filled
    # Append with '0' until requirements have been met
    while i < config.stop:
        if config.hepa_min <= i < config.hepa_max:
            hepa_string += '0'
        i += 1

    return hepa_string


def _extract_chars_PCCN(line, header_string, payload_string, hepa_string, config):
    """
    Extract characters from a line corresponding with the PCCN method:
        -header characters
        -payload characters
        -header and payload characters
    
    Params:

    config(PCCNConfig): Configuration of the PCCN approach
    """
    i = 0
    for char in line:
        if i < config.start:
            pass
        elif i > config.stop:
            break
        else:
            if config.header_min <= i < config.header_max:
                header_string += char
            if config.payload_min <= i < config.payload_max:
                payload_string += char
            if config.hepa_min <= i < config.hepa_max:
                hepa_string += char
            i += 1
    # If not all strings have been sufficiently filled
    # Append with '0' until requirements have been met
    while i < config.stop:
        if config.header_min <= i < config.header_max:
            header_string += '0'
        if config.payload_min <= i < config.payload_max:
            payload_string += '0'
        if config.hepa_min <= i < config.hepa_max:
            hepa_string += '0'
        i += 1
    
    return header_string, payload_string, hepa_string


def generate_numeric_hepa_features(flow_data, config, dataset_config):
    packet_counter = 0
    hepa_feature_arrays = []

    hepa_size = (config.hepa_max - config.hepa_min) // 2 * config.num_packets + config.num_packets - 1
    hepa_side = int(np.sqrt(hepa_size))

    def place_byte_in_array(byte, array, progress_list):
        array[progress_list[0]] = int(byte, 16)
        progress_list[0] += 1

    # Initialize new strings
    hepa_feature = ""

    for line in flow_data:
        # Line preprocessing
        if dataset_config.include_ts:
            _, _, line = line.split(",")
        # Remove trailing line ending
        line = line.rstrip("\n")

        # The packet's information can simply be extracted
        hepa_feature = __extract_chars_PCCN_hepa(line, hepa_feature, config)

        # Add required FF-bytes
        if packet_counter < (config.num_packets - 1):
            hepa_feature += "FF"

        # If the feature has been generated
        if packet_counter == (config.num_packets - 1):
            # Turn the packet strings into numpy-arrays
            hepa_array = np.zeros(hepa_size)
            hepa_progress_list = [0]

            operate_on_bytes(hepa_feature, action={'function': place_byte_in_array,
                                                   'params': {
                                                       'array': hepa_array,
                                                       'progress_list': hepa_progress_list,
                                                   }
                                                   })
            hepa_feature_arrays.append(hepa_array.reshape((hepa_side, hepa_side)))

            hepa_feature = ""
            packet_counter = 0
        else:
            packet_counter += 1

    # If packet counter > 0, append zero-packets
    if packet_counter > 0:
        hepa_array = np.zeros(hepa_size)
        hepa_progress_list = [0]
        operate_on_bytes(hepa_feature, action={'function': place_byte_in_array,
                                               'params': {
                                                   'array': hepa_array,
                                                   'progress_list': hepa_progress_list,
                                               }
                                               })
        hepa_feature_arrays.append(hepa_array.reshape((hepa_side, hepa_side)))

    return hepa_feature_arrays


def generate_numeric_payload_features(flow_data, config, dataset_config):
    packet_counter = 0
    payload_feature_arrays = []

    payload_size = (config.payload_max - config.payload_min) // 2 * config.num_packets + config.num_packets + 1
    payload_side = int(np.sqrt(payload_size))

    def place_byte_in_array(byte, array, progress_list):
        array[progress_list[0]] = int(byte, 16)
        progress_list[0] += 1

    # Initialize new strings
    payload_feature = ""

    for line in flow_data:
        # Line preprocessing
        if dataset_config.include_ts:
            _, _, line = line.split(",")
        # Remove trailing line ending
        line = line.rstrip("\n")

        if packet_counter == 0:
            # If we generate new feature strings, first add required heading FF-bytes
            payload_feature += "FF"

        # Then extract the packet's information
        payload_feature = __extract_chars_PCCN_payload(line, payload_feature, config)

        # Add required FF-bytes
        payload_feature += "FF"

        # If the feature has been generated
        if packet_counter == (config.num_packets - 1):
            # Turn the packet strings into numpy-arrays
            payload_array = np.zeros(payload_size)
            payload_progress_list = [0]

            operate_on_bytes(payload_feature, action={'function': place_byte_in_array,
                                                      'params': {
                                                         'array': payload_array,
                                                         'progress_list': payload_progress_list,
                                                        }
                                                     })
            payload_feature_arrays.append(payload_array.reshape((payload_side, payload_side)))

            payload_feature = ""
            packet_counter = 0
        else:
            packet_counter += 1

    # If packet counter > 0, append zero-packets
    if packet_counter > 0:
        payload_array = np.zeros(payload_size)
        payload_progress_list = [0]

        operate_on_bytes(payload_feature, action={'function': place_byte_in_array,
                                                  'params': {
                                                      'array': payload_array,
                                                      'progress_list': payload_progress_list,
                                                  }
                                                  })
        payload_feature_arrays.append(payload_array.reshape((payload_side, payload_side)))

    return payload_feature_arrays


def generate_numeric_header_features(flow_data, config, dataset_config):
    packet_counter = 0
    header_feature_arrays = []

    header_size = (config.header_max - config.header_min) // 2 * config.num_packets + config.num_packets + 1
    header_side = int(np.sqrt(header_size))

    def place_byte_in_array(byte, array, progress_list):
        array[progress_list[0]] = int(byte, 16)
        progress_list[0] += 1

    # Initialize new strings
    header_feature = ""

    for line in flow_data:
        # Line preprocessing
        if dataset_config.include_ts:
            _, _, line = line.split(",")
        # Remove trailing line ending
        line = line.rstrip("\n")

        if packet_counter == 0:
            # If we generate new feature strings, first add required heading FF-bytes
            header_feature += "FF"
        # Then extract the packet's information
        header_feature = __extract_chars_PCCN_header(line, header_feature, config)

        # Add required FF-bytes
        header_feature += "FF"

        # If the feature has been generated
        if packet_counter == (config.num_packets - 1):
            # Turn the packet strings into numpy-arrays
            header_array = np.zeros(header_size)
            header_progress_list = [0]
            operate_on_bytes(header_feature, action={'function': place_byte_in_array,
                                                     'params': {
                                                         'array': header_array,
                                                         'progress_list': header_progress_list,
                                                     }
                                                     })

            header_feature_arrays.append(header_array.reshape((header_side, header_side)))

            header_feature = ""
            packet_counter = 0
        else:
            packet_counter += 1

            # If packet counter > 0, append zero-packets
            if packet_counter > 0:
                header_array = np.zeros(header_size)
                header_progress_list = [0]
                operate_on_bytes(header_feature, action={'function': place_byte_in_array,
                                                         'params': {
                                                             'array': header_array,
                                                             'progress_list': header_progress_list,
                                                         }
                                                         })

                header_feature_arrays.append(header_array.reshape((header_side, header_side)))

            return header_feature_arrays


def generate_numeric_features(ifile, include_ts, config):
    """
    For a given flow file, extract header, payload and header-payload features. These features are represented in
    numpy arrays and stored in a list.

    :param ifile: Path to input flow file to extract features from
    :param include_ts: Should be True if the flow file contains timestamps
    :param config: Feature extraction configuration object
    :type config: ``PCCNConfig``
    :type ifile: ``str``
    :type include_ts: ``bool``

    :return: header_feature_arrays, payload_feature_arrays, hepa_feature_arrays: The three lists containing the \
    images extraction from the flow file as numpy arrays.
    :rtype: ``tuple`` of ``(``list``, ``list``, ``list``)``
    """

    with open(ifile, "r") as f:

        packet_counter = 0
        header_feature_arrays = []
        payload_feature_arrays = []
        hepa_feature_arrays = []

        header_size = (config.header_max - config.header_min) // 2 * config.num_packets + config.num_packets + 1
        header_side = int(np.sqrt(header_size))
        payload_size = (config.payload_max - config.payload_min) // 2 * config.num_packets + config.num_packets + 1
        payload_side = int(np.sqrt(payload_size))
        hepa_size = (config.hepa_max - config.hepa_min) // 2 * config.num_packets + config.num_packets - 1
        hepa_side = int(np.sqrt(hepa_size))

        def place_byte_in_array(byte, array, progress_list):
            array[progress_list[0]] = int(byte, 16)
            progress_list[0] += 1

        # Initialize new strings
        header_feature = ""
        payload_feature = ""
        hepa_feature = ""

        for _, line in enumerate(f):
            # Line preprocessing
            if include_ts:
                try:
                    _, _, line = line.split(",")
                except ValueError:
                    print('\n\nDefective line: {}\nIn file {}'.format(line, ifile))
                    raise ValueError('not enough values to unpack (expected 3, got 1)')
            # Remove trailing line ending
            line = line.rstrip("\n")

            if packet_counter == 0:
                # If we generate new feature strings, first add required heading FF-bytes
                header_feature += "FF"
                payload_feature += "FF"
                # Then extract the packet's information
                header_feature, payload_feature, hepa_feature = _extract_chars_PCCN(line, header_feature,
                                                                                    payload_feature, hepa_feature,
                                                                                    config)
            else:
                # The packet's information can simply be extracted
                header_feature, payload_feature, hepa_feature = _extract_chars_PCCN(line, header_feature,
                                                                                    payload_feature, hepa_feature,
                                                                                    config)

            # Add required FF-bytes
            header_feature += "FF"
            payload_feature += "FF"
            if packet_counter < (config.num_packets - 1):
                hepa_feature += "FF"

            # If the feature has been generated
            if packet_counter == (config.num_packets - 1):
                # Turn the packet strings into numpy-arrays
                header_array = np.zeros(header_size)
                header_progress_list = [0]
                payload_array = np.zeros(payload_size)
                payload_progress_list = [0]
                hepa_array = np.zeros(hepa_size)
                hepa_progress_list = [0]
                operate_on_bytes(header_feature, action={'function': place_byte_in_array,
                                                         'params': {
                                                             'array':header_array,
                                                             'progress_list':header_progress_list,
                                                            }
                                                        })

                header_feature_arrays.append(header_array.reshape((header_side, header_side)))

                operate_on_bytes(payload_feature, action={'function': place_byte_in_array,
                                                          'params': {
                                                             'array': payload_array,
                                                             'progress_list': payload_progress_list,
                                                            }
                                                         })
                payload_feature_arrays.append(payload_array.reshape((payload_side, payload_side)))

                operate_on_bytes(hepa_feature, action={'function': place_byte_in_array,
                                                        'params': {
                                                             'array': hepa_array,
                                                             'progress_list': hepa_progress_list,
                                                            }
                                                        })
                hepa_feature_arrays.append(hepa_array.reshape((hepa_side, hepa_side)))

                header_feature = ""
                payload_feature = ""
                hepa_feature = ""
                packet_counter = 0
            else:
                packet_counter += 1

        # If packet counter > 0, append zero-packets
        if packet_counter > 0:
            header_array = np.zeros(header_size)
            header_progress_list = [0]
            payload_array = np.zeros(payload_size)
            payload_progress_list = [0]
            hepa_array = np.zeros(hepa_size)
            hepa_progress_list = [0]
            operate_on_bytes(header_feature, action={'function': place_byte_in_array,
                                                     'params': {
                                                         'array': header_array,
                                                         'progress_list': header_progress_list,
                                                     }
                                                     })

            header_feature_arrays.append(header_array.reshape((header_side, header_side)))

            operate_on_bytes(payload_feature, action={'function': place_byte_in_array,
                                                      'params': {
                                                          'array': payload_array,
                                                          'progress_list': payload_progress_list,
                                                      }
                                                      })
            payload_feature_arrays.append(payload_array.reshape((payload_side, payload_side)))

            operate_on_bytes(hepa_feature, action={'function': place_byte_in_array,
                                                   'params': {
                                                       'array': hepa_array,
                                                       'progress_list': hepa_progress_list,
                                                   }
                                                   })
            hepa_feature_arrays.append(hepa_array.reshape((hepa_side, hepa_side)))

        return header_feature_arrays, payload_feature_arrays, hepa_feature_arrays


def generate_features_python(ifile, ofile_headers, ofile_payload, ofile_hepa, include_ts, config):
    """
    Generate textual PCCN features using a Python implementation.

    :param ifile: Path to input file from which the PCCN features will be extracted
    :type ifile: ``str``
    :param ofile_headers: Paht of output file for the header features
    :type ofile_headers: ``str``
    :param ofile_payload: Path of output file for the payload features
    :type ofile_payload: ``str``
    :param ofile_hepa: Path of output file for the header-payload features
    :type ofile_hepa: ``str``
    :param include_ts: Must be True if the input file contains timestamps
    :type include_ts: ``bool``
    :param config: Feature extraction configuration
    :type config: ``PCCNConfig``
    :return: None
    """
    
    with open(ifile, "r") as f:
        
        packet_counter = 0
        
        # Initialize new strings
        header_feature = ""
        payload_feature = ""
        hepa_feature = ""
        
        for _, line in enumerate(f):
            # Line preprocessing
            if include_ts:
                _, _, line = line.split(",")
            # Remove trailing line ending
            line = line.rstrip("\n")

            if packet_counter == 0:
                # If we generate new feature strings, first add required heading FF-bytes
                header_feature += "FF"
                payload_feature += "FF"
                # Then extract the packet's information
                header_feature, payload_feature, hepa_feature = _extract_chars_PCCN(line, header_feature, payload_feature, hepa_feature, config)
            else:
                # The packet's information can simply be extracted
                header_feature, payload_feature, hepa_feature = _extract_chars_PCCN(line, header_feature, payload_feature, hepa_feature, config)
            
            # Add required FF-bytes
            header_feature += "FF"
            payload_feature += "FF"
            if packet_counter < (config.num_packets - 1):
                hepa_feature += "FF"

            # If the feature has been generated
            if packet_counter == (config.num_packets - 1):
                with open(ofile_headers, "a") as of:
                    of.write(header_feature + "\n")
                with open(ofile_payload, "a") as of:
                    of.write(payload_feature + "\n")
                with open(ofile_hepa, "a") as of:
                    of.write(hepa_feature + "\n")

                header_feature = ""
                payload_feature = ""
                hepa_feature = ""
                packet_counter = 0
            else:
                packet_counter += 1

        # If packet counter > 0, append zero-packets
        if packet_counter > 0:
            while packet_counter < config.num_packets:
                header_feature += "".join(['0' for i in range(config.header_max - config.header_min)]) + "FF"
                payload_feature += "".join(['0' for i in range(config.payload_max - config.payload_min)]) + "FF"
                hepa_feature += "".join(['0' for i in range(config.hepa_max - config.hepa_min)]) + "FF"

                packet_counter += 1
            hepa_feature = hepa_feature.rstrip("FF")

            with open(ofile_headers, "a") as of:
                of.write(header_feature + "\n")
            with open(ofile_payload, "a") as of:
                of.write(payload_feature + "\n")
            with open(ofile_hepa, "a") as of:
                of.write(hepa_feature + "\n")


def generate_features_c(ifile, ofile_headers, ofile_payload, ofile_hepa, include_ts):
    """
    Generate PCCN-based features for the specified input file and store them in the specified output files.
    DEPRECATED

    :param ifile: Path to input file from which the PCCN features will be extracted
    :type ifile: ``str``
    :param ofile_headers: Paht of output file for the header features
    :type ofile_headers: ``str``
    :param ofile_payload: Path of output file for the payload features
    :type ofile_payload: ``str``
    :param ofile_hepa: Path of output file for the header-payload features
    :type ofile_hepa: ``str``
    :param include_ts: Must be True if the input file contains timestamps
    :type include_ts: ``bool``

    :return: None
    """
    if pccn is None:
        pccn = ctypes.CDLL(dll_path)

    # Make sure the necessary files are present
    if ifile==None or ofile_headers==None or ofile_payload==None or ofile_hepa==None:
        if ifile == None:
            raise CTypesInvalidArgument("Input file undefined")
        elif ofile_headers == None:
            raise CTypesInvalidArgument("Ouput file for header features undefined")
        elif ofile_hepa == None:
            raise CTypesInvalidArgument("Output file for header&payload features undefined")
        elif ofile_payload == None:
            raise CTypesInvalidArgument("Output file for payload features undefined")
    
    # Check if the input file exists:
    if not os.path.isfile(ifile):
        raise CTypesInvalidArgument("Specified inputfile \"{}\" does not exist. Please provide an existing file as input.".format(ifile))

    c_ifile = ifile.encode("utf-8")
    c_ofile_headers = ofile_headers.encode("utf-8")
    c_ofile_payload = ofile_payload.encode("utf-8")
    c_ofile_hepa = ofile_hepa.encode("utf-8")
    
    #print(ifile, ofile_headers, ofile_payload, ofile_hepa)
    #print(ofile_hepa)
    c_include_ts = 0
    if include_ts:
        c_include_ts = 1
    
    c_return = pccn.PCCN_features(c_ifile, c_ofile_headers, c_ofile_payload, c_ofile_hepa, c_include_ts)

    if c_return == 0:
        return
        #print("Done", c_return)
    else:
        print(ifile, ofile_headers, ofile_payload, ofile_hepa)

# def generate_filenames(label):
#     """
#     Generate the necessary filenames for the given label.
#
#     Params:
#     label (str): The label for which feature filenames must be generated
#
#     Returns:
#     list containing the generated filenames
#     """
#     return [label + "-header" + ".txt", label + "-payload" + ".txt", label + "-hepa" + ".txt"]


def flow_data_to_numeric_features_PCCN(flow_data, features_config, dataset_config):

    results = []
    while not flow_data.is_EOF():
        if features_config.get_feature_type() == 'header':
            results += generate_numeric_header_features(flow_data=flow_data, config=features_config, dataset_config=dataset_config)
        elif features_config.get_feature_type() == 'payload':
            results += generate_numeric_payload_features(flow_data=flow_data, config=features_config, dataset_config=dataset_config)
        elif features_config.get_feature_type() == 'hepa':
            results += generate_numeric_hepa_features(flow_data=flow_data, config=features_config, dataset_config=dataset_config)
        else:
            raise NotImplementedException(
                'Please select at least one feature type out of Header, Payload or Header-Payload (HePa)'
            )
    return results


def flow_file_to_numeric_features_PCCN(flow_file, current_path, root_target_dir, include_ts, config, arrays):
    """

    :param flow_file: Name of file containing the flow to process
    :param current_path: Path to the directory containing the flow file
    :param root_target_dir: Directory to store the generated numeric features into
    :param include_ts: Should be True if the flow file contains timestamps
    :param config: Configuration for the feature extraction
    :param arrays: Dictionary containing a list of all numeric features, stored per header, payload, hepa and label. \
    Structure is the following: {'header': {'label1": list1, 'label2': list2}, 'payload': {...}, 'hepa': {...}}
    :type flow_file: ``str``
    :type current_path: ``str``
    :type root_target_dir: ``str``
    :type include_ts: ``bool``
    :type config: ``PCCNConfig``
    :type arrays: ``dict`` of ``{``str``: ``{``str``: ``list`` of ``numpy.ndarray``}``}``
    :return: None
    """
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        return

    label = "-".join(flow_file_parts[0].split("-")[:-1])

    header_arrays, payload_arrays, hepa_arrays = generate_numeric_features(os.path.join(current_path, flow_file),
                                                                           include_ts, config)
    arrays['header'][label] += header_arrays
    arrays['payload'][label] += payload_arrays
    arrays['hepa'][label] += hepa_arrays

    for tag in ['header', 'payload', 'hepa']:
        if len(arrays[tag][label]) >= config.batch_size:

            memmap_shape = tuple([len(arrays[tag][label])] + list(arrays[tag][label][0].shape))
            output_dir = os.path.join(root_target_dir, tag, label)

            output_file = "arrays_" + str(len(os.listdir(output_dir))) + '--' + shape_to_str(memmap_shape) + '.npy'

            # Instead of saving in a compressed file, we use a memory mapped file with dtype=uint8
            #np.savez_compressed(os.path.join(output_dir, output_file), *arrays[tag][label])
            memmap = np.memmap(os.path.join(output_dir, output_file),
                               mode="w+",
                               shape=memmap_shape,
                               dtype=np.uint8)
            memmap[:] = np.array(arrays[tag][label])

            # - Empty list
            arrays[tag][label] = []


def flow_file_to_features_PCCN(flow_file, current_path, root_target_dir, include_ts, config):
    """
    Generated features for a flow file, based on the PCCN feature extraction strategy and stored in text files

    :param flow_file: Name of file containing the flow to process
    :param current_path: Path to the directory containing the flow file
    :param root_target_dir: Directory to store the generated text file into
    :param include_ts: Should be True if the flow file contains timestamps
    :param config: Configuration for the feature extraction
    :type flow_file: ``str``
    :type current_path: ``str``
    :type root_target_dir: ``str``
    :type include_ts: ``bool``
    :type config: ``PCCNConfig``
    :return: None
    """
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")
    
    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        return
    
    label = "-".join(flow_file_parts[0].split("-")[:-1])
    
    # Find files that features need to be stored in
    target_file_header = os.path.join(root_target_dir, label + "-header" + ".txt")
    target_file_payload= os.path.join(root_target_dir, label + "-payload" + ".txt")
    target_file_hepa = os.path.join(root_target_dir, label + "-hepa" + ".txt")
    
    if config.use_c_libs:
        # Perform optimized feature extraction
        generate_features_c(os.path.join(current_path, flow_file), target_file_header, target_file_payload,
                            target_file_hepa, include_ts)
    else:
        generate_features_python(os.path.join(current_path, flow_file), target_file_header, target_file_payload,
                                 target_file_hepa, include_ts, config)


def generate_PCCN_features(datasets_root, features_root, dataset, use_existing=True, verbose=True, **kwargs):
    """
    :param datasets_root: Root directory where all datasets are stored
    :param features_root: Root directory where all extracted features are stored
    :param dataset: Selected dataset
    :param use_existing: If True and applicable, use an existing configuration
    :param verbose: If True, print additional informative text as output
    :param kwargs: See ``FeatureStrategyConfig`` and ``PCCNConfig`` for more information
    :type datasets_root: ``str``
    :type features_root: ``str``
    :type dataset: ``Dataset``
    :type use_existing: ``bool``
    :return: PCC Configuration
    :rtype: ``PCCNConfig``
    """
    # Generate the configuration
    config = PCCNConfig(features_root=features_root, datasets_root=datasets_root, dataset=dataset,
                        use_existing=use_existing, verbose=verbose, **kwargs)
    
    # Generate the target output directory:
    output_dir = os.path.join(config.features_root, config.name)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    # Use the configuration to traverse the flow trees for the different days of the dataset
    dataset_config = config.get_dataset_configuration()
    for day in dataset_config.get_labelled_days():
        # Only use days for which features have not been generated
        if not config.features_generated[day]:

            if verbose:
                print('Generating features for', day)

            target_output_dir = os.path.join(output_dir, day)
            # Clean previous feature data
            if os.path.isdir(target_output_dir):
                if verbose:
                    print("Removing previous generated features:", target_output_dir)
                remove_dir(target_output_dir, remove_root=True, verbose=verbose)
            # Create clean directory
            os.mkdir(target_output_dir)

            # Get root directory containing sorted flows to extract features from
            sorted_flows_root = dataset_config.get_sorted_flows_root(day)

            # Determine right action:
            # A: Generate numeric features
            if config.numeric_features:
                arrays = {}
                for tag in ['header', 'payload', 'hepa']:
                    os.mkdir(os.path.join(target_output_dir, tag))
                    arrays[tag] = {}
                    for label in dataset_config.get_labels(day):
                        os.mkdir(os.path.join(target_output_dir, tag, label))
                        arrays[tag][label] = []
                action = {
                    "function": flow_file_to_numeric_features_PCCN,
                    "params": {
                        "root_target_dir": target_output_dir,
                        "include_ts": dataset_config.include_ts,
                        "config": config,
                        "arrays": arrays,
                    }
                }
                process_flow_tree(root=sorted_flows_root,
                                  action=action,
                                  verbose=verbose)

                for tag, label_dict in arrays.items():
                    for label, array_list in label_dict.items():
                        if len(array_list) == 0:
                            continue
                        remainder_output_dir = os.path.join(target_output_dir, tag, label)

                        #np.savez_compressed(os.path.join(remainder_output_dir, remainder_output_file), *array_list)

                        memmap_shape = tuple([len(arrays[tag][label])] + list(arrays[tag][label][0].shape))

                        remainder_output_file = "arrays_" + str(len(os.listdir(remainder_output_dir))) + '--' + \
                                                shape_to_str(memmap_shape) + '.npy'

                        # Instead of saving in a compressed file, we use a memory mapped file with dtype=uint8
                        memmap = np.memmap(os.path.join(remainder_output_dir, remainder_output_file),
                                           mode="w+",
                                           shape=memmap_shape,
                                           dtype=np.uint8)
                        memmap[:] = np.array(arrays[tag][label])
            # B: Generate text-files with features
            else:
                action = {
                    "function": flow_file_to_features_PCCN,
                    "params": {
                        "root_target_dir": target_output_dir,
                        "include_ts": dataset_config.include_ts,
                        "config": config
                    }
                }
                process_flow_tree(root=sorted_flows_root,
                                  action=action,
                                  verbose=verbose)
            # Process the sorted flows to get the features
            if verbose:
                print("\nFinished generating features for", day)
            
            # Save progress
            config.features_generated[day] = True
            config.update()

    return config


if __name__ == "__main__":
    source_dir = os.path.join("cicids2017tester", "CICIDS2017", "features")
    target_dir = os.path.join("..", "learning", "data")
    move_and_aggregate_features(source_dir, target_dir)
