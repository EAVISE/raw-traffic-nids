"""
Module responsible for all things related to the extraction and processing of the UNSW-NB15 dataset.
"""

import os
import help_functions as hf
from dataset_config import UNSW_NB15Config, get_dataset_days
from enums import Dataset
from flow_processing import preprocess, flow_sorter, flow_labeler
from flow_processing.flow import Flow


def setup_preliminaries(root, use_existing=True, verbose=True, **kwargs):
    """
    Generate all directories required for the preprocessing of a dataset at the given location.

    :param root: Path to root directory where the dataset will be stored and processed.
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :param use_existing: If True, try to use an existing UNSW-NB15 configuration
    :param kwargs: Consult ``dataset_config.UNSW_NB15Config`` for available keyword arguments.
    :type root: ``str``
    :type verbose: ``bool``
    :type use_existing: ``bool``

    :return: Dataset configuration, list of days with data, \
    list of days without data
    :rtype: ``UNSW_NB15Config``, ``list``, ``list``
    """
    # Necessary structure
    # Days in UNSW-NB15:
    # 22/01 -> Write as 22-1 (not 22-01)
    # 17/02 -> Write as 17-2 (not 17-02)
    # days = ["22-01", "17-02"]
    days = get_dataset_days(Dataset.UNSW_NB15)
    dataset_dirs = ["csv_data", "pcap_data", "raw_packets", "sorted_flows", "debug"]

    config = UNSW_NB15Config(root, days=days, use_existing=use_existing, verbose=verbose, **kwargs)

    # Check if the configuration has already been completed
    if config.is_completed():
        return config, None, None

    # Create roots
    data_root = config.data_root

    if not os.path.isdir(data_root):
        os.mkdir(data_root)

    # Create necessary directories for dataset dir, if applicable
    if verbose: print("Checking presence of necessary directories")
    for d in dataset_dirs:
        dir_path = os.path.join(data_root, d)
        if not os.path.isdir(dir_path):
            if verbose: print("Creating directory " + dir_path)
            os.mkdir(dir_path)
        for day in days:
            if not os.path.isdir(os.path.join(dir_path, day)):
                if verbose: print("Creating directory " + os.path.join(dir_path,day))
                os.mkdir(os.path.join(dir_path, day))

    # Check downloading settings:
    if config.use_download:
        for day in days:
            # DOWNLOAD PCAP FILES
            for filename, cmd, status in config.download_status_pcaps[day]:
                if not status:
                    # Remove remains of previous, unsuccesful download
                    target_file = os.path.join(config.data_root, "pcap_data", day, filename)
                    if os.path.isfile(target_file):
                        os.remove(target_file)
                    # Download the file
                    if verbose: print("Downloading", filename)
                    output = os.system(cmd)
                    # Check if the download was completed correctly
                    if output == 0:
                        config.update_pcaps_download_status(day, filename)
            
            # DOWNLOAD LABEL CSV FILES
            for filename, cmd, status in config.download_status_labels[day]:
                if not status:
                    # Remove remains of previous, unsuccesful download
                    target_file = os.path.join(config.data_root, "csv_data", day, filename)
                    if os.path.isfile(target_file):
                        os.remove(target_file)
                    if verbose:
                        print("Downloading", filename)
                    output = os.system(cmd)
                    # Check if the download was completed correctly
                    if output == 0:
                        config.update_labels_download_status(day, filename)

    # Split UNSW-NB15_2.csv
    # Problem: UNSW-NB15_2.csv contains data for both 22-01 as well as 17-02. Therefore, when downloaded,
    # the csv must be split to correctly assign the right labels to the right flows
    # Check if UNSW-NB15_2.csv is present
    target_file = os.path.join(config.data_root, dataset_dirs[0], days[0], 'UNSW-NB15_2.csv')
    print('Splitting', target_file, os.path.isfile(target_file))
    if os.path.isfile(target_file):
        target_file_split = os.path.join(config.data_root, dataset_dirs[0], days[1], 'UNSW-NB15_2.csv')
        # Check if it has already been split
        if not os.path.isfile(target_file_split):
            _split_label_csv(target_file, target_file_split, config, verbose=verbose)
        else:
            print('File was previously split, no splitting was conducted')
    # Check that the dataset directories are not empty
    empty_dirs = [] # List where all empty dirs will be put into

    # We check beginning from the csv files, but for each csv directory the corresponding pcap directory is checked
    for day in os.listdir(os.path.join(data_root, dataset_dirs[0])):
        if os.path.isdir(os.path.join(data_root, dataset_dirs[0], day)):

            # Check if this directory is empty
            this_dir_empty = len(os.listdir(os.path.join(data_root, dataset_dirs[0], day))) < 1
            
            # Download status: If the files needed to be downloaded, all downloads have to be completed succesfully
            if config.use_download:
                download_status = config.has_completed_downloading_labels(day) and \
                                  config.has_completed_downloading_pcaps(day)
            else:
                download_status = True
            
            # Check if the corresponding pcap directory is empty
            correspond_dir_empty = len(os.listdir(os.path.join(data_root, dataset_dirs[1], day))) < 1
            
            if this_dir_empty or correspond_dir_empty or not download_status:
                empty_dirs.append(day)

    if verbose: print("Dataset directories for " + ", ".join(empty_dirs) +
                      " are empty. A dataset consists of csv-files and pcap(ng) files for a certain day.")
    if len(empty_dirs) >= len(days):
        print("All dataset directories are empty. No dataset available. Please add a dataset.")
        config.update()
        return config, None, None

    # Find all days with datasets:
    full_dirs = [day for day in days if day not in empty_dirs]
    for day in full_dirs:
        config.dataset_present[day] = True

    config.update()

    return config, full_dirs, dataset_dirs


def _split_label_csv(target1, target2, config, verbose=True):
    """
    Function to split a UNSW-NB15 label csv file.

    :param target1: Path of the file to split, the first part after splitting will remain at this path
    :param target2: Path to file to be created that contains second part after splitting.
    :param config: UNSW-NB15 configuration with contains the line to split on
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type target1: ``str``
    :type target2: ``str``
    :type config: ``UNSW_NB15Config``
    :type verbose: ``bool``
    """

    if verbose: print("Splitting", target1, "at line", config.get_csv_split_line(), "to target", target2)
    # Create temporary file in same directory as target1
    tempfile = os.path.join(os.path.split(target1)[0], "tempfile.csv")

    # Copy the first part of the split to tempfile, and the second part to target2
    with open(target1, "r") as f1:
        with open(target2, "w") as f2:
            with open(tempfile, "w") as ftemp:
                for idx, line in enumerate(f1):
                    if idx < config.get_csv_split_line():
                        ftemp.write(line)
                    else:
                        f2.write(line)
    # Remove target1 and turn tempfile into target1
    os.remove(target1)
    os.rename(tempfile, target1)


def flow_sorting(raw_packets_storage_path, flows_storage_path, ts_included, verbose=True):
    """
    Sort extracted network packets in flows according to their flow identifier (flow ID).

    :param raw_packets_storage_path: Path to the directory containing the extracted packets to sort.
    :param flows_storage_path: Path to the directory the sorted flows will be stored into.
    :param ts_included: If timestamps were extracted along with packets, set True
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type raw_packets_storage_path: ``str``
    :type flows_storage_path: ``str``
    :type ts_included: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    # Put the raw packet files in the right order:
    filelist = hf.list_files_ordered_from_path(path=raw_packets_storage_path, trailer=".csv", verbose=verbose)
    # Sort the packets inside the generated csv-files
    for raw_packets_file in filelist:
        # Check if file is a csv file:
        if raw_packets_file.split(".")[-1] == "csv":
            # Calculate correct file path
            raw_packets_file_path = os.path.join(raw_packets_storage_path, raw_packets_file)
        
            if verbose: print("Sorting flows for " + raw_packets_file)
            flow_sorter.sort_flows_1_file(raw_packets_file_path, flows_storage_path, ts_included=ts_included,
                                          verbose=verbose)


def get_columns():
    """
    Get the column headers of a UNWS-NB15 label file.

    :return: List of the column headers of a UNNSW-NB15 label file in the correct order.
    :rtype: ``list`` of ``str``
    """
    return ["srcip", "sport", "dstip", "dsport", "proto",
            "state", "dur", "sbytes", "dbytes", "sttl", "dttl",
            "sloss", "dloss", "service", "sload", "dload",
            "spkts", "dpkts", "swin", "dwin", "stcpb", "dtcpb",
            "smeansz", "dmeansz", "trans_depth", "res_bdy_len",
            "sjit", "djit", "stime", "ltime", "sintpkt", "dintpkt",
            "tcprtt", "synack", "ackdat", "is_sm_ips_ports",
            "ct_state_ttl", "ct_flw_http_mthd", "is_ftp_login",
            "ct_ftp_cmd", "ct_srv_src", "ct_srv_dst", "ct_dst_ltm",
            "ct_src_ltm", "ct_src_dport_ltm", "ct_dst_sport_ltm",
            "ct_dst_src_ltm", "attack_cat", "Label"]


def csv_preliminaries(csv_file, column_headers, config, verbose):
    """
    Perform some preliminary actions specifically required for UNSW-NB15 files.

    :param csv_file: Path to CSV file
    :param column_headers: Column headers of the USNW-NB15 label CSV file
    :param config: UNSW-NB15 dataset configuration
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_file: ``str``
    :type column_headers: ``list`` of ``str``
    :type verbose: ``bool``
    :type config: ``UNSW_NB15Config``
    :return: None
    """

    with open(csv_file, "r") as f:
        filelines = f.readlines()[1:]

    # Remove junk from the first line (which is necessary for the first csv file)
    # filelines[0] = hf.remove_front_not_numbers(filelines[0])

    with open(csv_file, "w") as f:
        # Add column headers to  the csv file
        if verbose:
            print("Adding column names to " + csv_file)
        f.write(",".join(column_headers) + "\n")

        # Iterate over every data line
        if verbose:
            print("Relabeling labels " + csv_file)
            if config.only_tcp_udp:
                print("Non-TCP/UDP flows are skipped.")

        for line in filelines:
            cols = line.split(",")
            # Check if we only need to keep TCP and UDP labels
            if config.only_tcp_udp and not ((cols[4] == "tcp") or (cols[4] == "udp")):
                # If we only need TCP and UDP flows, and this line does not represent such a flow,
                # we ignore it.
                continue
            else:
                # If necessary, attach the traffic label
                cols[47] = _get_correct_label(cols[47])
                f.write(",".join(cols))


def add_column_names(csv_file, names, verbose=False):
    """
    Add the column names to the label csvs of UNSW-NB15.

    :param csv_file: Path to CSV label file to add column names to
    :param names: Names to add to the file
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_file: ``str``
    :type names: ``list`` of ``str``
    :type verbose: ``bool``
    :return: None
    """
    if verbose:
        print("Adding column names to " + csv_file)

    with open(csv_file, "r") as f:
        filelines = f.readlines()
    
    # Remove junk from the first line (which is necessary for the first csv file)
    filelines[0] = hf.remove_front_not_numbers(filelines[0])
    
    with open(csv_file, "w") as f:
        f.write(",".join(names)+"\n")
        f.writelines(filelines)


def _get_correct_label(label):
    """
    Function to correct labels that are not correctly formulated in the provided label files.

    :param label: Label to correct
    :type label: ``str``
    :return: Corrected label
    :rtype: ``str``
    """
    if label == "":
        return "Normal"
    elif label == " Fuzzers":
        return "Fuzzers"
    else:
        return label


def relabel_csv(csv_file, verbose=False):
    """
    As no all labels in the UNSW-NB15 CSV files are in the ideal format, relabel those label files.

    :param csv_file: Path to CSV file to relabel
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_file: ``str``
    :type verbose: ``bool``
    :return:
    """
    if verbose:
        print("Relabeling the labels in " + csv_file)

    with open(csv_file, "r") as f:
        filelines = f.readlines()

    with open(csv_file, "w") as f:
        # Write column headers to the file
        f.write(filelines[0])
        # For every other line, the labels must be redone
        for i in range(1, len(filelines)):
            columns = filelines[i].split(",")
            columns[47] = _get_correct_label(columns[47])
            f.write(",".join(columns))


def csv_preprocess(csv_data_dir, config, verbose=True):
    """
    Preprocess the UNSW-NB15 CSV label files before using them for labelling.

    :param csv_data_dir: Path to directory containing all CSV label files.
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :param config: UNSW-NB15 dataset configuration
    :type csv_data_dir: ``str``
    :type config: ``UNSW_NB15Config``
    :type verbose: ``bool``
    :return: None
    """
    # Process each CSV file
    for labels_csv in os.listdir(csv_data_dir):
        # Add column headers, relabel some labels and skip unnecessary flows
        csv_preliminaries(os.path.join(csv_data_dir, labels_csv), get_columns(), config, verbose=verbose)
        # First, add column names to the CSV files
        #add_column_names(os.path.join(csv_data_dir, labels_csv), get_columns(), verbose=verbose)

        # Then, correctly add labels whenever necessary
        #relabel_csv(os.path.join(csv_data_dir, labels_csv), verbose=verbose)

        # Then, minimize the label CSVs
        if labels_csv.split("-")[-1].strip(".csv") != "light":
            if verbose:
                print("Cleaning and minimizing " + labels_csv)
            preprocess.preprocess_dataset_csv(Dataset.UNSW_NB15, old_file=labels_csv, path_to_csv_dir=csv_data_dir,
                                              verbose=True)


def csv_process_order(day):
    """
    For both, multiple labelling CSV files exist. Due to the chronological nature of the PCAP data, it is
    important that these are used in the right order when labelling flows.

    :param day: '22-1' or '22-01' or '17-2' or '17-02'
    :type day: ``str``

    :return: A list with the order of the CSV files. The list contains the CSV filenames.
    :rtype: ``list`` of ``str``
    """
    if ("22-1" in day) or ("22-01" in day):
        return ["UNSW-NB15_1-light.csv", "UNSW-NB15_2-light.csv"]
    elif ("17-2" in day) or ("17-02" in day):
        return ["UNSW-NB15_2-light.csv", "UNSW-NB15_3-light.csv", "UNSW-NB15_4-light.csv"]
    else:
        return []


def flow_labelling(csv_data_dir, flows_root_dir, error_file, day, config, verbose=True):
    """
    Label the UNSW-NB15 flows.

    :param csv_data_dir: Path to directory containing the CSV label files.
    :param flows_root_dir: Path to the root directory containing al sorted flows.
    :param error_file: Path to file to store error messages into
    :param day: Day for which the flows will be labelled
    :param config: UNSW-NB15 dataset configuration
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_data_dir: ``str``
    :type flows_root_dir: ``str``
    :type error_file: ``str``
    :type day: ``str``
    :type config: ``UNSW_NB15Config``
    :type verbose: ``bool``
    :return: None
    """
    # Label the flows
    order = csv_process_order(day)
    for item in order:
        # Process the next item in the order
        for labels_csv in os.listdir(csv_data_dir):
            # Check if the file is next in the order
            if item in labels_csv:
                # Only use relevant CSV files
                if labels_csv.split("-")[-1].strip(".csv") == "light":
                    if verbose:
                        print("Labelling according to " + labels_csv)
                    
                    flow_labeler.label_sorted_flows(label_csv=os.path.join(csv_data_dir, labels_csv),
                                                    flows_root_dir=flows_root_dir,
                                                    error_file=error_file,
                                                    config=config, verbose=verbose)


def label_csv_line_to_flow(line, use_ts=True):
    """
    Extract the flow information from a line of a light label CSV file for UNSW-NB15 to create the corresponding flow.

    :param line: Line CSV file line containing flow information.
    :param use_ts: If True, use the timestamps provided in the label CSV file.
    :type line: ``str``
    :type use_ts: ``bool``

    :return: The CICIDS2017 flow corresponding to the line.
    :rtype: ``CICIDS2017Flow``
    """
    [_, srcip, sport, dstip, dsport, proto, spkts, dpkts, _, _, attack_cat, _] = line.rstrip("\n").split(",")

    # Sometimes, the int values are stored as float strings ("48.0" instead of "48")
    # These have to be converted to int strings: "48.0" -> 48.0 -> 48 -> "48"
    src_port = str(int(float(sport)))   # Used
    dst_port = str(int(float(dsport)))  # Used

    proto = hf.get_protcol_number(proto)

    # Are not used as strings
    total_fwd = int(float(spkts))  # Used
    total_bwd = int(float(dpkts))  # Used if bidirectional == True

    # We added "Normal" to the attack_cat column, so we only need that column
    label = attack_cat

    return Flow(srcip, dstip, src_port, dst_port, proto, label, total_fwd, total_bwd)


if __name__ == "__main__":
    pass

