import os

from parsers import pcap_parser_c
from parsers.flow_parsing import packet_file_to_raw_packets, packet_file_to_sorted_flows, \
    packet_file_to_sorted_stream_flows
from flow_processing import CICIDS2017, ISCX2012, UNSW_NB15
from help_functions import process_flow_tree, remove_empty_dirs, list_files_ordered_from_path, \
    get_all_files_with_extension
from flow_processing.hdf5_interface import HDF5FlowStorage
from enums import Dataset
import time


def __get_dataset_pcap_files(dataset, day, datasets_storage):
    """
    Get all prerequisite pcap(ng) files for the specified dataset on the specified day.

    :param dataset: Specified dataset
    :type dataset: ``Dataset``
    :param day: Specified dataset day
    :type day: ``str``
    :return: A list of all the paths to the required dataset pcap(ng) files
    :rtype: ``list`` of ``str``
    """
    target_dir = os.path.join(datasets_storage, dataset.name, day, 'pcap')

    filenames = get_all_files_with_extension(target_dir, 'pcap') + get_all_files_with_extension(target_dir, 'pcapng')

    return [os.path.join(target_dir, filename) for filename in filenames]


def __get_dataset_csv_files(dataset, day, datasets_storage):
    """
    Get all prerequisite csv files for the specified dataset on the specified day.

    :param dataset: Specified dataset
    :type dataset: ``Dataset``
    :param day: Specified dataset day
    :type day: ``str``
    :return: A list of all the paths to the required dataset csv files
    :rtype: ``list`` of ``str``
    """
    target_dir = os.path.join(datasets_storage, dataset.name, day, 'csv')
    return [os.path.join(target_dir, filename) for filename in get_all_files_with_extension(target_dir, 'csv')]


def create_symbolic_links(dataset_config, datasets_storage):
    """
    Create symbolic link to dataset files to avoid copying existing dataset files and wasting storage.

    :param dataset_config: Dataset configuration for which the symbolic links need to be created
    :param datasets_storage: Path to storage directory containing the actual dataset files.
    :return: None
    """
    for day in dataset_config.days:
        # Create PCAP symlinks
        # 1) Target PCAP destination dir
        dest_dir = os.path.join(dataset_config.data_root, 'pcap_data', day)

        # 2) Get all PCAP(NG) filepaths
        pcap_filepaths = __get_dataset_pcap_files(Dataset[dataset_config.dataset], day, datasets_storage)

        # 3) Create a symbolic link for each file
        for filepath in pcap_filepaths:
            filename = os.path.split(filepath)[-1]
            symlink_path = os.path.join(dest_dir, filename)

            if not os.path.isfile(symlink_path):
                os.symlink(filepath, symlink_path)

        # Create CSV symlinks
        # 1) Target PCAP destination dir
        dest_dir = os.path.join(dataset_config.data_root, 'csv_data', day)

        # 2) Get all PCAP(NG) filepaths
        csv_filepaths = __get_dataset_csv_files(Dataset[dataset_config.dataset], day, datasets_storage)

        print(csv_filepaths)

        # 3) Create a symbolic link for each file
        for filepath in csv_filepaths:
            filename = os.path.split(filepath)[-1]
            symlink_path = os.path.join(dest_dir, filename)
            os.symlink(filepath, symlink_path)

        # Finished generating symlinks!


def pcap_to_sorted_stream_flows(pcap_data_dir, sorted_flows_dir, config, day, verbose=True):
    """
    Function to extract relevant packets from a PCAP or PCAPNG file in a stream-based manner. These packets are sorted
    and stored at the designated location.

    :param pcap_data_dir: Path to the directory containing the packet capture files.
    :param sorted_flows_dir: Path to the directory designated for sorted flows storage.
    :param config: Dataset configuration
    :param verbose: If True, report on progress. Default True.
    :type pcap_data_dir: ``str``
    :type sorted_flows_dir: ``str``
    :type config: ``DatasetConfig``
    :type verbose: ``bool``
    :return: None
    """
    # Process all datafiles
    # First, sort the datafiles chronologically, if necessary

    pcap_files = os.listdir(pcap_data_dir)
    if len(pcap_files) > 1:
        pcap_files = list_files_ordered_from_path(pcap_data_dir, '.pcap', verbose=verbose)

    # Then, process the files one after another
    for pcap_data_file in pcap_files:
        print(pcap_data_file)
        pcap_data_path = os.path.join(pcap_data_dir, pcap_data_file)

        extension = pcap_data_file.split(".")[-1]

        if os.path.isfile(pcap_data_path) and \
                ((extension == "pcap") or (extension == "tcpdump") or (extension == "pcapng")):
            # Extraction arguments
            ifile = pcap_data_path  # Input (.pcap/.pcapng) file
            packet_file_to_sorted_stream_flows(ifile, config, storage_path=sorted_flows_dir, day=day, verbose=verbose)


def pcap_extract_and_sort(day, pcap_data_dir, sorted_flows_dir, config, debug_file=None, verbose=True):
    """
    Function to extract relevant packets from a PCAP or PCAGNG file, and sort those packets into their flows.\n
    Sorted flows are order in a directory tree: src_addr -> dst_addr -> src_port -> dst_port -> protocol -> packets\n
    Packets are stored inside .txt files following a csv scheme.\n

    :param pcap_data_dir: Path to directory containing the PCAP/PCAPNG files to extract packets from
    :param sorted_flows_dir: Path to directory to store the sorted flows into
    :param config: Dataset configuration
    :param debug_file: Path to optional debug file that can be used to store debug information
    :param verbose: If True, print additional informative text as output
    :type pcap_data_dir: ``str``
    :type sorted_flows_dir: ``str``
    :type config: ``DatasetConfig``
    :type debug_file: ``str``
    :type verbose: ``bool``
    """
    # Process all datafiles
    # First, sort the datafiles chronologically, if necessary

    pcap_files = os.listdir(pcap_data_dir)
    if len(pcap_files) > 1:
        pcap_files = list_files_ordered_from_path(pcap_data_dir, '.pcap', verbose=verbose)

    if config.enable_hdf5:
        kwargs = {
            'use_hdf5': True,
            'hdf5_packet_size': config.hdf5_packet_size,
            'hdf5_filename': config.get_sorted_hdf5_path(day),
            'labels': config.get_labels(day)
        }
    else:
        kwargs = {}

    for pcap_data_file in pcap_files:
        print(pcap_data_file)
        pcap_data_path = os.path.join(pcap_data_dir, pcap_data_file)

        if os.path.isfile(pcap_data_path) and \
                ((pcap_data_file.split(".")[-1] == "pcap") or (pcap_data_file.split(".")[-1] == "tcpdump")
                 or (pcap_data_file.split(".")[-1] == "pcapng")):

            # Extraction arguments
            ifile = pcap_data_path  # Input (.pcap) file

            if config.use_c_libs:
                debug = debug_file  # Debug file, for diagnostics
                bran = 0  # Start of the range of packets to be processed
                eran = -1  # End of the range of packets to be processed (-1 indicates all following packets)

                # Run the internal C function
                pcap_parser_c.pcap_to_sorted_flows(input_file=ifile, root=sorted_flows_dir,
                                                   debug_file=debug, bran=bran, eran=eran, batch_size=config.batch_size,
                                                   verbose=verbose, include_ts=config.include_ts)
            else:
                # Use Python implementation
                packet_file_to_sorted_flows(ifile, config.batch_size, config.include_ts, sorted_flows_dir, verbose, **kwargs)


def pcap_extraction(pcap_data_dir, raw_packets_storage_path, config, debug_file=None, verbose=True):
    """
    Function to extract relevant packets from a PCAP or PCAPNG file, and write their raw content into txt files.

    :param pcap_data_dir: Path to directory containing the PCAP/PCAPNG files to extract packets from
    :param raw_packets_storage_path: Path to directory to store the raw packets into
    :param config: Dataset configuration
    :param debug_file: Path to optional debug file that can be used to store debug information
    :param verbose: If True, print additional informative text as output
    :type pcap_data_dir: ``str``
    :type raw_packets_storage_path: ``str``
    :type config: ``DatasetConfig``
    :type verbose: ``bool``
    :return: None
    """
    # Process all datafiles
    for pcap_data_file in os.listdir(pcap_data_dir):
        pcap_data_path = os.path.join(pcap_data_dir, pcap_data_file)

        if os.path.isfile(pcap_data_path) and \
                ((pcap_data_file.split(".")[-1] == "pcap") or (pcap_data_file.split(".")[-1] == "tcpdump")
                 or (pcap_data_file.split(".")[-1] == "pcapng")):

            # First, define if any previous calculations has been done
            raw_csvs = os.listdir(raw_packets_storage_path)  # calculated CSVs with
            for raw_csv in raw_csvs:
                # Get index rank of the file:
                rank = int(raw_csv.split("-")[-1].strip(".csv"))
                if rank >= len(raw_csvs):
                    # If we have a batch size n, then each file contains the results of n processed packets
                    # (which are not necessarily all stored). The rank of a file then is the index of the batch that
                    # was processed, starting to count from 1. Examples:
                    # Rank 1: [0, n-1]
                    # Rank 2: [n, 2*n - 1]
                    # Rank 3: [2*n, 3*n - 1] ...
                    # One can be sure a file contains all necessary packets when the next file has been created
                    # (indicating that the next batch was started, so that the relevant back was finished).
                    os.remove(os.path.join(raw_packets_storage_path, raw_csv))
                    if verbose:
                        print("Removing " + os.path.join(raw_packets_storage_path, raw_csv) + " for being unfinished.")

            # Update the list of processed raw packets csv files
            raw_csvs = os.listdir(raw_packets_storage_path)  # calculated CSVs with

            # Extraction arguments
            ifile = pcap_data_path  # Input (.pcap) file
            ofile = os.path.join(raw_packets_storage_path, pcap_data_file.strip(".pcap") + "-")  # Output raw csv file
            debug = debug_file  # Debug file, for diagnostics
            bran = len(raw_csvs) * config.batch_size  # Start of the range of packets to be processed
            eran = -1  # End of the range of packets to be processed (-1 indicates all following packets)

            if config.use_c_libs:
                # Run the internal C function
                pcap_parser_c.pcap_to_text(input_file=ifile, output_file=ofile, debug_file=debug, bran=bran,
                                           eran=eran, batch_size=config.batch_size, verbose=verbose,
                                           include_ts=config.include_ts)
            else:
                # Use Python implementation
                packet_file_to_raw_packets(ifile, raw_packets_storage_path, bran, eran, config.batch_size,
                                           config.include_ts, verbose)


def remove_unlabelled_flow_file(flow_file, path_to_flow_file, counter):
    """
    Remove the provided flow file, if it is unlabelled.

    :param flow_file:
    :param path_to_flow_file:
    :param counter:
    :return:
    """
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is labelled
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        # Remove the file:
        os.remove(os.path.join(path_to_flow_file, flow_file))
        counter['count'] += 1


def step_recursive_cleanup(cleanup_dir, cleanup_filter, verbose=True):
    """
    Remove all files fitting the filter as well as the empty directories inside cleanup_dir.

    :param cleanup_dir: Path to the directory that should be cleaned
    :param cleanup_filter: If the filename contains this string, it will be removed (note: using "" allows for removal \
    of every file)
    :param verbose: If True, print additional informative text as output
    :type cleanup_dir: ``str``
    :type cleanup_filter: ``str``
    :type verbose: ``bool``
    :return: None
    """

    for target in os.listdir(cleanup_dir):
        target_path = os.path.join(cleanup_dir, target)

        if os.path.isfile(target_path):
            if cleanup_filter in target:
                if verbose:
                    print("Removing " + target_path)
                os.remove(target_path)
        elif os.path.isdir(target_path):
            # Remove the files
            step_recursive_cleanup(cleanup_dir=target_path, cleanup_filter=cleanup_filter, verbose=verbose)
            # Remove the directories themselves, if they are now empty
            if len(os.listdir(target_path)) < 1:
                os.rmdir(target_path)


def setup_preliminaries(root, dataset, verbose=True, use_existing=True, **kwargs):
    """
    Generate all directories required for the preprocessing of a dataset at the given location.

    :param root: Path to root directory where the dataset will be stored and processed.
    :param dataset: Dataset for which this procedure will be initiated.
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :param use_existing: If True, try to use an existing dataset configuration
    :param kwargs: Consult the dataset configuration documentation for available keyword arguments.
    :type root: ``str``
    :type dataset: ``Dataset``
    :type verbose: ``bool``
    :type use_existing: ``bool``

    :return: Dataset configuration, list of days with data, \
    list of days without data
    :rtype: subclass of ``DatasetConfig``, ``list``, ``list``
    """
    if dataset == Dataset.CICIDS2017:
        return CICIDS2017.setup_preliminaries(root=root, verbose=verbose, use_existing=use_existing, **kwargs)
    elif dataset == Dataset.ISCX2012:
        return ISCX2012.setup_preliminaries(root=root, xml_root=kwargs.get('xml_root', None), verbose=verbose,
                                            use_existing=use_existing, **kwargs)
    elif dataset == Dataset.UNSW_NB15:
        return UNSW_NB15.setup_preliminaries(root=root, verbose=verbose, use_existing=use_existing, **kwargs)
    else:
        raise NotImplemented('Requested dataset ' + dataset.name + ' is not supported.')


def flow_sorting(dataset, raw_packets_storage_path, flows_storage_path, ts_included, verbose=True):
    """
    Sort extracted network packets in flows according to their flow identifier (flow ID).

    :param dataset: Dataset for which this procedure will be initiated.
    :param raw_packets_storage_path: Path to the directory containing the extracted packets to sort.
    :param flows_storage_path: Path to the directory the sorted flows will be stored into.
    :param ts_included: If timestamps were extracted along with packets, set True
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type dataset: ``Dataset``
    :type raw_packets_storage_path: ``str``
    :type flows_storage_path: ``str``
    :type ts_included: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    if dataset == Dataset.CICIDS2017:
        return CICIDS2017.flow_sorting(raw_packets_storage_path=raw_packets_storage_path,
                                       flows_storage_path=flows_storage_path,
                                       ts_included=ts_included,
                                       verbose=verbose)
    elif dataset == Dataset.ISCX2012:
        return ISCX2012.flow_sorting(raw_packets_storage_path=raw_packets_storage_path,
                                     flows_storage_path=flows_storage_path,
                                     ts_included=ts_included,
                                     verbose=verbose)
    elif dataset == Dataset.UNSW_NB15:
        return UNSW_NB15.flow_sorting(raw_packets_storage_path=raw_packets_storage_path,
                                      flows_storage_path=flows_storage_path,
                                      ts_included=ts_included,
                                      verbose=verbose)
    else:
        raise NotImplemented('Requested dataset ' + dataset.name + ' is not supported.')


def csv_preprocess(dataset, csv_data_dir, config, verbose=True):
    """
    Preprocess the dataset CSV label files before using them for labelling.

    :param dataset: Dataset for which this procedure will be initiated.
    :param csv_data_dir: Path to directory containing all CSV label files.
    :param config: Dataset configuration
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type dataset: ``Dataset``
    :type csv_data_dir: ``str``
    :type config: ``DatasetConfig``
    :type verbose: ``bool``
    :return: None
    """
    if dataset == Dataset.CICIDS2017:
        return CICIDS2017.csv_preprocess(csv_data_dir=csv_data_dir, verbose=verbose)
    elif dataset == Dataset.ISCX2012:
        return ISCX2012.csv_preprocess(csv_data_dir=csv_data_dir, verbose=verbose)
    elif dataset == Dataset.UNSW_NB15:
        return UNSW_NB15.csv_preprocess(csv_data_dir=csv_data_dir, config=config, verbose=verbose)
    else:
        raise NotImplemented('Requested dataset ' + dataset.name + ' is not supported.')


def flow_labelling(dataset, csv_data_dir, flows_root_dir, error_file, day, config, verbose=True):
    """
    Label the dataset flows.

    :param dataset: Dataset for which this procedure will be initiated.
    :param csv_data_dir: Path to directory containing the CSV label files.
    :param flows_root_dir: Path to the root directory containing al sorted flows.
    :param error_file: Path to file to store error messages into
    :param day: Day for which the flows will be labelled
    :param config: Dataset configuration
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type dataset: ``Dataset``
    :type csv_data_dir: ``str``
    :type flows_root_dir: ``str``
    :type error_file: ``str``
    :type day: ``str``
    :type config: ``DatasetConfig``
    :type verbose: ``bool``
    :return: None
    """
    if dataset == Dataset.CICIDS2017:
        CICIDS2017.flow_labelling(csv_data_dir=csv_data_dir,
                                  flows_root_dir=flows_root_dir,
                                  error_file=error_file,
                                  day=day,
                                  config=config,
                                  verbose=verbose)
    elif dataset == Dataset.ISCX2012:
        ISCX2012.flow_labelling(csv_data_dir=csv_data_dir,
                                flows_root_dir=flows_root_dir,
                                error_file=error_file,
                                day=day,
                                config=config,
                                verbose=verbose)
    elif dataset == Dataset.UNSW_NB15:
        UNSW_NB15.flow_labelling(csv_data_dir=csv_data_dir,
                                 flows_root_dir=flows_root_dir,
                                 error_file=error_file,
                                 day=day,
                                 config=config,
                                 verbose=verbose)
    else:
        raise NotImplemented('Requested dataset ' + dataset.name + ' is not supported.')

    if config.enable_hdf5:
        hdf5_obj = HDF5FlowStorage(filename=config.get_sorted_hdf5_path(day),
                                   mode='a', packet_size=config.hdf5_packet_size)
        hdf5_obj.remove_redundant_records()


def preprocess_dataset(root, dataset, verbose, **kwargs):
    """
    Preprocess a dataset: Start with PCAP files and CSV files, and end up with sorted and labelled network flow files.

    :param root: Path to root directory containing the dataset.
    :param dataset: Dataset to preprocess
    :param verbose: If True, report on meta-information and progress
    :param kwargs: Keyword arguments, consult DatasetConfig documentation
    :type root: ``str``
    :type dataset: ``Dataset``
    :type verbose: ``bool``
    :return: Dataset configuration containing all relevant information about the dataset.
    :rtype: ``DatasetConfig``
    """

    config, full_dirs, dataset_dirs = setup_preliminaries(root=root, dataset=dataset, verbose=verbose, **kwargs)

    # Catch the case that no dataset files are available
    if (dataset_dirs is None) and (full_dirs is None):
        return config

    # Check if the configuration has already been completed
    if config.is_completed():
        if verbose:
            print('{} was already preprocessed, skipping any further dataset preprocessing steps.'.format(dataset.name))
        return config

    # Create list of already processed days
    processed_days = []
    for day, status in config.days_completed.items():
        if status:
            processed_days.append(day)

    if verbose:
        print("Processed days:", processed_days)

    # Find dirs that still need to be processed
    dirs_to_process = [day for day in full_dirs if day not in processed_days]
    if verbose:
        print("Days to process:" + ", ".join(dirs_to_process))

    # Perform preprocessing for each day in the dataset:
    # if 'friday' in processed_days:
    #     temp_day_list = []
    # else:
    #     temp_day_list = ['friday']

    # for day in temp_day_list:
    for day in dirs_to_process:
        if verbose:
            print("Processing dataset for day \"{}\"".format(day))

        # Path definitions:
        pcap_data_dir = os.path.join(config.data_root, dataset_dirs[1], day)
        csv_data_dir = os.path.join(config.data_root, dataset_dirs[0], day)

        flows_storage_path = os.path.join(config.data_root, dataset_dirs[3], day)
        raw_packets_storage_path = os.path.join(config.data_root, dataset_dirs[2], day)

        debug_storage_path = os.path.join(config.data_root, dataset_dirs[4], day)

        # Print meta-information:
        if verbose:
            print("Path definitions:")
        if verbose:
            print("PCAP data directory:        " + pcap_data_dir)
        if verbose:
            print("CSV data directory:         " + csv_data_dir)
        if verbose:
            print("Flows storage directory:    " + flows_storage_path)
        if verbose:
            print("Raw packets storage directory: " + raw_packets_storage_path)

        # If the labelling step has crashed: cleanup the sorted flows:
        if config.started_labelling[day] and not (config.labelling_complete[day]):
            # Cleanup sorted flows
            step_recursive_cleanup(cleanup_dir=flows_storage_path, cleanup_filter="", verbose=verbose)
            # After cleaning, update the status
            config.started_labelling[day] = False
            config.sorting_complete[day] = False
            config.update()

        t_start = time.time()

        # If 'fast_sort' is enabled, packets extracted from pcap files will immediately be sorted and stored into a
        # tree. This computation will take longer than simply extracting them, but requires less storage as no unsorted
        # raw packet files will be generated.
        if config.fast_sort and not (config.sorting_complete[day]):
            if verbose:
                print("Using fast sort to extract and sort packet flows from PCAP(NG) files.")
            # Cleanup:
            step_recursive_cleanup(cleanup_dir=flows_storage_path, cleanup_filter="", verbose=verbose)

            if config.stream_based:
                # Use stream-based extraction and sorting
                pcap_to_sorted_stream_flows(pcap_data_dir=pcap_data_dir, sorted_flows_dir=flows_storage_path,
                                            config=config, day=day, verbose=verbose)
            else:
                # Perform the default fast sort
                pcap_extract_and_sort(day, pcap_data_dir=pcap_data_dir, sorted_flows_dir=flows_storage_path,
                                      config=config, verbose=verbose)
            # Update configuration
            config.sorting_complete[day] = True
            config.update()

        # Otherwise, first write extracted raw packets to large .csv files,
        # then use the data in those files to generated the sorted flows tree
        # This option preserves
        elif not (config.extraction_complete[day]) and not (config.sorting_complete[day]):
            if verbose:
                print("Extracting raw packets from PCAP(NG) files.")
            # No cleanup required as of right now

            # Actual preprocessing
            pcap_extraction(pcap_data_dir=pcap_data_dir,
                            raw_packets_storage_path=raw_packets_storage_path,
                            config=config, verbose=verbose)

            # Update configuration
            config.extraction_complete[day] = True
            config.update()

        if not config.sorting_complete[day]:
            if verbose:
                print("Sorting raw packets into packet flows.")
            # Cleanup:
            step_recursive_cleanup(cleanup_dir=flows_storage_path, cleanup_filter="", verbose=verbose)

            flow_sorting(dataset=dataset, raw_packets_storage_path=raw_packets_storage_path,
                         flows_storage_path=flows_storage_path, ts_included=config.include_ts, verbose=verbose)
            # Update configuration
            config.sorting_complete[day] = True
            config.update()

        t_stop = time.time()

        if verbose:
            elapsed_time = t_stop - t_start
            print("Extraction & sorting step took {} seconds (or {} minutes)".format(elapsed_time, round(elapsed_time / 60)))

        if not config.csv_preprocess_complete[day]:
            if verbose:
                print("Preprocessing CSV label files.")
            # Cleanup: Not necessary, the generated csv files are correct

            csv_preprocess(dataset=dataset, csv_data_dir=csv_data_dir, config=config, verbose=verbose)
            # Update configuration
            config.csv_preprocess_complete[day] = True
            config.update()

        if not config.labelling_complete[day]:
            config.started_labelling[day] = True
            config.update()
            if verbose:
                print("Labelling packet flows")

            # Cleanup: If previous
            # Actual labelling
            flow_labelling(dataset=dataset, csv_data_dir=csv_data_dir, flows_root_dir=flows_storage_path,
                           error_file=os.path.join(debug_storage_path, "labelling_errors.txt"), day=day,
                           config=config, verbose=verbose)
            # Update configuration
            config.labelling_complete[day] = True
            config.update()

        # Remove too large memory files that have become obsolete after feature generation
        if config.clear_csvs:
            step_recursive_cleanup(cleanup_dir=csv_data_dir, cleanup_filter="", verbose=verbose)

        if config.clear_pcaps:
            step_recursive_cleanup(cleanup_dir=pcap_data_dir, cleanup_filter="", verbose=verbose)

        if config.clear_raw_packets:
            step_recursive_cleanup(cleanup_dir=raw_packets_storage_path, cleanup_filter="",
                                   verbose=verbose)

        if config.clear_sorted_flows:
            step_recursive_cleanup(cleanup_dir=flows_storage_path, cleanup_filter="", verbose=verbose)

        if config.prune_sorted_flows:
            counter = {'count': 0}
            if verbose:
                print('Removing unlabelled flow files from system for day', day)
            process_flow_tree(flows_storage_path,
                              {
                                  'function': remove_unlabelled_flow_file,
                                  'params': {'counter': counter}
                              },
                              True)
            if verbose:
                print('\nRemoved', counter['count'], 'flow files')

            remove_empty_dirs(flows_storage_path)

        config.days_completed[day] = True
        config.update()
        if verbose:
            print("Completed ", day)

    if verbose:
        print("Completed available days")
    return config
