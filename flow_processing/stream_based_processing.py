import os
import sys
from enum import Enum
from help_functions import mkdir
from enums import Dataset
from flow_processing import CICIDS2017, UNSW_NB15, ISCX2012


class LabelStatus(Enum):
    MISSING_DIR = 1     # When target directory is missing
    MISSING_FILE = 2    # When target unlabelled file is missing
    INCOMPLETE = 3      # When unlabelled file does not contain sufficient packets
    VALID = 4           # If labelling went right


class FlowStreamSorter:
    flows_map = {}
    flows_order = []

    def __init__(self, n_flows, n_packets, root, dynamic_queue=True, hdf5=None):
        """
        Constructor for FlowStreamSorter objects. These serve to sort a stream of network packet flows into storage,
        considering certain constraints.

        :param n_flows: Number of flows to consider at once. If n_flows are stored in the object, the arrival of a new \
        flow will cause the oldest flow to be written to storage. The oldest flow is the flow that, of all the flows \
        currently in the object, first entered the object.
        :type n_flows: ``int``
        :param n_packets: Number of packets to consider per flow. If this number is exceeded, all packets are stored \
        inside storage before being removed from the object.
        :type n_packets: ``int``
        :param root: Path of root directory wherein the flows should be stored.
        :type root: ``str``
        """

        if n_flows == -1:
            self.n_f = float('inf')
        else:
            self.n_f = n_flows

        if n_packets == -1:
            self.n_p = float('inf')
        else:
            self.n_p = n_packets

        self.root = root
        self.dynamic_queue = dynamic_queue
        self.hdf5 = hdf5

    def add_packet(self, packet_data, flow_id):
        """
        Add a packet to the the object. This packet will to added to a current flow, if applicable, or otherwise a new
        flow will be added to store the packet into instead.


        :param packet_data:
        :param flow_id:
        :return:
        """
        if flow_id in self.flows_order:
            if len(self.flows_map[flow_id]) < self.n_p:
                self.__add_packet(packet_data=packet_data, flow_id=flow_id)
                self.__update_queue_position(flow_id)
            else:
                self.__remove_flow(flow_id)
                self._add_flow(flow_id=flow_id, packet_data=packet_data)
        elif len(self.flows_order) < self.n_f:
            self._add_flow(flow_id=flow_id, packet_data=packet_data)
        else:
            self.__pop_flow()
            self._add_flow(flow_id=flow_id, packet_data=packet_data)

    def __update_queue_position(self, flow_id):
        """
        Update a flow's position in the queue: It will be put at the last place, removing it's previous occurence in
        that queue.

        :param flow_id: Flow identifier
        :type flow_id: ``str``
        :return: None
        """
        if self.dynamic_queue:
            self.flows_order.remove(flow_id)
            self.flows_order.append(flow_id)

    def store_remaining_flows(self):
        """
        Write remaining flows to storage.

        :return: None
        """
        for _ in range(len(self.flows_order)):
            self.__pop_flow()

    def __del__(self):
        """
        Destructor, will write any remaining flows to the storage

        :return: None
        """
        self.store_remaining_flows()

    def __add_packet(self, packet_data, flow_id):
        """
        Add a packet to a flow currently in this FlowStreamSorter.

        :param packet_data: Packet data, with optional timestamps
        :type packet_data: ``str``
        :param flow_id: Identifier of the flow
        :type flow_id: ``str``
        :return: None
        """
        self.flows_map[flow_id].append(packet_data)

    def _add_flow(self, packet_data, flow_id):
        """
        Add a flow to this FlowStreamSorter.

        :param packet_data: Packet data, with optional timestamps
        :type packet_data: ``str``
        :param flow_id: Identifier of the flow
        :type flow_id: ``str``
        :return: None
        """
        self.flows_order.append(flow_id)
        self.flows_map[flow_id] = [packet_data]

    def __remove_flow(self, flow_id):
        """
        Remove a flow from this FlowStreamSorter by storing it on disk.

        :param flow_id: Identifier of the flow
        :type flow_id: ``str``
        :return: None
        """
        # Extract saved packets, while removing them from storage
        packets_out = self.flows_map.pop(flow_id)
        # Remove flow id from queue
        self.flows_order.remove(flow_id)

        # Then store the extracted packets
        self.__store_flow(flow_id=flow_id, packets=packets_out)

    def __pop_flow(self):
        """
        Remove the flow was added first from this FlowStreamSorter

        :return: None
        """
        flow_out_id = self.flows_order.pop()

        packets_out = self.flows_map.pop(flow_out_id)

        self.__store_flow(flow_id=flow_out_id, packets=packets_out)

    def __store_flow(self, flow_id, packets):
        """
        Store a flow on the targeted storage.

        :param packets: Packets of the flow to store. Each packet should be terminated with '\\n'.
        :type packets: ``list`` of ``str``
        :param flow_id: Identifier of the flow
        :type flow_id: ``str``
        :return: None
        """

        # Check which approach is used to store the packet files
        if self.hdf5 is not None:
            id_elements = flow_id.split('-')
            self.hdf5.store_packets_from_root(src_addr=id_elements[0],
                                              dst_addr=id_elements[1],
                                              src_port=id_elements[2],
                                              dst_port=id_elements[3],
                                              proto=id_elements[4],
                                              packets=packets)

        else:
            # Start with file root
            target = self.root

            # For every element of the path to the actual file
            # Make sure the prerequisite directory exists
            for id_element in flow_id.split('-'):
                target = os.path.join(target, id_element)
                mkdir(target)

            # The target filename simply comprises the flow id
            filename = '{}.txt'.format(flow_id)

            # Write the packets to the file,
            # every packet occupies one line
            # These lines are appended to the original file
            with open(os.path.join(target, filename), "a") as f:
                for packet in packets:
                    f.write(packet)
                # EOL to indicate this section of packets ended
                f.write('\n')


def label_stream_based_flow(label_csv, flows_root_dir, error_file, config, verbose=True):
    dataset = Dataset[config.dataset]
    status = {
        LabelStatus.MISSING_DIR: 0,
        LabelStatus.MISSING_FILE: 0,
        LabelStatus.INCOMPLETE: 0,
        LabelStatus.VALID: 0,
    }

    if not config.chronological:
        with open(label_csv, "r") as f:
            for idx, line in enumerate(f):
                if idx >= 1:
                    if verbose:
                        print("Current line: " + str(idx) + " |", end="\r")

                    # Turn the line into a Flow object
                    flow = None
                    if dataset == Dataset.CICIDS2017:
                        flow = CICIDS2017.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.ISCX2012:
                        flow = ISCX2012.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.UNSW_NB15:
                        flow = UNSW_NB15.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    # Also get the return flow
                    # Try to label the flow
                    if flow.fwd_packets > 0:
                        status[label_stream_based_flow_no_chronological(flows_root_dir, flow, verbose=verbose)] += 1

                    if (flow.bwd_packets > 0) and config.bidirectional:
                        status[label_stream_based_flow_no_chronological(flows_root_dir,
                                                                        flow.get_return_flow(), verbose=verbose)] += 1
    else:
        raise NotImplemented('Not yet implemented.')

    if verbose:
        print('The labelling resulted in the following accumulation of statuses:')
        print(status)


def label_stream_based_flow_no_chronological_v0(flows_root_dir, flow):
    """
    DEPRECATED

    :param flows_root_dir:
    :param flow:
    :return:
    """
    flows_root_dir = flow.get_storage_dir(flows_root_dir)

    # Check if the directory exists:
    if not os.path.isdir(flows_root_dir):
        return LabelStatus.MISSING_DIR

    # Set up the base filename
    base_filename = flow.get_filename_v2()
    base_path = os.path.join(flows_root_dir, base_filename)

    # Check if the target file exists
    if not os.path.isfile(base_path):
        # print('Missing', base_path)
        return LabelStatus.MISSING_FILE

    # Map contents of target directory
    labelled, unlabelled = examine_flow_stream_directory(flows_root_dir)

    # Calculate the index of the target flow file
    if flow.label in labelled.keys():
        index = len(labelled[flow.label])
    else:
        index = 0

    # Start labelling
    labelled_packets = 0
    read_lines = 0
    finished_labelling = False

    with open(base_path, 'r') as unlabelled_f:
        label_f = open(os.path.join(flows_root_dir, '{}-{}_{}'.format(flow.label, index, base_filename)), 'w')
        temp_f = None

        for line in unlabelled_f:
            # If we finished labelling, we copy the remainder of the file to a temporary file
            if finished_labelling:
                temp_f.write(line)
            # Otherwise, continue labelling
            else:
                # If we reach the end of a group of packets
                # We check if are done labelling,
                # or we close the current label file and open a new label file
                if line == '\n':
                    # Close the currently opened file
                    label_f.close()

                    # Check if we are done with labelling
                    finished_labelling = (labelled_packets >= flow.fwd_packets)
                    # Otherwise, open a new file
                    if not finished_labelling:
                        index += 1
                        label_f = open(os.path.join(flows_root_dir, '{}-{}_{}'.format(flow.label, index,
                                                                                      base_filename)),
                                       'w')
                    else:
                        # Open the temporary file to store lines into
                        temp_f = open(os.path.join(flows_root_dir, 'temp.txt'), 'w')
                else:
                    label_f.write(line)
                    labelled_packets += 1

                read_lines += 1

        if not label_f.closed:
            # This means the temp file never was opened,
            # and hence does not need to be renamed or closed
            # We immediately return the issue
            label_f.close()
            return LabelStatus.INCOMPLETE

        # Close the temp file
        temp_f.close()

        # Finally, remove the original file and rename the new file
        os.remove(base_path)
        os.rename(os.path.join(flows_root_dir, 'temp.txt'), base_path)

        return LabelStatus.VALID


def label_stream_based_flow_no_chronological(flows_root_dir, flow, verbose=True):
    """

    :param flows_root_dir:
    :param flow:
    :return:
    """
    flows_root_dir = flow.get_storage_dir(flows_root_dir)

    # Check if the directory exists:
    if not os.path.isdir(flows_root_dir):
        return LabelStatus.MISSING_DIR

    # Set up the base filename
    base_filename = flow.get_filename_v2()
    base_path = os.path.join(flows_root_dir, base_filename)

    # Check if the target file exists
    if not os.path.isfile(base_path):
        if verbose:
            print('Missing', base_path)
        return LabelStatus.MISSING_FILE

    # Map contents of target directory
    labelled, unlabelled = examine_flow_stream_directory(flows_root_dir)

    # Calculate the index of the target flow file
    if flow.label in labelled.keys():
        index = len(labelled[flow.label])
    else:
        index = 0

    # Start labelling
    labelled_packets = 0
    finished_labelling = False

    with open(base_path, 'r') as unlabelled_f:
        label_f = open(os.path.join(flows_root_dir, '{}-{}_{}'.format(flow.label, index, base_filename)), 'w')
        temp_f = None

        for line in unlabelled_f:
            # If we finished labelling, we copy the remainder of the file to a temporary file
            if finished_labelling:
                temp_f.write(line)
            # Otherwise, continue labelling
            else:
                # If we reach the end of a group of packets
                # We check if are done labelling,
                if line == '\n':

                    # Check if we are done with labelling
                    finished_labelling = (labelled_packets >= flow.fwd_packets)
                    # Otherwise, open a new file
                    if finished_labelling:
                        # Close the currently opened file
                        label_f.close()
                        # Open the temporary file to store lines into
                        temp_f = open(os.path.join(flows_root_dir, 'temp.txt'), 'w')
                    else:
                        label_f.write('\n')
                else:
                    label_f.write(line)
                    labelled_packets += 1

        if not label_f.closed:
            # This means the temp file never was opened,
            # and hence does not need to be renamed or closed
            # We immediately return the issue
            label_f.close()

            # If no packets were labelled
            # an unnecessat file has been created: remove it
            if labelled_packets == 0:
                os.remove(os.path.join(flows_root_dir, '{}-{}_{}'.format(flow.label, index, base_filename)))

            # As we only get to this loop if the entire base file has been read, and no more packets remain
            # Thus, remove the base file
            os.remove(base_path)

            return LabelStatus.INCOMPLETE

        # Close the temp file
        temp_f.close()

        # Finally, remove the original file and rename the new file
        os.remove(base_path)
        os.rename(os.path.join(flows_root_dir, 'temp.txt'), base_path)

        return LabelStatus.VALID


def is_labelled_file(flow_file):
    """
    Check if the flow file is labelled, and return the label.

    :param flow_file: Flow file name
    :type flow_file: ``str``
    :return: Label, if applicable, or None
    :rtype: ``str`` or None
    """
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        label = None
    else:
        #   - Generate flow image
        label = "-".join(flow_file_parts[0].split("-")[:-1])

    return label


def examine_flow_stream_directory(path_to_directory):
    """
    Build overview of labelled and unlabelled files
    :param path_to_directory: Path to flow stream directory to be examined
    :return:
    """
    labelled = {}
    unlabelled = []
    for f in os.listdir(path_to_directory):
        label = is_labelled_file(f)
        if label:
            if label in labelled.keys():
                labelled[label].append(f)
            else:
                labelled[label] = [f]
        else:
            unlabelled.append(f)

    return labelled, unlabelled
