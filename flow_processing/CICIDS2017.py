"""
Module responsible for all things related to the extraction and processing of the CICIDS2017 dataset.
"""

import os
import sys

import help_functions as hf
from enums import Dataset
from dataset_config import CICIDS2017Config, get_dataset_days
from flow_processing.flow import CICIDS2017Flow
from flow_processing import preprocess, flow_sorter, flow_labeler


def setup_preliminaries(root, verbose=True, use_existing=True, **kwargs):
    """
    Generate all directories required for the preprocessing of a dataset at the given location.

    :param root: Path to root directory where the dataset will be stored and processed.
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :param use_existing: If True, try to use an existing CICIDS2017 configuration
    :param kwargs: Consult ``dataset_config.CICIDS2017Config`` for available keyword arguments.
    :type root: ``str``
    :type verbose: ``bool``
    :type use_existing: ``bool``

    :return: Dataset configuration, list of days with data, \
    list of days without data
    :rtype: ``CICIDS2017Config``, ``list``, ``list``
    """
    # Necessary structure
    # days = ["monday", "tuesday", "wednesday", "thursday", "friday"]
    days = get_dataset_days(Dataset.CICIDS2017)
    dataset_dirs = ["csv_data", "pcap_data", "raw_packets", "sorted_flows", "debug"]

    config = CICIDS2017Config(root, days=days, use_existing=use_existing, verbose=verbose, **kwargs)

    # Check if the configuration has already been completed
    if config.is_completed():
        return config, None, None

    # Create roots
    data_root = config.data_root

    if not os.path.isdir(data_root):
        os.mkdir(data_root)

    # Create necessary directories for dataset dir, if applicable
    print("Checking presence of necessary directories")
    for d in dataset_dirs:
        dir_path = os.path.join(data_root, d)
        if not os.path.isdir(dir_path):
            print("Creating directory " + dir_path)
            os.mkdir(dir_path)
        for day in days:
            if not os.path.isdir(os.path.join(dir_path, day)):
                print("Creating directory " + os.path.join(dir_path, day))
                os.mkdir(os.path.join(dir_path, day))

    # Check that the dataset directories are not empty
    empty_dirs = []  # List all empty dirs will be put into

    # We check beginning from the csv files, but for each csv directory the corresponding pcap directory is checked
    for day in os.listdir(os.path.join(data_root, dataset_dirs[0])):
        if os.path.isdir(os.path.join(data_root, dataset_dirs[0], day)):

            # Check if this directory is empty
            this_dir_empty = len(os.listdir(os.path.join(data_root, dataset_dirs[0], day))) < 1
            # Check if the corresponding pcap directory is empty
            correspond_dir_empty = len(os.listdir(os.path.join(data_root, dataset_dirs[1], day))) < 1
            
            if this_dir_empty or correspond_dir_empty:
                empty_dirs.append(day)

    print("Dataset directories for " + ", ".join(empty_dirs) + " are empty. A dataset consists of csv-files and pcap "
                                                               "files for a certain day.")
    if len(empty_dirs) >= len(days):
        print("All dataset directories are empty. No dataset available. Please add a dataset.")
        config.update()
        return config, None, None

    # Find all days with datasets:
    full_dirs = [day for day in days if day not in empty_dirs]
    for day in full_dirs:
        config.dataset_present[day] = True

    config.update()

    return config, full_dirs, dataset_dirs


def csv_process_order(day):
    """
    For Thursday and Friday, multiple labelling CSV files exist. Due to the chronological nature of the PCAP data, it is
    important that these are used in the right order when labelling flows. For Thurday and Friday, this order is
    provided. For all other days, an empty list is returned.

    :param day: 'thursday' or 'friday'
    :type day: ``str``

    :return: A list with the order of the CSV files. The list contains words unique to the filename of each CSV file \
    (e.g. 'Morning').
    :rtype: ``list`` of ``str``
    """
    if day == "friday":
        return ["Morning", "PortScan", "DDos"]
    elif day == "thursday":
        return ["WebAttack", "Infilt"]
    else:
        return [""]


def label_csv_line_to_flow(line, use_ts=True):
    """
    Extract the flow information from a line of a light label CSV file for CICIDS2017 to create the corresponding flow.

    :param line: Line CSV file line containing flow information. Include the '\\n' at the end of the line.
    :param use_ts: If True, use the timestamps provided in the label CSV file.
    :type line: ``str``
    :type use_ts: ``bool``

    :return: The CICIDS2017 flow corresponding to the line.
    :rtype: ``CICIDS2017Flow``
    """
    [_, label, src_addr, src_port, dst_addr, dst_port, proto, duration,
        total_fwd, total_bwd, speed, timestamp] = line.split(",")
    timestamp = timestamp.strip("\n")

    # Sometimes, the int values are stored as float strings ("48.0" instead of "48")
    # These have to be converted to int strings: "48.0" -> 48.0 -> 48 -> "48"
    src_port = str(int(float(src_port)))
    dst_port = str(int(float(dst_port)))
    duration = str(int(float(duration)))
    proto = str(int(float(proto)))
    
    # Are not used as strings
    total_fwd = int(float(total_fwd))  # Used
    total_bwd = int(float(total_bwd))  # Used if bidirectional == True

    return CICIDS2017Flow(src_addr, dst_addr, src_port, dst_port, proto, label, total_fwd, total_bwd,
                          timestamp=timestamp, duration=duration, use_ts=use_ts)


def flow_sorting(raw_packets_storage_path, flows_storage_path, ts_included, verbose=True):
    """
    Sort extracted network packets in flows according to their flow identifier (flow ID).

    :param raw_packets_storage_path: Path to the directory containing the extracted packets to sort.
    :param flows_storage_path: Path to the directory the sorted flows will be stored into.
    :param ts_included: If timestamps were extracted along with packets, set True
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type raw_packets_storage_path: ``str``
    :type flows_storage_path: ``str``
    :type ts_included: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    # Put the raw packet files in the right order:
    filelist = hf.list_files_ordered_from_path(path=raw_packets_storage_path, trailer=".csv", verbose=verbose)
    # Sort the packets inside the generated csv-files
    for raw_packets_file in filelist:
        # Check if file is a csv file:
        if raw_packets_file.split(".")[-1] == "csv":
            # Calculate correct file path
            raw_packets_file_path = os.path.join(raw_packets_storage_path, raw_packets_file)
        
            if verbose: print("Sorting flows for " + raw_packets_file)
            flow_sorter.sort_flows_1_file(raw_packets_file_path, flows_storage_path,
                                          ts_included=ts_included, verbose=verbose)


def csv_relabeling(csv_data_dir, verbose):
    """
    Perform some preliminary actions specifically required for CICIDS2017 files.

    :param csv_file: Path to CSV file
    :param column_headers: Column headers of the USNW-NB15 label CSV file
    :param config: UNSW-NB15 dataset configuration
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_file: ``str``
    :type column_headers: ``list`` of ``str``
    :type verbose: ``bool``
    :type config: ``UNSW_NB15Config``
    :return: None
    """

    # Now relabel whenever necessary
    for csv_file in os.listdir(csv_data_dir):
        if csv_file.split("-")[-1].strip(".csv") == "light":
            if verbose: print("Relabeling " + csv_file)

            csv_file = os.path.join(csv_data_dir, csv_file)

            with open(csv_file, "r") as f:
                filelines = f.readlines()

            with open(csv_file, "w") as f:
                # Iterate over every data line
                if verbose:
                    print("Relabeling labels " + os.path.split(csv_file)[-1])

                for line in filelines:
                    cols = line.split(",")

                    # If necessary, attach the traffic label
                    cols[1] = _get_correct_label(cols[1])
                    f.write(",".join(cols))


def _get_correct_label(label):
    """
    Function to correct labels that are not correctly formulated in the provided label files.

    :param label: Label to correct
    :type label: ``str``
    :return: Corrected label
    :rtype: ``str``
    """
    if label == "Web Attack  Sql Injection":
        return "Web Attack Sql Injection"
    elif label == "Web Attack  Brute Force":
        return "Web Attack Brute Force"
    elif label == "Web Attack  XSS":
        return "Web Attack XSS"
    else:
        return label


def csv_preprocess(csv_data_dir, verbose=True):
    """
    Preprocess the CICIDS2017 CSV label files before using them for labelling.

    :param csv_data_dir: Path to directory containing all CSV label files.
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_data_dir: ``str``
    :type verbose: ``bool``
    :return: None
    """
    # Process each CSV file
    for labels_csv in os.listdir(csv_data_dir):
        # Clean and minimize the label CSVs
        if labels_csv.split("-")[-1].strip(".csv") != "light":
            if verbose:
                print("Cleaning and minimizing " + labels_csv)
            preprocess.preprocess_dataset_csv(Dataset.CICIDS2017, old_file=labels_csv, path_to_csv_dir=csv_data_dir,
                                              verbose=True)

    # Relabel the CSV files
    csv_relabeling(csv_data_dir=csv_data_dir, verbose=verbose)


def flow_labelling(csv_data_dir, flows_root_dir, error_file, day, config, verbose=True):
    """
    Label the CICIDS2017 flows.

    :param csv_data_dir: Path to directory containing the CSV label files.
    :param flows_root_dir: Path to the root directory containing al sorted flows.
    :param error_file: Path to file to store error messages into
    :param day: Day for which the flows will be labelled
    :param config: CICIDS2017 dataset configuration
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type csv_data_dir: ``str``
    :type flows_root_dir: ``str``
    :type error_file: ``str``
    :type day: ``str``
    :type config: ``CICIDS2017Configuration``
    :type verbose: ``bool``
    :return: None
    """
    # Label the flows
    order = csv_process_order(day)
    for item in order:
        # Process the next item in the order
        for labels_csv in os.listdir(csv_data_dir):
            # Check if the file is next in the order
            if item in labels_csv:
                # Only use relevant CSV files
                if labels_csv.split("-")[-1].strip(".csv") == "light":
                    if verbose:
                        print("Labelling according to " + labels_csv)
                    
                    flow_labeler.label_sorted_flows(label_csv=os.path.join(csv_data_dir, labels_csv),
                                                    flows_root_dir=flows_root_dir,
                                                    error_file=error_file,
                                                    config=config, day=day, verbose=verbose)
