"""
Module responsible for the sorting and storage of netword traffic flows. This module provides the capability to sort
network packets into flows according to their flow identifier (5-tuple), namely their IPv4 source address, IPv4
destination address, TCP/UDP source port, TCP/UDP destination port and protocol numer (6 or 17).
"""

import os
from flow_processing.hdf5_interface import HDF5FlowStorage


def create_new_dicts(keys, data):
    """
    Create new dictionaries for the provided sequence of keys to store the data. Target:
    {key1: {key2: {key3: .... {keyn: data}}}}

    :param keys: Sequence of keys.
    :param data: Data to store.
    :return: Create nested dictionaries
    :rtype: ``dict``
    """
    if len(keys) == 1:
        return {keys[0]: [data]}
    else:
        return {keys[0]: create_new_dicts(keys[1:], data)}


def add2dict(dictionary, keys, data):
    """
    Add the provided data to the provided dictionary following the provided keys. The data will be appended to a list of
    data already at that location.

    :param dictionary: Dictionary in which the data needs to be stored.
    :param keys: The keys required to  store the data at the right location in the dictionary. This implies that the \
    dictionary may contain other dictionaries.
    :param data: Data to add to the dictionary.
    :type dictionary: ``dict``
    :type keys: ``list``
    :type data: ``Any``
    :return: None
    """
    if len(keys) == 1:
        try:
            dictionary[keys[0]].append(data)
        except:
            dictionary[keys[0]] = [data]
    else:
        try:
            add2dict(dictionary[keys[0]], keys[1:], data)
        except:
            dictionary[keys[0]] = create_new_dicts(keys[1:], data)


def dict2files(filename, path, dictionary):
    """
    This recursive function turns a given dictionary (hash-table) into a tree of directories.
    Essentially, each file corresponds to one network flow, indicated by its identifier of the form:
    source_address-destination_address-source_port-destination_port-protocol.
    Concretely, this means that for each source address (src_addr) a directory is created (with this address as its
    name). A source address directory then contains a directory for each destination address (dst_addr) that was paired
    with this source address. Each destination address folder contains a directory with each source port (src_port) used
    for this source-destination address pair. Evidently, each destination port (dst_port) folder then finally contains
    a folder for the protocols used for this combination of sockets. The resulting identifier consisting of two sockets
    and a protocol is a specific flow identifier.

    :param filename: Name of the file that is being generated inside the tree. Leave blank initially.
    :param path: Path to the current location in the tree. Initially set as root of the tree.
    :param dictionary: Dictionary with all of the flows that need to be sorted inside the tree.
    :type filename: ``str``
    :type path: ``str``
    :type dictionary: ``dict``

    :return: None
    """
    for key, item in dictionary.items():
        new_path = os.path.join(path, key)
        
        if not os.path.isdir(new_path):
            os.mkdir(new_path)
        
        if isinstance(item, dict):
            dict2files(filename + key + "-", new_path, item)
        else:
            with open(os.path.join(new_path, filename.strip("-") + ".txt"), "a") as f:
                for data in item:
                    f.write(data)


def dict2hdf5(hdf5sorter: HDF5FlowStorage, path, dictionary):
    """
    This recursive function turns a given dictionary (hash-table) into a tree of directories.
    Essentially, each file corresponds to one network flow, indicated by its identifier of the form:
    source_address-destination_address-source_port-destination_port-protocol.
    Concretely, this means that for each source address (src_addr) a directory is created (with this address as its
    name). A source address directory then contains a directory for each destination address (dst_addr) that was paired
    with this source address. Each destination address folder contains a directory with each source port (src_port) used
    for this source-destination address pair. Evidently, each destination port (dst_port) folder then finally contains
    a folder for the protocols used for this combination of sockets. The resulting identifier consisting of two sockets
    and a protocol is a specific flow identifier.

    :param filename: Name of the file that is being generated inside the tree. Leave blank initially.
    :param path: Path to the current location in the tree. Initially set as root of the tree.
    :param dictionary: Dictionary with all of the flows that need to be sorted inside the tree.
    :type filename: ``str``
    :type path: ``str``
    :type dictionary: ``dict``

    :return: None
    """
    for key, item in dictionary.items():
        current_group = hdf5sorter.file[path]

        subgroup = hdf5sorter.get_subgroup(group=current_group, path_to_group=path, subgroup_name=key)
        new_path = path + '/' + key

        if isinstance(item, dict):
            dict2hdf5(hdf5sorter, new_path, item)
        else:
            hdf5sorter.store_packets(subgroup, new_path, item)


def sort_flows_1_file(data_path, storage_root, ts_included, verbose=True):
    """
    WARNING: ONLY USE FOR UP TO 1-4 MILLION PACKETS IN A FILE, OTHERWISE RAM WILL OVERFLOW
    Sort all raw packets in a CSV-file into their corresponding flows.

    :param data_path: Path to the data csv containing all flows
    :param storage_root: Path to the root directory for storing sorted flows
    :param ts_included: Set True if timestamps have been extracted along with the packets.
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type data_path:  ``str``
    :type storage_root: ``str``
    :type ts_included: ``bool``
    :type verbose: ``bool``

    :return: None
    """
    
    if not os.path.exists(storage_root):
        os.mkdir(storage_root)
    
    # Dictionary to store data
    with open(data_path, "r") as f:
        # Hash table to temporarly store data in  
        hash_table = {}
        
        for idx, line in enumerate(f):
            if verbose:
                print(idx, end="\r")
            # Extract relevant information from line
            if ts_included: 
                [flow_id, ts_sec, ts_usec, hex_data] = line.split(",")
                hex_data = ",".join([ts_sec, ts_usec, hex_data])
            else: 
                [flow_id, hex_data] = line.split(",")
            # Extract the keys for the hash table
            keys = flow_id.split("-")
            # Add the information to the hash table
            add2dict(hash_table, keys, hex_data)
        print("\nWriting to file..")
        # Write current hash table to files
        dict2files("", storage_root, hash_table)
        print("Done printing to file!")


def sort_flows_from_list(packet_list, storage_root, ts_included, verbose=True):
    """
    Sort all the provided packets corresponding to their flow ID.

    :param packet_list: Chronological list of packets to sort
    :param storage_root: Path to root directory to store flows into
    :param ts_included: If True, timestamps have been included together with the packets
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type packet_list: ``list``
    :type storage_root: ``str``
    :type ts_included: ``bool``
    :type verbose: ``bool``
    :return: None
    """
    # Hash table to temporarly store data in  
    hash_table = {}
    idx = 0
    for packet_info in packet_list:
        idx += 1
        if ts_included: 
            flow_id, ts_sec, ts_usec, hex_data = packet_info
            hex_data = ",".join([ts_sec, ts_usec, hex_data]) + "\n"
        else: 
            flow_id, hex_data = packet_info
            hex_data += "\n"
        if verbose:
            print(idx, end="\r")
        # Extract the keys for the hash table
        keys = flow_id.split("-")
        # Add the information to the hash table
        add2dict(hash_table, keys, hex_data)
    if verbose:
        print("\nWriting to file..")

    # Write current hash table to files
    dict2files("", storage_root, hash_table)
    if verbose:
        print("Done printing to file!")


def sort_flows_from_dict(flows_dict, storage_root, verbose=True):
    """
    Store provided flows.

    :param flows_dict: Dictionary containing provided flows, sorted via flow ID
    :param storage_root: Path to root directory to store flows into
    :param verbose: If True, the algorithm will output relevant information at each preprocess step
    :type flows_dict: ``dict``
    :type storage_root: ``str``
    :type verbose: ``bool``
    :return: None
    """
    if verbose:
        print("\nWriting to file..")
    dict2files("", storage_root, flows_dict)
    if verbose:
        print("Done printing to file!")


def sort_flows_from_dict_to_hfd5(flows_dict, hdf5_sorted: HDF5FlowStorage, verbose=True):
    """
    Store provided flows in an HDF5 file

    :param flows_dict:
    :param hdf5_sorted:
    :param verbose:
    :return:
    """
    if verbose:
        print("\nWriting to file..")
    dict2hdf5(hdf5_sorted, hdf5_sorted.root_group_name + '/' + hdf5_sorted.sorted_flows_group_name, flows_dict)
    if verbose:
        print("Done printing to file!")
