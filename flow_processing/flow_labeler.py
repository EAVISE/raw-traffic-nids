import os, sys
from enums import Dataset
from flow_processing.flow import Flow
from flow_processing.hdf5_interface import HDF5FlowStorage
from flow_processing import CICIDS2017, ISCX2012, UNSW_NB15
import help_functions as hf
from enums import Enum, LabelStatus


def _check_for_existing_labelled_files(directory, label):
    """
    For a given directory containing flow files, find all flow files labelled 
    """
    count = 0
    for item in os.listdir(directory):
        # Labelled files are stored as: LABEL-n_x.x.x.x-x.x.x.x-x-x-x.txt
        # Split label part from flow ID:
        item_parts = item.split("_")

        # Make sure that the item actually is labelled
        if len(item_parts) > 1:
            # Get label from label part (the first part of the item's name)
            item_label = item_parts[0].split("-")[0]
            
            # If the label is of the kind we're looking for, increment the count
            if (item_label == label): count += 1
    return count


def _rewrite_old_file(target_dir, target_file, n_current):

    n = 0  # Number of lines parsed in old file
    with open(os.path.join(target_dir,'temp.txt'),'w') as tempf:
        with open(os.path.join(target_dir, target_file), "r") as oldf:
            for line in oldf:
                if n >= n_current:
                    tempf.write(line)
                n += 1
        
    # Remove old file
    os.remove(os.path.join(target_dir, target_file))
    
    # If the old file had no more lines left (stepsize was equal to the remaining amount of lines),
    # no file with remaining lines needs to be created
    if n <= n_current:
        # Remove temp file:
        os.remove(os.path.join(target_dir, 'temp.txt'))
    else:
        # Rename temporary file to create the old file with fewer lines
        os.rename(os.path.join(target_dir, 'temp.txt'), os.path.join(target_dir, target_file))


def _label_flow_chronologically_with_step(target_dir, target_file, target_dir_return, target_file_return, flow,
                                          renamed_target_file, error_file, stepsize):
    # Lines that are written to the labelled file
    all_lines = []
    # Total number of packets captured per flow
    n_packets = 0
    n_packets_return = 0

    # Set n_current to 0: These parameters keep track of the step size
    n_current = 0
    n_current_return = 0

    # Track presence of lines in file
    stillPresent = True         # Indicates whether there are still lines present in the target file
    stillPresent_return = True  # Indicates whether there are still lines present in the target return file
    end_of_task = False

    while not end_of_task:
        with open(os.path.join(target_dir, target_file), "r") as f:
            with open(os.path.join(target_dir_return, target_file_return), "r") as f_return:
                # If statements to ensure that a new line is only read if there were still lines present the last time the file was read
                if stillPresent:
                    line1 = f.readline()
                    stillPresent = (line1 != "")
                
                if stillPresent_return:
                    line2 = f_return.readline()
                    stillPresent_return = (line2 != "")
                
                # If the new lines are still valid, extract the useful information
                # Otherwise, set the ts to zero (to avoid errors in hf.precedes())
                if stillPresent:
                    ms1, ns1, current_line = line1.split(",")
                else:
                    ms1, ns1 = 0, 0
                
                if stillPresent_return:
                    ms2, ns2, current_line_return = line2.split(",")
                else:
                    ms2, ns2 = 0, 0


                # Reset intermediary progress:
                n_current = 0
                n_current_return = 0
                while (stillPresent_return or stillPresent) and ((n_current + n_current_return) < stepsize):
                    #print("|Total: {}:{}| Current: {}:{} <-> {}| Lines: {}...:{}...|{}:{}|".format(n_packets, n_packets_return, \
                    #        n_current, n_current_return, stepsize, current_line, current_line_return, stillPresent, stillPresent_return))
                    if (stillPresent and (hf.precedes(ms1, ns1, ms2, ns2) or (not stillPresent_return))) and (n_packets < flow.fwd_packets):
                        #print("a ", end="")
                        all_lines.append(current_line)
                        next_line = f.readline()
                        stillPresent = (next_line != "")
                        if stillPresent:
                            ms1, ns1, current_line = next_line.split(",")
                        n_packets += 1
                        n_current += 1

                    elif stillPresent_return and (n_packets_return < flow.bwd_packets):
                        ##print("b ",end="")
                        all_lines.append(current_line_return)
                        next_line = f_return.readline()
                        stillPresent_return = (next_line != "")
                        if stillPresent_return:
                            ms2, ns2, current_line_return = next_line.split(",")
                        n_packets_return += 1
                        n_current_return += 1
                    else:
                        break
                    
                    # elif stillPresent:
                    #     old_lines = f.readlines()
                    #     stillPresent = False
                    # else:
                    #     old_lines_return = f_return.readlines()
                    #     stillPresent_return = False
                    #     break
        
        # Calculate end_of_task:
        end_of_task = ((not (stillPresent or stillPresent_return)) or ((n_packets >= flow.fwd_packets) and (n_packets_return >= flow.bwd_packets)))
        
        # If end of task:
        if end_of_task:
            # Write remaining packets to target file
            with open(os.path.join(target_dir, renamed_target_file), "a") as f:
                f.write("".join(all_lines))

            # Remove empty files if necessary:
            if not stillPresent:
                # Report the issue:
                with open(error_file, "a") as f:
                    f.write("Too few packets," + target_file + "," + flow.label + "\n")
                # Remove the empty file
                os.remove(os.path.join(target_dir, target_file))
            
            if not stillPresent_return:
                # Report the issue:
                with open(error_file, "a") as f:
                    f.write("Too few packets," + target_file_return + "," + flow.label + "\n")
                # Remove the empty file
                os.remove(os.path.join(target_dir_return, target_file_return))

        # Not finished with the task, but no EOF has been reached            
        elif stillPresent and stillPresent_return:
            # Write the relevant line in the corresponding file:
            with open(os.path.join(target_dir, renamed_target_file), "a") as f:
                f.write("".join(all_lines))    
            
            all_lines = []
            # Overwrite the original file with fewer lines
            # 1: For forward file
            _rewrite_old_file(target_dir=target_dir, target_file=target_file, n_current=n_current)
            # 2: For backward file:
            _rewrite_old_file(target_dir=target_dir_return, target_file=target_file_return, n_current=n_current_return)
        
        elif (not stillPresent) and stillPresent_return:
            # Write the relevant line in the corresponding file:
            with open(os.path.join(target_dir, renamed_target_file), "a") as f:
                f.write("".join(all_lines))    
            
            all_lines = []
            # Overwrite the original file with fewer lines
            # For backward file:
            _rewrite_old_file(target_dir=target_dir_return, target_file=target_file_return, n_current=n_current_return)
        
        elif stillPresent and (not stillPresent_return):
            # Write the relevant line in the corresponding file:
            with open(os.path.join(target_dir, renamed_target_file), "a") as f:
                f.write("".join(all_lines))    
            
            all_lines = []
            # Overwrite the original file with fewer lines
            # or forward file
            _rewrite_old_file(target_dir=target_dir, target_file=target_file, n_current=n_current)


def _label_flow_with_step(target_dir, target_file, flow, renamed_target_file, errors, stepsize):
    current_lines = []
    n_current_packets = 0
    n_total_packets = 0
    EOF = False

    while not EOF:
        with open(os.path.join(target_dir, target_file), "r") as f:
            # Read the file into memory, one line at a time
            for line in f:
                if line == "":
                    # EOF reached:
                    EOF = True
                    break
                current_lines.append(line)
                n_current_packets += 1
                n_total_packets += 1
                # Check whether we have to stop collecting packets
                if (n_current_packets >= stepsize) or (n_total_packets >= flow.fwd_packets):
                    break
        
        # If EOF was reached before collecting all flow.fwd_packets, we report the issue
        if EOF and (n_total_packets < flow.fwd_packets):
            errors['insufficient'].append((flow.label_idx, target_file, flow.label))
            #with open(error_file, "a") as f:
            #    f.write("Too few packets," + target_file + "," + flow.label + "\n")
            # Copy remainder of target file to the renamed target file:
            with open(os.path.join(target_dir,renamed_target_file), 'a') as new_file:
                    new_file.write("".join(current_lines))
                            
            # Remove old file:
            os.remove(os.path.join(target_dir, target_file))
        else:
            # If we reached our goal of flow.fwd_packets
            if (n_total_packets >= flow.fwd_packets):
                # Write the relevant line in the corresponding file:
                with open(os.path.join(target_dir, renamed_target_file), "a") as f:
                    f.write("".join(current_lines))    
                
                current_lines = []
                # Overwrite the original file with fewer lines
                _rewrite_old_file(target_dir=target_dir, target_file=target_file, n_current=n_current_packets)

        # If we have processed all necessary packets, break out of the global loop
        if n_total_packets >= flow.fwd_packets:
            EOF = True


def _label_flow_with_step2(target_dir, target_file, flow, renamed_target_file, errors, stepsize, use_ts):
    current_lines = []
    n_current_packets = 0
    n_total_packets = 0
    EOF = False
    timing_issue = False

    while not (EOF or timing_issue):
        with open(os.path.join(target_dir, target_file), "r") as f:
            # Read the file into memory, one line at a time
            for line in f:
                if line == "":
                    # EOF reached:
                    EOF = True
                    break
                if use_ts and (hf.is_in_interval(line, flow)):
                    errors['timing'].append((flow.label_idx, target_dir, flow.label,
                                             flow.fwd_packets - n_total_packets))
                    timing_issue = True
                    break
                current_lines.append(line)
                n_current_packets += 1
                n_total_packets += 1
                # Check whether we have to stop collecting packets
                if (n_current_packets >= stepsize) or (n_total_packets >= flow.fwd_packets):
                    break

        # If EOF was reached before collecting all flow.fwd_packets, we report the issue
        if EOF and (n_total_packets < flow.fwd_packets):
            errors['timing'].append((flow.label_idx, target_dir, flow.label))
            # Copy remainder of target file to the renamed target file:
            with open(os.path.join(target_dir, renamed_target_file), 'a') as new_file:
                new_file.write("".join(current_lines))

            # Remove old file:
            os.remove(os.path.join(target_dir, target_file))
        elif timing_issue:
            # Copy remainder of target file to the renamed target file:
            with open(os.path.join(target_dir, renamed_target_file), 'a') as new_file:
                new_file.write("".join(current_lines))
            # Overwrite the original file with fewer lines
            _rewrite_old_file(target_dir=target_dir, target_file=target_file, n_current=n_current_packets)

        else:
            # If we reached our goal of flow.fwd_packets
            if (n_total_packets >= flow.fwd_packets):
                # Write the relevant line in the corresponding file:
                with open(os.path.join(target_dir, renamed_target_file), "a") as f:
                    f.write("".join(current_lines))

                current_lines = []
                # Overwrite the original file with fewer lines
                _rewrite_old_file(target_dir=target_dir, target_file=target_file, n_current=n_current_packets)

        # If we have processed all necessary packets, break out of the global loop
        if n_total_packets >= flow.fwd_packets:
            EOF = True


def _label_flow_chronologically_no_steps(target_dir, target_file, target_dir_return, target_file_return, flow, \
                                         renamed_target_file, error_file):
    all_lines = []
    old_lines = []
    old_lines_return = []
    n_packets = 0
    n_packets_return = 0
    #print("Haloooo")
    with open(os.path.join(target_dir, target_file), "r") as f:
        with open(os.path.join(target_dir_return, target_file_return), "r") as f_return:
            line1 = f.readline()
            stillPresent = (line1 != "")
            line2 = f_return.readline()
            stillPresent_return = (line2 != "")
            
            if(stillPresent):
                ms1, ns1, current_line = line1.split(",")
            else:
                ms1, ns1 = 0, 0
            
            if(stillPresent_return):
                ms2, ns2, current_line_return = line2.split(",")
            else:
                ms2, ns2 = 0, 0

            while stillPresent_return or stillPresent:
                if stillPresent and (hf.precedes(ms1, ns1, ms2, ns2) or (not stillPresent_return)) and (flow.fwd_packets > n_packets):
                    all_lines.append(current_line)
                    next_line = f.readline()
                    stillPresent = (next_line != "")
                    if stillPresent:
                        ms1, ns1, current_line = next_line.split(",")
                    n_packets += 1

                elif stillPresent_return and (flow.bwd_packets > n_packets_return):
                    all_lines.append(current_line_return)
                    next_line = f_return.readline()
                    stillPresent_return = (next_line != "")
                    if stillPresent:
                        ms2, ns2, current_line_return = next_line.split(",")
                    n_packets_return += 1
                
                elif stillPresent:
                    old_lines = f.readlines()
                    stillPresent = False
                else:
                    old_lines_return = f_return.readlines()
                    stillPresent_return = False
                    break
            
    if (flow.fwd_packets == n_packets):
        # To ensure that no missing packets are rapported (as no packets are missing)
        stillPresent = True
    
    if (flow.bwd_packets == n_packets_return):
        # To ensure that no missing packets are rapported (as no packets are missing)
        stillPresent_return = True
    
    #print(stillPresent, stillPresent_return, n_packets, n_packets_return, flow)
    # If the number of packets is greater than the expected number of forward packets,
    # another flow with the same ID will succeed this one. The packets belonging to that flow
    # should not be combined with this label.
    if (n_packets >= flow.fwd_packets) and (n_packets_return >= flow.bwd_packets):
        # Write the relevant line in the corresponding file:
        # We combine the results in ONE file
        with open(os.path.join(target_dir, renamed_target_file), "w") as f:
            f.write("".join(all_lines))
        
        # Overwrite the original files with fewer lines,
        # but only if they still contain packets.
        # Otherwise, the old files are removed
        if n_packets == flow.fwd_packets:
            os.remove(os.path.join(target_dir, target_file))
        else:
            with open(os.path.join(target_dir, target_file), "w") as f:
                f.write("".join(old_lines))
        
        if n_packets_return == flow.bwd_packets:
            os.remove(os.path.join(target_dir_return, target_file_return))
        else:
            with open(os.path.join(target_dir_return, target_file_return), "w") as f:
                f.write("".join(old_lines_return))
    
    # If EOF was reached with still necessary packets remaining
    elif (not stillPresent) and (n_packets_return < flow.bwd_packets):
        with open(error_file, "a") as f:
            f.write("Too few packets," + target_file + "," + flow.label + "\n")
        
        # Now still write the useful data away
        with open(os.path.join(target_dir, renamed_target_file), "a") as f:
            f.write("".join(all_lines))
        # And remove the empty file
        os.remove(os.path.join(target_dir_return, target_file_return))

    elif (not stillPresent_return) and (n_packets < flow.fwd_packets):
        with open(error_file, "a") as f:
            f.write("Too few packets," + target_file_return + "," + flow.label + "\n")
    
        # No still write the useful data away
        with open(os.path.join(target_dir, renamed_target_file), "a") as f:
            f.write("".join(all_lines))
        # And remove the empty file
        os.remove(os.path.join(target_dir, target_file))
        
    else:
        with open(error_file, "a") as f:
            f.write("Too few packets," + target_file_return + "," + flow.label + "\n")

        with open(error_file, "a") as f:
            f.write("Too few packets," + target_file + "," + flow.label + "\n")
        
        with open(os.path.join(target_dir, renamed_target_file), "a") as f:
            f.write("".join(all_lines))

        os.remove(os.path.join(target_dir, target_file))
        os.remove(os.path.join(target_dir_return, target_file_return))


def _label_flow_no_steps(target_dir, target_file, flow, renamed_target_file, errors):
    with open(os.path.join(target_dir, target_file), "r") as f:
        all_lines = f.readlines()
        n_packets = len(all_lines)

    # If the number of packets is greater than the expected number of forward packets,
    # another flow with the same ID will succeed this one. The packets belonging to that flow
    # should not be combined with this label.
    if n_packets > flow.fwd_packets:
        # Write the relevant line in the corresponding file:
        with open(os.path.join(target_dir, renamed_target_file), "w") as f:
            f.write("".join(all_lines[:flow.fwd_packets]))    
        
        # Overwrite the original file with fewer lines
        with open(os.path.join(target_dir, target_file), "w") as f:
            f.write("".join(all_lines[flow.fwd_packets:]))
    
    # Otherwise, if there is a perfect fit
    elif n_packets == flow.fwd_packets:
        # Simply renaming the target file suffices
        os.rename(os.path.join(target_dir, target_file), os.path.join(target_dir,renamed_target_file))
    
    # If too few packets remain, report the issue:
    else:

        #with open(error_file, "a") as f:
        #    f.write("Too few packets," + target_file + "," + flow.label + "\n")
        errors['insufficient'].append((flow.label_idx, target_file, flow.label))
        # Then also rename the file
        os.rename(os.path.join(target_dir, target_file), os.path.join(target_dir, renamed_target_file))


def _label_flow_no_steps2(target_dir, target_file, flow, renamed_target_file, errors, use_ts):
    fwd_packets = flow.fwd_packets
    with open(os.path.join(target_dir, target_file), "r") as f:
        all_lines = f.readlines()
        n_packets = len(all_lines)
        final_packet_pointer = n_packets - 1

    # Check for errors
    # Check if the file contains sufficient packets
    if n_packets < fwd_packets:
        errors['insufficient'].append((flow.label_idx, target_file, flow.label))
    else:
        final_packet_pointer = fwd_packets - 1
    # Check if the packets fall in the timeframe of the corresponding flow
    if use_ts and (not hf.is_in_interval(all_lines[final_packet_pointer], flow)):
        # If not, try each packet until it falls in the timeframe,
        # starting from the last working to the first
        # idx is the number of packets that, while present, fail to comply to timing requirements
        idx = 1
        # Increase idx until a legitimate packet has been found.
        while idx < fwd_packets:
            try:
                if hf.is_in_interval(all_lines[final_packet_pointer - idx], flow):
                    break
                idx += 1
                if idx == (final_packet_pointer + 1):
                    # If this is the case, none of the available packets lie within the desired timing range
                    # In that case, we have a critical timing error: A complete mismatch between csv and pcap
                    errors['timing_critical'].append((flow.label_idx, target_file, flow.label, idx))
                    return
            except Exception:
                pass
        final_packet_pointer -= idx
        # We report idx
        errors['timing'].append((flow.label_idx, target_file, flow.label, idx))

    # Now write the packets to their new labelled files
    if final_packet_pointer == (n_packets - 1):
        # Simply renaming the target file suffices
        os.rename(os.path.join(target_dir, target_file), os.path.join(target_dir, renamed_target_file))
    # If the number of packets is greater than the expected number of forward packets,
    # another flow with the same ID will succeed this one. The packets belonging to that flow
    # should not be combined with this label.
    else:
        # Write the relevant lines in the corresponding file:
        with open(os.path.join(target_dir, renamed_target_file), "w") as f:
            f.write("".join(all_lines[:final_packet_pointer + 1]))
        # Overwrite the original file with fewer lines
        with open(os.path.join(target_dir, target_file), "w") as f:
            f.write("".join(all_lines[final_packet_pointer + 1:]))


def label_flow2(root, flow, errors, config):
    """
    Label a specific flow by looking up the corresponding flow file in storage. If the file contains multiple flows with the same ID, \
        extract the packets corresponding to this given flow and store them in a unique file.
    \nroot(str): root directory of the flows
    \nflow(Flow): Flow object containing all relevant information of the flow that needs to be labelled
    \nerror_file(str): path to file wherein errors will be recorded
    """
    target_dir = flow.get_storage_dir(root)
    # Check if the directory exists
    if os.path.isdir(target_dir):
        # Target file: the unlabeled file
        target_file = flow.get_filename()

        # Check if it exists:
        if not os.path.isfile(os.path.join(target_dir, target_file)):
            # Report the issue
            errors['missing_file'].append((flow.label_idx, target_file, flow.label))
            return

        # Get the number of files that already exist for the label:
        n_labelled_files = _check_for_existing_labelled_files(target_dir, flow.label)

        # Target file labeled with the current label
        renamed_target_file = flow.label + "-" + str(n_labelled_files) + "_" + target_file

        # Check if the available RAM can effectively contain the data
        # Packets file size
        packets_file_size = hf.get_filesize(os.path.join(target_dir, target_file))
        max_size = config.max_ram_usage
        # We have to be able to load the
        if packets_file_size > max_size:
            # Remaining memory after loading packets file:
            remaining_mem = max_size - packets_file_size

            if remaining_mem <= 0:
                print("Insufficient RAM available for labelling flow files. Program will be shut down.")
                sys.exit(0)

            # Fraction of memory we wish to use (leaves open room for copies and inaccuracies in the averages)
            usable_mem = 0.5 * remaining_mem

            # Calculate the expected amount of packets corresponding with this amount of usable memory:
            mem_per_packet = config.avg_packet_size + 1  # +1 for "\n"
            stepsize = usable_mem / mem_per_packet
            print("Potential memory issues, work with stepsize")
            _label_flow_with_step2(target_dir, target_file, flow, renamed_target_file,
                                   errors, stepsize, use_ts=config.use_ts_labelling)

        else:
            # No memory issues
            _label_flow_no_steps2(target_dir, target_file, flow, renamed_target_file,
                                  errors, use_ts=config.use_ts_labelling)

    # The directory (and packets file) does not exist
    else:
        # Report the issue
        errors['missing_dir'].append((flow.label_idx, flow.get_filename(), flow.label))


def label_flow_basic_hdf5(hdf5, flow):

    present, path_to_flow = hdf5.flow_root_exists(flow)

    # Check if the directory exists:
    if not present:
        return LabelStatus.MISSING_DIR

    # Check if there remain any unlabelled packets
    if not hdf5.check_for_unlabelled_packets(path_to_flow):
        return LabelStatus.MISSING_FILE

    # Get the index of the next packet(s) to label
    return hdf5.label_flow(path_to_flow, flow)


def label_flow(root, flow, errors, config):
    """
    Label a specific flow by looking up the corresponding flow file in storage. If the file contains multiple flows with the same ID, \
        extract the packets corresponding to this given flow and store them in a unique file.
    \nroot(str): root directory of the flows
    \nflow(Flow): Flow object containing all relevant information of the flow that needs to be labelled
    \nerror_file(str): path to file wherein errors will be recorded
    """
    target_dir = flow.get_storage_dir(root)
    # Check if the directory exists
    if os.path.isdir(target_dir):
        # Target file: the unlabeled file
        target_file = flow.get_filename()

        # Check if it exists:
        if not os.path.isfile(os.path.join(target_dir, target_file)):
            # Otherwise, report the problem
            errors['missing_file'].append((flow.label_idx, target_file, flow.label))
            #with open(error_file, "a") as f:
                #print(os.path.join(target_dir, target_file), flow)
                #import sys
                #sys.exit(0)
                #f.write("File doesn't exist," + target_file + "," + flow.label + "\n")
            return

        # Get the number of files that already exist for the label:
        n_labelled_files = _check_for_existing_labelled_files(target_dir, flow.label)

        # Target file labeled with the current label
        renamed_target_file = flow.label + "-" + str(n_labelled_files) + "_" + target_file
        
        # Check if the available RAM can effectively contain the data
        # Packets file size
        packets_file_size = hf.get_filesize(os.path.join(target_dir, target_file))
        max_size = config.max_ram_usage
        if packets_file_size > (max_size / 2.0):
            # Remaining memory after loading packets file:
            remaining_mem = max_size - packets_file_size
            
            if remaining_mem <= 0:
                print("Insufficient RAM available for labelling flow files. Program will be shut down.")
                sys.exit(0)
            
            # Fraction of memory we wish to use (leaves open room for copies and inaccuries in the averages)
            usable_mem = 0.4 * remaining_mem

            # Calculate the estimated expected amount of packets corresponding with this amount of usable memory:
            mem_per_packet = config.avg_packet_size + 23  # +23 for timestamps and "\n"
            stepsize = usable_mem / mem_per_packet
            print("Potential memory issues, work with stepsize")
            _label_flow_with_step(target_dir, target_file, flow, renamed_target_file, errors, stepsize)
        
        else:
            # No memory issues
            _label_flow_no_steps(target_dir, target_file, flow, renamed_target_file, errors)

    # The directory (and packets file) does not exist
    else:
        # Report the issue
        errors['missing_dir'].append((flow.label_idx, flow.get_filename(), flow.label))
        #with open(error_file, "a") as f:
        #        f.write("Missing," + target_dir + "," + flow.label + "\n")


def label_flow_chronologically(root, flow, error_file, config):
    """
    Label a specific flow by looking up the corresponding flow file in storage. If the file contains multiple flows
    with the same ID, extract the packets corresponding to this given flow and store them in a unique file.

    :param root: Path to root directory of the flows
    :param flow: Flow object containing all relevant information of the flow that needs to be labelled
    :param error_file: Path to file wherein errors will be recorded
    :param config: Dataset configuration
    :type root: ``str``
    :type flow: ``Flow``
    :type error_file: ``str``
    :type config: ``DatasetConfig``
    :return: None
    """
    """
    
    \nroot(str): 
    \nflow(Flow): 
    \nerror_file(str): 
    """
    # If there is no return flow, standard procedure follows
    if(flow.bwd_packets == 0):
        label_flow(root, flow, error_file)

    return_flow = flow.get_return_flow()
    target_dir = flow.get_storage_dir(root)
    target_dir_return = return_flow.get_storage_dir(root)
    # Check if the directories exists
    if os.path.isdir(target_dir) and os.path.isdir(target_dir_return):
        # Target files: the unlabeled file
        target_file = flow.get_filename()
        target_file_return = return_flow.get_filename()

        # Check if they exists:
        if not os.path.isfile(os.path.join(target_dir, target_file)):
            # Otherwise, report the problem
            with open(error_file, "a") as f:
                f.write("File doesn't exist," + target_file + "," + flow.label + "\n")
            return

        if not os.path.isfile(os.path.join(target_dir_return, target_file_return)):
            # Otherwise, report the problem
            with open(error_file, "a") as f:
                f.write("File doesn't exist," + target_file_return + "," + return_flow.label + "\n")
            return

        # Get the number of files that already exist for the label:
        n_labelled_files = _check_for_existing_labelled_files(target_dir, flow.label)
        n_labelled_files_return = _check_for_existing_labelled_files(target_dir_return, return_flow.label)

        # Target file labeled with the current label
        renamed_target_file = flow.label + "-" + str(n_labelled_files) + "_" + target_file
        renamed_target_file = return_flow.label + "-" + str(n_labelled_files_return) + "_" + target_file_return
    
        # Check if the available RAM can effectively contain the data
        # Packets file size
        packets_file_size = hf.get_filesize(os.path.join(target_dir, target_file))
        packets_file_size_return = hf.get_filesize(os.path.join(target_dir_return, target_file_return))
        
        max_size = config.max_ram_usage
        if (packets_file_size + packets_file_size_return) > ( max_size / 2.0):
            # Remaining memory after loading packets file:
            remaining_mem = max_size - (packets_file_size + packets_file_size_return)
            
            if(remaining_mem <= 0):
                print("Insufficient RAM available for labelling flow files. Program will be shut down.")
                sys.exit(0)
            
            # Fraction of memory we wish to use (leaves open room for copies and inaccuries in the averages)
            usable_mem = 0.4 * remaining_mem

            # Calculate the expected amount of packets corresponding with this amount of usable memory:
            mem_per_packet = config.avg_packet_size + 1  # +1 for "\n"
            stepsize = usable_mem / mem_per_packet
            print("Potential memory issues, work with stepsize")
            _label_flow_chronologically_with_step(target_dir, target_file, target_dir_return, target_file_return, flow,
                                                  renamed_target_file, error_file, stepsize)
        
        else:
            # No memory issues
            _label_flow_chronologically_no_steps(target_dir, target_file, target_dir_return, target_file_return, flow,
                                                 renamed_target_file, error_file)

    # The directory (and packets file) does not exist
    else:
        # Report the issue
        with open(error_file, "a") as f:
                f.write("Missing," + target_dir + "," + flow.label + "\n")


def label_sorted_flows_with_timestamps(label_csv, flows_root_dir, error_file, config, verbose=True):
    """
    Label the sorted flows, considering bidirectional flow traffic and the timestamps of forward and backward traffic.

    :param label_csv: Path to label CSV file from which the flows are labelled
    :param flows_root_dir: Path to root directory of the sorted flows tree
    :param error_file: Path to file to store reported errors into
    :param config: Dataset configuration with labelling parameters
    :param verbose: If True, report on progress and other meta-information
    :type label_csv: ``str``
    :type flows_root_dir: ``str``
    :type error_file: ``str``
    :type config: ``DatasetConfig``
    :type verbose: ``bool``
    :return: None
    """
    # If chronological: assume the src -> dst and dst -> src flows are sorted chronologically:
    # 1 src -> dst
    # 2 src -> dst
    # 3 src -> dst
    # 4 dst -> src
    # 5 src -> dst
    # 6 dst -> src
    # 7 dst -> src
    # ...
    dataset = Dataset[config.dataset]
    errors = {
        'timing': [],
        'insufficient': [],
        'missing_dir': [],
        'missing_file': [],
        'timing_critical': []
    }
    if config.bidirectional and config.chronological:
        with open(label_csv, "r") as f:
            for idx, line in enumerate(f):
                if idx >= 1:
                    if verbose:
                        print("Current line: " + str(idx) + " |",end="\r")
                    
                    # Turn the line into a Flow object
                    flow = None
                    if dataset == Dataset.CICIDS2017:
                        flow = CICIDS2017.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.ISCX2012:
                        flow = ISCX2012.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.UNSW_NB15:
                        flow = UNSW_NB15.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    # Also get the return flow
                    # Try to label the flow
                    if flow.fwd_packets > 0:
                        label_flow_chronologically(flows_root_dir, flow, error_file)

    elif config.bidirectional:
        with open(label_csv, "r") as f:
            for idx, line in enumerate(f):
                if idx >= 1:
                    if verbose:
                        print("Current line: " + str(idx) + " |", end="\r")
                    
                    # Turn the line into a Flow object
                    flow = None
                    if dataset == Dataset.CICIDS2017:
                        flow = CICIDS2017.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.ISCX2012:
                        flow = ISCX2012.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.UNSW_NB15:
                        flow = UNSW_NB15.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    # Also get the return flow
                    # Try to label the flow
                    if flow.fwd_packets > 0:
                        label_flow(flows_root_dir, flow, errors, config)
                        #label_flow2(flows_root_dir, flow, errors, config)
                    if flow.bwd_packets > 0:
                        label_flow(flows_root_dir, flow.get_return_flow(), errors, config)
                        #label_flow2(flows_root_dir, flow, errors, config)
            for error, error_list in errors.items():
                if len(error_list) > 0:
                    print('Error', error, 'had', len(error_list), 'occurrences')


def label_sorted_flows_no_timestamps(label_csv, flows_root_dir, error_file, config, verbose=True):
    """
    Put the correct label at each flow in the sorted flows directory tree

    :param label_csv: Path to label CSV file from which the flows are labelled
    :param flows_root_dir: Path to root directory of the sorted flows tree
    :param error_file: Path to file to store reported errors into
    :param config: Dataset configuration with labelling parameters
    :param verbose: If True, report on progress and other meta-information
    :type label_csv: ``str``
    :type flows_root_dir: ``str``
    :type error_file: ``str``
    :type config: ``DatasetConfig``
    :type verbose: ``bool``
    :return: None
    """
    dataset = Dataset[config.dataset]
    with open(label_csv,"r") as f:
        for idx, line in enumerate(f):
            if idx >= 1:
                if verbose: print("Current line: " + str(idx) + " |",end="\r")
                
                # Turn the line into a Flow object
                flow = None
                if dataset == Dataset.CICIDS2017:
                    flow = CICIDS2017.label_csv_line_to_flow(line=line)
                elif dataset == Dataset.ISCX2012:
                    flow = ISCX2012.label_csv_line_to_flow(line=line)
                elif dataset == Dataset.UNSW_NB15:
                    flow = UNSW_NB15.label_csv_line_to_flow(line=line)
                
                # Try to label the flow
                if flow.fwd_packets > 0:
                    label_flow(flows_root_dir, flow, error_file, config)
                
                # If bidirectional == True, also label the return flow
                if config.bidirectional and (flow.bwd_packets > 0):
                    label_flow(flows_root_dir, flow.get_return_flow(), error_file, config)


def _label_flow_file(flow_file, path_to_ff_dir, label):
    """
    Function to label one specific flow file with the given label. Meant for use in the process_flow_tree help function.

    :param flow_file: Name of the file to be labelled
    :param path_to_ff_dir: Path to the directory containing the flow file in question
    :param label: Label to be given to the flow file
    :type flow_file: ``str``
    :type path_to_ff_dir: ``str``
    :type label: ``str``
    :return: None
    """
    # Create labelled filename
    new_filename = label + "-0_" + flow_file
    # Rename the flow file
    os.rename(os.path.join(path_to_ff_dir, flow_file), os.path.join(path_to_ff_dir, new_filename))


def label_sorted_flows_no_csv(label, flows_root_dir, verbose=True):
    """
    Function designed to specifically label flows that have no CSV label file assigned to them. This is for example the
    case for Friday 11th in the ISCX2012 dataset. Therefor, this function labels EVERY flow in the specified directory
    as the designated label (benign, normal, ...).  Note however that in the current implementation, no distinction is
    made between separate flows with the same flow id (src and dst ip and port address as well as protocol).

    :param label: Every flow will be labelled with this label
    :param flows_root_dir: Root directory containing all sorted flows to be labelled
    :param verbose: If True, print additional information as output
    :type label: ``str``
    :type flows_root_dir: ``str``
    :type verbose: ``bool``
    :return: None
    """
    
    # We cannot use time stamp functionality, highy complicating this process, as distinct flows cannot be distinguished
    # Therefore, we just label every flow once, instead of trying to find distinction based on TCP information or timing
    action = {'function': _label_flow_file, 'params': {'label': label}}
    hf.process_flow_tree(flows_root_dir, action, verbose=verbose)



def label_sorted_flows(label_csv, flows_root_dir, error_file, config, day=None, verbose=True):
    """
    Function to label flows at specific location corresponding to information contained in the label CSV and according
    to the provided configuration.

    :param label_csv: Path to the csv-file used to label specific flows at the given location
    :type label_csv: ``str``
    :param flows_root_dir: Root directory containing the sorted flows to be labelled
    :type flows_root_dir: ``str``
    :param error_file: File to store error message into
    :type error_file: ``str``
    :param config: Contains all necessary dataset configuration information
    :type config: ``DatasetConfig``
    :param day: Day of the dataset
    :type day: ``str``
    :param verbose: If True, print additional information as output
    :type verbose: ``bool``
    """
    if config.enable_hdf5:
        label_hdf5_flow(label_csv=label_csv, config=config, day=day, verbose=True)

    elif config.stream_based:
        label_stream_based_flow(label_csv=label_csv, flows_root_dir=flows_root_dir, error_file=error_file,
                                config=config, verbose=True)
    else:
        if config.include_ts:
            label_sorted_flows_with_timestamps(label_csv=label_csv, flows_root_dir=flows_root_dir,
                                               error_file=error_file, config=config, verbose=True)
        else:
            label_sorted_flows_no_timestamps(label_csv, flows_root_dir, error_file, config=config, verbose=verbose)


def label_stream_based_flow(label_csv, flows_root_dir, error_file, config, verbose=True):
    dataset = Dataset[config.dataset]
    status = {
        LabelStatus.MISSING_DIR: 0,
        LabelStatus.MISSING_FILE: 0,
        LabelStatus.INCOMPLETE: 0,
        LabelStatus.VALID: 0,
    }

    if not config.chronological:
        with open(label_csv, "r") as f:
            for idx, line in enumerate(f):
                if idx >= 1:
                    if verbose:
                        print("Current line: " + str(idx) + " |", end="\r")

                    # Turn the line into a Flow object
                    flow = None
                    if dataset == Dataset.CICIDS2017:
                        flow = CICIDS2017.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.ISCX2012:
                        flow = ISCX2012.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.UNSW_NB15:
                        flow = UNSW_NB15.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    # Also get the return flow
                    # Try to label the flow
                    if flow.fwd_packets > 0:
                        status[label_stream_based_flow_no_chronological(flows_root_dir, flow, verbose=verbose)] += 1

                    if (flow.bwd_packets > 0) and config.bidirectional:
                        status[label_stream_based_flow_no_chronological(flows_root_dir,
                                                                        flow.get_return_flow(), verbose=verbose)] += 1
    else:
        raise NotImplemented('Not yet implemented.')

    if verbose:
        print('The labelling resulted in the following accumulation of statuses:')
        print(status)


def label_stream_based_flow_no_chronological(flows_root_dir, flow, verbose=True):
    """

    :param flows_root_dir:
    :param flow:
    :return:
    """
    flows_root_dir = flow.get_storage_dir(flows_root_dir)

    # Check if the directory exists:
    if not os.path.isdir(flows_root_dir):
        return LabelStatus.MISSING_DIR

    # Set up the base filename
    base_filename = flow.get_filename_v2()
    base_path = os.path.join(flows_root_dir, base_filename)

    # Check if the target file exists
    if not os.path.isfile(base_path):
        if verbose:
            print('Missing', base_path)
        return LabelStatus.MISSING_FILE

    # Map contents of target directory
    labelled, unlabelled = examine_flow_stream_directory(flows_root_dir)

    # Calculate the index of the target flow file
    if flow.label in labelled.keys():
        index = len(labelled[flow.label])
    else:
        index = 0

    # Start labelling
    labelled_packets = 0
    finished_labelling = False

    with open(base_path, 'r') as unlabelled_f:
        label_f = open(os.path.join(flows_root_dir, '{}-{}_{}'.format(flow.label, index, base_filename)), 'w')
        temp_f = None

        for line in unlabelled_f:
            # If we finished labelling, we copy the remainder of the file to a temporary file
            if finished_labelling:
                temp_f.write(line)
            # Otherwise, continue labelling
            else:
                # If we reach the end of a group of packets
                # We check if are done labelling,
                if line == '\n':

                    # Check if we are done with labelling
                    finished_labelling = (labelled_packets >= flow.fwd_packets)
                    # Otherwise, open a new file
                    if finished_labelling:
                        # Close the currently opened file
                        label_f.close()
                        # Open the temporary file to store lines into
                        temp_f = open(os.path.join(flows_root_dir, 'temp.txt'), 'w')
                    else:
                        label_f.write('\n')
                else:
                    label_f.write(line)
                    labelled_packets += 1

        if not label_f.closed:
            # This means the temp file never was opened,
            # and hence does not need to be renamed or closed
            # We immediately return the issue
            label_f.close()

            # If no packets were labelled
            # an unnecessat file has been created: remove it
            if labelled_packets == 0:
                os.remove(os.path.join(flows_root_dir, '{}-{}_{}'.format(flow.label, index, base_filename)))

            # As we only get to this loop if the entire base file has been read, and no more packets remain
            # Thus, remove the base file
            os.remove(base_path)

            return LabelStatus.INCOMPLETE

        # Close the temp file
        temp_f.close()

        # Finally, remove the original file and rename the new file
        os.remove(base_path)
        os.rename(os.path.join(flows_root_dir, 'temp.txt'), base_path)

        return LabelStatus.VALID

def is_labelled_file(flow_file):
    """
    Check if the flow file is labelled, and return the label.

    :param flow_file: Flow file name
    :type flow_file: ``str``
    :return: Label, if applicable, or None
    :rtype: ``str`` or None
    """
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        label = None
    else:
        #   - Generate flow image
        label = "-".join(flow_file_parts[0].split("-")[:-1])

    return label


def examine_flow_stream_directory(path_to_directory):
    """
    Build overview of labelled and unlabelled files
    :param path_to_directory: Path to flow stream directory to be examined
    :return:
    """
    labelled = {}
    unlabelled = []
    for f in os.listdir(path_to_directory):
        label = is_labelled_file(f)
        if label:
            if label in labelled.keys():
                labelled[label].append(f)
            else:
                labelled[label] = [f]
        else:
            unlabelled.append(f)

    return labelled, unlabelled


def label_hdf5_flow(label_csv, config, day, verbose=True):
    dataset = Dataset[config.dataset]
    status = {
        LabelStatus.MISSING_DIR: 0,
        LabelStatus.MISSING_FILE: 0,
        LabelStatus.INCOMPLETE: 0,
        LabelStatus.VALID: 0,
    }

    if not config.chronological:
        with open(label_csv, "r") as f:
            for idx, line in enumerate(f):
                if idx >= 1:
                    if verbose:
                        print("Current line: " + str(idx) + " |", end="\r")

                    # Turn the line into a Flow object
                    flow = None
                    if dataset == Dataset.CICIDS2017:
                        flow = CICIDS2017.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.ISCX2012:
                        flow = ISCX2012.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    elif dataset == Dataset.UNSW_NB15:
                        flow = UNSW_NB15.label_csv_line_to_flow(line=line, use_ts=config.use_ts_labelling)
                        flow.label_idx = idx
                    # Also get the return flow
                    # Try to label the flow
                    hdf5_file = HDF5FlowStorage(filename=config.get_hdf5_path(day=day),
                                                mode='a',
                                                packet_size=config.hdf5_packet_size)
                    if flow.fwd_packets > 0:
                        status[label_flow_basic_hdf5(hdf5=hdf5_file, flow=flow)] += 1

                    if (flow.bwd_packets > 0) and config.bidirectional:
                        status[label_flow_basic_hdf5(hdf5=hdf5_file, flow=flow.get_return_flow())] += 1

    else:
        raise NotImplemented('Not yet implemented.')

    if verbose:
        print('The labelling resulted in the following accumulation of statuses:')
        print(status)


if __name__ == "__main__":

    
    root1 = os.path.join("cicids2017tester","CICIDS2017","sorted_flows","friday")
    root2 = os.path.join("cicids2017tester","CICIDS2017","debug","friday","custom_bugs.txt")
    filename = "13.107.4.50-192.168.10.15-80-49808.txt"
    
    flowtest = Flow("192.168.10.15","13.107.4.50", "49474", "80", "6", "BENIGN", 33, 50)
    
    flow0 = Flow("192.168.10.15", "13.107.4.50", "49808", "80", "6", "BENIGN", 160324, 218124)
    flow1 = Flow("192.168.10.15", "13.107.4.50", "49808", "80", "6", "BENIGN", 196265, 265594)
    flow2 = Flow("192.168.10.15", "13.107.4.50", "49808", "80", "6", "BENIGN", 207964, 284602)
    flow3 = Flow("192.168.10.15", "13.107.4.50", "49808", "80", "6", "BENIGN", 206687, 281741)
    flow4 = Flow("192.168.10.15", "13.107.4.50", "49808", "80", "6", "BENIGN", 197473, 265615)
    flow5 = Flow("192.168.10.15", "13.107.4.50", "49808", "80", "6", "BENIGN",  77620, 104354)

    # Test1: Test using steps with too large files, no timestamps
    # label_flow(root=root1, flow=flow0.get_return_flow(), error_file=root2)
    # label_flow(root=root1, flow=flow1.get_return_flow(), error_file=root2)
    # label_flow(root=root1, flow=flow2.get_return_flow(), error_file=root2)
    # label_flow(root=root1, flow=flow3.get_return_flow(), error_file=root2)
    # label_flow(root=root1, flow=flow4.get_return_flow(), error_file=root2)
    # label_flow(root=root1, flow=flow5.get_return_flow(), error_file=root2)

    # Test2: Label flow chronologically using timestamps
    label_flow_chronologically(root=root1, flow=flowtest, error_file=root2)
    # label_flow_chronologically(root=root1, flow=flow0, error_file=root2)
    # label_flow_chronologically(root=root1, flow=flow1, error_file=root2)
    # label_flow_chronologically(root=root1, flow=flow2, error_file=root2)
    # label_flow_chronologically(root=root1, flow=flow3, error_file=root2)
  