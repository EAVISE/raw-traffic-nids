import os
import numpy as np
from features_config import SimpleVectorsConfig, HAST2Config
from help_functions import remove_dir, process_flow_tree, operate_on_bytes, shape_to_str


def _add_byte(byte, target, progress_list):
    """
    Function reserved for internal use. Used to add a hexidecimal string representing a byte to a target array while
    converting the byte to its integer 10-based integer.

    :param byte: Byte represented by two characters.
    :param target: Target to store the byte into
    :param progress_list: List with one element containing the current position in the target array.
    :type byte: ``str``
    :type target: ``numpy.ndarray``
    :type progress_list: ``list`` of [``int``]
    :return: None
    """
    target[progress_list[0]] = int(byte, 16)
    progress_list[0] += 1


def packet_to_bytes(packet, max_n=100):
    """
    Extract the bytes from provided packet, and one-hot encode them

    :param packet: Packet to extract bytes from
    :param max_n: Maximum number of bytes that should be extracted from the packet, default 100
    :type packet: ``str``
    :type max_n: ``int``

    :return: Numpy array of byte values, number of extracted bytes (<= max_n)
    :rtype: ``tuple`` of ``numpy.array``, ``int``
    """
    extracted_bytes = np.zeros(max_n, dtype='int32')
    # Use progress as a mutable counter that keeps track of the position in the np array that needs to be filled
    progress = [0]

    n_bytes = operate_on_bytes(packet=packet,
                               action={
                                   'function': _add_byte,
                                   'params': {'target': extracted_bytes, 'progress_list': progress},
                               },
                               max_n=max_n)
    return extracted_bytes, n_bytes


def _extract_all_simple_vectors(flow_file, feature_config, dataset_config):
    """
    Extract all simple vectors from the given flow file, sorting them in batches of n_packets, where n_batches is
    specified in feature_config.

    :param flow_file: Path to flow file to extract from
    :param feature_config: Configuration for the feature extraction
    :param dataset_config: Configuration of dataset to extract features from
    :type flow_file: ``str``
    :type feature_config: ``SimpleVectorsConfig``
    :type dataset_config: Object inherited from ``DatasetConfig``
    :return: List containing one extracted simple vectors array
    :rtype: ``list`` of ``numpy.array``
    """
    n_packets = feature_config.n_packets
    n_bytes = feature_config.n_bytes
    return_list = []
    with open(flow_file, "r") as ff:
        simple_vectors = np.zeros((n_packets, n_bytes), dtype='int32')

        current_n_packets = 0
        line = ff.readline()
        while line:
            # Take the possible presence of timestamps into account
            if not dataset_config.include_ts:
                packet = line.rstrip("\n")
            else:
                packet = line.split(",")[-1].rstrip("\n")

            # Calculate the image from the packet
            simple_vector, _ = packet_to_bytes(packet, max_n=n_bytes)

            # Update the image list
            simple_vectors[current_n_packets] = simple_vector

            # Prepare next line/packet
            current_n_packets += 1

            if (current_n_packets % n_packets == 0) and (current_n_packets != 0):
                return_list.append(simple_vectors)
                current_n_packets = 0

            line = ff.readline()

        if current_n_packets > 0:
            return_list.append(simple_vectors)

        return return_list


def _extract_first_simple_vectors(flow_file, feature_config, dataset_config):
    """
    Extract the first n_packets vectors from the given flow file, where n_packets is an attribute of feature_config
    specifying the number of packets to extract from a flow.

    :param flow_file: Path to flow file to extract from
    :param feature_config: Configuration for the feature extraction
    :param dataset_config: Configuration of dataset to extract features from
    :type flow_file: ``str``
    :type feature_config: ``SimpleVectorsConfig`` or ``HAST2Config``
    :type dataset_config: Object inherited from ``DatasetConfig``
    :return: List containing one extracted simple vectors array
    :rtype: ``list`` of ``numpy.array``
    """
    if isinstance(feature_config, SimpleVectorsConfig):
        n_packets = feature_config.n_packets
        n_bytes = feature_config.n_bytes
    elif isinstance(feature_config, HAST2Config):
        n_packets = feature_config.r
        n_bytes = feature_config.q
    else:
        raise Exception('Invalid configuration, got {} but should be SimpleVectorsConfig or HAST2Config'.format(
            feature_config.name))

    with open(flow_file, "r") as ff:
        simple_vectors = np.zeros((n_packets, n_bytes), dtype='int32')

        current_n_packets = 0
        line = ff.readline()
        while line and (current_n_packets < n_packets):
            # Take the possible presence of timestamps into account
            if not dataset_config.include_ts:
                packet = line.rstrip("\n")
            else:
                packet = line.split(",")[-1].rstrip("\n")

            # Calculate the image from the packet
            simple_vector, _ = packet_to_bytes(packet, max_n=n_bytes)

            # Update the image list
            simple_vectors[current_n_packets] = simple_vector

            # Prepare next line/packet
            current_n_packets += 1
            line = ff.readline()

        return [simple_vectors]


def __extract_all_simple_vectors_from_flow_data(flow_data, feature_config, dataset_config):
    """
    Extract all simple vectors from the given flow file, sorting them in batches of n_packets, where n_batches is
    specified in feature_config.

    :param flow_data: Path to flow file to extract from
    :param feature_config: Configuration for the feature extraction
    :param dataset_config: Configuration of dataset to extract features from
    :type flow_data: ``FlowData``
    :type feature_config: ``SimpleVectorsConfig``
    :type dataset_config: Object inherited from ``DatasetConfig``
    :return: List containing one extracted simple vectors array
    :rtype: ``list`` of ``numpy.array``
    """
    n_packets = feature_config.n_packets
    n_bytes = feature_config.n_bytes
    return_list = []

    simple_vectors = np.zeros((n_packets, n_bytes), dtype='int32')
    current_n_packets = 0
    line = next(flow_data)

    while not flow_data.has_stopped():
        # Take the possible presence of timestamps into account
        if not dataset_config.include_ts:
            packet = line.rstrip("\n")
        else:
            packet = line.split(",")[-1].rstrip("\n")

        # Calculate the image from the packet
        simple_vector, _ = packet_to_bytes(packet, max_n=n_bytes)

        # Update the image list
        simple_vectors[current_n_packets] = simple_vector

        # Prepare next line/packet
        current_n_packets += 1

        if (current_n_packets % n_packets == 0) and (current_n_packets != 0):
            return_list.append(simple_vectors)
            current_n_packets = 0

        line = next(flow_data)

    if current_n_packets > 0:
        return_list.append(simple_vectors)

    return return_list


def __extract_first_simple_vectors_from_flow_data(flow_data, feature_config, dataset_config):
    """
    Extract the first n_packets vectors from the given flow file, where n_packets is an attribute of feature_config
    specifying the number of packets to extract from a flow.

    :param flow_data: Path to flow file to extract from
    :param feature_config: Configuration for the feature extraction
    :param dataset_config: Configuration of dataset to extract features from
    :type flow_data: ``FlowData``
    :type feature_config: ``SimpleVectorsConfig`` or ``HAST2Config``
    :type dataset_config: Object inherited from ``DatasetConfig``
    :return: List containing one extracted simple vectors array
    :rtype: ``list`` of ``numpy.array``
    """
    if isinstance(feature_config, SimpleVectorsConfig):
        n_packets = feature_config.n_packets
        n_bytes = feature_config.n_bytes
    elif isinstance(feature_config, HAST2Config):
        n_packets = feature_config.r
        n_bytes = feature_config.q
    else:
        raise Exception('Invalid configuration, got {} but should be SimpleVectorsConfig or HAST2Config'.format(
            feature_config.name))

    simple_vectors = np.zeros((n_packets, n_bytes), dtype='int32')
    current_n_packets = 0

    line = next(flow_data)
    while (not flow_data.has_stopped()) and (current_n_packets < n_packets):
        # Take the possible presence of timestamps into account
        if not dataset_config.include_ts:
            packet = line.rstrip("\n")
        else:
            packet = line.split(",")[-1].rstrip("\n")

        # Calculate the image from the packet
        simple_vector, _ = packet_to_bytes(packet, max_n=n_bytes)

        # Update the image list
        simple_vectors[current_n_packets] = simple_vector

        # Prepare next line/packet
        current_n_packets += 1
        line = next(flow_data)

    return [simple_vectors]


def flow_data_to_simple_vectors(flow_data, features_config, dataset_config):

    results = []

    while not flow_data.is_EOF():
        if features_config.use_all_flow_packets():
            results += __extract_all_simple_vectors_from_flow_data(flow_data, features_config, dataset_config)
        else:
            results += __extract_first_simple_vectors_from_flow_data(flow_data, features_config, dataset_config)
            # As the above function only extracts the first n packets of a flow, make sure that all other packets are
            # correctly skipped. This is done with the continue_to_end_flow method of the FlowData object.
            flow_data.continue_to_end_flow()
    return results


def extract_simple_vectors(flow_file, feature_config, dataset_config):
    """
    Extract vectors from the given flow file.

    :param flow_file: Path to flow file to extract from
    :param feature_config: Configuration for the feature extraction
    :param dataset_config: Configuration of dataset to extract features from
    :type flow_file: ``str``
    :type feature_config: ``SimpleVectorsConfig`` or ``HAST2Config``
    :type dataset_config: Object inherited from ``DatasetConfig``
    :return: List of extracted simple vectors arrays
    :rtype: ``list`` of ``numpy.array``
    """
    if isinstance(feature_config, HAST2Config):
        return _extract_first_simple_vectors(flow_file, feature_config, dataset_config)
    elif feature_config.use_all_packets:
        return _extract_all_simple_vectors(flow_file, feature_config, dataset_config)
    else:
        return _extract_first_simple_vectors(flow_file, feature_config, dataset_config)


def flow_file_to_features_SimpleVectors(flow_file, current_path, target_output_dir,
                                        feature_config, dataset_config, simple_vectors_dict):
    """

    :param flow_file:
    :param current_path:
    :param target_output_dir:
    :param feature_config:
    :param dataset_config:
    :param simple_vectors_dict:
    :type flow_file: ``str``
    :return:
    """
    # Extract the relevant parts of the flow file name
    flow_file_parts = flow_file.split("_")

    # Check if the flow file is valid for feature extraction
    if len(flow_file_parts) < 2:
        # There is no label available for this file
        return

    #   - Generate flow image
    label = "-".join(flow_file_parts[0].split("-")[:-1])

    simple_vectors_list = extract_simple_vectors(os.path.join(current_path, flow_file), feature_config, dataset_config)
    #   - Add flow image to list of flow images of the same traffic class
    simple_vectors_dict[label] += simple_vectors_list
    #   - If a "batch_size" number of images are present in the list, save it to a .npz file
    if len(simple_vectors_dict[label]) >= feature_config.batch_size:
        memmap_shape = tuple([len(simple_vectors_dict[label])] + list(simple_vectors_dict[label][0].shape))
        output_dir = os.path.join(target_output_dir, label)

        output_file = "arrays_" + str(len(os.listdir(output_dir))) + '--' + shape_to_str(memmap_shape) + '.npy'


        # Instead of saving in a compressed file, we use a memory mapped file with dtype=uint8
        #np.savez_compressed(os.path.join(output_dir, output_file), *simple_vectors_dict[label])
        memmap = np.memmap(os.path.join(output_dir, output_file),
                           mode="w+",
                           shape=memmap_shape,
                           dtype=np.uint8)
        memmap[:] = np.array(simple_vectors_dict[label])
        # - Empty list
        simple_vectors_dict[label] = []


def generate_SimpleVectors_features(datasets_root, features_root, dataset,
                                    use_existing=True, verbose=True, name=None, **kwargs):

    # Generate the configuration
    config = SimpleVectorsConfig(features_root=features_root, datasets_root=datasets_root, dataset=dataset,
                                 use_existing=use_existing, name=name, verbose=verbose, **kwargs)

    # Generate the target output directory:
    output_dir = os.path.join(config.features_root, config.name)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    # Use the configuration to traverse the flow trees for the different days of the dataset
    dataset_config = config.dataset_config

    for day in dataset_config.get_labelled_days():
        if not config.features_generated[day]:

            target_output_dir = os.path.join(output_dir, day)
            # 1) Clean previous feature data
            if os.path.isdir(target_output_dir):
                if verbose:
                    print("Removing previous generated features:", target_output_dir)
                remove_dir(target_output_dir, remove_root=True, verbose=verbose)
            # Create clean directory
            os.mkdir(target_output_dir)

            # 2) Make a directory for each traffic class
            for label in dataset_config.get_labels(day):
                os.mkdir(os.path.join(target_output_dir, label))

            # Get root directory containing sorted flows to extract features from
            sorted_flows_root = dataset_config.get_sorted_flows_root(day)

            # 3) "flow_images" keeps a list of the flow images per label
            simple_vectors_dict = {}
            for label in dataset_config.get_labels(day):
                simple_vectors_dict[label] = []

            # Process the sorted flows to get the features
            process_flow_tree(root=sorted_flows_root, action={"function": flow_file_to_features_SimpleVectors,
                                                              "params": {"target_output_dir": target_output_dir,
                                                                         "feature_config": config,
                                                                         "dataset_config": dataset_config,
                                                                         "simple_vectors_dict": simple_vectors_dict}
                                                              }, verbose=verbose)

            # Store the remaining features present in the "flow_images" dict:
            for key, item in simple_vectors_dict.items():

                if len(item) == 0:
                    continue

                remaining_output_dir = os.path.join(target_output_dir, key)

                memmap_shape = tuple([len(simple_vectors_dict[key])] + list(simple_vectors_dict[key][0].shape))

                remainder_output_file = "arrays_" + str(len(os.listdir(remaining_output_dir))) + '--' + \
                                        shape_to_str(memmap_shape) + '.npy'

                # Instead of saving in a compressed file, we use a memory mapped file with dtype=uint8
                memmap = np.memmap(os.path.join(remaining_output_dir, remainder_output_file),
                                   mode="w+",
                                   shape=memmap_shape,
                                   dtype=np.uint8)
                memmap[:] = np.array(simple_vectors_dict[key])

                #remaining_output_file = "arrays_" + str(len(os.listdir(remaining_output_dir)))
                #np.savez_compressed(os.path.join(remaining_output_dir, remaining_output_file), *item)
            if verbose:
                print("\nFinished generating features for", day)

            # Save progress
            config.features_generated[day] = True
            config.update()

    return config
