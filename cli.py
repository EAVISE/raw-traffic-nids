import argparse
import os
import sys

from enums import Dataset, FeatureStrategy, DataType
from flow_processing.preprocess import preprocess, generate_symbolic_links, generate_features
from learning.data_setup import create_setup
from data_exporter import export_memmaps_dataset
from dataset_interface.help_functions import mkdir
from parsers.export_pcap_pcapng import to_axis


def check_preliminaries(root):
    """
    Function that generates all required directories that must be present before usage of the framework.
    :return:
    """
    datasets = os.path.join(root, "datasets")
    datasets_config = os.path.join(datasets, "config")

    features = os.path.join(root, "features")
    features_config = os.path.join(features, "config")

    ml_data = os.path.join(root, "ml_data")
    ml_features_config = os.path.join(ml_data, "features_config")
    ml_data_features = os.path.join(ml_data, "memmaps")

    mkdir(datasets)
    mkdir(datasets_config)

    mkdir(features)
    mkdir(features_config)

    mkdir(ml_data)
    mkdir(ml_features_config)
    mkdir(ml_data_features)

    return datasets, features, ml_data


parser = argparse.ArgumentParser(description='Extract raw network traffic-based features')
subparsers = parser.add_subparsers(help='Framework operation', dest='subcommand')
parser.add_argument('--root', required=False, default='')
fe_parser = subparsers.add_parser('feature_extraction')
export_parser = subparsers.add_parser('export')
# General information

fe_parser.add_argument('-d', '--dataset', dest='dataset', help='Dataset that will be processed')
fe_parser.add_argument('--dataset_dir', dest='dataset_dir', help='Directory that contains the base dataset')
fe_parser.add_argument('--export_dir', dest='export_dir', help='Directory to which the final dataset will be exported',
                       default=None)
fe_parser.add_argument('--name', dest='name', default=None)


fe_parser.add_argument('--bucket_config', nargs=3)

fe_parser.add_argument('--use_existing', dest='use_existing', default=True,
                       help='If True, continue existing extraction if applicable')

fe_parser.add_argument('--verbose', dest='verbose', default=True,
                       help='Enables verbose output if True')

fe_parser.add_argument('--extract_batch_size', default=100000, type=int)

fe_parser.add_argument('--hdf5_enable', dest='hdf5_enable', default=False,
                       help='If True, will store all data inside HDF5 files')

fe_parser.add_argument('--hdf5_packet_size', dest='hdf5_packet_size', default=100,
                       help='Maximum size of packets stored in the HDF5-file')

# fe_parser.add_argument('--feature_strategy',
#                        dest='feature_strategy',
#                        help='Feature extraction strategy, such as PCCN, HAST-II or something custom')

features_config_parsers = fe_parser.add_subparsers(help='Feature extraction information', dest='features_type')

# PCCN feature extraction configuration information
pccn_config_parser = features_config_parsers.add_parser('PCCN')
pccn_config_parser.add_argument('--header_min', default=0, type=int)
pccn_config_parser.add_argument('--header_max', default=100, type=int)
pccn_config_parser.add_argument('--payload_min', default=100, type=int)
pccn_config_parser.add_argument('--payload_max', default=200, type=int)
pccn_config_parser.add_argument('--hepa_min', default=0, type=int)
pccn_config_parser.add_argument('--hepa_max', default=192, type=int)
pccn_config_parser.add_argument('--n_packets', default=5, type=int)
pccn_config_parser.add_argument('--batch_size', default=100000, type=int)

# HAST-II feature extraction configuration information
hast1_config_parser = features_config_parsers.add_parser('HAST1')
hast1_config_parser.add_argument('--n', default=600, type=int)
hast1_config_parser.add_argument('--m', default=256, type=int)
hast1_config_parser.add_argument('--batch_size', default=100000, type=int)

# HAST-II feature extraction configuration information
hast2_config_parser = features_config_parsers.add_parser('HAST2')
hast2_config_parser.add_argument('--r', default=5, type=int)
hast2_config_parser.add_argument('--p', default=256, type=int)
hast2_config_parser.add_argument('--q', default=100, type=int)
hast2_config_parser.add_argument('--batch_size', default=100000, type=int)

custom_config_parser = features_config_parsers.add_parser('custom')
custom_config_parser.add_argument('--n_packets', default=6, type=int)
custom_config_parser.add_argument('--n_bytes', default=64, type=int)
custom_config_parser.add_argument('--use_all_packets', default=True)
custom_config_parser.add_argument('--padding', default=0, type=int)
custom_config_parser.add_argument('--batch_size', default=100000, type=int)


exporters = export_parser.add_subparsers(help='Different export optiones', dest='export_type')
pcap_export = exporters.add_parser('pcap')


pcap_export.add_argument('--ifile', help='Path to input capture file')
pcap_export.add_argument('--ofile', help='Path to output file')
pcap_export.add_argument('--n_packets', help='Number of packets to include', type=int)

pcap_exporters = pcap_export.add_subparsers(help='Different export optiones', dest='format')
axis_exporter = pcap_exporters.add_parser('axis')
axis_exporter.add_argument('--bus_width', help='Width of the output AXIS bus', type=int)
axis_exporter.add_argument('--fid_file', help='Additional file containing corresponding flow IDs')
axis_exporter.add_argument('--only_ipv4', help='Only use IPv4 packets with a TCP/UDP session (default True)',
                           type=bool, default=True)
# Parse all provided arguments
p = parser.parse_args()

# p = parser.parse_args(['--root', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'temp'),
#                        'feature_extraction',
#                        '--dataset', 'CICIDS2017',
#                        '--dataset_dir', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Raw data', 'datasets'),
#                        '--export_dir', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets'),
#                        '--name', 'test_custom',
#                        '--bucket_config', '64', '2', '2',
#                        'custom',
#                        '--n_packets', '2',
#                        '--n_bytes', '64'])

# p = parser.parse_args(['--root', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'temp'),
#                        'feature_extraction',
#                        '--dataset', 'CICIDS2017',
#                        '--dataset_dir', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Raw data', 'datasets'),
#                        '--export_dir', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets'),
#                        '--name', 'no_bucket_64_5',
#                        'custom',
#                        '--n_packets', '5',
#                        '--n_bytes', '64'])

# p = parser.parse_args(['export', 'pcap', '--ifile',
#                        os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Raw data', 'datasets', 'CICIDS2017', 'friday', 'pcap', 'Friday-WorkingHours.pcap'),
#                        '--ofile',
#                        os.path.join('test.dat'),
#                        '--n_packets', '5',
#                       'axis',
#                        '--bus_width', '4', '--only_ipv4', 'True',
#                        '--fid_file',
#                        os.path.join('test_fids.dat')])

# p = parser.parse_args(['--root', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'temp'),
#                        'feature_extraction',
#                        '--dataset', 'CICIDS2017',
#                        '--dataset_dir', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Raw data', 'datasets'),
#                        '--export_dir', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets'),
#                        '--name', 'CICIDS2017_no_bucket_64_5_HDF',
#                        '--hdf5_enable', 'True',
#                        '--extract_batch_size', '500000',
#                        '--hdf5_packet_size', '100',
#                        'custom',
#                        '--n_packets', '5',
#                        '--n_bytes', '64'])


p = parser.parse_args(['--root', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'temp'),
                       'feature_extraction',
                       '--dataset', 'CICIDS2017',
                       '--dataset_dir', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Raw data', 'datasets'),
                       '--export_dir', os.path.join('/', 'media', 'laurens', 'Preprocessed datasets', 'Datasets'),
                       '--name', 'CICIDS2017_no_bucket_64_5_HDF_sc',
                       '--hdf5_enable', 'True',
                       '--extract_batch_size', '500000',
                       '--hdf5_packet_size', '100',
                       'custom',
                       '--n_packets', '5',
                       '--n_bytes', '64'])


if p.root == '':
    root = '/home/laurens'   # Default working root
else:
    root = p.root

if not os.path.isdir(root):
    print('Invalid experiments root directory')
    print('Root directory provided was {}'.format(root))
    sys.exit(0)
else:
    pass
    #check_preliminaries(root)

if p.subcommand == 'feature_extraction':
    print('Interpreting feature extraction commands')

    kwargs = {
        'dataset': Dataset[p.dataset],
        'enable_hdf5': (p.hdf5_enable == 'True') or (p.hdf5_enable == 'true'),
        'hdf5_packet_size': int(p.hdf5_packet_size),
        'batch_size': p.extract_batch_size,
    }
    print(bool(p.hdf5_enable), p.hdf5_enable)
    if p.name is not None:
        kwargs['name'] = p.name

    if p.bucket_config is None:
        stream_based = False
    else:
        stream_based = True
        bucket_config = p.bucket_config  # l, m, n
        kwargs['n_stream_flows'] = int(bucket_config[2])
        kwargs['n_stream_packets'] = int(bucket_config[1])

    if p.features_type == 'PCCN':
        feature_strat = FeatureStrategy.PCCN
        kwargs['header_min'] = p.header_min
        kwargs['header_max'] = p.header_max
        kwargs['payload_min'] = p.payload_min
        kwargs['payload_max'] = p.payload_max
        kwargs['hepa_min'] = p.hepa_min
        kwargs['hepa_max'] = p.hepa_max

        kwargs['n_packets'] = p.n_packets

    elif p.features_type == 'HAST1':
        feature_strat = FeatureStrategy.HAST_I
        kwargs['n'] = p.n
        kwargs['m'] = p.m

    elif p.features_type == 'HAST2':
        feature_strat = FeatureStrategy.HAST_II
        kwargs['r'] = p.r
        kwargs['q'] = p.q
        kwargs['p'] = p.p

    elif p.features_type == 'custom':
        feature_strat = FeatureStrategy.SIMPLE_VECTORS
        kwargs['n_packets'] = p.n_packets
        kwargs['n_bytes'] = p.n_bytes
        kwargs['padding'] = p.padding
        kwargs['use_all_packets'] = p.use_all_packets
    else:
        feature_strat = None

    dataset_dir = p.dataset_dir # Directory with raw dataset files

    datasets_root, features_root, ml_root = check_preliminaries(root)

    dataset_config = preprocess(root=datasets_root,
                                verbose=p.verbose,
                                fast_sort=True,
                                use_existing=True,
                                stream_based=stream_based,
                                **kwargs)

    # If the dataset was not completed, this means there was no dataset available
    if not dataset_config.is_completed():
        generate_symbolic_links(dataset_config, datasets_storage=dataset_dir)

        dataset_config = preprocess(root=datasets_root,
                                    verbose=p.verbose,
                                    fast_sort=True,
                                    use_existing=True,
                                    stream_based=stream_based,
                                    **kwargs)

    kwargs['batch_size'] = p.batch_size

    # Extract the required features
    features_config = generate_features(dataset_config=dataset_config,
                                        features_root=features_root,
                                        featurestrategy=feature_strat,
                                        use_existing=True,
                                        verbose=p.verbose,
                                        **kwargs)

    # Create training, validation and test set
    ml_config = create_setup(root=ml_root,
                             name=features_config.name,
                             use_existing=True,
                             features_config=features_config,
                             dtype=DataType.UINT8,
                             verbose=True, train=0.75, valid=0.15, test=0.1)

    if p.export_dir is not None:
        export_memmaps_dataset(ml_features_config=ml_config,
                               target_directory=os.path.join(p.export_dir, ml_config.name))

    #remove_preprocessed_dataset(ml_config)

elif p.subcommand == 'export':
    if p.export_type == 'pcap':
        capture_file = p.ifile
        output_file = p.ofile
        n_packets = p.n_packets
        if p.format == 'axis':
            bus_width = p.bus_width
            fid_file = p.fid_file
            only_ipv4 = p.only_ipv4
            print('Exporting {} packets from {} to {} in the AXIS-format'.format(n_packets, capture_file, output_file))

            to_axis(capture_file=capture_file, output_file=output_file, fid_file=fid_file,
                    bus_width=bus_width, n_packets=n_packets, only_ipv4=True, verbose=True, hex=True)
